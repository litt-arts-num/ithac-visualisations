(fork du [travail de Maxime Bouton et Vincent Maillard](https://framagit.org/protocole-astral/ithac-visualisations)
))
# Ithac

Ce site donne accès à trois visualisations pour le projet Ithac.

### Carte

La première visualisation est une carte de la répartition des lieux de publication des paratextes dans l’espace européen. Cette page dispose également d’un histogramme pour visualiser la répartition temporelle des dates de publication. Les interactions au survol et au clic sur le cercle d’un lieu de publication permettent d’afficher les paratextes concernés par la sélection en cours. En cliquant sur un paratexte, il est possible d’accéder à une page où le contenu entier du paratexte sera affiché.

### XML Explorer

La seconde visualisation permet d’obtenir des réponses chiffrées sur une question posée. Il est possible d’organiser la question avec des choix hiérarchiques et de filtrer les résultats. En cliquant sur un paratexte, il est possible d’accéder à une page où le contenu entier du paratexte sera affiché.

### Détail

La troisième visualisation permet d’afficher le détail d’un paratexte. Par défaut, le paratexte Ae1548_Auratus_p1.xml est affiché (c’est le premier par ordre alphabétique). Pour accéder à d’autres paratextes, il faut passer par les deux premières visualisations et cliquer sur un paratexte.

### Auteurs

Développement original : Maxime Bouton et Vincent Maillard (Protocole astral)
Modifications et maintenance : Arnaud Bey

### Licence

GNU AFFERO GENERAL PUBLIC LICENSE Version 3
