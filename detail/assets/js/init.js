document.addEventListener("DOMContentLoaded", function() {
    init_paratxt_html();
}, false);

async function init_paratxt_html() {
    const search_params = getURLparams();
    let filename =  search_params.has("file")
      ? search_params.get("file")
      : "pla1499_valla_p2-63f7913259619.xml";

    const datum_for_metadata = await getDatum(filename);
    const datum_for_html = await getDatum(filename, true);

    /***************
        METADATAS
    ****************/
    const aside_metadata = document.querySelector("aside#metadata")

    const metadata = [
        "paratxt_title",
        "paratxt_author",
        "paratxt_lang",
        "paratxt_date",
        "paratxt_pubplace",
        "paratxt_publisher",
        "paratxt_editor",
        "ancient_title",
        "ancient_author"
    ];

    var source = [8, 7, 6, 5, 4, 3];
    var source_arr = [];
    for (var i = 0; i < source.length; i++) {
        var key = metadata[source[i]];
        var node = datum_for_metadata.querySelector(data_model[key].path);
        var value = (node == null || node.firstElementChild != null) ? "" : node.innerHTML;
        source_arr.push(value);
    }
    createMetadataBlock("Source", source_arr.join(", ") + ".")

    for (var i = 0; i < metadata.length; i++) {
        if (metadata[i] == "paratxt_lang") {
            var value = datum_for_metadata.querySelector(data_model[metadata[i]].path).getAttribute("xml:lang");
            value = data_model.paratxt_lang.alignment[value]
        } else {
            var node = datum_for_metadata.querySelector(data_model[metadata[i]].path);
            var value = (node == null || node.firstElementChild != null) ? "" : node.innerHTML;
        }
        createMetadataBlock(data_model[metadata[i]].definition, value)
    }

    const pensoirLink = createPensoirLink(filename)
    aside_metadata.appendChild(pensoirLink)

    const downloadLink = createDownloadXmlLink(filename)
    aside_metadata.appendChild(downloadLink)
    /***************
          BODY
    ****************/
    var arr_of_head = datum_for_html.querySelectorAll(data_model.paratxt_head.path);
    var arr_of_ab = datum_for_html.querySelectorAll(data_model.paratxt_ab.path);

    var main = document.querySelector("main");
    for (var i = 0; i < arr_of_head.length; i++) {
        var h2 = document.createElement("H2");
        h2.innerHTML = arr_of_head[i].innerHTML;
        main.appendChild(h2);
    }
    for (var i = 0; i < arr_of_ab.length; i++) {
        main.appendChild(arr_of_ab[i]);
    }

    var seg = document.querySelectorAll(".seg");
    seg.forEach(el => el.addEventListener("mouseenter", toggleNote));
    seg.forEach(el => el.addEventListener("mouseleave", toggleNote));
}

function toggleNote() {
    var note = this.querySelector(".note");
    this.classList.toggle("hovered");
    note.classList.toggle("active");
}

function createMetadataBlock(def, value) {
  const aside_metadata = document.querySelector("aside#metadata")

  var span_def = document.createElement("SPAN")
  span_def.classList.add("definition")
  span_def.innerHTML = def

  var span_val = document.createElement("SPAN")
  span_val.classList.add("value")
  span_val.innerHTML = value

  var p = document.createElement("P")
  p.appendChild(span_def)
  p.appendChild(span_val)

  aside_metadata.appendChild(p);
}
