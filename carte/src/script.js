// PLACES
let places = {};

Place = function (svg_el) {
  this.name = svg_el.getAttribute("pubplace");
  this.ancients = {
    "ar": 0,
    "pl": 0,
    "eschl": 0,
    "sen": 0,
    "ter": 0,
    "soph": 0,
    "eur": 0,
  }
  this.svg_el = svg_el;
  this.ancients_elements = {};
  this.setup_ancients_circles();
  places[this.name] = this;
}

let svg_container = document.getElementById("svg2");

function generate_dots(n, glyph) {
  let str = '';
  for (var i = 0; i < n; i++) {
    str += glyph;
  }
  return str;
}

let clicked = false;

Place.prototype.setup_ancients_circles = function () {
  this.pos_x = this.svg_el.getAttribute("cx");
  this.pos_y = this.svg_el.getAttribute("cy");

  this.text = document.createElementNS('http://www.w3.org/2000/svg', 'text');

  let order = [];
  for (var ancient in this.ancients) {
    this.ancients_elements[ancient] = document.createElementNS("http://www.w3.org/2000/svg", "circle");
    this.ancients_elements[ancient].place = this;
    this.ancients_elements[ancient].setAttributeNS(null, "cx", this.pos_x);
    this.ancients_elements[ancient].setAttributeNS(null, "cy", this.pos_y);
    this.ancients_elements[ancient].setAttributeNS(null, "r", '0px');
    this.ancients_elements[ancient].setAttributeNS(null, "ancient", ancient);
    this.ancients_elements[ancient].setAttributeNS(null, "ancient_circle", this.name + '_' + ancient);
    this.ancients_elements[ancient].linked_text = this.text;
    this.ancients_elements[ancient].addEventListener('mouseenter', function () {
      if (clicked != this.place.name) {
        clicked = false;
        let circle_count = document.getElementsByClassName("text_count_circle");
        for (var i in places) {
          for (var ancient in places[i].ancients) {
            event = document.createEvent("HTMLEvents");
            event.initEvent("mouseout", true, true);
            event.e; ventName = "mouseout";
            if (!places[i].ancients_elements[ancient]) break;
            places[i].ancients_elements[ancient].dispatchEvent(event);
          }
          for (var j = 0; j < circle_count.length; j++) {
            if (circle_count[j].getAttribute("place") == this.place.name) continue;
            circle_count[j].style.display = "none";
          }
        }
      }
      let cards = document.getElementsByClassName("paratxt_card");
      let circle_count = document.getElementsByClassName("text_count_circle");
      this.linked_text.style.opacity = '1';
      for (var i in places) {
        for (var j = 0; j < cards.length; j++) {
          if (cards[j].getAttribute("place") == this.place.name) continue;
          cards[j].style.display = "none";
        }
        for (var j = 0; j < cards.length; j++) {
          if (cards[j].getAttribute("place") == this.place.name) continue;
          cards[j].style.display = "none";
        }
        for (var j in places[i].ancients_elements) {
          if (places[i] == this.place) continue;
          places[i].ancients_elements[j].style.opacity = 0.1;
        }
      }
      select_place(this.place);
    });
    this.ancients_elements[ancient].addEventListener('mousedown', function () {
      for (var i in places) {
        for (var ancient in places[i].ancients) {
          event = document.createEvent("HTMLEvents");
          event.initEvent("mouseout", true, true);
          event.e; ventName = "mouseout";
          if (!places[i].ancients_elements[ancient]) break;
          places[i].ancients_elements[ancient].dispatchEvent(event);
        }
      }
      let circle_count = document.getElementsByClassName("text_count_circle");
      let cards = document.getElementsByClassName("paratxt_card");
      this.linked_text.style.opacity = '1';
      for (var i in places) {
        for (var j = 0; j < cards.length; j++) {
          if (cards[j].getAttribute("place") == this.place.name) continue;
          cards[j].style.display = "none";
        }
        for (var j = 0; j < circle_count.length; j++) {
          if (circle_count[j].getAttribute("place") == this.place.name) continue;
          circle_count[j].style.display = "none";
        }
        for (var j in places[i].ancients_elements) {
          if (places[i] == this.place) continue;
          places[i].ancients_elements[j].style.opacity = 0.1;
        }
      }
      select_place(this.place);
      if (clicked == this.place.name) {
        clicked = false;
      } else {
        clicked = this.place.name;
      }
    });
    this.ancients_elements[ancient].addEventListener('mouseout', function () {
      if (clicked) return;
      let cards = document.getElementsByClassName("paratxt_card");
      let circle_count = document.getElementsByClassName("text_count_circle");
      this.linked_text.style.opacity = '0';
      for (var j = 0; j < cards.length; j++) {
        cards[j].style.display = "block";
      }
      for (var j = 0; j < circle_count.length; j++) {
        circle_count[j].style.display = "block";
      }
      for (var i in places) {
        for (var j in places[i].ancients_elements) {
          places[i].ancients_elements[j].style.opacity = 1;
        }
      }
      reset_ancients_global();
      update_ancients_global();
      update_paratxt();
      update_total_ancients();
      document.getElementById("pubplace_focus_info").textContent = "Lieu de publication selectionné ·········································· aucun";
    });
    order.push(this.ancients_elements[ancient]);
  }
  order.reverse();
  for (var i = 0; i < order.length; i++) {
    svg_container.appendChild(order[i]);
  }

  this.text.setAttribute('x', this.pos_x);
  this.text.setAttribute('y', this.pos_y);
  this.text.textContent = alignments[this.name]; // alignments est déclaré dans common/alignments.js
  this.text.style.opacity = '0';

  svg_container.appendChild(this.text);
}

function select_place(place) {
  reset_ancients_global();
  update_ancients_local(place);
  update_paratxt();
  update_total_ancients();
  let text_length = ("Lieu de publication selectionné" + alignments[place.name]).length + 2;
  document.getElementById("pubplace_focus_info").textContent = "Lieu de publication selectionné " + generate_dots(80 - text_length, '·') + ' ' + alignments[place.name];
}

Place.prototype.updateSize = function (ratio) {
  let previous_size = 0;
  let base = 6;
  for (var ancient in this.ancients) {
    if (this.ancients_elements[ancient] == undefined) continue;
    let size = !this.ancients[ancient] ? 0 : this.ancients[ancient] < base ? base : this.ancients[ancient];
    this.ancients_elements[ancient].setAttributeNS(null, "r", (size + previous_size) * ratio);
    previous_size += size;
  }
  this.text.setAttribute('x', parseInt(this.pos_x) + previous_size * ratio + 2);
}

// MAP

let viewBox = {
  x: 80,
  y: 80,
  width: 200,
  height: 200
}

let zoom_level = 1;

function zoomOut() {
  if (zoom_level < -15) return;
  viewBox.x = viewBox.x - viewBox.width / 16;
  viewBox.y = viewBox.y - viewBox.height / 16;
  viewBox.width = viewBox.width * 1.0625;
  viewBox.height = viewBox.height * 1.0625;
  zoom_level--;
  svg_container.setAttributeNS(null, "viewBox", viewBox.x + ' ' + viewBox.y + ' ' + viewBox.width + ' ' + viewBox.height);
}


// RESET CARTO

let ancients_global = {
  "ar": 0,
  "pl": 0,
  "eschl": 0,
  "sen": 0,
  "ter": 0,
  "soph": 0,
  "eur": 0,
}

let ancients_names = {
  "ar": "Aristophanes",
  "pl": "Plautus",
  "eschl": "Aeschylus",
  "sen": "Seneca",
  "ter": "Terentius",
  "soph": "Sophocles",
  "eur": "Euripides"
}

function update_ancients_local(place) {
  for (var ancient in place.ancients) {
    if (ancients_global[ancient] == undefined) continue;
    ancients_global[ancient] += place.ancients[ancient];
  }
  for (var ancient in ancients_global) {
    let quantity = generate_dots(Math.floor(ancients_global[ancient] / 2 + 1), '#');
    let text_length = (ancients_names[ancient] + '  ' + ancients_global[ancient]).length + quantity.length + 1;
    document.getElementById(ancient + "_info").innerHTML = "<span>" + quantity + '</span> ' + ancients_names[ancient] + ' ' + generate_dots(80 - text_length, '·') + ' ' + ancients_global[ancient];
  }
}

function update_ancients_global() {
  for (var place in places) {
    for (var ancient in places[place].ancients) {
      if (ancients_global[ancient] == undefined) continue;
      ancients_global[ancient] += places[place].ancients[ancient];
    }
  }
  for (var ancient in ancients_global) {
    let quantity = generate_dots(Math.floor(ancients_global[ancient] / 2 + 1), '#');
    let text_length = (ancients_names[ancient] + '  ' + ancients_global[ancient]).length + quantity.length + 1;
    document.getElementById(ancient + "_info").innerHTML = "<span>" + quantity + '</span> ' + ancients_names[ancient] + ' ' + generate_dots(80 - text_length, '·') + ' ' + ancients_global[ancient];
  }
}

function update_paratxt() {
  let n_prtxt = 0;
  for (var ancient in ancients_global) {
    if (ancients_global[ancient] == undefined) continue;
    n_prtxt += ancients_global[ancient];
  }
  let text_length = ('Paratextes  ' + n_prtxt).length;
  document.getElementById("paratxt_info").textContent = 'Paratextes ' + generate_dots(80 - text_length, '·') + ' ' + n_prtxt;
}

function update_total_ancients() {
  let n_ancients = 0;
  for (var ancient in ancients_global) {
    if (ancients_global[ancient] == undefined || !ancients_global[ancient]) continue;
    n_ancients += 1;
  }
  let text_length = ('Auteurs anciens cités ' + n_ancients).length + 1;
  document.getElementById("total_ancients_info").textContent = 'Auteurs anciens cités ' + generate_dots(80 - text_length, '·') + ' ' + n_ancients;
}

function reset_ancients_global() {
  for (var ancient in ancients_global) {
    ancients_global[ancient] = 0;
  }
}

function reset_carto() {
  reset_ancients_global();
  for (var place in places) {
    for (var ancient in places[place].ancients) {
      places[place].ancients[ancient] = 0;
      places[place].updateSize(1);
    }
  }
}

let paratext_section = document.getElementById("paratxt_list");

function create_paratextes_cards(place, year, place_name) {
  for (var ancient in place) {
    for (var title in place[ancient]) {
      let card = document.createElement("div");
      card.setAttribute("file", title_files_table[title]);
      card.addEventListener("mousedown", function () {
        window.open("../detail/?file=" + this.getAttribute("file"));
      });
      card.classList.add("paratxt_card");
      card.setAttribute("place", place_name);
      let date_card = document.createElement("p");
      var text_length = ("Date" + year).length + 2;
      date_card.textContent = "Date " + generate_dots(80 - text_length, '·') + ' ' + year;
      card.appendChild(date_card);
      //var br = document.createElement("br");
      //card.appendChild(br);
      let title_card = document.createElement("p");
      text_length = ("Titre " + title).length + 2;
      title_card.textContent = "Titre ·" + generate_dots(80 - text_length, '·') + ' ' + title;
      card.appendChild(title_card);
      let author_card = document.createElement("p");
      text_length = ("Author " + Object.keys(place[ancient][title])[0]).length + 2;
      author_card.textContent = "Author ·" + generate_dots(80 - text_length, '·') + ' ' + Object.keys(place[ancient][title])[0];
      card.appendChild(author_card);
      let place_card = document.createElement("p");
      text_length = ("Place " + place_name).length + 2;
      place_card.textContent = "Place ·" + generate_dots(80 - text_length, '·') + ' ' + place_name;
      card.appendChild(place_card);
      let ancient_card = document.createElement("p");
      text_length = ("Auteur antique " + ancient).length + 2;
      ancient_card.textContent = "Auteur antique ·" + generate_dots(80 - text_length, '·') + ' ' + ancient;
      card.appendChild(ancient_card);
      paratext_section.appendChild(card);
    }
  }
}

function print_years_on_map(years) {
  reset_carto();
  let text_length = ("Période selectionnée" + (years.length > 1 ? (years[0] + ' - ' + years[years.length - 1]) : years[0])).length + 2;
  document.getElementById("period_info").textContent = "Période selectionnée " + generate_dots(80 - text_length, '·') + ' ' + (years.length > 1 ? (years[0] + ' - ' + years[years.length - 1]) : years[0]);
  text_length = ("Années" + years.length).length + 2;
  document.getElementById("years_info").textContent = "Années " + generate_dots(80 - text_length, '·') + ' ' + years.length;
  let pubplaces = {};
  let pubplaces_number = 0;
  while (paratext_section.children.length) paratext_section.removeChild(paratext_section.children[0]);
  while (years.length) {
    let year = years.pop();
    let places_data = date_place_ancient_author[year];
    if (places_data == undefined) continue;
    for (var place in places_data) {
      if (!pubplaces[place]) {
        pubplaces[place] = true;
        pubplaces_number++;
      }
      create_paratextes_cards(places_data[place], year, place);
      let total_ancients = get_all_child_total_from_data(places_data[place]);
      for (var ancient in places_data[place]) {
        if (places[place] == undefined) continue;
        places[place].ancients[ancient] += total_ancients[ancient];
        places[place].updateSize(0.15);
      }
    }
  }
  text_length = ("Lieux de publication" + pubplaces_number).length + 2;
  document.getElementById("pubplace_info").textContent = "Lieux de publication " + generate_dots(80 - text_length, '·') + ' ' + pubplaces_number;
  if (clicked) {
    update_ancients_local(places[clicked]);
    let cards = document.getElementsByClassName("paratxt_card");

    for (var j = 0; j < cards.length; j++) {
      if (cards[j].getAttribute("place") == clicked) continue;
      cards[j].style.display = "none";
    }
  } else {
    update_ancients_global();
  }
  update_total_ancients();
  update_paratxt();
}

// Pubplaces SVG

let pubplaces_svg_elements = document.querySelectorAll("[pubplace]");
for (var i = 0; i < pubplaces_svg_elements.length; i++) {
  new Place(pubplaces_svg_elements[i]);
}

window.onload = function () {
  chronos.init()
  document.getElementById("pubplace_focus_info").textContent = "Lieu de publication selectionné ·········································· aucun";
  zoomOut();
}

