function make_list_from_range(from,to){
  let list = [];
  let interval = to - from;
  while (interval--) {
    list.push(from + interval);
  }
  list.reverse();
  list.push(to);
  return list;
}

function get_total_from_data(data,total) {
  for (var key in data) {
    if (Object.keys(data[key]).length) {
      get_total_from_data(data[key],total);
    } else if (data[key] > 0) {
      total.result += data[key];
    }
  }
}

function get_all_child_total_from_data(data) {
  let results = {};
  for (var i in data) {
    let total = {"result":0};
    get_total_from_data(data[i],total);
    results[i] = total.result;
  }
  return results;
}
