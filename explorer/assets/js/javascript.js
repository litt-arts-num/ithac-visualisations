document.addEventListener("DOMContentLoaded", function() {
  initHierarchiserGUI();
}, false);

async function initHierarchiserGUI() {

  // fetch de ttes les fiches xml
  data_fetched = await getData();
  data_filtered = data_fetched;

  // ajout chargement de page
  appendLoader();

  // creation obj pour aligner "attribute_value" et "readable_value"
  makeObjectForAlignments();

  // ajout outils interactifs
  appendTools();

  updateTools();
  updateVisu();
}


/* LOADER */

function appendLoader() {
  loaded = true;
  var loader_container = document.querySelector(".loader_container");
  document.querySelector("body").removeChild(loader_container);
}


/* TOOLS */

function appendTools() {
  // appendCheckboxesOperator();
  appendHierachicalForm();
  // appendHierachicalSelect();
  appendCheckboxesForm("paratxt_lang");
  appendCheckboxesForm("paratxt_author");
  appendCheckboxesForm("paratxt_pubplace");
  appendCheckboxesForm("paratxt_publisher");
  appendCheckboxesForm("paratxt_editor");
  appendCheckboxesForm("paratxt_ana");
  appendCheckboxesForm("ancient_author");
  appendResetButton();
}

function appendResetButton() {

  var button = document.createElement("BUTTON");
  button.innerHTML = "Reset";
  button.classList.add("reset");

  var tools = document.querySelector("#tools");
  var p = document.createElement("P");
  p.innerHTML = "Supprimer choix et filtres";
  p.classList.add("definition");
  tools.appendChild(p);
  tools.appendChild(button);
  button.addEventListener("click", resetVisu);
}

function resetVisu() {
  var search_params = new URLSearchParams();
  updateURL(search_params);
  updateTools();
  updateVisu();
  resetInspector();
}

function appendCheckboxesOperator() {

  var div = document.createElement("DIV");
  var input = document.createElement("INPUT");
  var label = document.createElement("LABEL");

  div.setAttribute("id", "undefined_operator_container");
  input.setAttribute("type", "checkbox");
  input.setAttribute("id", "undefined_operator");
  input.setAttribute("name", "undefined_operator");
  input.setAttribute("value", "undefined_operator");
  input.checked = false;

  label.setAttribute("for", "undefined_operator")
  label.innerHTML = "Afficher les valeurs sans données";

  var tools = document.querySelector("#tools");

  div.appendChild(input);
  div.appendChild(label);
  tools.appendChild(div);

  input.addEventListener("change", eventFilter);
}

function appendCheckboxesForm(key) {

  var list = listData(data_fetched, key);
  if (data_model[key].is_list_of_values) {
    var list = splitList(list, key);
  }
  var list = uniqList(list, "attribute_value");
  var list = sortList(list, "readable_value");

  // création du details/summary
  var detail = document.createElement("DETAILS");
  detail.classList.add("checkboxes_container");
  var summary = document.createElement("SUMMARY");
  summary.innerHTML = data_model[key].definition;
  detail.appendChild(summary);

  // création du bouton toggle tout cocher/décocher
  var button_uncheck = document.createElement("BUTTON");
  button_uncheck.classList.add("uncheck");
  button_uncheck.classList.add("active");
  button_uncheck.innerHTML = "Tout cocher";

  // création du formulaire
  var form = document.createElement("FORM");
  form.classList.add("filter_checkboxes");
  form.setAttribute("data-filter", key);
  for (var i = 0; i < list.length; i++) {
    var input = document.createElement("INPUT");
    var label = document.createElement("LABEL");
    var span = document.createElement("SPAN");

    input.setAttribute("type", "checkbox");
    input.setAttribute("id", list[i].attribute_value);
    input.setAttribute("name", list[i].attribute_value);
    input.setAttribute("value", list[i].attribute_value);

    // label.setAttribute("for", list[i].attribute_value);
    var read_val = list[i].readable_value != undefined ? list[i].readable_value : list[i].attribute_value;
    span.innerHTML = read_val;

    label.appendChild(input);
    label.appendChild(span);
    form.appendChild(label);
  }

  // append du button .uncheck
  detail.appendChild(button_uncheck);
  // append du formulaire
  detail.appendChild(form);
  var col_tools = document.querySelector("#tools");
  col_tools.appendChild(detail);

  // event sur le button .uncheck
  button_uncheck.addEventListener('click', toggleCheckboxes);
  // event à chaque input sur le form
  form.addEventListener("change", eventFilter);
}

function toggleCheckboxes() {
  var details = closest(this, function(el) {
    return el.classList.contains('checkboxes_container');
  });
  var form = details.querySelector("form");
  var filter = form.getAttribute("data-filter");
  var checkboxes = details.querySelectorAll('input[type=checkbox]');

  var search_params = getURLparams();

  if (this.classList.contains("active")) {
    search_params.delete(filter);
    checkboxes.forEach(checkbox => search_params.append(filter, checkbox.value));
    this.innerHTML = "Tout décocher";
  } else {
    search_params.delete(filter);
    this.innerHTML = "Tout cocher";
  }
  checkboxes.forEach(el => el.nextElementSibling.classList.toggle("active"));
  this.classList.toggle("active");

  updateURL(search_params);
  updateTools();
  updateVisu();
  resetInspector();
}

function appendHierachicalForm() {
  var col_tools = document.querySelector("#tools");

  var p = document.createElement("P");
  p.innerHTML = "Choix hiérarchiques";
  p.classList.add("definition");
  col_tools.appendChild(p);

  // création du formulaire
  var form = document.createElement("FORM");
  form.classList.add("hierarchical_selects");
  form.addEventListener("click", function(event) {
    event.preventDefault()
  });
  col_tools.appendChild(form);

  // création bouton create
  var button_create = document.createElement("BUTTON");
  button_create.innerHTML = "Ajouter un choix";
  button_create.classList.add("create");
  button_create.addEventListener("click", appendHierachicalSelect);
  col_tools.appendChild(button_create);

  var p = document.createElement("P");
  p.innerHTML = "Filtres";
  p.classList.add("definition");
  col_tools.appendChild(p);
}

function appendHierachicalSelect() {

  // création du conteneur
  var div = document.createElement("DIV");
  div.classList.add("hierarchical_entry");

  // création des boutons de déplacement de niveaux
  var div_level = document.createElement("DIV");
  div_level.classList.add("buttons_level")
  var button_level_up = document.createElement("BUTTON");
  button_level_up.classList.add("level_up");
  // button_level_up.innerHTML = "▲";
  var button_level_down = document.createElement("BUTTON");
  button_level_down.classList.add("level_down");
  // button_level_down.innerHTML = "▼";

  // création du bouton pour supprimer
  var button_remove = document.createElement("BUTTON");
  button_remove.classList.add("remove");
  // button_remove.innerHTML = "✖";

  // création du <select>
  var div_select = document.createElement("DIV");
  div_select.classList.add("select");
  var select = document.createElement("SELECT");
  div_select.appendChild(select);

  // création première option vide
  var option = document.createElement("OPTION");
  option.value = "";
  option.innerHTML = "";

  // ajout des options
  select.appendChild(option);
  // "hierarchical_choices" est déclaré en global
  hierarchical_choices.forEach(function(value) {
    var option = document.createElement("OPTION");
    option.value = value;
    option.innerHTML = data_model[value].definition;
    select.appendChild(option);
  });

  // ajout des événements pour modif le html
  button_remove.addEventListener("click", removeEvent);
  button_level_up.addEventListener("click", levelUpEvent);
  button_level_down.addEventListener("click", levelDownEvent);

  // ajout événement pour modif les params url
  button_remove.addEventListener("click", eventHierarchy);
  button_level_up.addEventListener("click", eventHierarchy);
  button_level_down.addEventListener("click", eventHierarchy);
  select.addEventListener("change", eventHierarchy);

  // ajout des événements pour la hiérarchie
  button_remove.addEventListener("click", updateVisu);
  button_level_up.addEventListener("click", updateVisu);
  button_level_down.addEventListener("click", updateVisu);
  select.addEventListener("change", updateVisu);

  // composition du conteneur
  div_level.appendChild(button_level_up);
  div_level.appendChild(button_level_down);
  div.appendChild(div_level);
  div.appendChild(div_select);
  div.appendChild(button_remove);
  var hierarchical_selects = document.querySelector(".hierarchical_selects");
  hierarchical_selects.appendChild(div);
}


function eventHierarchy() {
  var selects = document.querySelectorAll(".hierarchical_selects select");
  var keys = [];
  selects.forEach(select => keys.push(select.options[select.selectedIndex].value));

  // retrait valeur vide
  var hierarchy = keys.filter(key => key.length > 0);

  var search_params = getURLparams();
  search_params.delete("hierarchy");
  hierarchy.forEach(param => search_params.append("hierarchy", param));
  updateURL(search_params);
}

function eventFilter(e) {
  var el = e.target;

  var filter_checkboxes = closest(el, function(el) {
    return el.classList.contains('filter_checkboxes');
  });
  var filter = filter_checkboxes.getAttribute("data-filter");
  var value = el.getAttribute("value");

  var search_params = getURLparams();
  var filter_search_params = search_params.getAll(filter);

  if (filter_search_params.includes(value)) {
    var param_values = filter_search_params.filter(v => v != value);
    search_params.delete(filter);
    param_values.forEach(param => search_params.append(filter, param));
  } else {
    search_params.append(filter, value);
  }

  updateURL(search_params);
  updateTools();
  updateVisu();
  resetInspector();
}

function updateTools() {

  // reset
  var already_selected = document.querySelector("#tools").querySelectorAll(".selected");
  already_selected.forEach(el => el.classList.remove("selected"));
  var hierarchical_selects = document.querySelector(".hierarchical_selects");
  while (hierarchical_selects.firstChild) {
    hierarchical_selects.removeChild(hierarchical_selects.firstChild);
  }

  var search_params = getURLparams();
  search_params.forEach(function(value, filter) {
    var div_filter = document.querySelector(".filter_checkboxes[data-filter='" + filter + "']");
    if (div_filter) {
      var input_filter = div_filter.querySelector("[value='" + value + "']");
      if (input_filter) input_filter.nextElementSibling.classList.add("selected");
    }
    if (filter == "hierarchy") {
      appendHierachicalSelect();
      var selects = document.querySelectorAll(".hierarchical_entry select");
      var current_select = selects[selects.length - 1];
      current_select.value = value;
    }
  });
  if (!search_params.has("hierarchy")) {
    appendHierachicalSelect();
  }
}

function updateVisu() {

  var search_params = getURLparams();

  var hierarchy = search_params.getAll("hierarchy");

  search_params.delete("hierarchy");
  data_filtered = filterData(data_fetched, search_params);
  var obj = {};
  hierarchizer(data_filtered, hierarchy, search_params, obj);
  // console.log(obj);

  var visu = document.querySelector("#visu");
  while (visu.firstChild) {
    visu.removeChild(visu.firstChild);
  }

  for (var key in obj) {
    appendHierarchy(key, obj[key], visu);
  }

}


function showInspector() {

  // reset
  var visu = document.querySelector("#visu");
  var already_selected = visu.querySelector(".selected");
  if (already_selected) already_selected.classList.remove("selected");
  this.classList.toggle("selected");

  var filter = this.parentNode.getAttribute("data-key");
  var value = this.parentNode.getAttribute("data-str");

  var arr = [];
  getParentsFilters(arr, this.parentNode);

  var search_params = getURLparams();
  search_params.delete("hierarchy");
  search_params.append(filter, value);
  arr.forEach(el => search_params.append(el.filter, el.value));

  var data_inspected = filterData(data_fetched, search_params);

  resetInspector();

  var ul = document.createElement("UL");
  ul.id = "filtrables"

  let filtersContainer = document.createElement("li");
  filtersContainer.id = "filters-container"
  let input = document.createElement("input");
  input.type = "text"
  input.id = "text-filter"
  input.value = ""
  input.placeholder = "filtrer"
  filtersContainer.appendChild(input)

  let dateSorter = document.createElement("span");
  dateSorter.id = "date-sorter"
  dateSorter.classList.add("sorter")
  dateSorter.dataset.criteria = "date"
  dateSorter.innerHTML = "trier par date"
  filtersContainer.appendChild(dateSorter)

  let nameSorter = document.createElement("span");
  nameSorter.id = "name-sorter"
  nameSorter.classList.add("sorter")
  nameSorter.innerHTML = "trier par nom"
  nameSorter.dataset.criteria = "name"
  filtersContainer.appendChild(nameSorter)

  ul.appendChild(filtersContainer)

  for (var i = 0; i < data_inspected.length; i++) {
    var datum = data_inspected[i];
    var paratxt_title = datum.querySelector(data_model.paratxt_title.path).innerHTML;
    var paratxt_author = datum.querySelector(data_model.paratxt_author.path).innerHTML
    var paratxt_date = datum.querySelector(data_model.paratxt_date.path).innerHTML
    let fileName = datum.getAttribute("data-file") + ".xml"
    let realFilename = fileName.substring(0, fileName.indexOf("-"))
    var li = document.createElement("LI");
    li.classList.add('filtrable')
    li.dataset.date = paratxt_date
    li.dataset.name = realFilename
    var a = document.createElement("A");
    a.href = "../detail/?file=" + fileName
    a.setAttribute("target", "_blank");
   
    a.innerHTML = "<em>" + paratxt_title + ",</em> " + paratxt_author;
    let date = document.createElement("span");
    date.classList.add('date-container')
    date.innerHTML = paratxt_date
    let filenameSpan = document.createElement("span")
    filenameSpan.classList.add('filename-container')
    filenameSpan.innerHTML = realFilename
    li.appendChild(date);
    li.appendChild(filenameSpan);
    li.appendChild(a);
    ul.appendChild(li);
  }
  var div_inspector = document.querySelector("#inspector")
  div_inspector.appendChild(ul)

  initFilter()
}

function sort(criteria) { 
  var items = Array.from(document.querySelectorAll('.filtrable'))
  return items.sort(function(a, b) {
    return a.getAttribute("data-"+criteria).localeCompare(b.getAttribute("data-"+criteria));
  });
}

function initFilter() {
  var input = document.querySelector('#text-filter');
  var items = document.querySelectorAll('.filtrable');
  var sorters = document.querySelectorAll('.sorter');

  sorters.forEach(sorter => {
    sorter.addEventListener('click', function () {
      let list = document.querySelector('#filtrables')
      sort(sorter.dataset.criteria).forEach(li => list.appendChild(li));
    })
  });

  input.addEventListener('keyup', function (ev) {
    var text = ev.target.value;
    var pat = new RegExp(text, 'i');
    for (var i = 0; i < items.length; i++) {
      var item = items[i];
      if (pat.test(item.innerText)) {
        item.classList.remove("hidden");
      }
      else {
        item.classList.add("hidden");
      }
    }
  });

  
}

function resetInspector() {
  var div_inspector = document.querySelector("#inspector");
  while (div_inspector.firstChild) div_inspector.removeChild(div_inspector.firstChild);
}

function getParentsFilters(arr, el) {
  if (el.parentNode.id != "visu") {
    var parent = closest(el.parentNode, function(parent_el) {
      return parent_el.classList.contains("details");
    });
    var parent_filter = parent.getAttribute("data-key");
    var parent_value = parent.getAttribute("data-str");
    arr.push({
      "filter": parent_filter,
      "value": parent_value
    });

    getParentsFilters(arr, parent);
  }
}


function appendHierarchy(str, obj, element, rank = 0) {

  var search_params = getURLparams();
  var hierarchy = search_params.getAll("hierarchy");

  var details = document.createElement("DETAILS");
  details.classList.add("details");
  details.setAttribute("open", true);
  details.setAttribute("data-key", hierarchy[rank]);
  details.setAttribute("data-str", str);

  var summary = document.createElement("SUMMARY");
  summary.classList.add("summary");
  details.appendChild(summary);
  element.appendChild(details);

  // var read_val = str;
  var read_val = alignments[str] == undefined ? str : alignments[str];

  if (Number.isInteger(obj)) {
    // derniers enfants
    summary.innerHTML = "<p>" + read_val + " <span>(" + obj + ")</span></p>";
    summary.classList.add("end");
    summary.addEventListener("click", showInspector);
  } else {
    // enfant parents
    summary.innerHTML = "<p>" + read_val + " <span>(" + get_all_child_total_from_data({
      obj
    }).obj + ")</span></p>";
  }

  // appel recursif s'il des valeurs en dessous
  rank++;
  for (var key in obj) {
    appendHierarchy(key, obj[key], details, rank);
  }
}



/* EVENTS POUR MODIFIER LE DOM */

function levelUpEvent() {
  var hierarchical_entry = closest(this, function(el) {
    return el.classList.contains("hierarchical_entry");
  });
  levelUp(hierarchical_entry);
}

function levelUp(el) {
  var previous_el = el.previousElementSibling;
  if (previous_el != null) {
    var hierarchical_selects = document.querySelector(".hierarchical_selects");
    hierarchical_selects.insertBefore(el, previous_el);
  }
}

function levelDownEvent() {
  var hierarchical_entry = closest(this, function(el) {
    return el.classList.contains("hierarchical_entry");
  });
  levelDown(hierarchical_entry);
}

function levelDown(el) {
  var next_el = el.nextElementSibling;
  if (next_el != null) {
    var next_next_el = next_el.nextElementSibling;
    var hierarchical_selects = document.querySelector(".hierarchical_selects");
    hierarchical_selects.insertBefore(el, next_next_el);
  }
}

function removeEvent() {
  var hierarchical_entry = closest(this, function(el) {
    return el.classList.contains('hierarchical_entry');
  });
  remove(hierarchical_entry);
}

function remove(el) {
  el.remove();
}



/* obj "alignments" pour aligner "attribute_value" "readable_value" */
function makeObjectForAlignments() {
  var arr = [];
  hierarchical_choices.forEach(function(key) {

    var list = listData(data_fetched, key);
    if (data_model[key].is_list_of_values) {
      var list = splitList(list, key);
    }
    var list = uniqList(list, "attribute_value");
    var list = sortList(list, "readable_value");
    arr.push(list);

  });

  alignments = {};
  arr.flat().forEach(function(key) {
    if (key.attribute_value != undefined) {
      alignments[key.attribute_value] = key.readable_value;
    }
  });
}



/* GESTION URL */

function updateURL(searchParams) {
  if (searchParams === undefined) var searchParams = getURLparams();
  // on ajoute les paramètres à l'url sans recharger la page
  var url = getURL();
  if (history.pushState) {
    var newurl = url.origin + url.pathname + "?" + searchParams.toString();
    history.replaceState({
      path: newurl
    }, '', newurl);
    // window.history.pushState("object or string", "Title", "?" + searchParams.toString());
  }
}

function getURL() {
  var url = new URL(document.location.href);
  return url;
}

function getURLparams() {
  var url = getURL();
  var searchParams = new URLSearchParams(url.search);
  return searchParams;
}


/* MODULES */

var closest = function(el, fn) {
  return el && (fn(el) ? el : closest(el.parentNode, fn));
}
