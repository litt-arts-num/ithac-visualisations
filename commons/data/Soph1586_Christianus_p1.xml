<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">


    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Q. Sept. Florens Christianus Doctissimo uiro Nicolao Gulonio
                    Graecarum literarum Regio doctori, S. dico.</title>
                <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : "la" pour le latin et "grc" pour le grec -->
                <!-- Attention ! ne pas faire de retour charriot dans le titre -->
                <author ref="#christianus_florens">Florens Christianus</author>
                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Ajouter autant de tâches (<respStmt>) que nécessaire -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Révision</resp>
                    <name>Christian NICOLAS</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>

                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne GARCIA-FERNANDEZ</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
            <sourceDesc>
                <bibl>
                    <title>Sophoclis Philoctetes in Lemno. Stylo ad ueteres tragicos Latinos
                        accedente quam proxime fieri potuit a Q. Septimio Florente Christiano.
                        Accesserunt eiusdem glossemata ad eandem Philocteteam.</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#soph">Sophocles</author>
                    <pubPlace ref="#lutetia">Lutetiae</pubPlace>
                    <publisher ref="#morellus_federicus">Federicus Morellus</publisher>
                    <date when="1586">1586</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor role="traducteur" ref="#christianus_florens">Florens
                        Christianus</editor>
                    <ref
                        target="https://books.google.fr/books?pg=RA1-PA44&amp;lpg=RA1-PA44&amp;dq=%22%E1%BC%A5%CF%81%CF%89%CF%82+%CF%84%CF%8C%CE%BE%CE%B1%22&amp;sig=ACfU3U3-VAPky93xKlOdqQ6C3rxaoQgp9A&amp;id=up1jAAAAcAAJ&amp;hl=fr&amp;ots=42uJQDopjC#v=onepage&amp;q&amp;f=false"/>
                    <!-- pour les liens google book, remplacer les & par &amp; pour que ça fonctionne -->
                </bibl>

                <listBibl>
                    <bibl/>
                </listBibl>

            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p/>
            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>

        <revisionDesc>
            <change when="2022-07-11">Sarah GAUCHER : création de la fiche TEI</change>
        </revisionDesc>

    </teiHeader>

    <text>
        <body>

            <!-- RAPPEL : on encode le texte latin ou grec; on n'encode dans le texte de la traduction que les notes d'éditeur (=membres du projet) -->

            <!-- Le couple d'éléments <head> contient le titre ou l'incipit du paratexte -->
            <!-- L'identifiant du TITRE est composé du titre de la fiche TT suivi d'un underscore (_) puis du chiffre 0 -->
            <head type="orig" xml:id="Soph1586_Christianus_p1_0"><persName
                    ref="#christianus_florens">Q. Sept. Florens Christianus</persName> Doctissimo
                uiro <persName role="destinataire" ref="#gulonius_nicolaus">Nicolao
                    Gulonio</persName> Graecarum literarum Regio doctori, S. dico.</head>
            <!-- Le deuxième élément du couple contient la traduction française du titre ou incipit -->
            <!-- L'identifiant de la traduction est le même que celui du latin, précédé d'un hashtag (#) -->
            <head corresp="#Soph1586_Christianus_p1_0" type="trad">Quintus Septimus Florens
                Christianus salue le très savant Nicolas Gulonius, docteur royal en lettres
                grecques.</head>

            <ab type="orig" xml:id="Soph1586_Christianus_p1_1"> Saepe mihi accidit (<persName
                    ref="#gulonius_nicolaus">Guloni</persName> doctissime) ut querelam apud plurimos
                instituerem de saeculi nostri aut infelicitate aut supinitate, quo Latinas literas
                uigere, et uelut postliminio uirescere cernimus ; scriptores Romanos ab omnibus fere
                tractari, coli, restitui, in solis Graecis praesertim iis qui prima uetustate et
                sententiarum grauitate posterioribus antiquis fuerunt commendabiles, parum operae
                insumi, cum fortia non desint ingenia et huic labori hauriendo non imparia. </ab>
            <ab type="trad" corresp="#Soph1586_Christianus_p1_1" ana="#PA #PH"> Il m’est souvent
                arrivé, très savant Gulonius, de me plaindre auprès de bien personnes de la ruine ou
                de la décadence de notre époque, où nous voyons que les lettres latines sont en
                vogue et en vigueur pour ainsi dire par retour de mode ; que les auteurs romains
                sont pour ainsi dire traités, étudiés et restitués par tous, tandis que peu de soin
                est consacré aux seuls Grecs, surtout à ceux que la plus ancienne antiquité et la
                gravité de leurs sentences ont recommandés aux auteurs anciens qui leur ont succédé,
                alors qu’on ne manque pas d’esprits vigoureux et capables d’achever ce labeur. </ab>
            <ab type="orig" xml:id="Soph1586_Christianus_p1_2"> Immo miratus sum doluique improbari
                a nonnullis hoc studium nostrum et tamquam inutile ludibrio haberi. </ab>
            <ab type="trad" corresp="#Soph1586_Christianus_p1_2" ana="#PA #PH"> Bien plus, je me
                suis étonné et j’ai souffert que quelques-uns désapprouvent notre étude et, la
                tournant en dérision, la considèrent comme inutile. </ab>
            <ab type="orig" xml:id="Soph1586_Christianus_p1_3"> Ridiculam, ita me Deus amet,
                falsamque rationem adferunt. </ab>
            <ab type="trad" corresp="#Soph1586_Christianus_p1_3" ana="#PA #PH"> Ils allèguent, que
                Dieu me vienne en aide, une raison ridicule et mensongère. </ab>
            <ab type="orig" xml:id="Soph1586_Christianus_p1_4"> Quid, inquiunt, in Graecis
                auctoribus reperias extra uerba, ac ut penitus eorum lectione imbutus fueris, quid
                famae uel nominis auferes, nisi ut Grammaticus audias ? </ab>
            <ab type="trad" corresp="#Soph1586_Christianus_p1_4" ana="#PA #PH"> Que découvrir,
                disent-ils, dans les auteurs grecs en dehors des mots et, à supposer qu’on se soit
                profondément abreuvé de leur lecture, quelle réputation ou renommée en tirera-t-on,
                si ce n’est celle d’être appelé grammairien ? </ab>
            <ab type="orig" xml:id="Soph1586_Christianus_p1_5"> Vtinam, mi <persName
                    ref="#gulonius_nicolaus">Guloni</persName>, id ipsum iure audiam ! </ab>
            <ab type="trad" corresp="#Soph1586_Christianus_p1_5" ana="#PA #PH"> Puissè-je, mon cher
                Gulonius, être ainsi appelé à bon droit ! </ab>
            <ab type="orig" xml:id="Soph1586_Christianus_p1_6"> Sed profecto ita est. </ab>
            <ab type="trad" corresp="#Soph1586_Christianus_p1_6"> Mais assurément il en va ainsi. </ab>
            <ab type="orig" xml:id="Soph1586_Christianus_p1_7"> Quamuis garrula sit omnis Graecia,
                tamen philosophos ueteres, praecipueque Principes in omni philosophia <persName
                    ref="#plat">Platonem</persName> et <persName ref="#arstt">Aristotelem</persName>
                non aliis fere testimoniis niti ad illustrandas omnis Philosophiae partes, quam quae
                petuntur ex tragicis imprimisque <persName ref="#eschl">Aeschylo</persName>,
                    <persName ref="#soph">Sophocle</persName>, <persName ref="#eur"
                    >Euripide</persName>, ut nullo magis tibicine fulciendam putarint doctrinam de
                moribus et rerum publicarum regnorumque administrationibus. </ab>
            <ab type="trad" corresp="#Soph1586_Christianus_p1_7" ana="#PHILO #TH_fin"> Bien que la
                Grèce tout entière fasse entendre sa parole, cependant les anciens philosophes et
                surtout les princes dans toute philosophie, Platon et Aristote, ne s’appuient pour
                illustrer toutes les parties de la philosophie sur aucune autre preuve que celle
                qu’on peut trouver chez les tragiques et en premier lieu chez Eschyle, Sophocle,
                Euripide : ainsi, ils pensèrent que c’était le meilleur accompagnement pour soutenir
                le savoir qui a trait aux mœurs et à l’administration des états et des royaumes. </ab>
            <ab type="orig" xml:id="Soph1586_Christianus_p1_8"> Itaque nulla habebant olim tam
                solemnia carmina aut concepta uerba quam tragicorum cum certamina et ludi poetarum
                scaenicaeque <foreign xml:lang="grc">διδασκαλίας</foreign> tam crebra essent, ut et
                magnates et populi ea decantarent, nihilque uulgi memoriae altius haereret. </ab>
            <ab type="trad" corresp="#Soph1586_Christianus_p1_8" ana="#TH_hist"> C’est pourquoi, ils
                ne tenaient aucun chant ou aucune parole pour aussi solennels ou graves que ceux des
                tragiques, alors que les concours et les jeux poétiques et théâtraux étaient si
                fréquentés qu’à la fois les grands et les peuples les chantaient et que rien n’était
                plus profondément fixé dans la mémoire des gens. </ab>
            <ab type="orig" xml:id="Soph1586_Christianus_p1_9"> Quin Romanos poetas nullis magis
                claruisse studiis quam fabulis de Gaeco uersis testes sunt <persName ref="#acc"
                    >Attius</persName>, <persName ref="#pacuu">Pacuuius</persName> et sola
                antiquitate uenerandus <persName ref="#enn">Ennius</persName>, Triumuiri fere ubique
                operis alieni interpretes (<seg type="paraphrase">ne soli <persName ref="#uarroatac"
                        >Varroni Atacino</persName> id cum <persName ref="#quint">Fabio</persName>
                        tribuamus<bibl resp="#SG"><author ref="#quint">Quint.</author>, <title
                            ref="#quint_io">I.O.</title>
                        <biblScope>10.1.87</biblScope>.</bibl></seg>) quorum literaria naufragia et
                    <foreign xml:lang="grc">μέλων λακίσματα</foreign> quae sparsa habemus in bonorum
                auctorum et Grammaticorum monimentis, non aliunde consuta erant quam ex tragicorum
                Graecorum ueteramentis, ut nec ipsi quorum plurimas integras habemus fabulas,
                Comici, <persName ref="#ter">Terentium</persName> inquam et <persName ref="#pl"
                    >Plautum</persName> : eorum si quidem Comoediae in Graeca ut plurimum basi
                constructae uisuntur et titulis personisque Palliatis ornatae. </ab>
            <ab type="trad" corresp="#Soph1586_Christianus_p1_9" ana="#TH_hist #T"> Bien plus, les
                poètes romains n’ont nulle part brillé davantage que dans des pièces traduites
                depuis le grec : sont témoins de ce fait Accius, Pacuvius et Ennius, vénérable par
                sa seule ancienneté, ces triumvirs presque partout traducteurs de l’œuvre d’autrui
                (pour ne pas, à la suite de Quintilien, attribuer ce fait au seul Varron d’Atax) ;
                et leurs oripeaux littéraires et leurs lambeaux versifiés, que nous possédons
                éparpillés dans les écrits des bons auteurs et des grammairiens, n’avaient pas été
                cousus d’un autre tissu que de celui des vieilleries des tragiques grecs ; il n’en
                va pas autrement des comiques dont nous possédons un grand nombre de pièces
                complètes (j’entends Térence et Plaute), pourvu qu’on remarque que leurs comédies
                sont construites sur une base le plus souvent grecque et parées des titres et des
                personnages des pièces grecques. </ab>
            <ab type="orig" xml:id="Soph1586_Christianus_p1_10"> Denique, si quid ipse iudico,
                pluris faciendi sunt uersus aliquot ex <persName ref="#soph">Sophocli</persName>
                <title ref="#soph_tr">Trachiniis</title> ab <persName ref="#acc">Attio</persName>
                uersi, qui apud <persName ref="#cic">Ciceronis</persName>
                <title ref="#cic_tusc">Tusculanas</title> extant (non enim ipsius <persName
                    ref="#cic">Ciceronis</persName> esse, sed <persName ref="#acc">Attii</persName>
                iamdudum mihi persuasi, ut toto errare caelo existimem eos qui cum <persName
                    ref="#cic">M. Tullium</persName> eorum auctorem putent, negant tamen bonum esse
                poetam : quamuis ex hoc solo fragmento, ut ex ungue Leo, magnum in poetica ingenium
                    <seg type="allusion">confessius fiat soricina naenia<bibl resp="#SG"><author
                            ref="#pl">Pl.</author>, <title ref="#pl_bacch">Bacch.</title>
                        <biblScope>889</biblScope>. <note>Pour une explication de la formule, voir
                            Erasme, <title>Adagia</title>, 235.</note></bibl></seg>) quam totus
                    <persName ref="#sen">Senecae</persName>
                <title ref="#sen_hercoe">Hercules Oeteus</title>, adeo omnes animos quorum interest
                uacare Musis Romanis afficere mihi uidetur plurima antiquitas. </ab>
            <ab type="trad" corresp="#Soph1586_Christianus_p1_10" ana="#TH_hist #T #PH"> Enfin, si
                je puis exprimer mon avis, quelques vers issus des Trachiniennes de Sophocle
                traduits par Accius et qui se trouvent dans les Tusculanes de Cicéron (car j’ai
                depuis longtemps acquis la conviction qu’ils ne sont point de Cicéron lui-même mais
                d’Accius, de sorte qu’à mon avis ils se trompent sur toute la ligne ceux qui,
                pensant que Cicéron en est l’auteur, affirment cependant qu’il est mauvais poète,
                bien que de ce seul fragment, comme le lion d’un ongle, un grand talent pour la
                poésie se dévoile davantage que le couinement d’une souris) doivent être estimés à
                plus haut prix que l’Hercule sur l’Oeta tout entier de Sénèque : bien plus, la plus
                haute antiquité me semble contaminer tous les esprits à qui il importe de s’atteler
                aux Muses romaines. </ab>
            <ab type="orig" xml:id="Soph1586_Christianus_p1_11"> Quod si prisci illius seculi
                felicitas ad hoc nostrum pertinuisset et porrecta esset, et plures haberemus
                Graecorum fabulas et Latinas integras, neque careremus tanto bono, scriptis scilicet
                illorum Trium uirorum. </ab>
            <ab type="trad" corresp="#Soph1586_Christianus_p1_11" ana="#TH_hist #PH"> Et si la
                fécondité de cette ancienne époque avait perduré et s’était prolongée jusqu’à notre
                siècle, nous aurions davantage de pièces grecques et latines complètes et nous
                n’aurions plus besoin du trésor que sont assurément les écrits de ces triumvirs. </ab>
            <ab type="orig" xml:id="Soph1586_Christianus_p1_12"> Nunc cum praeclare nobiscum agitur
                si uel umbra Latini sermonis fruimur, dolendum est non extare qui desideria et uota
                nostra explere uelint in tam utilium auctorum interpretatione. </ab>
            <ab type="trad" corresp="#Soph1586_Christianus_p1_12" ana="#PH #T"> Mais, comme c’est
                déjà merveille pour nous que de jouir de l’ombre, pour ainsi dire, de la langue
                latine, il faut se plaindre de n’avoir plus personne pour vouloir exaucer nos désirs
                et nos vœux en traduisant de si utiles auteurs. </ab>
            <ab type="orig" xml:id="Soph1586_Christianus_p1_13"> Frustra id multi tentarunt ; pauci
                felici audacia ad has partes accesserunt, atque inter paucos <persName
                    ref="#erasmus_desiderius">Erasmus</persName> et <persName
                    ref="#buchananus_georgius">Buchananus</persName> magna cum laude operam suam
                nauarunt : sed <persName ref="#erasmus_desiderius">Erasmus</persName> (quod in
                Graecorum gloriam cedit) ex aliorum ingeniis (ut recte censet Princeps Criticus)
                poeta fuit, ex suo uersificator. </ab>
            <ab type="trad" corresp="#Soph1586_Christianus_p1_13" ana="#PH #T"> Beaucoup l’ont tenté
                en vain ; peu ont atteint ces contrées avec une heureuse audace, et parmi ce petit
                nombre Érasme et Buchanan ont fourni un soutien puissant : mais Érasme (ce qui
                aboutit à la gloire des Grecs) fut poète en puisant dans le génie des autres (comme
                le pense à juste titre le prince des Critiques<note>C’est-à-dire Scaliger</note>),
                tandis qu’il fut versificateur en puisant dans le sien. </ab>
            <ab type="orig" xml:id="Soph1586_Christianus_p1_14"> Vnus <persName
                    ref="#scaliger_iosephus">Iosephus Scaliger</persName> felicissimo successu id
                praestitit et me iudice <cit type="nonref">
                    <quote xml:lang="grc">οἶος πέπνυται, τοὶ δὲ σκιαὶ ἀίσσουσι</quote>
                    <bibl resp="#SG"><author ref="#hom">Hom.</author>, <title ref="#hom_od"
                            >Od.</title>
                        <biblScope>10.495</biblScope>.</bibl>
                </cit>: cuius qui <title>Lycophrona</title>, <title>Orphei initia</title>,
                        <title><persName ref="#eschl">Aeschyli</persName>
                    <title ref="#eschl_eum">Diras</title></title> et <title><persName ref="#soph"
                        >Sophocli</persName>
                    <title ref="#soph_aj">Aiacem</title></title> legerit, attentiore cura, aequiore
                feret animo iacturam <persName ref="#acc">Attianae</persName> et <persName
                    ref="#pacuu">Pacuuianae</persName> poeseos, parumque aut nihil restare
                intelliget ad illorum antiquam <foreign xml:lang="grc">δεινότητα</foreign>, nitore
                quidem et cultu antiquitatem illam superari fatebitur. </ab>
            <ab type="trad" corresp="#Soph1586_Christianus_p1_14" ana="#PH #T"> Seul Joseph Scaliger
                a réussi cette entreprise avec un succès fort heureux, et, à mon avis, « lui, et lui
                seul, a la sagesse ; les autres ne sont que des ombres flottantes » : et si on lit
                avec assez d’attention et de soin son <title>Lycophron</title>, ses <title
                    xml:lang="la">Initia Orphei</title>, ses <title>Euménides d’Eschyle</title> et
                son <title>Ajax de Sophocle</title>, on supportera davantage la perte des poésies
                d’Accius et de Pacuvius et on comprendra qu’il ne reste à peu près rien à comparer à
                la splendeur antique des Grecs et on avouera que l’antiquité est vaincue par son
                brio et son raffinement. </ab>
            <ab type="orig" xml:id="Soph1586_Christianus_p1_15"> Huius ego uestigia procul adorans,
                certus tantum uirum sequi, nulla adsequendi spe, ausus sum experiri ecquid possem
                uetera recentare in interpretandis <persName ref="#ar">Aristophani</persName>
                <title ref="#ar_pax">Irena</title>, <persName ref="#arh">Apollonii</persName>
                <title ref="#arh_arg">Argonautis</title>, <persName ref="#eschl">Aechyli</persName>
                <title ref="#eschl_pr">Desmota</title> et <title ref="#eschl_sept">Septem
                    Thebana</title>, <persName ref="#eur">Euripidi</persName>
                <title ref="#eur_it">Taurica</title>, aliisque : et ecce <title ref="#soph_ph"
                    >Philocteta</title> in Lemno sese offert, quem scilicet ad te mitterem suasu
                    <persName ref="#morellus_federicus">Morelli</persName> nostri (quem nuper
                collegam uestrum renuntiatum esse plaudente, ut spero, tota literatorum cauea,
                gratulor et mirifice laetor). </ab>
            <ab type="trad" corresp="#Soph1586_Christianus_p1_15" ana="#PH #T"> Pour ma part,
                adorant de loin ses vestiges, résolu à suivre un si grand homme sans espoir de
                l’égaler, j’ai entrepris de chercher si je pourrais renouveler d’anciens écrits en
                traduisant la <title>Paix</title> d’Aristophane, les <title>Argonautiques</title>
                d’Apollonios, le <title>Prométhée</title> et les <title>Sept contre Thèbes</title>
                d’Eschyle, l’<title>Iphigénie en Tauride</title> d’Euripide etc. : et voici que se
                présente Philoctète à Lemnos, pour que je te l’envoie sur le conseil de notre cher
                Morellus (j’applaudis et me réjouis grandement qu’il ait tout récemment été nommé
                votre collègue, sous les applaudissements, je l’espère, de toute l’assemblée des
                lettrés). </ab>
            <ab type="orig" xml:id="Soph1586_Christianus_p1_16"> Quod si audacter factum dixeris,
                occurret haec defensio, <seg type="reecriture">Tute tibi intristi, tibi exedendum
                        est<bibl resp="#SG"><author ref="#ter">Ter.</author>
                        <title ref="#ter_phorm">Phorm.</title>
                        <biblScope>318</biblScope>.</bibl></seg>. </ab>
            <ab type="trad" corresp="#Soph1586_Christianus_p1_16" ana="#PA">Et si tu dis que j’ai
                agi avec audace, il se présentera cette défense : « Tu t’es mis dans le pétrin, tu
                dois t’en sortir ». </ab>
            <ab type="orig" xml:id="Soph1586_Christianus_p1_17"> Cum enim magno fauore gratiaque
                affeceris <persName ref="#eschl_sept">Septem Thebanam</persName> Tragoediam, ut eam
                in corona auditorum tuorum non aspernandam putaueris, fecisti iudicii tui
                praerogatiua ut pudorem, quo retinebar, emittendae huius tragoediae abiecerim, et
                    <persName>Pub. Mimi</persName> dictum quasi usurpauerim, <seg type="reecriture"
                    >Veterem ferendo audaciam inuitas nouam<bibl resp="#SG"><author ref="#gell"
                            >Gell.</author>, <title ref="#gell_noct">Noct.</title>
                        <biblScope>17.14.4</biblScope>. <note>Publius Syrus, <title>Sent.</title>
                            5.16 : <foreign xml:lang="la">Veterem ferendo iniuriam inuites
                                nouam.</foreign></note>
                    </bibl></seg>.</ab>
            <ab type="trad" corresp="#Soph1586_Christianus_p1_17" ana="#PA #PE"> En effet, puisque
                tu as accordé à la tragédie des <title>Sept contre Thèbes</title> une faveur et un
                crédit important, si bien que tu as pensé qu’elle ne devait pas être dédaignée pour
                l’assemblée de tes auditeurs, tu as fait, par la prérogative de ton jugement, en
                sorte que je chasse la honte qui me retenait de t’envoyer cette tragédie et que
                j’aie pour ainsi dire usurpé le dicton du mime Publius Syrus : « En supportant un
                vieux mal, tu en invites un nouveau. ». </ab>
            <ab type="orig" xml:id="Soph1586_Christianus_p1_18"> Itaque uide sis, mi <persName
                    ref="#gulonius_nicolaus">Guloni</persName>, ut in meis culpis tibi ipse ueniam
                des, et <title ref="#soph_ph">Philoctetam</title> nostrum ames : nostrum inquam,
                nihil enim habet <persName ref="#soph">Sophocli</persName>, quod scio habiturum,
                quia iudicio tuo committo. </ab>
            <ab type="trad" corresp="#Soph1586_Christianus_p1_18" ana="#PA"> Ainsi, fais-en sorte,
                mon cher Gulonius, s’il te plaît, de te pardonner à toi-même mes fautes et d’aimer
                notre Philoctète ; le nôtre, dis-je, car il n’a rien de Sophocle, ce que je sais que
                tu prendras en considération , parce que je m’en remets à ton jugement. </ab>
            <ab type="orig" xml:id="Soph1586_Christianus_p1_19"> Addidi et Glossemata aliquot,
                Notasque iamdudum natas in Schedis meis, sed quae statim forsitan denascentur, et
                publicatione sua, <seg type="allusion">tanquam sorex indicio suo<bibl resp="#SG"
                            ><author ref="#ter">Ter.</author>, <title ref="#ter_eun">Eun.</title>
                        <biblScope>1024</biblScope>.<note>Donat explique que la souris couine
                            bruyamment en mangeant et se trahit ainsi, même dans
                        l'obscurité.</note></bibl></seg>, peribunt : ut me qui pudoris fines, te
                potissimum auctore transgressus sum, non dubites factum gnauiter impudentem,
                uetusque prouerbium tollentem <seg type="reecriture" xml:lang="grc">τὸ ἐπὶ τῇ φακῇ
                        μύρον<bibl resp="#SG"><author ref="#erasmus_desiderius">Erasme</author>,
                            <title ref="#er_ad">Adagia</title>, <biblScope>623</biblScope><ref
                            target="http://ihrim.huma-num.fr/nmh/Erasmus/Proverbia/Adagium_623.html"
                            />.<note>C'est-à-dire, utiliser une huile parfumée coûteuse pour
                            assaisonner un plat de lentilles ; proverbial pour un divertissement
                            voyant avec peu de nourriture.</note></bibl></seg>. </ab>
            <ab type="trad" corresp="#Soph1586_Christianus_p1_19" ana="#PH #PA"> J’ai également
                ajouté un certain nombre de glossaires et des notes qui ont il y a longtemps pris
                naissance dans mes pages mais qui peut être aussitôt s’évaporeront et, par leur
                publication causeront leur perte comme une souris par son couinement, si bien que tu
                ne douteras pas que moi, qui ai dépassé les limites de la bienséance principalement
                sous ton égide, j’ai été absolument impudent et que j’ai porté haut le vieux
                proverbe « de la myrrhe sur les lentilles » </ab>
            <ab type="orig" xml:id="Soph1586_Christianus_p1_20"> Omne quicquid est, ad hoc mitto, ut
                censurae tuae lima <foreign xml:lang="grc">ἀπεξεσμένον καὶ ἀφειδῶς
                    νενουθετημένον</foreign>, iterum aliquando in ora hominum prodire possit. </ab>
            <ab type="trad" corresp="#Soph1586_Christianus_p1_20" ana="#PA"> Tout ce qu’il y a, je
                l’envoie pour que ces écrits puissent paraître et aller un jour dans la bouche des
                hommes, sans pitié frottés et corrigés de la lime de ta censure. </ab>
            <ab type="orig" xml:id="Soph1586_Christianus_p1_21"> Vale, uir doctisime, et <persName
                    ref="#auratus_ioannes">Aurato</persName> socero tuo, quem magistrum habuisse
                nunquam paenitebit me si sapiam, plurimam a me salutem. </ab>
            <ab type="trad" corresp="#Soph1586_Christianus_p1_21"> Adieu, homme très savant, et
                adresse un chaleureux salut de ma part à ton beau-père Dorat, que, si je suis avisé,
                je ne regretterai jamais d’avoir eu pour maître. </ab>




        </body>
    </text>
</TEI>
