<?xml version="1.0" encoding="UTF-8"?>


<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Ioannis Egidii Nuceriensis epigramma ad iuuenes</title>
                <author ref="#egidius_ioannes">Ioannes Egidius Nuceriensis</author>

                <respStmt>
                    <resp>Transcription</resp>
                    <name>Christian NICOLAS</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Christian NICOLAS</name>
                </respStmt>
                <respStmt>
                    <resp>Révision</resp>
                    <name>Laure HERMAND</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Commentaire</resp>
                    <name>Laure Hermand</name>
                </respStmt>
                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne Garcia-Fernandez</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <sourceDesc>

                <bibl>
                    <title>Guidonis Iuuenalis natione Cenomani in Terentium familiarissima
                        interpretatio cum figuris unicuique scaenae praepositis.</title>
                    <author ref="#ter">Terentius</author>
                    <pubPlace ref="#lugdunum">Lugduni</pubPlace>
                    <publisher ref="#trechsel_ioannes">Iohannes Trechsel</publisher>
                    <date when="1493">1493</date>
                    <editor ref="#ascensius_iodocus">Iodocus Badius</editor>
                    <editor ref="#iuuenalis_guido">Guido Juvenalis</editor>
                    <ref
                        target="https://gallica.bnf.fr/ark:/12148/bpt6k8710568w.r=trechsel%20t%C3%A9rence?rk=42918;4"
                        >https://gallica.bnf.fr/ark:/12148/bpt6k8710568w.r=trechsel%20t%C3%A9rence?rk=42918;4</ref>
                </bibl>

                <listBibl>
                    <bibl/>
                </listBibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Cette épigramme en distiques élégiaques explique le projet éditorial de
                    Jouenneaux.</p>
            </abstract>

            <langUsage>
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>

            </langUsage>

            <textClass n="vers"></textClass>

        </profileDesc>

        <revisionDesc>
            <change when="2022-04-08">Sarah GAUCHER : création de la fiche TEI, encodage de la
                transcription</change>
        </revisionDesc>

    </teiHeader>

    <text>
        <body>

            <head type="orig" xml:id="Te1493_Egidius_p1_0">Ioannis Egidii Nuceriensis epigramma ad
                iuuenes.</head>

            <head corresp="#Te1493_Egidius_p1_0" type="trad">De <seg>Jean Gilles de
                        Noyer<note>Ioannes Aegidius Nucerinus, Jean Nucerin, Jean Gilles de Noyer,
                        Jean-Gilles Desnoyers, écrivain, auteur de compilation de proverbes et de
                        vers élégiaques.</note></seg> épigramme aux jeunes gens.</head>
            <ab type="orig" xml:id="Te1493_Egidius_p1_1"><l>Quid teris ignauum damnosa per otia
                    tempus</l><l> Immemor aetatis culta iuuenta breuis ? </l></ab>
            <ab type="trad" corresp="#Te1493_Egidius_p1_1">Pourquoi perdre ton temps dans des
                loisirs coupables, ô docte jeunesse oublieuse de la brièveté de la vie ?</ab>
            <ab type="orig" xml:id="Te1493_Egidius_p1_2"><l>Semper eunt tacito fugientia tempora
                    motu,</l><l> Hinc redit ad primos lapsa nec hora dies. </l></ab>
            <ab type="trad" corresp="#Te1493_Egidius_p1_2">Le temps s’enfuit toujours, se meut sans
                faire de bruit, et jamais l’heure labile ne revient en arrière vers les premiers
                jours.</ab>
            <ab type="orig" xml:id="Te1493_Egidius_p1_3"><l>Fers modo longinquas uestigia crebra per
                    urbes</l><l> Nunc teris incertis compita cuncta uiis. </l></ab>
            <ab type="trad" corresp="#Te1493_Egidius_p1_3">Tu portes tantôt tes pas pressés parmi
                les villes lointaines, tantôt tu foules tous les chemins aux routes
                incertaines.</ab>
            <ab type="orig" xml:id="Te1493_Egidius_p1_4"><l>Haec sectatores leuitas turpissima
                    uanos</l><l> Reddit et ingenii semina lenta rudis,</l><l> Dum tua sunt uiridi
                    florentia corda sub aeuo </l><l> Et cupis ore nouo uerba diserta loqui.
                </l></ab>
            <ab type="trad" corresp="#Te1493_Egidius_p1_4">Cette légèreté indigne rend les disciples
                vains et lent l’ensemencement des esprits encore grossiers, alors que ton cœur
                fleurit sous le vert printemps et que tu désires parler en mots diserts d’une bouche
                nouvelle.</ab>
            <ab type="orig" xml:id="Te1493_Egidius_p1_5"><l>Ingeniosa legas nitidi monumenta
                    laboris</l><l> Si placeant studiis comica dicta tuis. </l></ab>
            <ab type="trad" corresp="#Te1493_Egidius_p1_5">Lis donc l’ingénieux témoignage de ce
                travail brillant, si les paroles comiques plaisent à tes goûts.</ab>
            <ab type="orig" xml:id="Te1493_Egidius_p1_6"><l>Splendet enim lepidi comoedia grata
                    Terenti,</l><l> Omnia perspicuo sensa decore nitent</l><l> Moribus ingenui
                    doctrina serena Guidonis,</l><l> Tersit difficiles arte nitente locos. </l></ab>
            <ab type="trad" corresp="#Te1493_Egidius_p1_6">Car brille la gracieuse comédie du
                charmant Térence, toutes les significations à la beauté révélée scintillent, la
                sereine science de Jouenneaux, noble de caractère, a épongé les passages difficiles
                d’un art scintillant.</ab>
            <ab type="orig" xml:id="Te1493_Egidius_p1_7"><l>Hinc tibi compositas clari Ciceronis ad
                    artes,</l><l> Vtile iam facili tramite carpis iter. </l></ab>
            <ab type="trad" corresp="#Te1493_Egidius_p1_7">De là, vers les savoir-faire du célèbre
                Cicéron, composés pour toi, s’ouvre sous tes pieds l’utile chemin au trajet
                facile.</ab>
            <ab type="orig" xml:id="Te1493_Egidius_p1_8"><l>Inde nouam celebris si mox interpretis
                    artem</l><l> Verba uel attenta mente uoluta legis, </l><l> Inclyta Donati
                    sollertis scripta patebunt,</l><l> Sit licet in uariis pagina scabra locis.
                </l></ab>
            <ab type="trad" corresp="#Te1493_Egidius_p1_8">Là, si tu lis avec attention le nouveau
                traité du célèbre exégète ou ses mots enveloppés, les gloses célèbres de l’habile
                Donat s’ouvriront devant toi, même s’il reste dans plusieurs passages des pages
                rudes.</ab>
            <ab type="orig" xml:id="Te1493_Egidius_p1_9"><l>Omnia per faciles sunt nota uocabula
                    scaenas,</l><l> Vana nec interpres uerba disertus habet. </l></ab>
            <ab type="trad" corresp="#Te1493_Egidius_p1_9">Tous les mots dans chaque scène
                accessible sont connus et l’exégète éloquent n’a pas de mots vains.</ab>
            <ab type="orig" xml:id="Te1493_Egidius_p1_10"><l>Hic quoque Romano cultissima lingua
                    lepore</l><l> Exterso lepidos edet ab ore sales. </l></ab>
            <ab type="trad" corresp="#Te1493_Egidius_p1_10">Ici aussi la langue très châtiée pleine
                du charme romain produira de délicates plaisanteries de sa bouche toute propre.</ab>
            <ab type="orig" xml:id="Te1493_Egidius_p1_11"><l>Candidus ecce Guido iuuenes miseratus
                    inertes</l><l> Tradidit ingenii commoda dona pii. </l></ab>
            <ab type="trad" corresp="#Te1493_Egidius_p1_11">Voici que Guy, dans sa candeur, a eu
                pitié des jeunes gens privés de traités et a transmis le cadeau commode de son pieux
                talent.</ab>
            <ab type="orig" xml:id="Te1493_Egidius_p1_12"><l>Tuque Palaemoniae sectator idoneus
                    artis,</l><l> Tradita sub memori dona reconde sinu</l><l> Et meritas agito
                    facili pro codice grates :</l><l> Non sunt ingratis munera digna uiris.
                </l></ab>
            <ab type="trad" corresp="#Te1493_Egidius_p1_12">Quant à toi, disciple approprié de l’art
                de <seg>Palémon<note>La grammaire, du nom d’un grammairien latin.</note></seg>,
                range ce cadeau au creux de ta mémoire et dis un merci mérité pour ce livre
                accessible : les cadeaux sont indignes des ingrats.</ab>
            <ab type="orig" xml:id="Te1493_Egidius_p1_13"><l>Otia sed fugias studiis inuisa seueris
                    :</l><l> Nullus ab ignauo pectore surgit honos. </l></ab>
            <ab type="trad" corresp="#Te1493_Egidius_p1_13">Mais fuis l’oisiveté, ennemie des études
                sérieuses : aucun honneur ne surgit d’un cœur paresseux.</ab>
            <ab type="orig" xml:id="Te1493_Egidius_p1_14"><l>Vt primum pigros serpet caua ruga per
                    artus,</l><l> Plorabit stultos tarda senecta dies. </l></ab>
            <ab type="trad" corresp="#Te1493_Egidius_p1_14">Dès que les rides creuses serpenteront
                sur tes membres oisifs, la vieillesse ralentie pleurera les jours sottement
                perdus.</ab>
            <ab type="orig" xml:id="Te1493_Egidius_p1_15"><l>Assiduum tolera nunc tota mente
                    laborem</l><l> Aetas ne in solitum curua recuset onus. </l></ab>
            <ab type="trad" corresp="#Te1493_Egidius_p1_15">Supporte aujourd’hui de tout ton cœur un
                travail régulier pour éviter à l’âge où l’on se courbe d’avoir à refuser une charge
                insolite.</ab>
            <ab type="orig" xml:id="Te1493_Egidius_p1_16"><l>Exerce studiis uirides feruentibus
                    annos,</l><l> Perpetuum claris stet decus ingeniis. </l></ab>
            <ab type="trad" corresp="#Te1493_Egidius_p1_16">Entraîne tes vertes années aux études
                ferventes et qu’un honneur perpétuel aille aux esprits éclairés.</ab>

        </body>
    </text>
</TEI>
