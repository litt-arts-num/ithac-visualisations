<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Idem autor</title>
                <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : "la" pour le latin et "grc" pour le grec -->
                <!-- Attention ! ne pas faire de retour charriot dans le titre -->
                <author ref="#melanchtonus_philippus">Philippus Melanchthon</author>
                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Ajouter autant de tâches (<respStmt>) que nécessaire -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Théophane TREDEZ</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Théophane TREDEZ</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Révision</resp>
                    <name>Laure HERMAND</name>
                    <name>Christian NICOLAS</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Commentaire</resp>
                    <name>Laure HERMAND</name>
                </respStmt>
                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne GARCIA-FERNANDEZ</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
            <sourceDesc>
                <bibl>
                    <title>Publii Terentii comoediae sex, cum prioribus ferme castigationibus et
                        plerisque explicationibus, et auctario insuper quodam, editae studio et cura
                        Ioachimi Camerarii Pabergensis.</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#ter">Terentius</author>
                    <pubPlace ref="#lipsia">Lipsiae</pubPlace>
                    <publisher ref="#papa_ualentinus">Valentinus Papa</publisher>
                    <date when="1546">1546</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor ref="#camerarius_ioachimus">Ioachimus Camerarius</editor>
                    <ref target="https://www.digitale-sammlungen.de/en/view/bsb00016248?page=,1"/>
                </bibl>

                <listBibl>
                    <bibl>HAMM, Joachim, « Joachim Camerarius d.Ä. », in W. Kühlmann et al. (éd.),
                        Frühe Neuzeit in Deutschland 1520-1620. Literaturwissenschaftliches
                        Verfasserlexikon. Bd. 1 (VL 16), Berlin, De Gruyter, 2011, p. 425-438 </bibl>
                    <bibl> LAWTON, Harold W., Térence en France au XVIe siècle. Editions et
                        traductions, Paris, Jouve, 1926 (Genève, Slatkine repr., 1970-1972)</bibl>
                    <bibl> STÄHLIN, Friedrich, « Camerarius, Joachim », in Neue Deutsche Biographie,
                        3, 1957, p. 104-105<ref
                            target="https://www.deutsche-biographie.de/pnd118518569.html#ndbcontent"
                        />
                    </bibl>
                </listBibl>

            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Cette édition n’est pas mentionnée par Lawton. L’éditeur scientifique en est
                    Joachim Camerarius l’Ancien, humaniste originaire de Bamberg. Elle paraît en
                    1546 chez l’imprimeur de Leipzig Valentin Bapst l’Ancien. Camerarius avait déjà
                    publié chez lui en 1545 cinq pièces de Plaute (<title xml:lang="la">Amphitruo,
                        Asinaria, Curculio, Casina, Cistellaria</title>).</p>
                <p> Il s’agit d’une édition scolaire où le texte des six pièces de Térence est suivi
                    de résumés des pièces et d’annotations dues à Camerarius. Ce dernier indique
                    également quelques variantes textuelles qu’il a observées. L’édition ne propose
                    pas de commentaire proprement dit.</p>
                <p>Mais elle contient plusieurs paratextes dus à Philippe Melanchthon. Le premier
                    prend la forme d’un bref exposé théorique illustré d’exemples, et porte le titre
                    « Exhortation à lire les tragédies et les comédies ». Melanchthon y justifie du
                    point de vue réformé l’utilité de la lecture du théâtre antique païen par les
                    jeunes étudiants. Il insiste sur la valeur morale tant de la tragédie que de la
                    comédie. Le second est une lettre adressée aux enseignants. Enfin Melanchthon
                    rédige pour chaque pièce un bref argument à visée pédagogique.</p>
                <p> Philippe Melanchthon est le nom traduit en grec de Philipp Schwarzerd,
                    humaniste, philosophe et réformateur protestant allemand, né le 16 février 1497
                    à Bretten et mort le 19 avril 1560 à Wittenberg, en Allemagne. Professeur de
                    grec à l’université de Wittenberg, il adhère dès le début à la Réforme et
                    devient l’ami et le collaborateur de Luther. Il donne un premier exposé
                    systématique de la pensée de Luther dans ses Loci Communes (1521). Il se
                    préoccupe de concilier la Réforme et l’humanisme chrétien. Il est l’auteur de la
                    Confession d’Augsbourg présentée à la diète d’Augsbourg en 1530. À la mort de
                    Luther en 1546, il devient le principal chef du luthéranisme. Il fait paraître
                    en 1516 à Tübingen une édition des comédies de Térence (<title>Comoediae metro
                        numerisque restitutae</title>). Voir Lawton n°154, p. 123. </p>


            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>
        <revisionDesc>
            <change when="2022-09-08">Sarah GAUCHER : création de la TEI, encodage de la
                transcription et de la traduction, encodage complet</change>
        </revisionDesc>
    </teiHeader>

    <text>
        <body>

            <!-- RAPPEL : on encode le texte latin ou grec; on n'encode dans le texte de la traduction que les notes d'éditeur (=membres du projet) -->

            <!-- Le couple d'éléments <head> contient le titre ou l'incipit du paratexte -->
            <!-- L'identifiant du TITRE est composé du titre de la fiche TT suivi d'un underscore (_) puis du chiffre 0 -->
            <head type="orig" xml:id="Te1546_Melanchthon_p3_0">Idem autor</head>
            <!-- Le deuxième élément du couple contient la traduction française du titre ou incipit -->
            <!-- L'identifiant de la traduction est le même que celui du latin, précédé d'un hashtag (#) -->
            <head corresp="#Te1546_Melanchthon_p3_0" type="trad">Même auteur</head>

            <ab type="orig" xml:id="Te1546_Melanchthon_p3_1"> Nullus est omnino scriptor in Latina
                lingua, quem tantopere cognosci ediscique retulerit, atque <persName ref="#ter"
                    >Terentium</persName>. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_1" ana="#PE"> Il n’y a absolument aucun
                écrivain en langue latine qu’il faille autant connaître et apprendre que Térence. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_2"> Nam cum prima uirtus in oratione sit
                proprie loqui, nec proprii sermonis artificem meliorem <persName ref="#ter"
                    >Terentio</persName> habeamus, dignus plane est in quo perdiscendo plurimum
                operae ac studii ponamus. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_2" ana="#PE"> En effet, puisque la
                première vertu du discours est de bien parler, et que nous n’avons pas de meilleur
                artisan d’une bonne langue que Térence, il mérite tout à fait que nous mettions le
                plus de soin et d’application à le connaître parfaitement. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_3"> Et ab iis, qui docent pueros,
                superstitiosam etiam diligentiam in hoc autore enarrando requiro. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_3" ana="#PE"> Et je demande à ceux qui
                enseignent aux enfants également une scrupuleuse attention dans le commentaire de
                cet auteur. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_4"> Primum prodest monere, quae comoediae
                natura sit, quidue spectarint comoediarum auctores, nomen unde factum sit. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_4" ana="#TH_hist"> D’abord il est utile
                d’éclairer la nature de la comédie, les intentions des auteurs comiques et l’origine
                du nom. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_5"> Atque haec quidem <persName
                    ref="#donat">Donatus</persName> copiose exposuit. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_5"> Et Donat en a fait d’ailleurs un
                riche exposé. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_6"> Conuenit autem inter omnes, autores
                comoediarum uoluisse, et communium morum et casuum exempla proponere, quibus uelut
                admoniti, prudentius iudicemus de rebus humanis, et genere orationis facundiam
                locupletare. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_6" ana="#TH_fin"> Tout le monde
                s’accorde pour dire que les auteurs de comédies ont voulu à la fois montrer des
                exemples de caractères et de situations communs afin qu’ainsi avertis nous jugions
                plus prudemment les affaires humaines, et enrichir l’éloquence oratoire par ce genre
                littéraire. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_7"> Quae inculcabunt professores eo
                diligentius, ut sciant pueri quid de illis petere debeant, quem fructum facturi sint
                legendis comoediis. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_7" ana="#PE"> Voilà ce qu’inculqueront
                les professeurs si scrupuleusement que les enfants sauront quoi demander à leur
                propos, et quel fruit ils obtiendront de leur lecture des comédies. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_8"> Nam quo propius cognorint, et uim
                literarum et utilitatem, eo impensius iuuat discere. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_8" ana="#PE"> En effet, plus ils
                connaîtront de près la force et l’intérêt littéraire, plus ils se hâteront de bien
                vouloir les apprendre. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_9"><seg type="allusion">Porro nomen
                    comoedia factum est (ut plurimi dicunt) <foreign xml:lang="grc">ἀπὸ τοῦ
                        κώμου</foreign>, id est, a comessatione et <foreign xml:lang="grc">ἀπὸ τῆς
                        ὠδῆς</foreign>, id est, a carmine, ut sit conuiuiale carmen.<bibl resp="#LH"
                            ><author ref="#euant">Evanthius</author>, <title ref="#euant_fab">De
                            fabula</title>
                        <biblScope>1.3</biblScope><ref
                            target="http://hyperdonat.huma-num.fr/editions/html/DonEva.html"
                        />.</bibl></seg></ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_9" ana="#TH_hist"> En outre, la comédie
                a reçu son nom (comme le disent la majorité des auteurs) <foreign xml:lang="grc">ἀπὸ
                    τοῦ κώμου</foreign>, c’est-à-dire du festin, et <foreign xml:lang="grc">ἀπὸ τῆς
                    ὠδῆς</foreign>, c’est-à-dire du chant, de telle sorte que c’est un chant de
                repas. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_10"> Nam in conuiuiis primo iocoso quodam
                carmine ciuium uitia iuuentus liberius taxabat ; postea ars accessit et theatrorum
                consuetudo. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_10" ana="#TH_hist"> En effet à l’origine
                lors d’un festin, les jeunes gens blâmaient assez ouvertement les vices de leurs
                concitoyens avec un chant plaisant ; ensuite se sont ajoutés l’art et la coutume du
                théâtre. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_11">
                <persName ref="#arstt">Aristoteles</persName>
                <foreign xml:lang="grc">περὶ ποιητικῆς</foreign>, de nominis origine aliter sentit,
                nec piget adscribere eius uerba : <cit type="hyperref">
                    <quote xml:lang="grc">ὡς κωμῳδους οὐκ ἀπὸ τοῦ κωμάζειν λεχθέντας, ἀλλὰ τῇ κατὰ
                        κώμας πλάνῃ ἀτιμαζομένους ἐκ τοῦ ἄστεος</quote>
                    <bibl resp="#LH"><author ref="#arstt">Arstt.</author>, <title ref="#arstt_poet"
                            >Poet.</title>
                        <biblScope>1448a</biblScope>.</bibl>
                </cit>. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_11" ana="#TH_hist"> Dans sa
                    <title>Poétique</title>, Aristote a un autre avis sur l’étymologie du nom, et je
                ne répugne pas à rapporter ses mots : « que le nom des ‘comédiens’ ne vient pas de
                kômazein (« festoyer »), mais de leur errance de kômè (« bourg ») en kômè, bannis de
                la ville qu’ils étaient ». </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_12"> Cetera Donatus. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_12" ana="#TH_hist"> On trouve le reste chez
                Donat. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_13"> In recensendis argumentis fabularum,
                consilium poetae summa cura exponendum est. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_13" ana="#TH_dram #TH_fin #A"> Lorsque
                l’on passe en revue les arguments des pièces, il faut faire voir avec très grand
                soin le projet du poète. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_14"> Quarum personarum descriptiones in
                primis admirationem mereantur, in quibus locis plurimum commoretur poeta, ubi
                plurimum neruorum, plurimum eloquentiae ostentet, quos locos ita commendent pueris,
                ut et familiariter ament et tanquam digitos suos cognorint. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_14" ana="#PE"> Il faut montrer quelles
                descriptions de ces personnages méritent surtout d’être admirées, dans quels
                passages le poète s’attarde le plus, où il montre le plus de force, le plus
                d’éloquence, quels passages il faut recommander aux enfants, de sorte qu’ils les
                aiment intimement et les connaissent sur le bout des doigts. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_15"> Et ob hanc causam <seg
                    type="allusion">fabularum omnium argumenta ueteri more in <foreign
                        xml:lang="grc">πρότασιν, ἐπίτασιν, καταστροφὴν</foreign> partiantur<bibl
                        resp="#LH"><author ref="#euant">Evanthius</author>, <title ref="#euant_fab"
                            >De fabula</title>
                        <biblScope>7.1 et 7.4</biblScope><ref
                            target="http://hyperdonat.huma-num.fr/editions/html/DonEva.html"
                        />.</bibl></seg>, ut distributa facilius assequatur iuuentus. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_15" ana="#RH #PE"> Et c’est pour cela
                qu’il faut diviser les arguments de toutes les pièces selon l’ancienne coutume en
                une protase, une épitase et une catastrophe, pour que les jeunes gens parviennent à
                les suivre plus facilement après cette distinction. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_16"> Fere autem fabulae continent
                periculum quoddam. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_16" ana="#TH_dram"> Mais les pièces
                comportent d’ordinaire une épreuve. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_17"> Nusquam enim consilio locus est, nisi
                in dubiis rebus. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_17" ana="#TH_dram"> En effet, ce n’est
                jamais le lieu d’une délibération, sauf pour des situations incertaines. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_18"> Neque uero aliud est comoedia, nisi
                humanorum consiliorum et euentuum imago quaedam. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_18" ana="#TH_dram"> Mais la comédie
                n’est rien d’autre qu’une peinture des délibérations et des issues données par les
                hommes. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_19"> In <title ref="#ter_andr"
                    >Andria</title> periclitatur Pamphilus, ut Glycerio fidem promissam praestet,
                postquam patri per errorem pollicitus est, futurum se in eius potestate ac ducturum
                quam uellet. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_19" ana="#A"> Dans
                    l’<title>Andrienne</title> on met à l’épreuve Pamphile de tenir la parole qu’il
                a donnée à Glycère après qu’il a dans son égarement promis à son père qu’il se
                soumettrait à son pouvoir et qu’il épouserait celle qu’il voudrait. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_20"> Estque haec ueluti <foreign
                    xml:lang="grc">στάσις</foreign> fabulae. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_20" ana="#TH_dram"> Et voilà ce qui est
                comme une stasis de la pièce. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_21"> Consilia omnia, querelae omnes,
                argumenta omnia huc referenda sunt. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_21" ana="#TH_dram"> Toutes les
                délibérations, toutes les plaintes, tous les arguments doivent être ramenés à cela. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_22"> Et tota fabula similis est orationi
                generis suasorii. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_22" ana="#RH"> Et la pièce entière est
                semblable au genre rhétorique de la suasoire. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_23"> Nam uarie de tota causa consultant
                senes, adolescentes ac serui. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_23" ana="#RH"> En effet, vieillards,
                jeunes gens et esclaves délibèrent de diverses manières sur toute l’affaire. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_24"> Sed ut argumentum propius cernere
                queant pueri, ordine singulas partes referemus. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_24" ana="#RH"> Mais pour que les enfants
                puissent saisir l’argument de plus près, rapportons dans l’ordre chaque partie. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_25"> Periculum <foreign xml:lang="grc"
                    >πρότασις</foreign>. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_25" ana="#TH_dram"> L’épreuve ou
                protase. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_26"> Cum amaret Pamphilus Glycerium eique
                fidem dedisset ducturum se esse uxorem, pater qui amare filium compererat, consilium
                capit abstrahundi eius a Glycerio, simulat se uxorem daturum, poscitque ut ne filius
                id recuset. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_26" ana="#A"> Alors que Pamphile aimait
                Glycère et qu’il lui avait donné sa parole de la prendre pour épouse, son père, qui
                avait découvert les sentiments de son fils, décide de l’éloigner de Glycère, feint
                d’être sur le point de donner une épouse son fils et lui demande de ne pas refuser. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_27"> Is cum et patrem uereretur, et
                Glycerium deserere parum pium iudicaret, pendet animi, nec habet quo se uortat ; ibi
                Dauus intelligens simulari rem a patre, consulit erili filio ut polliceatur patri se
                in eius potestate futurum, periculi nihil esse quandoquidem simulentur nuptiae. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_27" ana="#A"> Puisque Pamphile respecte
                son père, autant qu’il juge déloyal de quitter Glycère, il est indécis et ne sait
                quel parti prendre ; c’est alors que Dave, comprenant que le père use d’une feinte,
                conseille au fils de son maître de promettre à son père qu’il se soumettra à lui,
                qu’il n’y a rien à craindre puisque les noces sont feintes. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_28"> Filius obsequitur, quanquam aegre :
                pollicetur patri ducturum se quam uellet. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_28" ana="#A"> Le fils cède, quoiqu’à
                regret : il promet à son père d’épouser celle qu’il voulait. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_29">
                <foreign xml:lang="grc">Ἐπίτασις</foreign>. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_29" ana="#TH_dram"> Epitase. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_30"> Interea spargitur de Pamphili nuptiis
                rumor, resciscunt et mulierculae quibus cum Glycerio consuetudo erat. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_30" ana="#A"> Pendant ce temps se répand
                la rumeur d’un mariage de Pamphile, et les femmes de petite vertu que fréquente
                Glycère ont vent de cela. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_31"> Et pater cum a filio quod uoluerat
                extorsisset, non iam simulat rem, sed seriam fabulam orditur. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_31" ana="#A"> Et alors que le père avait
                obtenu par la force ce qu’il voulait de son fils, il ne feint plus l’affaire, mais
                commence une histoire sérieuse. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_32"> Chremetem orat ut natam det uxorem
                filio suo. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_32" ana="#A"> Il prie Chrémès de donner
                la main de sa fille à son fils. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_33"> Facile impetrat. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_33" ana="#A"> Il l’obtient aisément. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_34"> Hic de Pamphilo actum fuit : maturare
                nuptias pater studet, et filius pollicitus erat se in patris potestate futurum esse. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_34" ana="#A"> C’est là que c’en est fait
                de Pamphile : le père s’applique à hâter le mariage, et le fils avait promis de se
                soumettre aux vœux de son père. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_35"> Quaeso quomodo ex his turbis se
                explicet ? </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_35" ana="#A #PE"> Comment peut-il, je te
                prie, se tirer d’affaire ? </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_36"> Quid faciat Pamphilus ? </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_36" ana="#A #PE"> Que doit faire
                Pamphile ? </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_37"> Hic Dauus cuius imprudentia periculum
                contractum erat, cum dolus non successisset, Chremetem deterret a nuptiis, fidem
                faciens amari aliam a Pamphilo. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_37" ana="#A"> Ici, Dave, dont
                l’ignorance avait diminué l’épreuve, après avoir échoué par la ruse, détourne
                Chrémès du mariage en lui faisant croire que Pamphile en aime une autre. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_38">
                <foreign xml:lang="grc">Καταστροφή</foreign>
            </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_38" ana="#TH_dram"> Catastrophe. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_39"> At pater rem urget ; et processisset,
                nisi casus, qui saepe plus ualet quam ratio, dubiis rebus opem attulisset. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_39" ana="#A"> Mais le père hâte
                l’affaire ; et il aurait réussi si le hasard, qui souvent est plus fort que la
                raison, n’avait pas apporté son aide à la situation incertaine. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_40"> Venit quispiam Crito, qui docet
                Glycerium Chremetis filiam esse, gratum omnibus nuntium. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_40" ana="#A"> Il vient un dénommé
                Criton, qui révèle que Glycère est la fille de Chrémès : une bonne nouvelle pour
                tous. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_41"> Nuptiae ex sententia Pamphili
                conficiuntur. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_41" ana="#A"> Le mariage est fait selon
                les vœux de Pamphile. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_42"> Vide ex quanto periculo casu euaserit
                Pamphilus. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_42" ana="#A"> Voyez de quelle grande
                épreuve Pamphile est sorti grâce au hasard. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_43"> Est autem in more <persName
                    ref="#ter">Terentio</persName>, in prima fere scaena, longa narratione
                commemorare occasionem fabulae, atque eas narrationes plurimi fecerunt ueteres. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_43" ana="#TH_dram #TH_hist"> Et Térence
                a pour habitude, d’ordinaire dans la première scène, de rappeler par un long récit
                les circonstances de la pièce, et la plupart des auteurs anciens ont écrit de tels
                récits. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_44"> Sic in <title ref="#ter_andr"
                    >Andria</title> exponit Pamphili amores, et patris consilium, quo is filium a
                Glycerio diuulsurus erat. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_44" ana="#TH_dram #A"> Ainsi dans
                    l’<title>Andrienne</title> sont racontées les amours de Pamphile et le plan de
                son père par lequel il avait l’intention de séparer son fils de Glycère. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_45"> In reliquis scaenis pergit consilium
                exequi. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_45" ana="#TH_dram"> Dans les autres
                scènes il poursuit son plan. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_46"> Sunt aspersa narrationi illi primae
                multa rhetorica ornamenta. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_46" ana="#RH"> Les nombreuses figures
                rhétoriques sont parsemées dans ce premier récit. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_47"> Est enim probabiliter aucta
                commemoratione morum, studiorum sodalitii Pamphili, qua occasione Pamphilus deperire
                in Glycerium coeperit, ubi pater id primum deprehenderit. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_47" ana="#TH_dram"> En effet, celui-ci
                est enrichi selon la vraisemblance par la mention des habitudes, des amis d’étude de
                Pamphile ; il précise dans quelles circonstances Pamphile est tombé éperdument
                amoureux de Glycère et quand son père a pour la première fois découvert la relation. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_48"> Haec accurate expendet professor, et
                uelim hic immodice etiam anxium esse, <foreign xml:lang="grc"> καὶ
                    πολυπραγμονεῖν</foreign> interpretem. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_48" ana="#PE"> Le professeur appréciera
                soigneusement ces points, et je voudrais également qu’il soit ici extrêmement
                vigilant et qu’il soit très aware quand il traduit . </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_49"> Est enim modis omnibus efficiendum ut
                mirentur, ut ament hunc autorem pueri. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_49" ana="#PE"> En effet il faut par tous
                les moyens faire que les enfants admirent et aiment cet auteur. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_50"> Id ita fiet si rectissime
                intellexerint. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_50" ana="#PE"> Il en sera ainsi s’ils
                comprennent parfaitement. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_51"> Exigendum ab ipsis pueris ut suo
                marte argumenta fabularum condant, ut locos aliquos retexant et pluribus exponant et
                tanquam ceram refingant. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_51" ana="#PE"> Il faut exiger des
                enfants qu’ils rédigent eux-mêmes des arguments des pièces avec leurs propres
                moyens, qu’ils recomposent certains passages, qu’ils en expliquent un assez grand
                nombre et que pour ainsi dire ils revoient la copie. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_52"> Quae exercitatio praeterquam quod
                familiarem hunc poetam facit, facundiam etiam alit. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_52" ana="#PE"> Cet exercice, outre le
                fait qu’il les familiarise avec le poète, nourrit aussi leur éloquence. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_53"> Finguntur hic cordati senes, Dauus
                ueterator, honesta ac pia mente praeditus adolescens Pamphilus. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_53" ana="#TH_dram"> On y voit les sages
                vieillards, le vieil esclave Dave, le jeune homme Pamphile doté d’un esprit honnête
                et loyal. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_54"> Verum ut Pamphili ingenium rectius
                perspici posset, et ut aliquanto plus in fabula turbae esset, confertur cum eo
                Charinus Pamphili dissimillimus : nihil moderatum in hoc, nihil consilii : contra in
                illo moderatiora omnia, paene quam uel aetas poscit, uel sinit amor. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_54" ana="#TH_dram"> Mais pour que le
                caractère de Pamphile puisse mieux être perçu et pour qu’il y ait un peu plus de
                rebondissements dans la pièce, Charinus, total opposé de Pamphile, est mis en
                parallèle avec lui : rien n’est modéré, rien n’est réfléchi chez lui ; chez Pamphile
                au contraire tout est presque plus modéré que son âge l’exige ou que l’amour le
                permet. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p3_55"> In primis mirantur eloquentiam poetae
                in Pamphili querelis, in senis narrationibus, consultationibus et obiurgationibus :
                ab his exemplis et uerba, et figuras Latini sermonis, et dicendi rationem assuescant
                petere adolescentes, felicius haud dubie collocaturi operam, quam si interea uelut
                formicae Indicae aurum conueherent. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p3_55" ana="#PE #RH"> On admire surtout
                l’éloquence du poète dans les lamentations de Pamphile, les récits, les
                délibérations et les réprimandes du vieillard ; à partir de ces exemples, les jeunes
                gens doivent prendre l’habitude de rechercher à la fois des mots et des figures de
                style en latin ainsi que le plan du discours ; sans aucun doute, ils tireront
                meilleur profit de leur activité que si, dans le même temps, ils convoyaient de l’or
                comme les fourmis d’Inde <note>Sur les « fourmis aurifères » (<foreign xml:lang="la"
                        >formicae auriferae</foreign>), voir Erasme, <title>Adagia</title>, 232<ref
                        target="http://ihrim.huma-num.fr/nmh/Erasmus/Proverbia/Adagium_232.html"
                    />.</note>. </ab>


        </body>
    </text>
</TEI>
