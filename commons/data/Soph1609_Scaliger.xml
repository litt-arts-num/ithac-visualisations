<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Periocha in tragoediam Aiacis</title>
                <author ref="#scaliger_iosephus">Iosephus Scaliger</author>

                <respStmt>
                    <resp>Transcription</resp>
                    <persName>Sarah GAUCHER</persName>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <persName>Sarah GAUCHER</persName>
                </respStmt>
                <respStmt>
                    <resp>Révision</resp>
                    <name>Christian NICOLAS</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <!-- Ajouter autant de tâches que nécessaire -->
                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>


            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>
            <sourceDesc>
                <bibl>
                    <title>Sophoclis Aiax Lorarius Stylo Tragio a Iosepho Scaligero Iulii F.
                        translatus, et in theatro argentinensi, Primum Anno 1587. Mense Iulio. Cum
                        nonnullis ornatus causa interpositis, Denuo uero Anno 1608. Mense eodem
                        exhibitus, nouisque insuper accessionibus locupletatus : Sic ut in gratiam
                        lectoris uno contexto sint coniuncta omnia ipsa tamen Scaligeri translatio
                        etiam curiosiis ut uocant, typis a caeteris additamentis distincta.</title>
                    <author ref="#soph">Sophocles</author>
                    <publisher ref="#bertramus_antonius">Antonius Bertramus</publisher>
                    <placeName ref="#argentoratum">Argentorati</placeName>
                    <date when="1609">1609</date>
                    <editor ref="#scaliger_iosephus" role="traducteur">Iosephus Scaliger</editor>
                    <ref target="https://digital.slub-dresden.de/werkansicht/dlf/21857/5"/>
                </bibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p/>
                <!-- s'il s'agit d'une liste, utiliser les éléments ci-dessous -->
                <list>
                    <item/>
                    <item/>
                    <item/>
                </list>
            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

            <textClass n="vers"></textClass>

        </profileDesc>

        <revisionDesc>
            <change when="2022-02-09">Sarah GAUCHER : Vérification et ajouts PersName, PlaceName,
                hi, title, quote, seg, foreign. Vérification de la mise en conformité du schéma </change>
            <change when="2021-11-19">Sarah GAUCHER : encodage transcription et traduction</change>
        </revisionDesc>

    </teiHeader>

    <text>
        <body ana="#A">

            <ab type="orig" xml:id="Soph1609_Scaligerus_0">Periocha in tragoediam Aiacis</ab>
            <ab type="trad" corresp="#Soph1609_Scaligerus_0">Abrégé de la tragédie d'Ajax.</ab>
            <ab type="orig" xml:id="Soph1609_Scaligerus_1"><l>Cum fratre Deiphobo Paris necem
                    Hectoris</l>
                <l>Vlturi Achillem, amoribus Polyxenae</l>
                <l>Captum, trahunt spe matrimonii addita,</l>
                <l>Indutiarum tempore, in fanum Apollinis,</l>
                <l>Ibique ad aras interemtum nequiter</l>
                <l>Linquunt ; cadauer auferens Telamonius</l>
                <l>Aiax, lacessentes repellit fortiter</l>
                <l>Hostes : pugnatur utrimque iusto proelio. </l></ab>
            <ab type="trad" corresp="#Soph1609_Scaligerus_1">Pâris et son frère Déiphobe, voulant
                venger la mort d’Hector, attirent à l’occasion d’une trêve Achille, captif de son
                amour pour Polyxène, dans le temple d’Apollon, après lui avoir fait miroiter une
                union ; là, ils le tuent et abandonnent lâchement son corps au pied des autels ; le
                fils de Télamon, Ajax, alors qu’il emporte le cadavre, repousse avec courage les
                ennemis qui l’attaquent : de part et d’autre on mène une juste bataille.</ab>
            <ab type="orig" xml:id="Soph1609_Scaligerus_2"><l>Mox inter ipsum, interque Vlyssem lis
                    grauis</l>
                <l>Ardescit, arma propter Achillis ; ad manus</l>
                <l>Descenditur ; sed imperante Agamemnone</l>
                <l>Ad iudices remittitur negotium :</l>
                <l>Sententias fert iudicum Laertius. </l></ab>
            <ab type="trad" corresp="#Soph1609_Scaligerus_2">Bientôt entre lui et Ulysse nait un
                grave litige à propos des armes d’Achille ; on en vient aux mains ; mais, sur ordre
                d’Agamemnon, on remet l’affaire à des juges ; le fils de Laërte emporte leurs
                suffrages.</ab>
            <ab type="orig" xml:id="Soph1609_Scaligerus_3"><l>Achillis arma addita non sibi
                    dolens</l>
                <l>Aiax, mentis furore correptus graui</l>
                <l> Malum statiuis moliens, Graios duces</l>
                <l> Impressionem nocte, ut occidat, facit.</l>
            </ab>
            <ab type="trad" corresp="#Soph1609_Scaligerus_3">Souffrant de s’être vu privé des armes
                d’Achille, Ajax, saisi par une puissante fureur, ourdit un malheur contre le séjour
                de l’armée et attaque de nuit les chefs grecs pour les tuer.</ab>
            <ab type="orig" xml:id="Soph1609_Scaligerus_4">
                <l>Hunc captis sensa aliena occellis oggerens</l>
                <l>Pecudumque armenta caede reprimit Dea</l><l> Minerua. Haec sternit ille, necare
                    credulus</l><l> Duces Pelasgos cordis caeci insania,</l><l> In mentis hinc
                    rediens potestatem, sui</l><l> Facinoris foedam deflet turpitudinem</l><l> Mox
                    non assuetae impatiens contumeliae,</l><l> Piatum lymphis ire se
                    lustralibus</l><l> Simulat, cruentus ense furtiuo incubans</l><l> Ingratas lucis
                    inuisae abrumpit moras.</l>
            </ab>
            <ab type="trad" corresp="#Soph1609_Scaligerus_4">Mais en assemblant devant ses yeux des
                sensations étrangères et des têtes de bétail, la déesse Minerve le détourne d’un
                meurtre. Quant à lui, il massacre les bêtes et la folie de son cœur aveuglé lui fait
                croire qu’il tue les chefs grecs ; reprenant ensuite ses esprits, il pleure le
                déshonneur honteux de son crime, et, ne pouvant bientôt plus supporter cet outrage
                extraordinaire, il feint d’aller offrir des sacrifices dans les eaux expiatoires ;
                conservant jalousement une épée emportée en secret, il rompt dans le sang les délais
                ignobles d’une vie odieuse.</ab>
            <ab type="orig" xml:id="Soph1609_Scaligerus_5"><l>Hunc funerare fratres Atridae
                    uetant</l><l> Teucrum ; cientur lites, probra, iurgia</l><l> Miscentur ; tollit
                    istaec Laerte satus.</l>
            </ab>
            <ab type="trad" corresp="#Soph1609_Scaligerus_5">Les frères Atrides défendent que Teucer
                enterre Ajax ; des litiges voient le jour ; s’y mélangent des altercations et des
                disputes ; le fils de Laërte y met un terme.</ab>
            <ab type="orig" xml:id="Soph1609_Scaligerus_6"><l>Tandemque Aiacem frater rite et
                    ordine</l>
                <l>Optato fratrem tumulo condit mortuum.</l>
            </ab>
            <ab type="trad" corresp="#Soph1609_Scaligerus_6">Finalement, Teucer enterre son frère
                Ajax selon le rite et la coutume dans le tombeau souhaité.</ab>


        </body>
    </text>
</TEI>
