<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">De decoro totius operis. Capitulum XXIII. </title>
                <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : "la" pour le latin et "grc" pour le grec -->
                <!-- Attention ! ne pas faire de retour charriot dans le titre -->
                <author ref="#ascensius_iodocus">Jodocus Badius Ascensius</author>

                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Ajouter autant de tâches (<respStmt>) que nécessaire -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Laurine Oulama</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Révision</resp>
                    <name>Laure HERMAND</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Diandra CRISTACHE</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Commentaire</resp>
                    <name>Laure HERMAND</name>
                </respStmt>
                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne GARCIA-FERNANDEZ</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
            <sourceDesc>
                <bibl>
                    <title>P. Terentii aphri comicorum elegantissimi Comedie a Guidone Juvenale viro
                        perquam litterato familiariter explanate : et ab Jodoco Badio Ascensio vna
                        cum explanationibus rursum annotate atque recognite : cumque eiusdem
                        Ascensii praenotamentis atque annotamentis suis locis adhibitis quam
                        accuratissime impresse venundantur</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#ter">Terentius</author>
                    <pubPlace ref="#londinium">Londonii</pubPlace>
                    <pubPlace ref="#lutetia">Lutetiae</pubPlace>
                    <publisher ref="#worda_winandus">Winandus de Worda</publisher>
                    <publisher ref="#morinus_michael">Michael Morinus</publisher>
                    <publisher ref="#brachius_ioannes"> Johannes Brachius</publisher>
                    <publisher ref="#ascensius_iodocus">Jodocus Badius Ascensius</publisher>
                    <date when="1504">1504</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor ref="#ascensius_iodocus">Jodocus Badius Ascensius</editor>
                </bibl>

                <listBibl>
                    <bibl>Philippe Renouard, Bibliographie des impressions et des œuvres de Josse
                        Badius Ascensius, Paris, E. Paul et fils et Guillemin, 1908, 3 vol.</bibl>
                    <bibl>M. Lebel, Les préfaces de Josse Bade (1462-1535) humaniste,
                        éditeur-imprimeur et préfacier, Louvain, Peeters, 1988</bibl>
                    <bibl>Paul White, Jodocus Badius Ascensius. Commentary, Commerce and Print in
                        the Renaissance, Oxford University Press, 2013</bibl>
                    <bibl>L. Katz, La presse et les lettres. Les épîtres paratextuelles et le projet
                        éditorial de l’imprimeur Josse Bade (c. 1462-1535), thèse de doctorat
                        soutenue à l’EPHE sous la direction de Perrine Galand, 2013</bibl>
                </listBibl>

            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Josse Bade ouvre son commentaire aux comédies de Térence par une longue
                    introduction qu’il nomme lui-même Praenotamenta. Il s’agit de « notes
                    préliminaires » plutôt que d’une « préface » à proprement parler. </p>
                <p>Cette section est composée de 26 chapitres qui forment un traité de poétique en
                    miniature auxquels il adjoint une série de remarques préliminaires sur le
                    prologue et la première scène de l’Andrienne. L’humaniste y développe une ample
                    réflexion sur la comédie. Partant d’une définition de la poésie, Bade aborde
                    ensuite l’origine de la comédie, ses caractéristiques et mène une longue
                    réflexion sur la scénographie antique. Les deux derniers chapitres traitent de
                    la vie et des œuvres de Térence.</p>


            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>
        <revisionDesc>
            <change when="2022-04-08">Sarah GAUCHER : correction de la transcription, encodage de la
                tranduction, conformité, identifiants Header</change>
        </revisionDesc>
    </teiHeader>

    <text>
        <body>

            <!-- RAPPEL : on encode le texte latin ou grec; on n'encode dans le texte de la traduction que les notes d'éditeur (=membres du projet) -->

            <!-- Le couple d'éléments <head> contient le titre ou l'incipit du paratexte -->
            <!-- L'identifiant du TITRE est composé du titre de la fiche TT suivi d'un underscore (_) puis du chiffre 0 -->
            <head type="orig" xml:id="Te1504_Ascensius_p3w_0">De decoro totius operis. Capitulum
                XXIII. </head>
            <!-- Le deuxième élément du couple contient la traduction française du titre ou incipit -->
            <!-- L'identifiant de la traduction est le même que celui du latin, précédé d'un hashtag (#) -->
            <head corresp="#Te1504_Ascensius_p3w_0" type="trad">Chapitre 23. Sur le decorum de
                l’œuvre dans son entier.</head>

            <ab type="orig" xml:id="Te1504_Ascensius_p3w_1"> Vt uero totum opus decorum suum habeat,
                non satis est partes singulas bonas esse et artificiosas, sed oportet ut omnes inter
                se bonam habeant congruitatem decenterque cohereant. </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3w_1" ana="#RH"> Pour que l’œuvre dans son
                entier soit convenable, il ne suffit pas que chacune de ses parties soit bonne et
                suive les principes de l’art, mais il faut que toutes aient entre elles une
                cohérence et qu’elles s’ajustent correctement. </ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3w_2"><seg type="allusion">Sicut enim in
                    picturis non esset satis facere bonum caput, aut bonas manus aut etiam omnia
                    membra seorsum accepta bona, nisi unius corporis formam debitam simul capiant,
                    ita non est satis ad bonum poema facere in aliquo genere bona carmina aut in uno
                    stilo bene incipere, nisi in eodem bene perficiatur et nisi tota structura
                        cohaereat<bibl resp="#SG"><author ref="#hor">Hor.</author>, <title
                            ref="#hor_p">P.</title>
                        <biblScope>1-9 et 23</biblScope>.</bibl></seg>.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3w_2" ana="#RH"> En effet, de même que dans
                les tableaux il ne serait pas suffisant de peindre une belle tête ou de belles mains
                ou même de beaux membres acceptables séparément, s’ils ne prenaient pas en même
                temps la forme d’un seul corps, il n’est pas suffisant, pour composer un beau poème,
                de faire de beaux vers dans un genre déterminé ou de le bien débuter dans un seul
                style, si l’on ne le termine pas bien dans ce même style et si la structure dans son
                entier n’a pas de cohérence. </ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3w_3"> Ideoque boni artifices poematum debent
                esse prouidi et memores : prouidi ut prius quam incipiant totam compositionem
                prospiciant, ut ea quae in principio dicantur seruiant eis quae post dicenda sunt ;
                memores autem ne ea quae postea dicent sint contraria aut dissona prius dictis. </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3w_3" ana="#RH"> Et par conséquent les bons
                poètes doivent avoir de la prévoyance et de la mémoire : de la prévoyance pour
                qu’ils anticipent, avant de commencer la composition tout entière, que ce qu’ils
                diront au début servira à ce qui doit être dit ensuite ; de la mémoire, pour que ce
                qu’ils diront ensuite ne soit pas en contradiction ou en dissonance avec ce qu’ils
                ont dit avant. </ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3w_4"> Ordo autem in poematibus est alius quam
                in historiis aut aliis rebus. </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3w_4" ana="#RH"> Quant à l’ordre à suivre
                dans les poèmes, il est différent de celui qu’on suit dans le genre historique et
                dans d’autres écrits. </ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3w_5"> Nam historiographi in rebus narrandis
                seruant ordinem quo gestae sunt. </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3w_5" ana="#RH"> En effet, les historiens,
                en racontant les évènements, conservent l’ordre dans lesquels ils sont arrivés. </ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3w_6"> Poetae autem saepe a medio historiarum
                incipiunt, nec omnia ut historiographi describunt ad longum, sed multa praesupponunt
                aut paucis uerbis tangunt. </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3w_6" ana="#RH"> Quant aux poètes, ils
                commencent souvent au milieu des récits et ils ne les rapportent pas toutes en
                entier, comme les historiens, mais ils en présupposent beaucoup et les traitent en
                peu de mots. </ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3w_7"> Turpe tamen est in ludis quae ut primum
                audiuntur intelligi debent obscuras historias tangere, ut <persName ref="#pl"
                    >Plautus</persName> saepe facit. </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3w_7" ana="#RH #TH_dram"> Cependant, il est
                dommage que, dans les présentations théâtrales, ils réservent un traitement obscur
                aux récits que l’on devrait comprendre dès qu’on les entend, ce que Plaute fait
                souvent. </ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3w_8"> Ideoque <persName ref="#donat"
                    >Donatus</persName>
                <seg type="paraphrase"><persName ref="#ter">Terentium</persName> in ea re maxime
                    laudat et <persName ref="#pl">Plauto</persName> praefert<bibl resp="#SG"><author
                            ref="#euant">Evanthius</author>, <title ref="#euant_fab">De
                            fabula</title>, <biblScope>3.5</biblScope>.</bibl></seg>. </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3w_8" ana="#TH_dram">Aussi Donat
                adresse-t-il sur ce point les plus grandes louanges à Térence et le préfère-t-il à
                Plaute. </ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3w_9"><seg type="paraphrase"><persName
                        ref="#pl">Plautus</persName> enim saepe obscuras tangit historias. <persName
                        ref="#ter">Terentius</persName> totus perspicuus est et si quas historias
                    aut fabulas tangit, communes notaeque omnibus sunt.<bibl resp="#SG"><author
                            ref="#euant">Evanthius</author>, <title ref="#euant_fab">De
                            fabula</title>, <biblScope>3.6</biblScope>.</bibl></seg></ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3w_9" ana="#TH_dram"> En effet, Plaute
                réserve souvent des traitements obscurs à ses récits. Térence est tout entier
                transparent et s’il traite de quelque récits ou histoires, ils sont connus et
                familiers à tous.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3w_10"> Sed haec satis pro nostro instituto. </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3w_10">Mais en voilà assez pour le but que
                nous nous sommes fixé. </ab>

        </body>
    </text>
</TEI>
