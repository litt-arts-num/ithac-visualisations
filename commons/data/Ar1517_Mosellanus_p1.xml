<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Petrus Mosellanus Protegensis Ioanni Caesario Iuliacensi medico
                    utriusque literaturae doctissimo s.d.p.</title>
                <author ref="#mosellanus_petrus">Petrus Mosellanus</author>

                <respStmt>
                    <resp>Transcription</resp>
                    <name>Christian NICOLAS</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Christian Nicolas</name>
                </respStmt>

                <respStmt>
                    <resp>Encodage</resp>
                    <name>Malika BASTIN-HAMMOU : Encodage sémantique</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>


                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <principal>Malika BASTIN-HAMMOU</principal>
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne Garcia-Fernandez</persName>
                </respStmt>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <sourceDesc>


                <bibl>
                    <title>Aristophanis Comici Facetissimi Plutus, graeci Sermonis Studiosis mire
                        utilis</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#ar">Aristophanes</author>
                    <pubPlace ref="#haganoa">Hagenoiae</pubPlace>
                    <publisher ref="#anshelmus_thomas">Thomas Anshelmus</publisher>
                    <date when="1517">Mense Novemb. Anno M.D.XVII</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor ref="#mosellanus_petrus">Petrus Mosellanus</editor>
                    
                    <ref target="url_edition_numérique"/>
                    <!-- pour les liens google book, remplacer les & par &amp; pour que ça fonctionne -->
                </bibl>

            </sourceDesc>

        </fileDesc>

        <profileDesc>

            <abstract>
                <p/>
                <!-- s'il s'agit d'une liste, utiliser les éléments ci-dessous -->
                <list>
                    <item/>
                    <item/>
                    <item/>
                </list>
            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>
        <revisionDesc>
            <change when="2022-09-30">Malika BASTIN-HAMMOU : encodage sémantique</change>
            <change when="2022-03-03">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
        </revisionDesc>
    </teiHeader>

    <text>
        <!-- Cette portion concerne le texte lui-même -->
        <body>
            <head type="orig" xml:id="Ar1517_Mosellanus_p1_0"><persName
                    ref="#mosellanus_petrus">Petrus Mosellanus Protegensis</persName>
                <persName role="destinataire" ref="#caesarius_ioannes">Ioanni Caesario
                    Iuliacensi</persName> medico utriusque literaturae doctissimo s.d.p.</head>
            <head type="trad" corresp="#Ar1517_Mosellanus_p1_0">Petrus Mosellanus salue grandement
                Johannes Caesarius de Jülich, médecin très savant dans les deux langues</head>
            <!-- quand le paratexte comporte une formule d'adresse par exemple -->

            <!-- le texte est divisé en bloc (<ab>) dont le type est soit "orig" pour original ou "trad" pour traduction.
         Le texte original a un identifiant (xml:id) sous la forme : identifiant du paratexte, numéro du paragraphe (exemple : Ar1670Ra_p1_2).
         La traduction renvoie au même identifiant (corresp) précédé d'un # -->

            <ab type="orig" xml:id="Ar1517_Mosellanus_p1_1">Multa sunt <persName
                    ref="#caesarius_ioannes">Caesari</persName> optime, quae rem literariam hoc
                nostro seculo iuuant.</ab>
            <ab type="trad" corresp="#Ar1517_Mosellanus_p1_1">Beaucoup d’éléments, excellent
                Caesarius, favorisent à notre époque la matière littéraire.</ab>

            <ab type="orig" xml:id="Ar1517_Mosellanus_p1_2">Sed mea sententia utriusque literaturae
                studia nihil aeque promouet, ac diuinum illud, et priscorum temporibus <seg
                    xml:lang="grc" type="reecriture">ἐν ἀγορᾷ λαλοῦσι<bibl resp="#CN"><author
                            ref="#eur">Eur.</author>
                        <title ref="#eur_ion">Ion</title>
                        <biblScope>565</biblScope>.</bibl></seg> speratum artificium, quo ueterum et
                earundem locupletissimarum bibliothecarum reliquiae, ex tanto uelut librorum
                naufragio superstites, in mille transfunduntur exemplaria. Id quod utinam tanta
                exerceretur, deligendi quae optima sunt, cura, ac emendandi, quae deprauata sunt,
                fide, quanta tractatur industria, frequentiaque.</ab>
            <ab type="trad" corresp="#Ar1517_Mosellanus_p1_2" ana="#PH #LI">Mais à mon sens, rien ne
                promeut autant les études grecques et latines que ce travail digne des dieux et que
                les temps anciens ne pouvaient pas même espérer en rêve qui voit les vestiges des
                vieilles bibliothèques et aussi les plus riches, sauvés de l’immense naufrage des
                livres, transvasés dans des milliers d’ouvrages, raison pour laquelle j’aimerais que
                s’exerçât autant de soin à choisir les meilleurs ouvrages et de conscience dans
                l’émendation de ceux qui sont mauvais qu’on y met de travail et de temps passé.</ab>

            <ab type="orig" xml:id="Ar1517_Mosellanus_p1_3">Non esset, quod tam saepe codices
                infoeliciter emisse poeniteret.</ab>
            <ab type="trad" corresp="#Ar1517_Mosellanus_p1_3" ana="#PH #LI">Ce ne serait pas qu’il y
                eût si souvent à regretter l’achat de volumes.</ab>

            <ab type="orig" xml:id="Ar1517_Mosellanus_p1_4">Nunc autem huic arti manus admoliuntur,
                non qui utilitatis publicae respectu formularum decies castigandarum taedia
                deuorant. Sed qui prae unius aureoli lucello omneis omnium literas
                floccipendunt.</ab>
            <ab type="trad" corresp="#Ar1517_Mosellanus_p1_4" ana="#PH #LI">Mais aujourd’hui mettent
                la main à cette besogne des gens qui, au lieu de viser le bien public en oubliant
                l’ennui de corriger dix fois des formules , bradent pour le bénéfice d’un sou toute
                la littérature universelle,</ab>

            <ab type="orig" xml:id="Ar1517_Mosellanus_p1_5">Adeo ut de bonis literis, quae sub
                    <persName>Laurentii Vallae</persName>, <persName>Theodori Gazae</persName>,
                    <persName>Hermolai Barbari</persName>, et <persName>Angeli Politiani</persName>
                manibus caput erigere coeperant, pene fuisset actum, nisi <persName
                    ref="#manutius_aldus">Aldus Manutius</persName> homo bonis studiis euehendis
                natus utriusque linguae scriptoribus optimis imprimendis huic malo remedium
                adhibuisset.</ab>
            <ab type="trad" corresp="#Ar1517_Mosellanus_p1_5" ana="#LI #PH">en sorte que c’en serait
                fait des belles lettres, qui entre les mains de Lorenzo Valla, Théodore Gaza,
                Hermolaus Barbarus et Ange Politien avaient commencé à dresser la tête, si Alde
                Manuce, homme né pour élever les études et imprimer les meilleurs auteurs des deux
                langues, n’avait prodigué un remède à ce mal.</ab>

            <ab type="orig" xml:id="Ar1517_Mosellanus_p1_6">Quanquam enim et in hoc nonnihil
                desydero (quis autem omnia praestitit unquam ?) tamen si non ingratissimi audire
                uolumus, ei deberi fatebimur, quod et latinae et graecae literaturae bibliothecas,
                si non instructissimas certe multo magis quam ante tolerabiles possidemus.</ab>
            <ab type="trad" corresp="#Ar1517_Mosellanus_p1_6" ana="#PH #LI">Car si même à son égard
                j’ai un peu de déception (mais qui a pu jamais tout réussir ?), force est toutefois
                de reconnaître, sauf à passer pour bien ingrats, que c’est à lui que nous devons
                d’avoir des bibliothèques grecques et latines peut-être pas très achalandées mais en
                tout cas bien plus acceptables qu’avant.</ab>

            <ab type="orig" xml:id="Ar1517_Mosellanus_p1_7">Olim regum ac principum erat munus
                librorum copiam in comunem studiosorum usum ingentibus parare impendiis, Quae res
                multos qui studio hoc tam honesto praecellerunt, posteris celebres fecit.</ab>
            <ab type="trad" corresp="#Ar1517_Mosellanus_p1_7" ana="#LI">Jadis c’est aux rois et
                princes qu’incombait la tâche de financer à grands frais la mise à disposition pour
                la communauté des savants d’innombrables livres, chose qui a assuré à beaucoup qui
                excellèrent dans ce travail la gloire auprès de la postérité.</ab>

            <ab type="orig" xml:id="Ar1517_Mosellanus_p1_8">Nostra uero memoria cum plaerique
                principes hanc gloriam, immo comunem omnium utilitatem negligant, <persName
                    ref="#manutius_aldus">Aldi</persName> tamen beneficio factum est ut nos ipsi
                quantumuis tenui fortuna homines mediocri codicum supellectili coemendae simus
                pares.</ab>
            <ab type="trad" corresp="#Ar1517_Mosellanus_p1_8" ana="#LI">Pour ce qui est de notre
                temps, vu que la plupart des princes négligent cette gloire - et même le bien de
                tous leurs sujets -, grâce toutefois à Alde, il nous a été possible à nous aussi,
                quelque peu fortunés que nous soyons, en acquérant un petit bagage de volumes, de
                nous sentir leurs égaux.</ab>

            <ab type="orig" xml:id="Ar1517_Mosellanus_p1_9">Quod si qua desunt, id quod non negamus,
                nostri Germani, quibus uel inuita Italia artis inuentae debetur gloria,
                supplebunt.</ab>
            <ab type="trad" corresp="#Ar1517_Mosellanus_p1_9" ana="#LI">Et s’il en manque certains,
                ce que nous ne nions pas, nos Allemands, auxquels, n’en déplaise à l’Italie, est due
                la gloire d’avoir inventé l’imprimerie, y pourvoiront.</ab>

            <ab type="orig" xml:id="Ar1517_Mosellanus_p1_10">Et in primis apud inclytam Basileam hoc
                iam strenue molitur <persName ref="#frobenius_ioannes">Ioannes Frobenius</persName>,
                qui quantum de rectis literis, tum ante sit meritus, tum posthac mereri, et possit
                et uelit, indicio sunt elaboratissimae illae diui <persName ref="#hier"
                    >Hieronymi</persName> lucubrationes, <persName ref="#erasmus_desiderius">Erasmi
                    Roterodami</persName> doctissimi Theologi opera potissimum restituae, et illius
                formulis excusae, Porro <persName ref="#anshelmus_thomas">Thomas
                    Anshelmus</persName> miro uir ingenio quantum momenti studiis adferre queat, cum
                ante et Graecis et Latinis adde etiam Hebraicis auctoribus impressis ostendit, tum
                de caetero hac <persName ref="#ar">Aristophanis</persName> fecetissimi hominis
                Comoedia, quam ei excudendam dedimus, apud omneis testatum relinquet.</ab>
            <ab type="trad" corresp="#Ar1517_Mosellanus_p1_10" ana="#LI">Et d’abord, dans la
                glorieuse Bâle, s’y emploie sans relâche Johann Froben dont les mérites dans les
                belles lettres qu’il a obtenus dans le passé et peut et souhaite obtenir depuis,
                sont prouvés par ces deux remarquables études de saint Jérôme dont la restitution
                est due au travail d’Erasme de Rotterdam l’éminent théologien et l’impression aux
                caractères de Froben, puis Thomas Anshelm, homme de grand talent : l’énergie qu’il
                peut déployer aux études, il l’a montrée d’abord en imprimant des auteurs grecs et
                latins (et ajoutes-y même des juifs), ensuite et surtout par cette comédie
                d’Aristophane, le plus drôle des hommes, dont nous lui avons confié la mise en
                pages, il en laissera la preuve aux yeux de tous.</ab>

            <ab type="orig" xml:id="Ar1517_Mosellanus_p1_11">Nam quia Illustrissimus princeps meus
                    <persName>Georgius Saxonum dux</persName> magnificentissimus studia nostra in
                hoc sua fouet munificentia, ut Lipsica haec Academia iam ante latinis artibus
                quantum fieri potuit, florentissima, nunc nostra opera graecis quoque studiis
                illustretur, operae precium uisum Comoedias aliquot in hoc selectas studiosae
                iuuentuti publice enarrare.</ab>
            <ab type="trad" corresp="#Ar1517_Mosellanus_p1_11" ana="#PA #PE">Car puisque mon très
                illustre Prince, le très magnifique duc Georges de Saxe, finance dans sa générosité
                nos travaux, au point que l’académie de Leipzig à laquelle j’appartiens, déjà
                brillante autant que possible dans les lettres latines, s’illustre aussi aujourd’hui
                grâce à nous dans le grec, il a paru utile, à cet effet, de commenter pour la
                studieuse jeunesse une sélection de quelques comédies.</ab>

            <ab type="orig" xml:id="Ar1517_Mosellanus_p1_12">Neque uero nos in id consilii temere
                incidisse putaris.</ab>
            <ab type="trad" corresp="#Ar1517_Mosellanus_p1_12">Mais ne va pas croire que nous avons
                entrepris ce projet au hasard.</ab>

            <ab type="orig" xml:id="Ar1517_Mosellanus_p1_13">Immo ad ueterem illam et incorruptam
                graecanici sermonis puritatem percipiendam <seg type="paraphrase">nihil magis
                    accurata <persName ref="#men">Menandri</persName> fabularum lectione<bibl
                        resp="#SG"><author ref="#quint">Quint.</author>, <title ref="#quint_io"
                            >I.O.</title>
                        <biblScope>1.8.8</biblScope>.</bibl></seg> conferre sensit grauissimi
                iudicii homo <persName ref="#quint">Fab. Quintilianus</persName>.</ab>
            <ab type="trad" corresp="#Ar1517_Mosellanus_p1_13" ana="#PE #TH_fin ">Bien au contraire
                : pour apprécier cette antique et authentique pureté de la langue grecque, rien ne
                vaut une lecture scrupuleuse des pièces de Ménandre selon l’homme au goût si sévère
                qu’est Quintilien.</ab>

            <ab type="orig" xml:id="Ar1517_Mosellanus_p1_14">Eae quando partim bellorum iniuria,
                partim maiorum nostrorum negligentia periereunt, proximum est <persName ref="#ar"
                    >Aristophanicas</persName> amplecti Quas sanctissimus ille et inter graecos pene
                facundissimus theologus <persName ref="#chrys">Chrysostomus</persName> sic semper
                manibus uersauit, sic accurate lectitauit, sic in precio habuit, ut, quemadmodum
                ferunt, <seg type="allusion">pro puluino dormiens uteretur<bibl resp="#SG"><author
                            ref="#manutius_aldus">Alde Manuce</author>, <date>1498</date>,
                            <biblScope>1.17</biblScope><ref target="Ar1498_Manutius_p1_17"
                        />.</bibl></seg>, unde et aureae eloquentiae cognomentum promeruit.</ab>
            <ab type="trad" corresp="#Ar1517_Mosellanus_p1_14">Vu que ces pièces, soit par la faute
                des guerres soit par la négligence de nos aïeux, ont disparu, le plus proche est de
                s’emparer de celles d’Aristophane, que ce très saint homme entre les Grecs et
                presque le plus éloquent théologien Jean Chrysostome a eues en mains sans arrêt, a
                lues et relues scrupuleusement, a tenues en si haute estime que, comme on le
                raconte, il s’en servait d’oreiller quand il dormait, ce qui lui a valu son surnom
                d’éloquence dorée.</ab>

            <ab type="orig" xml:id="Ar1517_Mosellanus_p1_15">Iam ex illis nouem quae <persName
                    ref="#ar">Aristophanis</persName> titulo adhuc supersunt, si qua caeteras, uel
                argumenti commoditate, uel morum correctione, uel festiuis, sed tamen castis salibus
                antecellit, ea meo quidem iudicio erit <title ref="#ar_pl" xml:lang="grc"
                    >πλοῦτος</title>.</ab>
            <ab type="trad" corresp="#Ar1517_Mosellanus_p1_15" ana="#TH_dram">Maintenant sur les
                neuf pièces attribuées à Aristophane qui ont survécu, s’il en est une qui dépasse
                les autres par la convenance de l’argument, la correction des mœurs, la drôlerie
                tout en restant dans les limites de la plaisanterie chaste, ce sera à mon avis celle
                du <title>Plutus</title>.</ab>

            <ab type="orig" xml:id="Ar1517_Mosellanus_p1_16">Hanc ergo in usum cum aliorum
                studiosorum, qui hinc proficient, tum uero maxime nostrorum auditorum a nobis
                selectam et <persName ref="#anshelmus_thomas">Anshelmi</persName> nostri typis
                excusam tui nominis auspicio foelicissimo prodire merito uoluimus preceptor et
                parens optime. </ab>
            <ab type="trad" corresp="#Ar1517_Mosellanus_p1_16" ana="#PE #PA">C’est donc elle que, à
                l’usage des autres savants, qui en tireront profit, mais aussi et surtout de nos
                étudiants, que j’ai choisie et confiée à la casse de notre ami Thomas Anshelm avec
                le désir de l’éditer à dessein sous l’auspice très favorable de ton nom, maître et
                père excellent.</ab>

            <ab type="orig" xml:id="Ar1517_Mosellanus_p1_17">Nam quum quidquid huius ingenioli
                agellus, quem tuis perceptionibus diligenter excoluisti, produxerit, id totum tibi
                deberetur, et huuis messis tempore ipsi nihil Caesario dignum in horreum
                colligissemus uorsuram interim facere statuimus potius quam in neruum ire, hoc est
                ab <persName ref="#ar">Aristophane</persName> mutuum sumere, quod tibi interim
                dependatur.</ab>
            <ab type="trad" corresp="#Ar1517_Mosellanus_p1_17" ana="#PA">Car puisque tout ce que mon
                petit domaine d’intelligence, que tu as diligemment cultivé de tes leçons, a pu
                produire t’est dû entièrement et que, au moment de cette moisson, nous n’avons rien
                entreposé au grenier qui fût digne de Caesarius lui-même, nous avons décidé de
                transférer notre dette plutôt que d’aller en prison, c’est-à-dire de faire un
                emprunt à Aristophane qui puisse te payer entre temps.</ab>

            <ab type="orig" xml:id="Ar1517_Mosellanus_p1_18">Suscipe ergo uir clarissime serena
                fronte quidquid id est libelli, et nostrum erga te amorem hoc munusculo, pro tempore
                utcumque ostensum, tantisper boni consule, dum per ocium et ingenii facultates
                maiora ad tui nominis gloriam parare liceat.</ab>
            <ab type="trad" corresp="#Ar1517_Mosellanus_p1_18" ana="#PA">Examine donc, homme très
                célèbre, d’un œil indulgent tout cet opuscule et agrée notre affection à ton
                endroit, marquée de toute manière par ce petit cadeau de circonstance, en attendant
                que le loisir et les facultés intellectuelles me permettent de t’en offrir de plus
                grands pour glorifier ton nom.</ab>

            <ab type="orig" xml:id="Ar1517_Mosellanus_p1_19">Vale foelix Lipsiae Octauo Kalend.
                Septemb. Anno restitutae salutis M.D.XVII.</ab>
            <ab type="trad" corresp="#Ar1517_Mosellanus_p1_19">Porte-toi bien dans la félicité.
                Leipzig, 25 août de l’an de grâce 1517.</ab>

        </body>
    </text>
</TEI>
<!-- Pour plus de précisions sur les règles de transcription, consulter le tableau "guide_encodage_ithac" -->
