<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <!--Ci-dessous le titre du paratexte-->
                <title xml:lang="la"> Trachiniae Sophoclis. Argumentum. </title>
                <author ref="#winshemius_uitus">Vitus Winshemius</author>

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Tâche n°1 -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Marius Massucco</name>
                </respStmt>
                <!-- Tâche n°2 -->
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <!-- Tâche n°3 -->
                <respStmt>
                    <resp>Révision</resp>
                    <name>Christian NICOLAS</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Marius Massucco</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <!-- Ajouter autant de tâches que nécessaire -->
                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
            <sourceDesc>

                <bibl>
                    <!-- l'attribut @level permet d'indiquer s'il s'agit d'une monographie (m), d'un article ou chapitre d'ouvrage (a) ou d'un périodique (j) -->
                    <title>Interpretatio tragoediarum sophoclis, ad utilitatem iuuentutis, quae
                        studiosa est graecae linguae, edita a Vito Winshemio</title>
                    <author ref="#soph">Sophocles</author>
                    <pubPlace ref="#francofortum">Francoforti ad Moenum</pubPlace>
                    <publisher ref="#brubachius_petrus">Petrus Brubachius</publisher>
                    <date when="1546">1546</date>
                    <editor ref="#winshemius_uitus">Vitus Winshemius</editor>
                    <editor ref="#winshemius_uitus" role="traducteur">Vitus Winshemius</editor>
                    <ref target="https://www.digitale-sammlungen.de/de/view/bsb10993807?page=5"
                        >https://www.digitale-sammlungen.de/de/view/bsb10993807?page=5</ref>
                </bibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p> Vitus Vuinshemius (Veit Winsheim). 1501 (Windsheim)-1570(Wittenberg), est un
                    philologue, helléniste et médecin allemand, diplômé d’une maîtrise en
                    philosophie à la faculté de Wittenberg en 1529. Il est proche de Mélanchthon,
                    auquel il succède dans sa chaire de langue grecque en 1541. Docteur en médecine
                    en 1550, puis successivement doyen de la faculté de philologie, recteur de
                    l’université de Wittenberg, et vice-recteur de l’Académie. Il est aussi
                    conseiller de la ville de Wittenberg. Il a laissé de nombreuses études sur les
                    textes antiques, notamment sur Démosthène, Thucydide, Théocrite, Homère,
                    Sophocle et Euripide, ainsi que des discours et éloges funèbres (notamment celui
                    de Mélanchthon). Son fils s’appelle aussi Veit Winsheim, dit “le jeune”(der
                    Jüngere), professeur de droit. [source : wikipédia :
                    https://de.wikipedia.org/wiki/Veit_Winsheim]</p>
            </abstract>

            <langUsage>
                <!-- Garder seulement les éléments indiquant les langues utilisées dans le paratexte -->
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>

        <revisionDesc>
            <!-- Ajouter ici autant d'éléments <change> que de modifications effectuées -->
            <change when="2002-02-09">Sarah GAUCHER : Vérification et ajouts PersName, PlaceName,
                hi, title, quote, seg, foreign. Vérification de la mise en conformité du
                schéma</change>
            <change when="2021-11-15">Sarah GAUCHER : encodage de la transcription corrigée et de la
                traduction</change>
            <change when="2021-05-08">Diandra CRISTACHE : Encodage du texte latin et chargement sur
                le Pensoir</change>

        </revisionDesc>

    </teiHeader>

    <text>
        <body>
            <head type="orig" xml:id="Soph1546_Winshemius_p6_0"><title ref="#soph_tr"
                    >Trachiniae</title>
                <persName ref="#soph">Sophoclis</persName>. Argumentum.</head>
            <head type="trad" corresp="#Soph1546_Winshemius_p6_0">Les <title>Trachiniennes</title>
                de Sophocle. Argument.</head>

            <ab type="orig" xml:id="Soph1546_Winshemius_p6_1">Haec fabula uberrime descripta est ab
                    <persName ref="#ou">Ouidio</persName> libro 9 <title ref="#ou_m"
                    >Metamorphoseon</title>.</ab>
            <ab type="trad" corresp="#Soph1546_Winshemius_p6_1" ana="#A">Cette histoire a été abondamment
                décrite par le 9e livre des <title>Métamorphoses</title> d’Ovide.</ab>
            <ab type="orig" xml:id="Soph1546_Winshemius_p6_2">Continet autem postremum actum rerum
                Herculis, quem inter Heroas primo loco collocauit antiquitas et attribuit ei summam
                rerum gestarum gloriam.</ab>
            <ab type="trad" corresp="#Soph1546_Winshemius_p6_2" ana="#A">Elle contient le dernier acte de la
                geste d’Hercule, que l’Antiquité a placé au premier rang des Héros et qu’elle a
                porté au pinacle pour ses exploits.</ab>
            <ab type="orig" xml:id="Soph1546_Winshemius_p6_3">Operae pretium uero est uidere, qualem
                catastrophen et exitum uitae sortitus sit uir tantus.</ab>
            <ab type="trad" corresp="#Soph1546_Winshemius_p6_3" ana="#PE">Il vaut la peine de voir quel
                dénouement et quelle fin un si grand héros a obtenu du sort.</ab>
            <ab type="orig" xml:id="Soph1546_Winshemius_p6_4">Qui cum uictoriarum fama et nominis
                sui gloria impleuisset totum orbem, potitus tandem ea uictoria, quae illi finem
                laborum allatura uidebatur, cum iam se in posterum quietam uitam acturum speraret,
                concidit domestico uulnere, interfectus errore a coniuge sua.</ab>
            <ab type="trad" corresp="#Soph1546_Winshemius_p6_4" ana="#A">Car lui, alors qu’il avait rempli la
                terre tout entière de la renommée de ses victoires et de la gloire de son nom, après
                s’être finalement emparé de la victoire qui semblait devoir mettre fin à ses
                travaux, alors qu’il espérait mener ensuite une existence paisible, il succomba
                d’une blessure domestique, puisque son épouse le tua par erreur.</ab>
            <ab type="orig" xml:id="Soph1546_Winshemius_p6_5">Praebuit uero huic tam tristi
                calamitati causam uaga libido ipsius.</ab>
            <ab type="trad" corresp="#Soph1546_Winshemius_p6_5" ana="#A">C’est son désir déréglé qui lui
                causa ce si triste malheur.</ab>
            <ab type="orig" xml:id="Soph1546_Winshemius_p6_6">Nam cum uxori pellicem ob oculos
                adduceret, illa dolore muliebri incensa, marito tunicam ueneno, quod ipsa philtrum
                esse credebat, infectam mittit.</ab>
            <ab type="trad" corresp="#Soph1546_Winshemius_p6_6" ana="#A">En effet, alors qu’il ramenait une
                rivale sous les yeux de son épouse, cette dernière, enflammée d’une douleur
                féminine, envoie à son mari une tunique infectée par un venin qu’elle croyait être
                un philtre.</ab>
            <ab type="orig" xml:id="Soph1546_Winshemius_p6_7">Quo ueneno cum inflammatus
                horribiliter cruciaretur tandem impatientia, et magnitudine doloris uictus, rogum
                sibi in monte Oeta instrui iubet seque in eo collocat et a Poeante, patre
                Philoctetae, ut incendatur, impetrat : ita obrumpit cum uita dolorem.</ab>
            <ab type="trad" corresp="#Soph1546_Winshemius_p6_7" ana="#A">Et, alors que ce venin qui l’a
                embrasé lui cause une horrible torture, finalement terrassé par une souffrance qu’il
                ne peut supporter et une douleur immense, il ordonne qu’on lui dresse un bûcher sur
                le mont Oeta, il s’y place et obtient que Poeas, le père de Philoctète, y mette le
                feu : il met ainsi fin à sa douleur en même temps qu’à sa vie.</ab>
            <ab type="orig" xml:id="Soph1546_Winshemius_p6_8">Sed hoc tamen memorandum est quod
                inter Ethnicos, homines ingeniosi, cum uiderent uiros magnos ac praestantes in hac
                uita multis duris certaminibus obiici et tandem multis calamitatibus exercitos
                multisque laboribus perfunctos, saepe etiam tragicum, tristemque uitae exitum
                sortiri, his exemplis moti, de prouidentia diuina, deque immortalitate cogitare
                coeperunt.</ab>
            <ab type="trad" corresp="#Soph1546_Winshemius_p6_8" ana="#RE">Mais il faut cependant se souvenir
                que, parmi les païens, les hommes intelligents, lorsqu’ils voient que des hommes
                grands et remarquables sont poussés ici-bas dans des combats nombreux et difficiles
                et qu’à la fin ceux qui ont été tourmentés par de nombreux malheurs et se sont
                acquittés de nombreux travaux obtiennent souvent du sort une fin tragique et triste,
                entreprennent, émus par ces exemples, de songer à la providence divine et à
                l’immortalité.</ab>
            <ab type="orig" xml:id="Soph1546_Winshemius_p6_9">Hinc est, quod Herculem uiuum adhuc et
                semiustulatum a rogo ad caelum transferunt ibique aeternam sedem, et debitum pro
                uirtute honorem illi tribuunt, significantes pietatem ac uirtutem, etsi in hac uita
                adflicta sit, tamen post hanc uitam aeterna praemia manere.</ab>
            <ab type="trad" corresp="#Soph1546_Winshemius_p6_9" ana="#RE #A">De là vient qu’ils font passer
                Hercule encore vivant et à demi-brûlé du bûcher au ciel et qu'ils lui attribuent
                là-bas un séjour éternel et un honneur que lui a fait mériter son courage, montrant
                ainsi que la piété et le courage, même s’ils ont été mis à mal ici-bas, attendent
                cependant dans l’au-delà d’éternelles récompenses.</ab>
            <ab type="orig" xml:id="Soph1546_Winshemius_p6_10">Haesit enim haec sententia Legis
                diuinitus sculpta in mentibus hominum saniorum ac recte institutorum omnibus
                saeculis inde usque a sanctis Patribus quod Deus malos puniat, bonis uero
                benefaciat.</ab>
            <ab type="trad" corresp="#Soph1546_Winshemius_p6_10" ana="#RE">En effet, cette phrase de la Loi
                gravée par Dieu est ancrée dans l’esprit des hommes sensés et de bonnes mœurs à
                toutes les époques depuis les Saints Pères : Dieu punit les méchants mais il
                favorise les hommes de bien.</ab>
            <ab type="orig" xml:id="Soph1546_Winshemius_p6_11">Cum igitur saepe contrarium in hac
                uita fieri uiderent, coacti sunt statuere aliam uitam restare, in qua iudicium
                fieret, discrimen ostenderetur, bonique praemia, et mali poenam sortirentur.</ab>
            <ab type="trad" corresp="#Soph1546_Winshemius_p6_11" ana="#RE">En conséquence, alors qu’ils
                connaissent souvent l’adversité ici-bas, ils sont forcés de penser qu’il reste une
                autre vie où le jugement advient ; où la distinction est faite et où les hommes de
                bien obtiennent des récompenses et les mauvais un châtiment.</ab>
            <ab type="orig" xml:id="Soph1546_Winshemius_p6_12">Est uero in hoc exemplo praecipue
                obseruandum, quod tam miserabiliter, et tam leui momento euertitur uir tantus : cui
                libido et causa est et occasio poenae.</ab>
            <ab type="trad" corresp="#Soph1546_Winshemius_p6_12" ana="#PE">Ce qu’il faut surtout observer dans
                cet exemple c’est qu’un si grand homme soit mis à bas de façon si misérable et en un
                si court instant : or, c’est son désir qui est à la fois la cause et l’occasion de
                sa punition.</ab>
            <ab type="orig" xml:id="Soph1546_Winshemius_p6_13">Corruit, qui foris inuictus erat,
                uulnere domestico : quae uulnera et acerbiora sunt et minus caueri possunt.</ab>
            <ab type="trad" corresp="#Soph1546_Winshemius_p6_13" ana="#PE #A">Lui qui était invaincu hors de chez
                lui, il s’écroule d’une blessure domestique ; or, ces blessures à la fois sont plus
                difficiles à endurer et moins faciles à soigner.</ab>
            <ab type="orig" xml:id="Soph1546_Winshemius_p6_14">Libido uero illi, quae et multis
                aliis, exitio fuit.</ab>
            <ab type="trad" corresp="#Soph1546_Winshemius_p6_14" ana="#PE #A">Le désir lui fut funeste, à lui et
                à bien d’autres également.</ab>
            <ab type="orig" xml:id="Soph1546_Winshemius_p6_15">Haec satis sit dixisse de argumento
                huius tragoediae.</ab>
            <ab type="trad" corresp="#Soph1546_Winshemius_p6_15" ana="#A">C’est assez parlé sur l’argument de
                cette tragédie.</ab>
            <ab type="orig" xml:id="Soph1546_Winshemius_p6_16">Omitto enim uaria et prodigiosa
                figmenta, quae de Hercule, magna libertate sparsit antiquitas.</ab>
            <ab type="trad" corresp="#Soph1546_Winshemius_p6_16">Je passe en effet sous silence les
                diverses inventions prodigieuses que l’Antiquité a répandues avec une grande liberté
                à propos d’Hercule.</ab>
            <ab type="orig" xml:id="Soph1546_Winshemius_p6_17">Conferunt enim in hunc Heroem paene
                quidquid est fabulosarum imaginum.</ab>
            <ab type="trad" corresp="#Soph1546_Winshemius_p6_17">On rapporte en effet à propos de ce
                héros presque tout ce qu’il y a de représentations mythiques.</ab>
            <ab type="orig" xml:id="Soph1546_Winshemius_p6_18">Debet uero haec fabula nobis esse
                carior, cum propter orationis splendorem, et magnificentiam, ac multa praeclara
                lumina, tum uero, quod <persName ref="#cic">Cicero</persName> 2 libro <title
                    ref="#cic_tusc">Tusculanae quaestionum</title>
                <seg type="allusion">quaedam ex ea in Latinam linguam conuertit<bibl resp="#SG"
                            ><author ref="#cic">Cic.</author>, <title ref="#cic_tusc">Tusc.</title>
                        <biblScope>2.20-22</biblScope>.</bibl></seg>.</ab>
            <ab type="trad" corresp="#Soph1546_Winshemius_p6_18" ana="#TH_hist #LA #T">Cette pièce doit nous être plus
                précieuse d’une part en raison de la splendeur de son discours, de sa magnificence
                et de ses nombreuses lumières éclatantes et d’autre part parce que Cicéron en
                traduit une partie en langue latine dans le livre 2 des
                <title>Tusculanes</title>.</ab>
            <ab type="orig" xml:id="Soph1546_Winshemius_p6_19">Quae nobis illustre exemplum esse
                possunt, ex quo discamus quomodo ex Graecis Latina sint facienda et quam in uertendo
                formam probarint ueteres.</ab>
            <ab type="trad" corresp="#Soph1546_Winshemius_p6_19">Et nous pouvons regarder ces vers
                comme un exemple illustre pour apprendre comment l’on doit forger du latin à partir
                du grec et quelles formes approuvent les Anciens en matière de traduction.</ab>

            <ab type="orig" xml:id="Soph1546_Winshemius_p6_20">Est uero, ipso <persName ref="#cic"
                    >Cicerone</persName> teste, non aliud utilius aut fructuosius exercitii genus,
                quam splendidiora loca ex Graecis auctoribus in Latinum sermonem transferre.</ab>
            <ab type="trad" corresp="#Soph1546_Winshemius_p6_20" ana="#T #TH_hist">Du témoignage de Cicéron lui-même,
                aucun autre genre d’exercice n’est plus utile ou plus fructueux que de faire
                traduire les passages assez brillants des auteurs grecs en langue latine.</ab>
            <ab type="orig" xml:id="Soph1546_Winshemius_p6_21">Ad quam rem, quid conferat <persName
                    ref="#soph">Sophocles</persName>, experientur hi, qui in eo euoluendo operam
                ponent et illustrioribus locis uertendis sese exercebunt.</ab>
            <ab type="trad" corresp="#Soph1546_Winshemius_p6_21" ana="#PE #T #LA">Et ce que peut apporter Sophocle en
                la matière, en feront l’expérience ceux qui mettront leur soin à l’étudier et qui
                s’exerceront à en traduire les passages particulièrement brillants.</ab>
        </body>
    </text>
</TEI>
