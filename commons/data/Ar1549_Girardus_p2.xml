<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>
            <titleStmt>

                <!-- Titre et auteur du paratexte (en latin) -->
                <title xml:lang="la">LECTORI</title>
                <author ref="#girardus_carolus">Carolus Girardus</author>

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Tâche n°1 -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Malika BASTIN-HAMMOU</name>
                </respStmt>
                <!-- Tâche n°2 -->
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Christian NICOLAS</name>
                </respStmt>
                <!-- Tâche n°3 -->
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Heidi-Morgane Roure</name>
                    <name>Sarah GAUCHER</name>
                    <name>Malika BASTIN-HAMMOU</name>
                </respStmt>

                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>

            </titleStmt>

            <publicationStmt>
                <!-- Informations de publication sur le laboratoire numérique du projet-->
                <!-- Ne pas modifier les données ci-dessous -->
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <sourceDesc>
                <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
                <bibl>
                    <!-- l'attribut @level permet d'indiquer s'il s'agit d'une monographie (m), d'un article ou chapitre d'ouvrage (a) ou d'un périodique (j) -->
                    <title><foreign xml:lang="grc">Ἀριστοφάνους κωμωδοροιῶν ἀρίστου
                            Πλοῦτος.</foreign> Aristophanis Poëte Comici Plutus, iam nunc per
                        Carolum Girardum Bituricum et Latinus factus, et Commentariis insuper sane
                        quam utiliss. recens illustratus. Editio prima.</title>
                    <author ref="#ar">Aristophanes</author>
                    <pubPlace ref="#lutetia">Parisiis</pubPlace>
                    <publisher ref="#dupuys_mathurnius">Mathurinus Dupuys</publisher>
                    <date when="1549">1549</date>
                    <editor ref="#girardus_carolus">Carolus Girardus</editor>
                    <editor role="traducteur" ref="#girardus_carolus">Carolus Girardus</editor>
                </bibl>
            </sourceDesc>

        </fileDesc>

        <profileDesc>

            <abstract>
                <p/>
            </abstract>

            <langUsage>
                <!-- Garder seulement les éléments indiquant les langues utilisées dans le paratexte -->
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>

        <revisionDesc>
            <!-- Ajouter ici autant d'éléments <change> que de modifications effectuées -->
            <change when="2022-10-01">Malika BASTIN-HAMMOU : encodage sémantique</change>
            <change when="2022-03-03">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
            <change when="2021-01-30">Heidi-Morgane ROURE : Encodage de la transcription et de la
                traduction</change>
        </revisionDesc>

    </teiHeader>

    <text>
        <body>
            <head type="orig" xml:id="Ar1549_Girard_p2_0">LECTORI</head>
            <head type="trad" corresp="#Ar1549_Girard_p2_0">AU LECTEUR</head>

            <ab type="orig" xml:id="Ar1549_Girard_p2_1">Progrediendum nobis non prius duxi, Lector,
                quam te praemonere in omnibus exemplaribus, quae certe mihi uidere licuit
                quatuordecim personas huic Comoediae tribui, quas ordine propono descriptas. Verum
                aut ego fallor aut ex iis quae undecima est falso inscribitur <foreign
                    xml:lang="grc">Ἕτερος ἀνὴρ ἄδικος</foreign> : legendum potius <foreign
                    xml:lang="grc">Ἕτερος ἀνὴρ δίκαιος</foreign>.</ab>
            <ab type="trad" corresp="#Ar1549_Girard_p2_1" ana="#PH #T #TH_repr">J’ai estimé, Lecteur, qu’il ne nous
                fallait pas avancer plus avant sans te prévenir de ce que, dans tous les
                exemplaires, du moins de ceux qu’il m’a été donné de voir, on attribue quatorze
                personnages à cette comédie, que je propose inscrits dans l’ordre. Mais ou je me
                trompe ou celui qui porte le numéro onze est mal nommé « Le deuxième homme injuste » :
                mieux vaut lire « Le deuxième homme juste ».</ab>

            <ab type="orig" xml:id="Ar1549_Girard_p2_2">Personae huic locus est, si quidem alibi
                usquam est, in ea scaena cuius principium habet ita : <cit type="hyperref">
                    <quote xml:lang="grc">ἆρ’, ὦ φίλοι γέροντες</quote>
                    <bibl resp="#CN"><author ref="#ar">Ar.</author>, <title ref="#ar_pl">Pl.</title>
                        <biblScope>959</biblScope>.</bibl>
                </cit> etc. Omnino <foreign xml:lang="grc">Δίκαιος ἀνὴρ</foreign> est, non <foreign
                    xml:lang="grc">ἄδικος</foreign>, qui anum uidet. Quod uel hinc scias, quod
                facetus sit et amicus Chremyli, quique de eius domo prodiens laetus aniculae ait
                    <cit type="hyperref">
                    <quote xml:lang="grc">ἐγὼ γὰρ αὐτὸς ἐξελήλυθα</quote>
                    <bibl resp="#CN"><author ref="#ar">Ar.</author>, <title ref="#ar_pl">Pl.</title>
                        <biblScope>965</biblScope>.</bibl>
                </cit>. Tum autem nemo gaudebat, nemo beatus erat, qui idem <foreign xml:lang="grc"
                    >Δίκαιος</foreign> non esset.</ab>
            <ab type="trad" corresp="#Ar1549_Girard_p2_2" ana="#TH_repr #TH_dram">Ce personnage se trouve (si du moins il
                n’est pas aussi ailleurs) dans la scène qui commence ainsi : <quote xml:lang="grc"
                    >ἆρ’, ὦ φίλοι γέροντες <note>« Ah, mes chers vieillards », réplique de la
                        vieille femme.</note></quote>. C’est tout à fait un « homme juste » et non «
                injuste » qui voit la vieille femme. On le sait de ce qu’il est facétieux et ami de
                Chrémyle, de chez qui il sort tout content quand il dit à la vieille : <seg
                    xml:lang="grc">ἐγὼ γὰρ αὐτὸς ἐξελήλυθα <note>« Donc me voilà sorti »,
                        d’ordinaire réplique attribuée à Chrémyle.</note></seg>. Or personne ne se
                réjouissait, personne n’était heureux qui ne soit pas par là même « Juste ».</ab>

            <ab type="orig" xml:id="Ar1549_Girard_p2_3">Amplius dico : undecimus ille actor ex hoc
                Catalogo prorsus est expungendus. Nam iustus qui illic anum ridet, ille ipse est qui
                Sycophantae misero paulo ante illuserat.</ab>
            <ab type="trad" corresp="#Ar1549_Girard_p2_3" ana="#TH_dram #TH_repr #PH">J'irai plus loin : ce onzième personnage
                doit carrément être exclu de la liste. Car cet homme juste qui se moque alors de la
                vieille est précisément celui qui, peu avant, s’était moqué du sycophante
                malheureux.</ab>

            <ab type="orig" xml:id="Ar1549_Girard_p2_4">In ea sententia ut sim, plurimum facit, quod
                iustus ait <cit type="hyperref">
                    <quote xml:lang="grc">τί δ’ ἐστίν ; ἦ που καὶ σὺ συκοφάντρια ἐν ταῖς γυναιξὶν
                        ἦσθα ;</quote>
                    <bibl resp="#CN"><author ref="#ar">Ar.</author>, <title ref="#ar_pl">Pl.</title>
                        <biblScope>970</biblScope>.</bibl>
                </cit> Tu cum eo uentum erit, utrum magis arrideat dispicies. Illis qui in locum
                Iusti Chremylum subrogauerunt, quid in mentem uenisset mirari satis nequeo.</ab>
            <ab type="trad" corresp="#Ar1549_Girard_p2_4" ana="#PH #TH_repr #TH_dram #T">Pour que j’arrive à cette conclusion, un
                excellent argument est que le Juste dit : <quote xml:lang="grc">τί δ’ ἐστίν ; ἦ που
                    καὶ σὺ συκοφάντρια ἐν ταῖς γυναιξὶν ἦσθα ; <note>« Eh quoi ? Est-ce que par
                        hasard toi aussi tu étais sycophante parmi les femmes ? », réplique attribuée à
                        Chrémyle.</note></quote> Toi, quand tu en seras rendu là, tu examineras
                duquel des deux il se moque plutôt. Pour ceux qui ont attribué la réplique non pas
                au Juste mais à Chrémyle, je n’arrive pas à savoir ce qui leur est passé par la
                tête.</ab>

            <ab type="orig" xml:id="Ar1549_Girard_p2_5">Leuius fortasse hoc fuerit, non tamen
                protinus mihi contemnendum. In hoc opusculo aliquot occurrerunt loca quae alio
                alioque modo accipi possunt ; in his secutus ego sum quae tum mihi uisa sunt
                probabiliora.</ab>
            <ab type="trad" corresp="#Ar1549_Girard_p2_5" ana="#T #PH">Cela pourrait être peu de chose mais j’ai
                jugé que ce n’était pas négligeable. Dans l’ouvrage il y a plusieurs passages qu’on
                peut prendre de telle ou telle façon ; j’y ai suivi ce qui m’a alors paru le plus
                probable.</ab>

            <ab type="orig" xml:id="Ar1549_Girard_p2_6">Nolim propterea quis meam interpretationem
                    <foreign xml:lang="grc">προκατάγνωσιν</foreign> quandam esse putet, mecumque
                censura agat seueriore.</ab>
            <ab type="trad" corresp="#Ar1549_Girard_p2_6" ana="#PH #T">Je ne voudrais pas en outre qu’on trouve
                que ma traduction est une sorte de <seg xml:lang="grc">προκατάγνωσις <note>«
                        Condamnation avant jugement ».</note></seg> ni qu’on me fasse des reproches
                trop sévères.</ab>
        </body>
    </text>

</TEI>
