<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">De partibus comoediarum et primum de tribus non princibalibus.
                    Capitulum XVI. </title>
                <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : "la" pour le latin et "grc" pour le grec -->
                <!-- Attention ! ne pas faire de retour charriot dans le titre -->
                <author ref="#ascensius_iodocus">Jodocus Badius Ascensius</author>
                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Ajouter autant de tâches (<respStmt>) que nécessaire -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Laurine Oulama</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Révision</resp>
                    <name>Laure HERMAND</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Diandra CRISTACHE</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Commentaire</resp>
                    <name>Laure HERMAND</name>
                </respStmt>
                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne GARCIA-FERNANDEZ</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
            <sourceDesc>
                <bibl>
                    <title>P. Terentii aphri comicorum elegantissimi Comedie a Guidone Juvenale viro
                        perquam litterato familiariter explanate : et ab Jodoco Badio Ascensio vna
                        cum explanationibus rursum annotate atque recognite : cumque eiusdem
                        Ascensii praenotamentis atque annotamentis suis locis adhibitis quam
                        accuratissime impresse venundantur</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#ter">Terentius</author>
                    <pubPlace ref="#londinium">Londonii</pubPlace>
                    <pubPlace ref="#lutetia">Lutetiae</pubPlace>
                    <publisher ref="#worda_winandus">Winandus de Worda</publisher>
                    <publisher ref="#morinus_michael">Michael Morinus</publisher>
                    <publisher ref="#brachius_ioannes"> Johannes Brachius</publisher>
                    <publisher ref="#ascensius_iodocus">Jodocus Badius Ascensius</publisher>
                    <date when="1504">1504</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor ref="#ascensius_iodocus">Jodocus Badius Ascensius</editor>
                </bibl>

                <listBibl>
                    <bibl>Philippe Renouard, Bibliographie des impressions et des œuvres de Josse
                        Badius Ascensius, Paris, E. Paul et fils et Guillemin, 1908, 3 vol.</bibl>
                    <bibl>M. Lebel, Les préfaces de Josse Bade (1462-1535) humaniste,
                        éditeur-imprimeur et préfacier, Louvain, Peeters, 1988</bibl>
                    <bibl>Paul White, Jodocus Badius Ascensius. Commentary, Commerce and Print in
                        the Renaissance, Oxford University Press, 2013</bibl>
                    <bibl>L. Katz, La presse et les lettres. Les épîtres paratextuelles et le projet
                        éditorial de l’imprimeur Josse Bade (c. 1462-1535), thèse de doctorat
                        soutenue à l’EPHE sous la direction de Perrine Galand, 2013</bibl>
                </listBibl>

            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Josse Bade ouvre son commentaire aux comédies de Térence par une longue
                    introduction qu’il nomme lui-même Praenotamenta. Il s’agit de « notes
                    préliminaires » plutôt que d’une « préface » à proprement parler. </p>
                <p>Cette section est composée de 26 chapitres qui forment un traité de poétique en
                    miniature auxquels il adjoint une série de remarques préliminaires sur le
                    prologue et la première scène de l’Andrienne. L’humaniste y développe une ample
                    réflexion sur la comédie. Partant d’une définition de la poésie, Bade aborde
                    ensuite l’origine de la comédie, ses caractéristiques et mène une longue
                    réflexion sur la scénographie antique. Les deux derniers chapitres traitent de
                    la vie et des œuvres de Térence.</p>


            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>
        <revisionDesc>
            <change when="2022-04-08">Sarah GAUCHER : correction de la transcription, encodage de la
                tranduction, conformité, identifiants Header</change>
        </revisionDesc>
    </teiHeader>

    <text>
        <body ana="#TH_dram">

            <!-- RAPPEL : on encode le texte latin ou grec; on n'encode dans le texte de la traduction que les notes d'éditeur (=membres du projet) -->

            <!-- Le couple d'éléments <head> contient le titre ou l'incipit du paratexte -->
            <!-- L'identifiant du TITRE est composé du titre de la fiche TT suivi d'un underscore (_) puis du chiffre 0 -->
            <head type="orig" xml:id="Te1504_Ascensius_p3p_0">De partibus comoediarum et primum de
                tribus non princibalibus. Capitulum XVI.</head>
            <!-- Le deuxième élément du couple contient la traduction française du titre ou incipit -->
            <!-- L'identifiant de la traduction est le même que celui du latin, précédé d'un hashtag (#) -->
            <head corresp="#Te1504_Ascensius_p3p_0" type="trad">Chapitre 16. Sur les parties des
                comédies et d’abord sur les trois parties secondaires.</head>
            <ab type="orig" xml:id="Te1504_Ascensius_p3p_1">Comoediarum quattuor, ut auctor est
                    <persName ref="#donat">Donatus</persName>, principales sunt partes : <seg
                    type="paraphrase">prologus, prothesis, epithasis, et catastropha<bibl resp="#SG"
                            ><author ref="#euant">Evanthius</author>, <title ref="#euant_fab">De
                            fabula</title>, <biblScope>7.1</biblScope>.</bibl></seg>, quarum tres
                ultimae de principali substantia sunt. </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3p_1"> Les principales parties des comédies,
                selon Donat, sont au nombre de quatre : le prologue, la protase, l’épitase et la
                catastrophe ; les trois dernières concernent l’essence principale de la comédie. </ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3p_2"> Prologus autem tanquam praeambulum est. </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3p_2"> Quant au prologue, il est pour ainsi
                dire son préambule. </ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3p_3">Praeter has autem partes tres aliae sunt
                quas non componunt poetae, sed uel actores uel grammatici : hae sunt titulus
                praeuius et titulus sequens et argumentum. </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3p_3"> Outre ces parties, il y en a trois
                autres qui ne sont pas le fait des poètes mais des acteurs ou des grammairiens : le
                premier titre, le second titre et l’argument. <note><foreign xml:lang="la">Titulus
                        praeuius</foreign> est une expression qui semble venir du droit. <foreign
                        xml:lang="la">Titulus sequens</foreign> se rencontre dans les Accessus ad
                    auctores médiévaux. </note>
            </ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3p_4"> Vt enim dixi in fronte totius proscenii
                ponebatur titulus continens nomen factoris actoris et fabulae, ut primae comoediae
                titulus praeuius esse potest <title ref="#ter_and">Andriam <persName ref="#ter"
                        >Terentii</persName> poetae comici agent hodie <persName>Lucius
                        Attilius</persName>, et <persName>Latinus Praestinus</persName></title> ubi
                est nomen fabulae uidelicet <title ref="#ter_andr">Andria</title>, et nomen factoris
                uidelicet <persName ref="#ter">Terentius</persName>, et nomina actorum uidelicet
                    <persName>Lucius Attilius</persName>, et <persName>Latinus
                Praestinus</persName>. </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3p_4" ana="#TH_repr"> En effet, comme je
                l’ai dit, sur tout le devant de l’avant-scène était inscrit un titre contenant le
                nom de l’auteur, de l’acteur et de la pièce, par exemple le titre de la première
                comédie peut être <title>Lucius Attilius et Latinus Praestinus jouent aujourd’hui
                    l’Andrienne du poète comique Térence</title>, où l’<title>Andrienne</title> est
                évidemment le nom de la pièce, Térence celui de l’auteur, Lucius Attilius et Latinus
                Praestinus ceux des acteurs. </ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3p_5"> Fabularum autem nomina ut dicit
                    <persName ref="#donat">Donatus</persName>
                <seg type="paraphrase">ex quattuor sumuntur, aut a nomine proprio eius cuius sunt
                    praecipuae partes, aut ab appellatiuo cuiusmodi sunt <title ref="#ter_phorm"
                        >Phormio</title>, <title ref="#ter_hec">Hecyra</title>, <title
                        ref="#pl_epid">Epidicus</title>, aut a loco in quo praecipue res gesta est
                    ut <title ref="#ter_andr">Andria</title>
                    <title>Leucadica</title>
                    <title>Brundusina</title> aut a facto ut <title ref="#ter_eun">Eunuchus</title>,
                        <title ref="#pl_as">Asinaria</title>, <title ref="#pl_capt">Captiui
                        duo</title>, aut ab euentu ut <title>Commorientes</title>,
                        <title>Crimen</title>, <title ref="#ter_haut"
                        >Heautontimoroumenos</title><bibl resp="#SG"><author ref="#euant"
                            >Evanthius</author>, <title ref="#euant_fab">De fabula</title>,
                            <biblScope>6.4</biblScope>.</bibl></seg>. </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3p_5"> Les noms des pièces, comme le dit
                Donat, sont tirés de quatre éléments : du nom propre du personnage qui revient dans
                les parties principales ; d’un appellatif, comme <title>Phormion</title>,
                    <title>Hécyre</title>, <title>Epidicus</title> ; du lieu dans lequel l’action se
                déroule principalement, comme <title>L’Andrienne</title>, <title>La
                    Léocadienne</title>, <title>La Fille de Brindes</title> ; d’un fait, comme
                    <title>L’Eunuque</title>, <title>La Comédie aux Ânes</title> et <title>Les Deux
                    Captifs</title>, d’un évènement, comme <title>Les deux Mourants</title>,
                    <title>Le Crime</title>, <title>Le Bourreau de soi-même</title>. </ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3p_6"> Quare autem illa nomina data sunt
                comoediis dicemus in principio cuiuslibet comoediae apud <persName ref="#ter"
                    >Terentium</persName>, de aliis autem alii uideant. </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3p_6"> Pourquoi ces noms ont été donnés aux
                comédies, nous le dirons au début de chaque comédie de Térence ; quant à celles qui
                restent, que d’autres les examinent. </ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3p_7"> Verum, ut ad rem redeam, in hoc titulo
                praeuio, siue in hac prima inscriptione, aliquando praeponebatur nomen fabulae,
                aliquando nomen poetae qui eam fecit. </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3p_7" ana="#TH_repr"> Mais, pour en revenir
                à notre sujet, dans ce premier titre ou dans cette première inscription on plaçait
                en premier parfois le nom de la pièce, parfois le nom du poète qui l’avait composée. </ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3p_8"> Cuius moris diuersitatem antiquitas
                comprobat. </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3p_8" ana="#TH_repr"> Cette diversité
                d’habitudes est confirmée par l’ancienneté du fait. </ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3p_9"><seg type="allusion">Nam, cum primum
                    aliqui fabulas ederent, fabularum nomina pronuntiabantur antequam poetae nomen
                    pronuntiaretur ne aliqua inuidia a scribendo deterreri posset.<bibl resp="#SG"
                            ><author ref="#euant">Evanthius</author>, <title ref="#euant_fab">De
                            fabula</title>, <biblScope>8.1</biblScope>.</bibl></seg>
            </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3p_9" ana="#TH_repr"> En effet, dès qu’on
                produisait une pièce, le nom de la pièce était annoncé avant le nom du poète afin
                qu’aucune jalousie ne puisse le détourner d’écrire. </ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3p_10"> Cum autem per editionem multarum
                fabularum poetae ipsi iam esset auctoritas et fama acquisita, rursus priora poetarum
                nomina proferebantur ut per ipsorum uocabula fabulis attentio acquireretur. <note>
                    Evanthius, De fabula, 8.1.</note>
            </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3p_10" ana="#TH_repr"> Mais une fois que la
                production de plusieurs pièces avait suffisamment assis l'autorité et la réputation
                du poète, alors au contraire on mettait en premier le nom du poète pour que la
                mention de ce nom attire sur la pièce l'attention du public. </ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3p_11"> Erat et alia notificatio quae post
                actas fabulas non in prosceniis, sed in libris scribebatur. </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3p_11"> Il y avait encore une autre
                indication qui après la représentation était inscrite non pas sur l’avant-scène,
                mais dans les livres. </ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3p_12"> Per quam etiam consulum quorum
                temporibus actae fuerunt et edilium a quibus emptae et ludorum nomina in quibus
                actae sunt significabantur ut in singulis comoediis <persName ref="#ter"
                    >Terentii</persName> uidebimus. </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3p_12"> Or, on y faisait figurer le noms des
                consuls à l’époque desquels les pièces avaient été jouées, des édiles qui les
                avaient achetées et des jeux lors desquels elles avaient été jouées, ainsi que nous
                le verrons dans chacune des comédies de Térence. </ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3p_13"> Tertia pars quae a poeta non edebatur
                est argumentum quo paucis uerbis tota materia comoediae perstringitur ut etiam
                uidebimus. </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3p_13"> La troisième partie que le poète ne
                faisait pas paraître est l’argument, où le sujet tout entier de la comédie est
                brièvement résumé, comme nous le verrons également. </ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p3p_14"> Nunc ad alias partes, et primum de
                prologis </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p3p_14"> À présent, tournons-nous vers les
                autres parties, en commençant par les prologues. </ab>


        </body>
    </text>
</TEI>
