<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Jodocus Badius Ascensius Magistro Herveo Besino, iureconsulto,
                    litteris eruditissimo amicorumque primario, salutem plurimam dicit.</title>
                <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : "la" pour le latin et "grc" pour le grec -->
                <!-- Attention ! ne pas faire de retour charriot dans le titre -->
                <author ref="#ascensius_iodocus">Jodocus Badius Ascensius</author>
                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Ajouter autant de tâches (<respStmt>) que nécessaire -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Laure HERMAND</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Laure HERMAND</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Diandra CRISTACHE</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Commentaire</resp>
                    <name>Laure HERMAND</name>
                </respStmt>
                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne GARCIA-FERNANDEZ</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
            <sourceDesc>
                <bibl>
                    <title>P. Terentii aphri comicorum elegantissimi Comedie a Guidone Juvenale viro
                        perquam litterato familiariter explanate : et ab Jodoco Badio Ascensio vna
                        cum explanationibus rursum annotate atque recognite : cumque eiusdem
                        Ascensii praenotamentis atque annotamentis suis locis adhibitis quam
                        accuratissime impresse venundantur</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#ter">Terentius</author>
                    <pubPlace ref="#londinium">Londonii</pubPlace>
                    <pubPlace ref="#lutetia">Lutetiae</pubPlace>
                    <publisher ref="#worda_winandus">Winandus de Worda</publisher>
                    <publisher ref="#morinus_michael">Michael Morinus</publisher>
                    <publisher ref="#brachius_ioannes"> Johannes Brachius</publisher>
                    <publisher ref="#ascensius_iodocus">Jodocus Badius Ascensius</publisher>
                    <date when="1504">1504</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor ref="#ascensius_iodocus">Jodocus Badius Ascensius</editor>
                </bibl>

                <listBibl>
                    <bibl>Philippe Renouard, Bibliographie des impressions et des œuvres de Josse
                        Badius Ascensius, Paris, E. Paul et fils et Guillemin, 1908, 3 vol.</bibl>
                    <bibl>M. Lebel, Les préfaces de Josse Bade (1462-1535) humaniste,
                        éditeur-imprimeur et préfacier, Louvain, Peeters, 1988</bibl>
                    <bibl>Paul White, Jodocus Badius Ascensius. Commentary, Commerce and Print in
                        the Renaissance, Oxford University Press, 2013</bibl>
                    <bibl>L. Katz, La presse et les lettres. Les épîtres paratextuelles et le projet
                        éditorial de l’imprimeur Josse Bade (c. 1462-1535), thèse de doctorat
                        soutenue à l’EPHE sous la direction de Perrine Galand, 2013</bibl>
                    <bibl>G. Torello-Hill et A.J. Turner, The Lyon Terence. Its Tradition and
                        Legacy, Leiden/Boston, Brill, 2020, en part. p. 99-101</bibl>
                </listBibl>

            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Il s’agit d’une courte épître dédicatoire à Hervé Bésin, jurisconsulte lyonnais
                    pour introduire non seulement l’édition commentée des six comédies de Térence,
                    mais surtout la longue introduction rédigée par Josse Bade, les Praenotamenta,
                    véritable art poétique en miniature, que Bade qualifie ici d’ « éclaircissements
                    au texte de Térence » (nostra in Terentium elucidamenta).</p>
                <p>La première édition de 1502, publiée à Lyon chez François Fradin, est extrêmement
                    rare et nous avons donc choisi de baser notre édition de ses nombreux paratextes
                    sur la réédition de 1504.</p>
            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>
        <revisionDesc>
            <change when="2022-04-08">Sarah GAUCHER : mise en conformité, mise en place des
                identifiants header</change>
        </revisionDesc>
    </teiHeader>

    <text>
        <body>

            <!-- RAPPEL : on encode le texte latin ou grec; on n'encode dans le texte de la traduction que les notes d'éditeur (=membres du projet) -->

            <!-- Le couple d'éléments <head> contient le titre ou l'incipit du paratexte -->
            <!-- L'identifiant du TITRE est composé du titre de la fiche TT suivi d'un underscore (_) puis du chiffre 0 -->
            <head type="orig" xml:id="Te1504_Ascensius_p1_0"><persName ref="#ascensius_iodocus"
                    >Iodocus Badius Ascensius</persName> Magistro <persName ref="#besinus_herueus"
                    role="destinataire">Herveo Besino</persName>, iureconsulto, litteris
                eruditissimo amicorumque primario, salutem plurimam dicit.</head>
            <!-- Le deuxième élément du couple contient la traduction française du titre ou incipit -->
            <!-- L'identifiant de la traduction est le même que celui du latin, précédé d'un hashtag (#) -->
            <head corresp="#Te1504_Ascensius_p1_0" type="trad">Josse Bade à Maître <seg>Hervé
                        Bésin<note> Nous n’avons pas d’autre information sur ce personnage d’Hervé
                        Besin, jurisconsulte lyonnais, ami de Bade. </note></seg>, jurisconsulte,
                lettré très savant et le meilleur de ses amis, un généreux salut ! </head>

            <ab type="orig" xml:id="Te1504_Ascensius_p1_1">Adhortaris, uirorum praestantissime, ut
                nostra in <persName ref="#ter">Terentium</persName> elucidamenta, que cum hisce
                diebus istic (Lugduni dico) agerem, dixi eo animo collecta, quo in uernaculam
                Gallorum linguam uersantur <persName>Simono Vincentio</persName>, amico communi et
                bibliopolarum diligentissimo, ipsius impendio curaque imprimenda concredam.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p1_1" ana="#PA">Tu m’incites, toi le plus
                remarquable des hommes, à confier à l’impression aux frais et aux soins de Simon
                Vincent nos éclaircissements sur Térence. <note> Simon Vincent est un libraire né
                    entre 1470 et 1480. Il fut en activité à Lyon dès 1499, il implante un dépôt
                    permanent (« cabal ») de livres à Toulouse, en 1506 au plus tard. Son testament
                    est daté du 29 août 1532. Il décède peu après. Ses fils Antoine et François lui
                    succèdent. </note> Alors que ces derniers jours ici (je veux dire à Lyon) je les
                élaborais, ils ont été rassemblés, comme je l’ai dit, avec le projet de les faire
                traduire en français par le même Simon Vincent, notre ami commun et le plus soigneux
                des libraires. <note> Ce projet de traduction en français des Praenotamenta n’a
                    jamais vu le jour, ou bien le texte français a été perdu. </note></ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p1_2">In qua re tametsi nihil mihi tua
                auctoritate maius, perspectaque sit amici nostri fides atque integritas, notusque et
                intus et in cute illius in scholasticos feruens, ut aiunt, zelus atque beniuolus
                animus, hesitaui tamen parumper, dubitans an illius sint aere atque accuratione
                digna.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p1_2" ana="#PA">Et même si en la matière rien
                n’est plus grand à mes yeux que ton autorité, que la loyauté et l’honnêteté de notre
                ami sont avérées, que son zèle plein de ferveur, comme on dit, et sa bienveillance à
                l’égard des étudiants sont connus, j’ai cependant hésité quelque peu, me demandant
                si nos éclaircissements étaient dignes de son argent et de son attention.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p1_3">Sunt etenim sermone fere plebeio atque
                humili, eruditionis, ni fallor, bonae atque sententiarum utilium plus quam salis
                atque elegantiarum habentia ; quippe uulgo, ut diximus, illiterato destinata.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p1_3" ana="#PE">Ils sont en effet écrits dans
                un langage courant et simple et sont pourvus, si je ne m’abuse, d’un honnête savoir
                et de maximes utiles plus que d’esprit et de raffinement ; ainsi ils sont destinés,
                comme nous l’avons dit, à un large public non lettré.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p1_4">Quocirca in hanc tandem concessimus
                sententiam, ut praenotamenta et primi prologi primaeque scaenae glossemata ueluti
                praegustamenta quedam de reliquis periculum factura ; illius fidei etiam integra
                atque immutata committam, cetera mihi recepturus dum haec largos attulerint
                manipulos.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p1_4" ana="#PH">C’est pourquoi nous avons
                fini par nous rendre à l’avis suivant : les notes préliminaires et les gloses du
                premier prologue et de la première scène, en quelque sorte des apéritifs annonçant
                le reste, constitueront un premier essai ; je les confierai entières et inchangées à
                sa bonne foi, gardant le reste pour moi jusqu’à ce qu’elles soient devenues de gros
                bataillons. <note> En fait, le dit commentaire ne semble jamais avoir été terminé
                    par Josse Bade, car les éditions ultérieures n’en proposent pas davantage que
                    celle de 1502. </note></ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p1_5">Recognoui tamen totum opus et quae
                singulis in quibus operae pretium uisum est scaenis annotatiunculas adieci.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p1_5" ana="#PH">J’ai toutefois revu l’ouvrage
                tout entier, et pour ce qui m’a semblé digne d’intérêt dans chaque scène, j’ai
                ajouté des petites annotations.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p1_6">Quae omnia ad tuam refero prudentiam ut
                si digna censueris imprimantur quamprimum ; sin minus ad nostram redeant limam.</ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p1_6" ana="#PA">C’est tout ce travail que je
                confie à ta sagacité pour que, si tu l’en juges digne, il soit imprimé au plus vite
                ; sinon qu’il revienne sous notre lime.</ab>

            <ab type="orig" xml:id="Te1504_Ascensius_p1_7">Tantum uolui. </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p1_7">C’est tout ce que j’ai voulu. </ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p1_8">Vale et <persName>Simonem</persName>
                nostrum itidem saluere et ualere iube. </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p1_8" ana="#PA">Salut et salue également
                chaleureusement, je te prie, notre ami Simon.</ab>
            <ab type="orig" xml:id="Te1504_Ascensius_p1_9">Ex illustri Parrhisiorum gymnasio ad
                kalendas Ianuarias millesimoquingentesimoprimo (1501). </ab>
            <ab type="trad" corresp="#Te1504_Ascensius_p1_9">Du célèbre collège parisien, aux
                calendes de janvier 1501. <note> Il s’agit du 1er janvier 1501. Il n’y a pas lieu de
                    supposer ici une coquille ni de dater la lettre de 1502, date de l’édition chez
                    Fradin. </note></ab>
        </body>
    </text>
</TEI>
