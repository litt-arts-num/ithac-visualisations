<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">De Autoribus Tragoediae. Joachimus Camerarius. </title>
                <!-- Attention ! ne pas faire de retour charriot dans le titre -->
                <author ref="#camerarius_ioachimus">Ioachimus Camerarius</author>

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Tâche n°1 -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Marius MASSUCCO</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Diandra Cristache</name>
                </respStmt>
                <respStmt>
                    <resp>Révision</resp>
                    <name>Pascale PARE-REY</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Marius MASSUCCO</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Commentaire</resp>
                    <name>Malika Bastin-Hammou</name>
                    <name>Diandra Christache</name>
                </respStmt>
                <!-- Ajouter autant de tâches que nécessaire -->
                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet : ne pas y toucher !-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
            <sourceDesc>

                <bibl>
                    <!-- l'attribut @level permet d'indiquer s'il s'agit d'une monographie (m), d'un article ou chapitre d'ouvrage (a) ou d'un périodique (j) -->
                    <title>Trachiniae Sophoclis, Poetae Tragicis: de extremo Actu rerum gestarum.
                        Herculis, eiusdemque interitu lamentabili. Cum interpretatione latina, Viti
                        Winshemii. </title>
                    <author ref="#soph">Sophocles</author>
                    <pubPlace ref="#argentoratum">Argentorati</pubPlace>
                    <publisher ref="#bertramus_antonius">Antonius Bertramus</publisher>
                    <date when="1584">M. D. LXXXIIII.</date>
                    <editor ref="#winshemius_uitus" role="traducteur">Vitus Winschemius</editor>
                </bibl>


            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Type d'édition : texte grec seul Court traité de Camerarius sur les poètes
                    dramatiques grecs. Camerarius : Joachim Camerarius (Bamberg, 1500 — Leipzig,
                    1574), dit « l’ancien » (der Ältere). Son père était trésorier épiscopal et
                    conseiller à Bamberg. Le nom de « Camerarius » témoigne d’ancêtres ayant exercé
                    de hautes fonctions au sein du Saint-Empire (Magister Camerae). Camerarius est
                    élève à Bamberg, avant de recevoir l’enseignement de Georg Helt (Georgius
                    Heltus, théologien protestant) à Leipzig dès 1512. En 1516, il étudie le grec
                    avec Petrus Mosellanus. En 1518 il étudie à l’université d’Erfurt, puis à celle
                    de Wittenberg en 1521 où il devient l’ami de Philipp Melanchthon. En 1522 il y
                    obtient la chaire de Zoologie, puis en 1525 il y devient professeur de langue et
                    littérature grecque. En 1526, il enseigne à l’Aegidianum (« Engydiengymnasium »
                    en allemand) fondée à Nuremberg la même année et que fréquentent de grandes
                    figures de la Réforme. En 1532 il traduit plusieurs écrits de Dürer, puis il
                    prend part à la réorganisation de l’université de Tübingen avant d’enseigner à
                    l’université de Leipzig de 1541 jusqu’à sa mort. On lui doit la publication de
                    nombreux auteurs grecs et latins : Démosthène, Esope, Hérodote, Homère,
                    Quintilien, Sophocle, Théocrite, Théophraste, Xénophon, Thucydide, et des
                    commentaires sur Cicéron et César. Parmi les enfants qu’il eut avec sa femme
                    Anna Truchsess von Grünberg, Joachim Camerarius (1534-1598), dit « le jeune »
                    s’est illustré comme botaniste et Philipp Camerarius (1537-1624) comme
                    historien. [source : wikipédia:
                    https://de.wikipedia.org/wiki/Joachim_Camerarius_der_%C3%84ltere] Winshemius :
                    Vitus Vuinshemius (Veit Winsheim). (Windsheim 1501 — Wittenberg 1570), est un
                    philologue, helléniste et médecin allemand, diplômé d’une maîtrise en
                    philosophie à la faculté de Wittenberg en 1529. Il est proche de Mélanchthon,
                    auquel il succède dans sa chaire de langue grecque en 1541. Docteur en médecine
                    en 1550, puis successivement doyen de la faculté de philologie, recteur de
                    l’université de Wittenberg, et vice-recteur de l’Académie. Il est aussi
                    conseiller de la ville de Wittenberg. Il a laissé de nombreuses études sur les
                    textes antiques, notamment sur Démosthène, Thucydide, Théocrite, Homère,
                    Sophocle et Euripide, ainsi que des discours et éloges funèbres (notamment celui
                    de Melanchthon). Son fils s’appelle aussi Veit Winsheim, dit « le jeune »(der
                    Jüngere), professeur de droit. [source : wikipédia :
                    https://de.wikipedia.org/wiki/Veit_Winsheim] </p>
            </abstract>


            <langUsage>
                <!-- Garder seulement les éléments indiquant les langues utilisées dans le paratexte -->
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>

        <revisionDesc>
            <change when="2022-02-17">Sarah GAUCHER : Vérification et ajouts PersName, PlaceName,
                hi, title, quote, seg, foreign. Vérification de la mise en conformité du
                schéma</change>
            <change when="2022-02-14">Sarah GAUCHER : reprise de l'intégralité de la TEI,
                harmonisation, reprise de la trasncription et de la traduction</change>
            <change when="2021-03-29">Diandra CRISTACHE : Mise à jour et chargement sur le
                Pensoir</change>
            <!-- Ajouter ici autant d'éléments <change> que de modifications effectuées -->
            <change when="2020-11-27">Marius MASSUCCO : séance de test avec Diandra
                CRISTACHE</change>
        </revisionDesc>

    </teiHeader>

    <text>
        <body ana="#TH_hist">

            <!-- Le couple d'éléments <head> contient le titre ou l'incipit du paratexte -->
            <!-- Attention ! Ne pas oublier de mettre à jour les identifiants en rouge. -->
            <!-- L'identifiant du TITRE est composé du titre de la fiche TT (Ar1586_Frischlinus_Vita_Aristophanis_p9) suivi d'un underscore (_) puis du chiffre 0 -->
            <!-- Le premier élément du couple contient le titre ou l'incipit en langue originale -->
            <head type="orig" xml:id="Soph1584_Camerarius_p1_0">De auctoribus Tragoediae <persName
                    ref="#camerarius_ioachimus">Ioachimus Camerarius.</persName></head>
            <!-- Le deuxième élément du couple contient la traduction française du titre ou incipit -->
            <!-- L'identifiant de la traduction est le même que celui du latin, précédé d'un hashtag (#) -->
            <head corresp="#Soph1584_Camerarius_p1_0" type="trad">Sur les auteurs de tragédie,
                Joachim Camerarius.</head>

            <!-- Chaque couple d'éléments <ab> contient une unité textuelle du paratexte (cf. tableau de saisie) -->
            <!-- Attention ! Ne pas oublier de mettre à jour les iSoph1546_impBrubachius_p1_Winschemius_LAT_0dentifiants en rouge. -->
            <!-- L'identifiant de l'UNITE TEXTUELLE est composé du titre de la fiche TT (Ar1586_Frischlinus_Vita_Aristophanis_p9) suivi d'un underscore (_) puis du chiffre 1, 2, 3... -->
            <!-- Le premier élément du couple contient l'unité textuelle en langue originale -->

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_1">Auctores fuere tragoediae tres eximii,
                    <persName ref="#eschl">Æschylus</persName>, <persName ref="#soph"
                    >Sophocles</persName>, <persName ref="#eur">Euripides</persName>.</ab>

            <ab corresp="#Soph1584_Camerarius_p1_1" type="trad">Il y eut trois célèbres auteurs de
                tragédie , Eschyle, Sophocle, Euripide. </ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_2"><seg type="allusion">
                    <persName ref="#eschl">Æschylus</persName> horum antiquissimus iussus per
                    quietem a Baccho Tragoedias componere, ut aiunt, scena personarum uarietate et
                    decore instruxit. <bibl resp="#CN" type="nontrouve"/>
                </seg>
            </ab>

            <ab corresp="#Soph1584_Camerarius_p1_2" type="trad" ana="#V">Eschyle, le plus ancien
                d’entre eux, ayant reçu de Bacchus dans son sommeil l’ordre de faire des tragédies,
                dit-on, équipa la scène de plus de personnages et de décor.</ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_3">Ipse non poeta solum bonum, sed
                bellator etiam fuit, fortitudinis laudem Marathonio proelio adeptus.</ab>

            <ab corresp="#Soph1584_Camerarius_p1_3" type="trad" ana="#V">Lui-même n’était pas
                seulement un bon poète mais aussi un soldat, qui se couvrit de gloire à la bataille
                de Marathon. </ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_4">Huic aetate inferiores <persName
                    ref="#soph">Sophocles</persName> et <persName ref="#eur">Euripides</persName>
                elegantiora poëmata fecerunt, referentes in scenam pleraque <persName ref="#eschl"
                    >Aeschyli</persName> perpolita, et tanquam correcta, cum illius fabulae decreto
                ciuitatis agendae essent.</ab>

            <ab corresp="#Soph1584_Camerarius_p1_4" type="trad"> Plus jeunes que lui, Sophocle et
                Euripide firent des pièces plus élégantes, traitant à nouveau sur scène plusieurs
                sujets qu’Eschyle avait élaborés, et les améliorant puisqu’on fit jouer les pièces
                de Sophocle par un décret de la cité. </ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_5"><seg type="paraphrase"> Vnde gloriatur
                    apud <persName ref="#ar">Aristophanem</persName>, non interiisse secum poesin,
                    quemadmodum cum <persName ref="#eur">Euripide</persName>. <bibl resp="#CN"
                            ><author ref="#ar">Ar.</author>, <title ref="#ar_ran">Ran.</title>
                        <biblScope>868-870</biblScope>.</bibl>
                </seg>
            </ab>

            <ab corresp="#Soph1584_Camerarius_p1_5" type="trad">Aussi se glorifie-t-il chez
                Aristophane que sa poésie n’ait pas péri avec lui, au contraire du cas d’Euripide. </ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_6"><seg type="allusion"> Faciunt autem
                        <persName ref="#soph">Sophoclem</persName> natu minorem <persName
                        ref="#eschl">Æschylo</persName> annis nouem, uiginti quator <persName
                        ref="#eur">Euripidem</persName>.<bibl resp="#CN" type="nontrouve"/>
                </seg></ab>

            <ab corresp="#Soph1584_Camerarius_p1_6" type="trad" ana="#V">On considère Sophocle plus
                jeune qu’Eschyle de vingt ans, Euripide de vingt-quatre. </ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_8"><seg type="allusion"> Quorum ille
                    grauior simpliciorque putatur, hic uersutior et disertior.<bibl resp="#CN"
                        type="nontrouve"/>
                </seg></ab>

            <ab corresp="#Soph1584_Camerarius_p1_8" type="trad" ana="#LA">Sophocle est, dans
                l’opinion, plus grave et plus simple, Euripide plus ingénieux et plus bavard.</ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_9"><seg type="reference"> Atque ad hunc
                    modum <persName ref="#ar">Aristophanes</persName> illos <foreign xml:lang="gr"
                        >ἐχαρακτήρισε</foreign>. <bibl resp="#CN" type="nontrouve"/>
                </seg></ab>

            <ab corresp="#Soph1584_Camerarius_p1_9" type="trad">Et c’est ainsi
                qu’Aristophane les a caractérisés.</ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_10"><persName ref="#eschl"
                    >Æschylus</persName> collapsis inter ludos, quibus ille fabulam dedisset
                spectaculis, Athenas deseruit, et in Sicilia paulo post mortuus est, ictus
                testudine.</ab>

            <ab corresp="#Soph1584_Camerarius_p1_10" type="trad" ana="#V">Eschyle, <seg>alors qu'au
                    milieu des jeux s'étaient effondrés les gradins du théâtre, dans lequel il avait
                    représenté sa pièce<note>Une référence à l’écroulement du théâtre de Dyonisos
                        autour du 500 av.J.-C. ? “Tra la fine del VI e l'inizio del V secolo a.C.,
                        però, si verificò un incidente: il crollo delle impalcature (ikria) dove
                        sedevano gli spettatori.”<ref
                            target="https://it.wikipedia.org/wiki/Teatro_di_Dioniso"/>. Mais Eschyle
                        a quitté Athène après 458 av.J.-C.. Il y a donc un écart de presque 40 ans
                        entre les deux événements. </note></seg> , quitta Athènes et mourut en
                Sicile peu de temps après, frappé par une tortue.</ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_11">Cuius tale fertur epitaphium.</ab>

            <ab corresp="#Soph1584_Camerarius_p1_11" type="trad" ana="#V">De cela il est rapporté
                dans cet épitaphe. </ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_12"><cit type="nonref">
                    <quote xml:lang="gr"><l><persName ref="#eschl">Αἰσχύλον</persName>
                            <persName>Ἐυφορίωνος</persName> ἀθηναῖον τόδε κεύθει</l>
                        <l> μνῆμα καταφθίμενον πυροφόροιο Γέλα.</l>
                        <l>ἀλκὴν δʹἐυδόκιμον Μαραθώνιον ἄλσος ἂν εἴπῃ,</l>
                        <l> καὶ βαθυχαιτήεις Μῆδος ἐπιστάμενος</l></quote>
                    <bibl resp="#CN"><title ref="#anth_grc">Anth. Gr.</title>
                        <biblScope>« Appendice » 17</biblScope>. <note>Le texte est dans la
                                <title>Vie d’Eschyle</title> avec une variante insignifiante :
                                <foreign xml:lang="grc">Αἰσχύλον Εὐφορίωνος Ἀθηναῖον τόδε κεύθει/
                                μνῆμα καταφθίμενον πυροφόροιο Γέλας/ ἀλκὴν δ’ εὐδόκιμον Μαραθώνιον
                                ἄλσος ἂν εἴποι/ καὶ βαθυχαιτήεις Μῆδος
                        ἐπιστάμενος</foreign></note></bibl>
                </cit></ab>

            <ab corresp="#Soph1584_Camerarius_p1_12" type="trad" ana="#V">"Eschyle fils d’Euphorion,
                Athénien, repose dans ce tombeau ; il s’est éteint dans la féconde Géla. Sa force
                glorieuse, le bois de Marathon pourrait en parler ainsi que le Mède aux longs
                cheveux, en connaissance de cause." </ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_13"><persName ref="#eur"
                    >Euripides</persName> et rhetores et philosophos, inque primis
                    <persName>Anaxagoram</persName> audiuerat, cuius casu etiam territum, putant
                philosophica studia reliquisse.</ab>

            <ab corresp="#Soph1584_Camerarius_p1_13" type="trad" ana="#V">Euripide avait été
                auditeur de rhéteurs et de philosophes, particulièrement d’Anaxagore, dont la mort
                l’avait terrifié et incité à abandonner la philosophie. </ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_14">Sunt igitur in hoc omnia argutiora,
                et pleraque sententiae philosophicae, ut <persName ref="#cic">Cicero</persName>
                quoque testatur exponens hanc e <title ref="#eur_th">Theseo</title> :</ab>

            <ab corresp="#Soph1584_Camerarius_p1_14" type="trad" ana="#RH">Il y a donc chez lui
                beaucoup d’arguties et de maximes philosophiques, comme Cicéron aussi l’atteste en
                citant ce passage du Thésée : </ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_15"><cit type="hyperref">
                    <quote xml:lang="grc"><l>Ἐγὼ δὲ τοῦτο παρὰ σοφοῦ τινὸς μαθὼν,</l><l>
                            εἰς φροντίδας νοῦν συμφοράς τʹἐβαλλόμην,</l><l> φυγάς τʹἐμαυτῷ
                            προστιθεὶς πάτρης ἑμῆς,</l><l> θανάτους τʹἀωρους, καὶ κακῶν
                            ἄλλας ὁδοὺς</l><l> ὡς εἴ τι πάσχοιμʹὧν ἐδόξαζόν
                            ποτε,</l><l> μή μοι νεαρὸν προσπεσὸν μᾶλλον δάκοι.</l></quote>
                    <bibl resp="#CN"><author ref="#eur">Eur.</author>
                        <title ref="#eur_th">Frag.</title>
                        <biblScope>964</biblScope>
                        <note>Avec quelques variantes par rapport au texte cité par Camerarius :
                                <foreign xml:lang="grc">ἐγὼ δὲ ταῦτα παρὰ σοφοῦ τινος μαθὼν / εἰς
                                φροντίδας νοῦν συμφοράς τ’ ἐβαλλόμην, / φυγάς τ’ ἐμαυτῷ προστιθεὶς
                                πάτρας ἐμῆς / θανάτους τ’ ἀώρους καὶ κακῶν ἄλλας ὁδούς, / ἵν’ εἴ τι
                                πάσχοιμ’ ὧν ἐδόξαζον φρενί, / μή μοι νεῶρες προσπεσὸν μᾶλλον
                                δάκοι.</foreign></note></bibl>
                </cit></ab>

            <ab corresp="#Soph1584_Camerarius_p1_15" type="trad"><quote xml:lang="grc"><l>Ἐγὼ δὲ
                        τοῦτο παρὰ σοφοῦ τινὸς μαθὼν,</l><l> εἰς φροντίδας νοῦν συμφοράς
                        τʹἐβαλλόμην,</l><l> φυγάς τʹἐμαυτῷ προστιθεὶς πάτρης ἑμῆς,</l><l>
                        θανάτους τʹἀωρους, καὶ κακῶν ἄλλας ὁδοὺς</l><l> ὡς εἴ τι
                        πάσχοιμʹὧν ἐδόξαζόν ποτε,</l><l> μή μοι νεαρὸν προσπεσὸν μᾶλλον
                        δάκοι.</l></quote>
            </ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_16"><cit type="refauteur">
                    <quote type="traduction"><l>Nam qui haec audita a docto meminissem uiro,</l><l>
                            Futuras mecum commentabar miserias,</l><l> Aut mortem acerbam, aut
                            exilii moestam fugam,</l><l> Aut semper aliquam molem meditabar mali
                            :</l><l> Vt si qua inuecta diritas casu foret,</l><l> Ne me imparatum
                            cura laceraret repens.</l></quote>
                    <bibl resp="#CN"><author ref="#cic">Cic.</author>, <title ref="#cic_tusc"
                            >Tusc.</title>
                        <biblScope>3.29</biblScope>. <note>Vu les petites nuances, il est possible
                            que Cicéron ait eu sous les yeux un texte légèrement
                        différent.</note></bibl>
                </cit></ab>

            <ab corresp="#Soph1584_Camerarius_p1_16" type="trad">« Car me souvenant des leçons d’un
                homme sage, j’imaginais en mon for intérieur de futurs malheurs, une mort cruelle ou
                la fuite désolante de l’exil, ou j’avais en tête toujours quelque autre malheur bien
                lourd, en sorte que si un sinistre coup du sort venait à m’agresser, la calamité
                subreptice ne me torturât point sans que j’y fusse préparé ». </ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_17"><seg type="allusion"> Huic obiecta
                    ignobilitas generis fuit a comicis, quod multi refellerunt.<bibl resp="#SG"
                        type="nontrouve"/>
                </seg></ab>

            <ab corresp="#Soph1584_Camerarius_p1_17" type="trad" ana="#V">Lui fut reprochée par les
                Comiques la bassesse de sa naissance, ce que beaucoup ont démenti. </ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_18"><seg type="allusion"> Fabulas edidit,
                    ut alii septuaginta quinque, ut alii nonaginta tres.<bibl resp="#SG"
                        type="nontrouve"/>
                </seg></ab>

            <ab corresp="#Soph1584_Camerarius_p1_18" type="trad" ana="#V">Il a donné des pièces au
                nombre de soixante-quinze selon les uns, quatre-vingt-treize selon d’autres. </ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_19">Palmam quinquies obtinuit, atque adeo
                quater in uita tantum nam quinta iam defuncto uita obtigit.</ab>

            <ab corresp="#Soph1584_Camerarius_p1_19" type="trad" ana="#V">Il a eu cinq fois la palme
                mais seulement quatre fois de son vivant car la cinquième lui fut octroyée à titre
                posthume. </ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_20"><seg type="allusion">
                    <persName ref="#soph">Sophoclem</persName> ualde multas fabulas edidisse
                    tradunt, uicisseque XXIV.<bibl resp="#SG" type="nontrouve"/>
                </seg></ab>

            <ab corresp="#Soph1584_Camerarius_p1_20" type="trad" ana="#V">Sophocle, dit-on, écrivit
                de nombreuses pièces et obtint vingt-quatre victoires.</ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_21"><cit type="nonref">
                    <quote type="traduction">Hoc mortuo cum esset hostilis exercitus Lacedaemoniorum
                        in Attica, uidere uisum ducem tradunt assistentem sibi dormienti Bacchum
                        mandare, ut conuenientibus inferiis prosequeretur Nouam Sirena : hoc illum
                        cognita morte poetae, de ipso et poematis eius interpretatum.</quote>
                    <bibl resp="#DC"><author ref="#paus">Paus.</author>, <title ref="#paus_gr"
                            >Description de la Grèce</title>, <biblScope>1.21.1</biblScope>. <note>
                            La <title>Vie de Sophocle</title> raconte ainsi l’épisode : « On raconte
                            que l’on mit une Sirène de bronze (ou une enchanteresse) sur son
                            tombeau. Et comme les Lacédémoniens avaient mis là une fortification
                            pour empêcher les Athéniens, Dionysos apparut en songe à Lysandre et lui
                            demanda de laisser mettre le corps au tombeau. Lysandre n’en tint pas
                            compte et Dionysos lui apparut de nouveau pour lui faire la même
                            demande. Lysandre demanda alors qui était le défunt et apprenant qu’il
                            s’agissait de Sophocle, envoya un héraut pour laisser passer le mort ».
                        </note></bibl>
                </cit></ab>

            <ab corresp="#Soph1584_Camerarius_p1_21" type="trad" ana="#V">A sa mort, alors que
                l’armée des ennemis Lacédémoniens était en Attique, on raconte que leur général crut
                voir Bacchus lui demander en songe de suivre la nouvelle Sirène pour des obsèques
                convenables et que, ayant appris la mort du poète, il interpréta qu’il s’agissait de
                lui et sur ses pièces.</ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_22"><seg type="allusion"> Solent autem
                    orationum uim laudantes cum Sirenum cantibus conferre.<bibl resp="#SG"
                        type="nontrouve"/>
                </seg></ab>

            <ab corresp="#Soph1584_Camerarius_p1_22" type="trad">Les panégyristes
                comparent d’ordinaire la force des discours au chant des Sirènes.</ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_23">Inuenio et <foreign xml:lang="grc"
                    >μέλιτταν ἀττικήν</foreign>, hoc est, atticam apiculam nominatum, <seg
                    type="allusion"> deque morte eius tradi, quod <persName>Anacreonti</persName>
                    <persName ref="#ual_max">Valerius</persName> attribuit. <bibl resp="#CN"><author
                            ref="#ual_max">Val. Max.</author>, <title ref="#ual_max_dfm">Dictorum
                            factorumque memorabilium libri</title>, <biblScope>9.12.8</biblScope>.
                            <note>Parmi d’autres hypothèses sur sa mort, Sophocle se serait étouffé
                            avec un grain de raisin : c’est précisément ainsi que serait mort
                            Anacréon. </note></bibl>
                </seg></ab>

            <ab corresp="#Soph1584_Camerarius_p1_23" type="trad" ana="#V">Je vois aussi qu’on
                l’appelle <foreign xml:lang="gr">μέλιττα ἀττικὴ</foreign>, c’est-à-dire l’abeille
                d’Attique et qu’on raconte de sa mort ce que Valerius attribue à
                    Anacréon.<note>Parmi d’autres hypothèses sur sa mort, Sophocle se serait étouffé
                    avec un grain de raisin : c’est précisément ainsi que serait mort Anacréon.
                </note>
            </ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_24">Vnde in epigrammate, hac de causa (ut
                uidetur) uotum extat: quo optatur hic in poetam. <cit type="nonref">
                    <quote type="traduction"><l>Semper taurigenis apibus stillare se pulchrum</l>
                        <l>Manet Hymettei mellis ut inde liquor.</l></quote>
                    <bibl resp="#SG"><title ref="#anth_grc">Anth. Gr.</title>
                        <biblScope>7.36</biblScope>.</bibl>
                </cit></ab>
            <ab type="trad" corresp="#Soph1584_Camerarius_p1_24" ana="#V">Et c’est pourquoi
                semble-t-il est formulé pour ce poète dans une épigramme le vœu « que son tombeau
                soit toujours entouré d'abeilles, filles du bœuf, pour que demeure du miel
                d'Hymettus ».</ab>
            <ab type="orig" xml:id="Soph1584_Camerarius_p1_25">De morte eius idem traditur, <seg
                    type="paraphrase"> quod <persName>Anacreonti</persName>
                    <persName ref="#ual_max">Valerius</persName> attribuit , nimirum, adhaerescente
                    gutture uua passa, eum suffocatum esse.<bibl resp="#CN"><author ref="#ual_max"
                            >Val. Max.</author>, <title ref="#ual_max_dfm">Dictorum factorumque
                            memorabilium libri</title>, <biblScope>9.12.8</biblScope>. <note>Parmi
                            d’autres hypothèses sur sa mort, Sophocle se serait étouffé avec un
                            grain de raisin : c’est précisément ainsi que serait mort Anacréon.
                        </note></bibl></seg>
            </ab>
            <ab type="trad" corresp="#Soph1584_Camerarius_p1_25" ana="#V">Sur sa mort, on raconte
                aussi qu'il fut étouffé par un grain de raisin qui lui avait obstrué la gorge, ce
                que Valère Maxime attribue à Anacréon.</ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_26"><seg type="reference">Id quod et in
                        <persName ref="#sim">Simonidis</persName> epigrammate legitur<bibl
                        resp="#SG" type="nontrouve"/></seg>, sed alii aliter memorant. </ab>
            <ab type="trad" corresp="#Soph1584_Camerarius_p1_26">C'est ce qu'on lit dans une
                épigramme de Simonide, mais d'autres en rapportent des versions différentes.</ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_27">Ingens studium fuit in hoc poeta
                exprimendi sententias <persName ref="#hom">Homericas</persName>, ita, <seg
                    type="allusion"> ut <foreign xml:lang="gr">φιλόμηρον</foreign> Graeci eum
                        uocarint.<bibl resp="#SG" type="nontrouve"/>
                </seg></ab>
            <ab type="trad" corresp="#Soph1584_Camerarius_p1_27" ana="#V">Il y eut chez ce poète si
                grand zèle pour rendre les mots d'Homère que les Grecs l'appelèrent « ami d'Homère
                ».</ab>

            <ab type="orig" xml:id="Soph1584_Camerarius_p1_28"><seg type="allusion"> Et
                        <persName>Polemon</persName> dicere solitus fuit <persName ref="#soph"
                        >Sophoclem</persName> esse <persName ref="#hom">Homerum</persName> Tragicum
                    et <persName ref="#hom">Homerum</persName> epicum <persName ref="#soph"
                        >Sophoclem</persName>. <bibl resp="#CN"><author ref="#dl">DL.</author>,
                            <title ref="#dl_vpi">Vie des Philosophes illustres</title>,
                            <biblScope>4.20</biblScope>.</bibl>
                </seg></ab>
            <ab type="trad" corresp="#Soph1584_Camerarius_p1_28" ana="#V">Et Polémon avait coutume
                de dire que Sophocle était l'Homère tragique et Homère le Sophocle épique.</ab>

        </body>
    </text>
</TEI>
