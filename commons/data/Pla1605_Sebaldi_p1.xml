<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Aliud</title>
                <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : "la" pour le latin et "grc" pour le grec -->
                <!-- Attention ! ne pas faire de retour charriot dans le titre -->
                <author ref="#sebaldi_uitus">Vitus Sebaldi</author>
                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Ajouter autant de tâches (<respStmt>) que nécessaire -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Révision</resp>
                    <name>Christian NICOLAS</name>
                    <name>Mathieu FERRAND</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
            <sourceDesc>
                <bibl>
                    <title>M. Acci Plauti Lat. comoediae facile principis fabulae XX superstites,
                        cum novo et luculento commentario doctorum virorum opera Friderici Taubmani,
                        Professoris Acad.</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#pl">Plautus</author>
                    <pubPlace ref="#uitteberga">Vittebergae</pubPlace>
                    <publisher ref="#meissnerus_wolffgangus">Wolffgangus Meissnerus</publisher>
                    <publisher ref="#schurerus_zacharia">Zacharia Schurerus</publisher>
                    <date when="1605">1605</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor ref="#taubmanus_fridericus">Fridericus Taubmanus</editor>
                    <ref
                        target="https://www.google.fr/books/edition/M_AcciI_Plauti_Lat_Comoediae_facile_prin/XjtpAAAAcAAJ?hl=fr&amp;gbpv=1&amp;dq=Risus+magister,+et+leporis+conditor&amp;pg=PP12&amp;printsec=frontcover"
                        >https://www.google.fr/books/edition/M_AcciI_Plauti_Lat_Comoediae_facile_prin/XjtpAAAAcAAJ?hl=fr&amp;gbpv=1&amp;dq=Risus+magister,+et+leporis+conditor&amp;pg=PP12&amp;printsec=frontcover</ref>
                    <!-- pour les liens google book, remplacer les & par &amp; pour que ça fonctionne -->
                </bibl>
                <listBibl>
                    <bibl/>
                </listBibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p/>
            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

            <textClass n="vers"></textClass>

        </profileDesc>
        <revisionDesc>
            <change when="2022-03-25">Sarah GAUCHER : crétion de la TEI, encodage de la traduction
                et de la transcription</change>
        </revisionDesc>
    </teiHeader>

    <text>
        <body ana="#PA">
            <head type="orig" xml:id="Pla1605_Sebaldi_p1_0">Aliud</head>
            <head type="trad" corresp="#Pla1605_Sebaldi_p1_0">Un autre</head>
            <ab type="orig" xml:id="Pla1605_Sebaldi_p1_1"><l>En Nox ipsa Dies ! en Vmber Lucius !
                    euge </l><l>Humani non hoc est opus ingenii.</l></ab>
            <ab type="trad" corresp="#Pla1605_Sebaldi_p1_1">Voici que la Nuit est le Jour ! Voici
                que l’Ombrien est Lumineux ! Bravo ! Ce n’est pas l’œuvre du génie humain.<note>Pour
                    un jeu similaire, voir <ref target="Sq1613_Tomkins_p1_1"
                        >Sq1613_Tomkins_p1_1</ref></note></ab>
            <ab type="orig" xml:id="Pla1605_Sebaldi_p1_2"><l>Aut praeeunte isthaec <persName
                        ref="#taubmanus_fridericus">Taubmanus</persName> Apolline fecit, </l><l>Ipso
                    in <persName ref="#taubmanus_fridericus">Taubmano</persName> aut ipsus Apollo
                    fuit. </l></ab>
            <ab type="trad" corresp="#Pla1605_Sebaldi_p1_2">Ou bien Taubmanus a fait cela alors
                qu’Apollon lui ouvrait la voie, ou bien Apollon lui-même fut en Taubmanus.</ab>
            <ab type="orig" xml:id="Pla1605_Sebaldi_p1_3"><l>Ne magnas grates <persName
                        ref="#taubmanus_fridericus">Taubmanus</persName> Apollo meretur,
                    </l><l>Vmbroso qui Vmbro lumina restituit.</l>
            </ab>
            <ab type="trad" corresp="#Pla1605_Sebaldi_p1_3">Voilà Taubmanus Apollon mérite d’être
                grandement remercié, lui qui a rendu la lumière à l’ombreux Ombrien.</ab>
            <ab type="orig" xml:id="Pla1605_Sebaldi_p1_4"><persName ref="#sebaldi_uitus">Vitus
                    Sebaldi</persName> Heilsbron Franconiae.</ab>
            <ab type="trad" corresp="#Pla1605_Sebaldi_p1_4">Vitus Sebaldi Heilsbron Franconie.</ab>


        </body>
    </text>
</TEI>
