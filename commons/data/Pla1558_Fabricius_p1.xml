<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Ornatissimo maximis uirtutibus et summa doctrina uiro Ioaschimo
                    Camerario, amico suo semper obseruando, Georgius Fabricius S. D.</title>
                <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : "la" pour le latin et "grc" pour le grec -->
                <!-- Attention ! ne pas faire de retour charriot dans le titre -->
                <author ref="#fabricius_georgius">Georgius Fabricius</author>
                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Ajouter autant de tâches (<respStmt>) que nécessaire -->
                <respStmt>
                    <resp>Transcription</resp>
                    <persName>Bérénice JARROSSAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <persName>Christian NICOLAS</persName>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>

                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <principal>Malika BASTIN-HAMMOU</principal>
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne Garcia-Fernandez</persName>
                </respStmt>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
            <sourceDesc>

                <bibl>
                    <title>M. Accii Plauti Comoediae XX. diligente cura et singulari studio Ioachimi
                        Camerarii Pabepergensis emendatius nunc quam ante unquam ab ullo editae :
                        Adiectis etiam eiusdem ad singulas comoedias argumentis et annotationibus.
                        Accesserunt iam indicationes quoque multorum quae ad lectionem fabularum
                        Plauti nonnihil momenti afferre possunt, a Georgio Fabricio Chemnicensi
                        collectae</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#pl">Plautus</author>
                    <pubPlace ref="#basilea">Basilae</pubPlace>
                    <publisher ref="#heruagius_ioannes">Ioannes Heruagius</publisher>
                    <publisher ref="#brand_bernhardus">Bernhardus Brand</publisher>
                    <date when="1558">1558</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor ref="camerarius_ioachimus">Ioachimus Camerarius</editor>
                    <ref
                        target="https://www.digitale-sammlungen.de/en/view/bsb10995370?page=872,873"
                        >https://www.digitale-sammlungen.de/en/view/bsb10995370?page=872,873</ref>
                </bibl>

                <listBibl>
                    <bibl>recopier le texte de la référence d'un titre tel qu'il est écrit dans le
                        fichier TT</bibl>
                    <!-- créer un <bibl> par référence -->
                </listBibl>

            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Franciscus Marcellus a été évêque de Trogir de 1488 à 1524.</p>
                <!-- s'il s'agit d'une liste, utiliser les éléments ci-dessous -->
                <list>
                    <item/>
                    <item/>
                    <item/>
                </list>
            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>
        <revisionDesc>
            <change when="2022-02-28">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
            <change when="2021-05-02">Sarah GAUCHER : création de la fiche TEI, encodage de la
                transcription et de la traduction</change>
        </revisionDesc>
    </teiHeader>

    <text>
        <body ana="#LI">

            <head type="orig" xml:id="Pla1558_Fabricius_p1_0">Ornatissimo maximis uirtutibus et
                summa doctrina uiro <persName ref="#camerarius_ioachimus" role="destinataire"
                    >Ioachimo Camerario</persName>, amico suo semper obseruando, <persName
                    ref="#fabricius_georgius">Georgius Fabricius</persName> S. D.</head>
            <head type="trad" corresp="#Pla1558_Fabricius_p1_0">À l’homme très orné des plus grandes
                vertus et de la meilleure science, Joachim Camerarius, son ami toujours respectable,
                    <seg>Georgius Fabricius<note>Georgius Fabricius (1516-1551), de Chemnitz, ou
                        Georg Goldschmidt, humaniste, épigraphiste et éditeur. On lui doit notamment
                        le premier recueil d’inscriptions latines juridiques. Il a aussi édité
                        Térence, Horace, Virgile et une œuvre personnelle. Il figure comme
                        contributeur dans l’édition de Camerarius : <title>Accesserunt iam
                            indicationes quoque multorum quae ad lectionem fabularum Plauti nonnihil
                            momenti afferre possunt, a Georgio Fabricio Chemnicensi
                            collectae</title> ; il a donc cédé à son ami des notes plautiniennes. On
                        peut supposer ainsi, vu la place de ce paratexte, juste avant les annexes
                        pourrait-on dire, qu’il est l’auteur (implicite) de la collecte des <foreign
                            xml:lang="la">testimonia ueterum</foreign> et des fragments
                        plautiniens.</note></seg> donne le bonjour.</head>
            <ab type="orig" xml:id="Pla1558_Fabricius_p1_1">Ad Cal. Quintil. te curaturum quaedam
                Basileam per mercatores Norimbergenses ad me scripsisti, quibus, nisi molestum est,
                uelim adiungas hunc fasciculum qui Oporino reddendus est.</ab>
            <ab type="trad" corresp="#Pla1558_Fabricius_p1_1"><seg>Tu m’as écrit qu’aux alentours du
                    1er juillet, tu t’occuperais d’envoyer à Bâle, par l’intermédiaire de marchands
                    de Nuremberg, des éléments auxquels je voudrais que tu ajoutes ce
                        fascicule<note>Il s’agit donc d’un échange de bons procédés : Camerarius
                        envoie des notules à ajouter à un livre en préparation de Fabricius, lequel
                        lui envoie ses fragments plautiniens.</note></seg> qu’il faut remettre à
                        <seg>Oporinus<note>C’est-à-dire Hans Herbst / Jean Oporin (1507-1568),
                        célèbre imprimeur de Bâle qui a beaucoup œuvré pour la renaissance des
                        classiques.</note></seg>.</ab>
            <ab type="orig" xml:id="Pla1558_Fabricius_p1_2">Mitto ad eum Romam meam, ut cum
                    <persName>Marliani</persName> libris, quos sub praelo habere dicitur,
                coniungatur.</ab>
            <ab type="trad" corresp="#Pla1558_Fabricius_p1_2">Je lui envoie ma
                        <seg><title>Roma</title><note>Il s’agit d’un recueil d’inscriptions,
                            <title>Roma. Antiquitatum libri duo, ex aere, marmoribus... collecti, ab
                            eodem.</title></note></seg>, pour qu’on l’ajoute aux livres de
                        <seg>Marlianus<note>Peut-être Raimundus Marlianus, (14..-15..), auteur d’un
                        index des commentaires de César ?</note></seg>, qui sont sous presse,
                dit-on.</ab>
            <ab type="orig" xml:id="Pla1558_Fabricius_p1_3">Ex antiquis grammaticis, dum alia
                inuestigo, locos, ut uides, <persName ref="#pl">Plautinos</persName> collegi, quos
                ad te mitto, tuoque iudicio permitto, quicquid de iis statueris.</ab>
            <ab type="trad" corresp="#Pla1558_Fabricius_p1_3" ana="#PH">À partir des grammairiens
                anciens, à la recherche d’autre chose, j’ai rassemblé les passages tirés de Plaute ;
                je te les envoie et les remets à ton jugement, quoi que tu décides à leur
                sujet.</ab>
            <ab type="orig" xml:id="Pla1558_Fabricius_p1_4"><title>Theognidem</title> tuum quando
                expectare debeamus, cum otium erit, mihi significa.</ab>
            <ab type="trad" corresp="#Pla1558_Fabricius_p1_4">Quant à ton <title>Théognis</title>,
                quand tu auras le temps, fais-moi savoir quand on doit l’attendre.</ab>
            <ab type="orig" xml:id="Pla1558_Fabricius_p1_5">Vale. Misenae, 4 Cal. Iulii MDL</ab>
            <ab type="trad" corresp="#Pla1558_Fabricius_p1_5">Adieu. Misène, 29 mai 1550.</ab>

        </body>
    </text>
</TEI>
