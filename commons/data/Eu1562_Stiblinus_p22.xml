<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Praefatio Gaspari Stiblini in Ionem</title>
                <author ref="#stiblinus_stiblinus">Gasparus Stiblinus</author>
                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <respStmt>
                    <resp>Transcription</resp>
                    <persName>Sarah GAUCHER</persName>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <persName>Christian NICOLAS</persName>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne Garcia-Fernandez</persName>
                </respStmt>
            </titleStmt>

            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>
            <sourceDesc>
                <bibl>
                    <title>Euripides Poeta Tragicorum princeps, in Latinum sermonem conuersus,
                        adiecto eregione textu Graeco : CUM ANNOTATIONIBUS ET PRAEFATIONIBUS in
                        omnes eius Tragoediae, autore GASPARO STIBLINO, Accesserunt JACOBI MICYLLI,
                        de Euripidis uita, ex diuersis autoribus collecta : item, De tragoedia et
                        eius partibus προλεγομένα quaedam. Item IOANNIS BRODAEI Turonensis
                        Annotationes doctissimae numquam antea in lucem editae. Ad haec Rerum et
                        uerborum toto Opere Praecipue memorabilium copiosus INDEX. Cum caesaris
                        Maiestatis et Christianissimi Gallorum Regis gratia ac priuilegio, ad
                        decennium. Basileae, per Ioannem Oporinum.</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#eur">Euripides</author>
                    <pubPlace ref="#basilea">Basilea</pubPlace>
                    <publisher ref="#oporinus_ioannes">Iohannes Oporinus</publisher>
                    <date when="1562">1562</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor ref="#stiblinus_gasparus">Gasparus Stiblinus</editor>
                    <editor role="traducteur" ref="#stiblinus_gasparus">Gasparus Stiblinus</editor>
                    <!-- pour les liens google book, remplacer les & par &amp; pour que ça fonctionne -->
                </bibl>

                <listBibl>
                    <bibl/>
                    <!-- créer un <bibl> par référence -->
                </listBibl>

            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p/>
            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>
        <revisionDesc>
            <change when="2022-02-19">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
            <change when="2021-12-15">Sarah GAUCHER : Encodage de la transcription et de la
                traduction</change>
            <change when="2021-11-19">Sarah GAUCHER : création de la fiche TEI et remplissage du
                Header</change>
        </revisionDesc>
    </teiHeader>

    <text>
        <body ana="#A">

            <head type="orig" xml:id="Eu1562_Stiblinus_p22_0">Praefatio <persName
                    ref="#stiblinus_stiblinus">Gaspari Stiblini</persName> in <title ref="#eur_ion"
                    >Ionem</title>.</head>
            <head type="trad" corresp="#Eu1562_Stiblinus_p22_0">Préface de Gaspar Stiblinus sur
                    <title>Ion</title>.</head>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_1">Haec fabula proprie pertinet ad laudem
                principatus Atheniensium.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_1" ana="#PE #TH_fin">Cette pièce participe proprement de
                l’éloge de la royauté athénienne.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_2">Hos enim ostendit aborigines nec aliunde
                insitos esse, deinde habere praesidem Mineruam, sapientiae ac pulcherrimarum artium
                antistitem, praeterea a solertissimis ducibus qui genus a diis trahant gubernari,
                denique gentibus longe lateque tum in Asia tum in Europa, quae etiam num a primis
                domitoribus cognominantur, imperare.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_2">Car elle montre les Athéniens comme
                des indigènes, et non des étrangers, qui ont ensuite comme divinité tutélaire
                Minerve, en charge de la sagesse et des beaux-arts, qui en outre sont gouvernés par
                des chefs très habiles qui tirent leur origine des dieux, qu’enfin ils dominent sur
                une vaste zone les peuples d’Asie comme d’Europe (qui aujourd’hui encore tirent leur
                nom de leurs premiers conquérants).</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_3">Verisimile siquidem est poetas Graecos
                sub inuolucro fabularum non raro inseruire uoluisse laudibus tum ceterarum Graeciae
                urbium tum Athenarum, ubi non solum omnes artes inuentae florebant ac emporium omnis
                eruditionis et philosophiae in toto orbe celeberrimum erat, sed etiam belli studia
                uigebant: id enim Poeta noster in <title ref="#eur_suppl">Supplicibus</title> quoque
                et <title ref="#eur_her">Heraclidis</title> luculenter significat.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_3" ana="#TH_fin #POET">Certes il est vraisemblable que les
                poètes grecs, sous le couvert fréquent des mythes, aient voulu contribuer à faire
                l’éloge des cité grecques et surtout d’Athènes, où non seulement fleurissaient les
                arts qu’on y avait inventés et où se trouvait le lieu d’échange le plus fréquenté de
                l’érudition et de la philosophie, mais aussi où l’art de la guerre était à son
                apogée ; car c’est ce que montre notre poète dans <title>Les Suppliantes</title> et
                dans <title>Les Héraclides</title>.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_4">Et Boeotia <persName ref="#hom"
                    >Homeri</persName> nihil aliud esse uidetur quam encomium Graeciae et
                finitimarum insularum.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_4">Et la Béotie d’Homère ne semble rien
                d’autre qu’un éloge de la Grèce et de ses îles voisines.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_5">Sunt praeterea non contemnendi morum
                typi obseruandi in praesenti Dramate.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_5" ana="#TH_dram #PE">En outre, il ne faut pas négliger la
                typologie de caractères qu’on peut observer dans le présent drame.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_6">In quo primum Creusa sterilitate, stupri
                et expositi pueri conscientia, adulteri (talem putabat) mariti odio, necandi noui
                filii uiri studio miserrima, sed quae mox commutatis rebus et filium et summas
                commoditates ac plane mare bonorum reperiat insperato.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_6">D’abord Créuse, que sa stérilité, sa
                culpabilité d’avoir été violée et d’avoir abandonné un enfant, la haine de son mari
                adultère (à ce qu’elle croyait) et le désir de supprimer ce récent fils de son mari
                rendaient très malheureuse, mais qui bientôt, par un revers de fortune, trouve un
                fils, les plus grands avantages et un océan de bonheurs tout à fait inespérés.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_7">Ea inquam nos docet, ut in utraque
                fortuna simus animo pari: hoc est, firmo et imperturbato: ita ut neque secundis
                efferamur nec rursus aduersis deiiciamur, sed cogitemus nihil in hac uita stabile,
                fidum aut sincerum esse; recte autem agentes homines ac pietatis studiosos
                periclitari aliquando posse, sed nunquam destitui a numine.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_7" ana="#TH_dram #PE">Elle, dis-je, nous montre à être dans
                le bonheur et le malheur de même sentiment, c’est-à-dire fermes et imperturbables,
                sans être ni au plus haut en cas de chance, ni au plus bas en cas de malchance, mais
                à penser que rien ici-bas n’est stable, fiable et sincère, mais que même les hommes
                qui agissent bien et qui pratiquent la piété peuvent être en danger parfois, mais
                que jamais on n’est abandonné par la divinité.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_8">Ion inter sacerdotes et pios homines
                educatus, et ipse pius ac bonus euasit.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_8">Ion, élevé parmi les prêtres et les
                hommes pieux, est lui-même devenu pieux et bon.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_9">Vnde intelligere debemus quantum momenti
                ad bonam mentem recta et sana institutio et quotidianae uitae pia semperque ob
                oculos uersantia exempla habeant.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_9" ana="#PE">D’où nous devons comprendre
                l’importance qu’ont pour faire une belle âme une éducation saine et de pieux
                exemples de la vie quotidienne mis sans cesse sous les yeux.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_10">Chorus fidelis famulitii quod omnia ad
                salutem et commodum dominorum refert typum exhibet.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_10" ana="#PE">Le chœur montre le type de serviteur
                fidèle qui rapporte tout au salut et à l’intérêt de ses maîtres.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_11">Senex aperti quidem et sinceri, sed
                parum felicis consiliarii specimen praebet, qui commoto et infesto animo (quos
                affectus in feriis deliberationibus longe abesse conuenit) instigat eram in necem
                filii noui et uiri; cuius consilio si paruisset aut si successus conatui
                respondisset, et se et omnem Atheniensium ducum stirpem funditus euertisset.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_11" ana="#PE">Le vieillard présente la catégorie du
                conseiller ouvert, certes, et sincère, mais qui a peu de succès, qui, par son
                tempérament émotif et hostile (sentiments qui sont bien loin de convenir aux
                délibérations sérieuses), pousse sa maîtresse au meurtre de ce nouveau fils et de
                son mari ; et si elle avait obéi à ce conseil ou si sa tentative avait été couronnée
                de succès, elle eût éradiqué et elle-même et la souche des rois d’Athènes.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_12">Ita saepe usu uenit, ut aetate uerendi
                et qui ex longo uitae usu prudentes uideri possint hallucinentur pestiferaque
                consulant principibus.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_12">Il est ainsi très fréquent que ceux
                que leur âge rend vénérables et qui, par la longue expérience vécue, semblent avisés
                s’aveuglent et prodiguent des conseils mortifères aux princes.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_13">Quare hic in primis cautos et
                circumspectos esse eos oportet ne specie boni decepti noxia pro salutaribus consilia
                admittant.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_13">Il faut donc que les princes soient
                en l’espèce surtout prudents et circonspects pour éviter que, trompés par une
                apparence de bien, ils n’admettent des conseils néfastes en guise de
                salutaires.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_14">Argumentum etiam breuiter
                subiiciemus.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_14">Nous ajouterons un bref
                argument.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_15">Xuthus externus ob egregiam
                Atheniensibus in bello operam nauatam et Creusae Erechthei filiae connubio et
                principatu donatus est.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_15">Xouthos, un étranger, pour ses
                éminents services rendus aux Athéniens pendant une guerre, se vit donner Créuse,
                fille d’Érechthée, en mariage, et le trône.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_16">Hi cum aliquandiu consueuissent nec
                prolem procrearent, Delphos abierunt scitatum oracula Phoebi.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_16">Tous deux, après une longue union
                sans engendrer d’enfant, allèrent à Delphes pour interroger l’oracle de Phébus.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_17">Ibi Apollo, ut celaretur uitium quod
                obtulerat olim Creusae adhuc uirgini maneretque sua turpitudo sepulta, Ionem ex
                Creusae illo compressu natum ac a Mercurio iussu Phoebi Delphos delatum illicque
                educatum Xutho, tamquam ex ancilla furtiuo concubitu genitum, filium dat.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_17">Là, Apollon, sans révéler le viol
                qu’il avait jadis commis sur Créuse encore vierge et gardant sa faute cachée, donne
                Ion, le fils qu’il a eu de son union avec Créuse et que Mercure, sur ordre de
                Phébus, a amené à Delphes où il a été élevé, comme fils à Xouthos, comme s’il
                l’avait conçu d’une liaison secrète avec une servante.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_18">Qua re laetus Xuthus epulas publicas,
                nataliumque celebrationem magnificam nouo filio instituit.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_18">Satisfait de la nouvelle, Xouthos
                organise un banquet public et une magnifique cérémonie de naissance pour ce nouveau
                fils.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_19">Creusa uero adulterum maritum rata
                conari spuriam prolem in regiam domum introducere uenena parat Ioni; qua re
                deprehensa ad necem poscitur infelix mulier.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_19">Mais Créuse, croyant que son adultère
                d’époux cherche à introduire son bâtard au palais, prépare un poison pour Ion ;
                trompée par les apparences, la malheureuse est appelée au meurtre.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_20">Sed interueniente subito Pythia ac
                prolatis cum arca (in qua Ion expositus fuerat) symbolis, discutitur
                error,agnosciturque Ion Creusae filius, unde mutua gratulatio fit.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_20">Mais la Pythie intervient aussitôt,
                apporte, avec le berceau (où Ion avait été abandonné), les preuves familiales, la
                méprise est dissipée, Ion est reconnu pour le fils de Créuse, d’où se produit une
                joie mutuelle. </ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_21">Quin et Minerua ipsa, ne quid amplius
                haererent, Phoebi consilia quibus ista sic euenirent et futuram Erechthidarum
                felicitatem ac potentiam aperit. Mater ergo cum inuento filio laeta Athenas
                reuertitur.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_21">Et même Minerve en personne, pour
                dissiper toute hésitation, révèle les intentions de Phébus qui ont causé toute
                l’affaire et la félicité et puissance futures des descendants d’Érechthée. La mère
                s'en retourne donc à Athènes avec le fils qu'elle a retrouvé.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_22">Argumentum actus primi</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_22">Argument de l’acte 1.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_23">Prologus totius argumenti habet
                    <foreign xml:lang="grc">κατασκευήν</foreign>: nimirum Creusae genus,
                consuetudinem cum Apolline, expositum Ionem, et eiusdem educationem; Xuthi item
                peregrini cum Creusa conubium et eiusdem successum.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_23">Le prologue contient la préparation
                de tout l’argument : la généalogie de Créuse, sa liaison avec Apollon, l’abandon
                d’Ion et son éducation ; et aussi le mariage de l’étranger Xouthos avec Créuse et
                ses succès.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_24">2 Ion suum studium ac ministerium cui
                praesit iactat, Phoebum interim faustis precationibus et hymnis prosequens.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_24">2 Ion vante son zèle et le ministère
                qu’il préside, tout en suivant Phébus de prières propitiatoires et d’hymnes.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_25">3 Mulieres famulae Creusae mirantur
                augustam structuram et elegantes picturas templi Apollinis apud Delphos, explicante
                eas ipsis Ione custode sacrarum aedium.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_25">3. Les servantes de Créuse admirent
                l’imposante structure et les élégantes peintures du temple d’Apollon à Delphes, que
                leur explique Ion, gardien du sanctuaire.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_26">4 Colloquium Creusae et Ionis in quo
                illa occulte insinuat suum concubitum cum Apolline et expositum partum, non
                intelligente tamen id Ione.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_26">4 Dialogue entre Créuse et Ion où
                elle insinue de façon cryptée sa liaison avec Apollon et l’enfant abandonné, sans
                qu’Ion le comprenne pourtant.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_27">Vnde et Deum a tanto probro defendit
                improbatque mulieris temeritatem, ut quae ausit Phoebum stupri insimulare.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_27">D’où il disculpe le dieu d’une faute
                si grave et s’en prend à cette femme qui ose accuser Phébus de viol.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_28">5 Interea Xuthus a Trophonii specu
                reuersus salutat uxorem et quid ibi egerit, quidque porro tum ipse tum Creusam
                facere uelit exponit.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_28">5 Entre temps Xouthos revenu de la
                caverne de Trophonios salue sa femme et explique ce qu’il y a fait et ce qu’il
                attend de voir faire désormais par lui et par Créuse.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_29">Ion autem nondum intelligens quidnam
                Creusa obliquis suis sermonibus sibi uelit, Apollinem, siquidem illa uera refert,
                acerbe reprehendit, ut qui nihil magis absurdum putet quam talia probra de diis
                dici.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_29">Mais Ion, qui continue à ne pas
                comprendre le sens des propos énigmatiques de Créuse, s’en prend sévèrement à
                Apollon, au cas où elle dise vrai, en homme qui croit qu’il est parfaitement absurde
                de dire de telles insultes aux dieux.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_30">6 Chorus primum numina deorum inuocat
                pro faustis responsis et propagatione Erechthei generis, cuius spes in una
                fecunditate Creusae erat.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_30">6 Le chœur invoque d’abord la
                puissance divine pour obtenir des réponses favorables et pour la propagation de la
                race d’Érechthée, dont l’espérance reposait sur la seule fertilité de Créuse.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_31">Deinde commendat liberorum
                procreationem, quorum et uoluptatem et commoda extollit.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_31">Puis il fait l’éloge de la naissance
                d’enfants, dont il exalte le plaisir et les avantages.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_32">Denique Panem et nymphas Atticas non
                sine miseratione Creusae implorat.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_32">Enfin il implore Pan et les nymphes
                d’Attique, rappelant le sort pitoyable de Créuse.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_33">Argumentum actus secundi.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_33">Argument de l’acte 2.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_34">Xuthus ex adytis oraculi rediens ex
                responso Ionem ut filium salutat. </ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_34">Xouthos revient du sanctuaire de
                l’oracle et, conformément à la réponse, salue Ion du nom de fils.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_35">Hac re inopinata ac noua ac plane (ut
                uidebatur) absurda commotus Ion non agnoscit illum patrem erroremque natum putat ex
                perperam intellecto oraculo.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_35">À cette nouvelle inattendue, inouïe
                et vraiment (semblait-il) absurde, Ion, troublé, ne le reconnaît pas pour son père
                et met la méprise sur le compte d’une mauvaise interprétation de l’oracle.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_36">Tandem uero post multam contentionem et
                patrem agnoscit Xuthum et optata ei offert oscula.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_36">Mais il finit, après un long débat,
                par reconnaître Xouthos pour son père et lui donne les baisers espérés.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_37">Error autem hic, quo scilicet Creusa
                Ionem suum esse ignorat, Xuthus ex se genitum putat, spargitur in sequentes usque
                Actus, epitasesque tandem ac motus atroces gignit.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_37">Mais cette méprise, par laquelle
                Créuse ignore qu’Ion est son fils et Xouthos croit qu’il est né de lui, court sur
                les actes suivants et génère des <seg>nœuds<note>Stiblinus utilise ici le terme
                            <foreign xml:lang="la">epitasis</foreign>, qu’on trouve chez Evanthius,
                        donc chez Donat, et que les doctes de la Renaissance prennent plaisir à
                        manier.</note></seg> et des émotions tragiques.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_38">2 Blande nouum filium tractat Xuthus
                eumque Athenas ad regnum et amplissimas opes inuitat.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_38">2 Xouthos traite ce nouveau fils avec
                tendresse et l’invite à Athènes vers le trône et d’abondantes richesses.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_39">Recusat istuc ire Ion idque
                elegantissima oratione, qua se dicit longe praeferre uitam humilem sed quietam,
                otiosam, periculis et molestiis carentem principum et regum diuitiis, quae raro aut
                tutae aut stabiles possideantur.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_39">Mais Ion refuse d’y aller, et ce dans
                un discours très élégant où il dit qu’il préfère de beaucoup une vie humble mais
                tranquille, sans charges, sans périls et sans ennuis aux richesses des princes et
                des rois, dont on a rarement une possession stable et sûre.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_40">3. Xuthus Ionem ad celebrationem
                natalium et epulas publicas quas ipsius nomine instructurus erat deducit.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_40">3 Xouthos emmène Ion célébrer sa
                naissance et un banquet public qu’il avait l’intention d’organiser en son nom.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_41">4. Chorus ex commiseratione dominae
                quam periclitaturam ex nouo illo filio uidebat primum de ipso euentu dubitat ac
                plane aliud portendi malum ista re coniicit.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_41">4 Le chœur a pitié de sa maîtresse,
                que l’arrivée de ce nouveau fils allait évidemment mettre en danger, commence par
                douter de l’événement et suppose que ce malheur en laisse présager un autre.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_42">Deinde Xuthum execratur, siquidem
                infidelis et perfidus in Creusam indigenam ipse peregrinus existat.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_42">Puis il maudit Xouthos, s’il a été
                infidèle et déloyal, lui un étranger, à l’égard de Créuse l’autochtone.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_43">Ita hic actus paulatim uiam praemunit
                ad epitases et turbas.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_43">Ainsi cet acte ouvre-t-il
                progressivement la voie aux nœuds et aux péripéties.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_44">Argumentum actus tertii.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_44">Argument de l’acte 3.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_45">Creusa adhuc ignorans quidnam spei
                procreandae subolis accepisset paedagogum ueterem domus Erechthidarum alumnum
                excitat ut una secum cognosceret ex uiro responsa Apollinis.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_45">Créuse, qui ignore encore quel espoir
                elle a obtenu de mettre au monde un enfant, enjoint un vieux pédagogue, esclave né
                dans la maison des Érechthides, à apprendre avec elle de son mari la réponse
                d’Apollon.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_46">At uero Chorus tristis interuenit
                nuntiatque dominae Xutho ab Apolline datum nothum filium.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_46">Alors le chœur affligé intervient et
                apprend à sa maîtresse que Xouthos s’est vu gratifier par Apollon d’un fils
                bâtard.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_47">Quae res Creusam in luctus et lacrimas
                coniicit.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_47">Cette nouvelle plonge Créuse dans le
                deuil et les larmes.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_48">Condolent afflictae dominae et senex et
                chorus famularum.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_48">Tous partagent la tristesse de leur
                maîtresse, le vieillard comme le chœur de servantes.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_49">2. Paedagogus ex coniecturis quibusdam
                addiuinat Xuthum furtiuo concubitu suscepisse Ionem simulque in eum grauiter
                inuehitur, ut quem etiam dolo una cum puero interficiendum suadeat.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_49">2 Le pédagogue, par quelques
                déductions, devine que Xouthos a conçu Ion lors d’une liaison clandestine,
                l’invective gravement et instille l’idée qu’il faut le tuer avec l’enfant.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_50">3 Creusa iam pro certo habens fidem a
                uiro proditam et ipsa non sine accusatione Apollinis et querela stuprum suum ac
                puerum expositum fatetur.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_50">3 Créuse désormais certaine d’avoir
                été trahie par son mari, avoue, elle aussi, en accusant Apollon et en se plaignant,
                le viol dont elle a été victime et l’abandon de l’enfant.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_51">Quam totam rem, ut facta erat, mox
                senex exquirit deque interficiendo puero simul cum ea consilium capit atque adeo
                ipse sibi huius occidendi partes deposcit.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_51">Le vieillard s’enquiert aussitôt du
                déroulé de toute l’affaire et prend avec elle la décision de supprimer l’enfant et
                réclame pour lui-même le rôle de l’assassin.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_52">Chorus primum precatur laetum successum
                consiliis Creusae.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_52">Le chœur commence par prier pour un
                heureux succès du plan de Créuse.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_53">Deinde Xuthum peregrinum et adulterum
                contendit indignum esse Atheniensium principatu.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_53">Puis il affirme que Xouthos, étranger
                et adultère, ne mérite pas le trône d’Athènes.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_54">Oblique etiam Phoebum perstringit, qui
                istam nequitiam suis quoque responsis et oraculis auctoritate muniat.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_54">Il effleure aussi indirectement
                Apollon, qui, par ses réponses et ses oracles, protège ces turpitudes de son
                autorité.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_55">Denique feminas, in quas poetae
                licentius adulterii probra dicant, defendit cum non minus hoc uitio, imo etiam magis
                perdite laborent uiri, id quod uel Xuthi exemplum satis declaret.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_55">Enfin il défend les femmes,
                auxquelles les poètes font bien librement reproche d’adultère, alors que ce vice est
                tout autant, voire bien plus gravement, l’apanage des hommes, ce dont l’exemple de
                Xouthos est un témoignage assez clair.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_56">Argumentum actus quarti.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_56">Argument de l’acte 4.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_57">Famulus quidam Choro patefectas
                insidias Creusae et eiusdem damnationem nuntiat: et longa elegantique narratione
                totum conuiuium adeoque rem omnem ut acta erat ordine persequitur.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_57">Un serviteur annonce au chœur que la
                ruse de Créuse est éventée et qu’elle-même a été condamnée et, dans un long et
                élégant récit, raconte le banquet et le déroulé de toute l’affaire dans
                l’ordre.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_58">2 Chorus ergo nouo malo perculsus
                euentum infelicem consiliorum erae exponit.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_58">2 Le chœur alors, ébranlé par ce
                nouveau malheur, développe l’issue fatale du projet de sa maîtresse.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_59">Argumentum actus quinti.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_59">Argument de l’acte 5.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_60">Iam nulla ratione periculum et uis a
                Creusa amplius prohiberi poterat cum subito Pythia Ionis nutrix interuenit,
                arculaque prolata in qua olim Ion expositus ac Delphis repertus fuerat, una cum
                symbolis quibusdam, moram praesenti regi interiicit utque ex his signis matrem
                inuestiget monet.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_60">Alors qu’aucune circonstance ne
                pouvait plus éviter à Créuse d’être condamnée et violentée, soudain intervient la
                Pythie, nourrice d’Ion ; elle apporte le petit berceau dans lequel Ion abandonné
                avait été trouvé à Delphes, avec les marques de reconnaissance, interjette appel
                d’un délai auprès du roi présent et lui recommande d’utiliser les signes de famille
                pour enquêter sur la mère.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_61">Hanc uenerabundus accipit Ion ; Creusa
                agnoscit ac euidentissimis argumentis planum facit se Ionis matrem esse.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_61">Ion l’accueille respectueusement ;
                Créuse le reconnaît et, devant des preuves si manifestes, clarifie qu’elle est la
                mère d’Ion.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_62">Vnde subito uota, mutua oscula et
                complexus, insperatarum rerum comites, se inuicem agnoscentium post periculosum
                errorem sequuntur.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_62">Alors s’ensuivent des vœux échangés,
                des baisers, des embrassades, qui accompagnent des événements inespérés et une
                reconnaissance mutuelle après une méprise dangereuse.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_63">2 Creusa exponit quomodo pepererit et
                ubi, sed haesitante nonnihil adhuc puero, subito Minerua dissolutura nodum adest
                omnemque rem prouidentia numinum sic gubernari et in posterum quoque successum Ionis
                et rerum Atheniensium diis curae fore dicit.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_63">2 Créuse explique comment et où elle
                a accouché, mais comme son fils est encore un peu hésitant, Minerve arrive d’un coup
                pour dissiper la méprise et elle déclare que toute cette histoire est dirigée par la
                providence divine et que les dieux auront à cœur le succès futur d’Ion et des
                affaires d’Athènes.</ab>
            <ab type="orig" xml:id="Eu1562_Stiblinus_p22_64">Ion ergo et Creusa repente mutatis
                rebus ad agendum gratias numinis benignitati excitantur.</ab>
            <ab type="trad" corresp="#Eu1562_Stiblinus_p22_64">Alors Ion et Créuse, devant ce
                soudain revirement de fortune, n’ont plus qu’à aller rendre grâces à la divinité
                pour sa bonté.</ab>


        </body>
    </text>
</TEI>
