<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

   <teiHeader>

      <fileDesc>

         <titleStmt>

            <title xml:lang="la">Praefatio Gaspari Stiblini in Heraclidas</title>
            <author ref="#stiblinus_gasparus">Gasparus Stiblinus</author>
            <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

            <respStmt>
               <resp>Transcription</resp>
               <persName>Sarah GAUCHER</persName>
            </respStmt>
            <respStmt>
               <resp>Traduction</resp>
               <persName>Sarah GAUCHER</persName>
            </respStmt>
            <respStmt>
               <resp>Révision</resp>
               <persName>Christian NICOLAS</persName>
            </respStmt>
            <respStmt>
               <resp>Encodage</resp>
               <name>Sarah GAUCHER</name>
            </respStmt>
            <principal>Malika BASTIN-HAMMOU</principal>
            <respStmt>
               <resp>Modélisation et structuration</resp>
               <persName>Elisabeth GRESLOU</persName>
               <persName>Elysabeth HUE-GAY</persName>
            </respStmt>
            <respStmt>
               <resp>Première version du modèle d'encodage</resp>
               <persName>Elisabeth GRESLOU</persName>
               <persName>Anne Garcia-Fernandez</persName>
            </respStmt>
         </titleStmt>

         <publicationStmt>
            <publisher/>
            <availability>
               <p>Le site est en accès restreint</p>
            </availability>
            <pubPlace>ithac.elan-numerique.fr</pubPlace>
         </publicationStmt>
         <sourceDesc>
            <bibl>
               <title>Euripides Poeta Tragicorum princeps, in Latinum sermonem conuersus, adiecto
                  eregione textu Graeco : CUM ANNOTATIONIBUS ET PRAEFATIONIBUS in omnes eius
                  Tragoediae, autore GASPARO STIBLINO, Accesserunt JACOBI MICYLLI, de Euripidis
                  uita, ex diuersis autoribus collecta : item, De tragoedia et eius partibus
                  προλεγομένα quaedam. Item IOANNIS BRODAEI Turonensis Annotationes doctissimae
                  numquam antea in lucem editae. Ad haec Rerum et uerborum toto Opere Praecipue
                  memorabilium copiosus INDEX. Cum caesaris Maiestatis et Christianissimi Gallorum
                  Regis gratia ac priuilegio, ad decennium. Basileae, per Ioannem Oporinum.</title>
               <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
               <author ref="#eur">Euripides</author>
               <pubPlace ref="#basilea">Basilea</pubPlace>
               <publisher ref="#oporinus_ioannes">Iohannes Oporinus</publisher>
               <date when="1562">1562</date>
               <!-- dans l'attribut @when, l'année en chiffre arabes -->
               <editor ref="#stiblinus_gasparus">Gasparus Stiblinus</editor>
               <editor role="traducteur" ref="#stiblinus_gasparus">Gasparus Stiblinus</editor>
               <!-- pour les liens google book, remplacer les & par &amp; pour que ça fonctionne -->
            </bibl>

            <listBibl>
               <bibl/>
               <!-- créer un <bibl> par référence -->
            </listBibl>

         </sourceDesc>
      </fileDesc>

      <profileDesc>

         <abstract>
            <p/>

         </abstract>

         <langUsage>
            <!-- ne pas modifier -->
            <language ident="fr">Français</language>
            <language ident="la">Latin</language>
            <language ident="grc">Grec</language>
         </langUsage>

      </profileDesc>
      <revisionDesc>
         <change when="2022-02-19">Sarah GAUCHER : identifiants Header, mise en conformité
            Elan</change>
         <change when="2022-01-06">Sarah GAUCHER : création de la fiche TEI, encodage de la
            transcription et de la traduction</change>

      </revisionDesc>
   </teiHeader>
   <text>
      <body ana="#A">
         <head type="orig" xml:id="Eu1562_Stiblinus_p20_0">Praefatio <persName
               ref="#stiblinus_gasparus">Gaspari Stiblini</persName> in <title ref="#eur_her"
               >Heraclidas</title></head>
         <head type="trad" corresp="#Eu1562_Stiblinus_p20_0">Préface de Gaspar Stiblinus aux
               <title>Héraclides</title>.</head>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_1">Haec <title ref="#eur_her"
               >Heraclidarum</title>, id est de Herculis liberis profugis fabula, quod ad oeconomiam
            et consilium poetae attinet, cum <title ref="#eur_suppl">Supplicibus</title> colludere
            uidetur, quas supra diximus pertinere ad laudem Atheniensis reipublicae.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_1"> Cette pièce des
               <title>Héraclides</title>, c'est-à-dire des enfants d'Hercule en fuite, semble, pour
            ce qui est de la disposition et du projet du poète, s'accorder avec les
               <title>Suppliantes</title>, qui, comme nous l'avons dit plus haut, tendent à l'éloge
            de la république athénienne.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_2">Sicut enim istic Theseus fortissimus
            Atheniensum princeps supplicantibus Argiuis matribus caesos ante Thebas armis sepulturae
            asseruit, ita hic Demophon Thesei filius, non minore pietate quam uirtute, afflictas et
            tota Graecia profligatas Herculis reliquias fortissime defendit Eurystheumque in exitium
            Heraclidarum natum cum omnibus fundit fugatque copiis.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_2">En effet, de même que dans cette dernière
            pièces Thésée, le plus brave chef des Athéniens, répondant à la supplique des mères
            argiennes, assura par la force des armes la sépulture de ceux qui avaient été tués
            devant Thèbes, de même dans celle-ci Démophon, fils de Thésée, avec non moins de piété
            que de vertu, défend très courageusement les rejetons d'Hercule lorsqu'ils sont
            persécutés et chassés à travers toute la Grèce, et il renverse et met en fuite
            Eurysthée, né pour la destruction des Héraclides, avec toute son armée.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_3">Insigne hoc est eulogium Atheniensis
            ciuitatis, cuius amplissima laus est defendere supplices, miseros et oppressos aduersus
            uim tueri, iustitiam absque omni respectu propugnare, malos persequi, pietatem colere,
            denique Rempublicam saluam ac florentem sine omni tyrannidis uel seruitii metu
            continere.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_3" ana="#TH_fin">Il s'agit là d'un remarquable éloge de la
            cité athénienne, dont la plus grande source de louanges était son habitude de défendre
            les suppliants, de protéger les malheureux et les opprimés contre la violence, de lutter
            pour la justice sans aucune hésitation, de punir les méchants, de cultiver la piété et,
            enfin, de maintenir la République sauve et florissante sans craindre aucun tyran ni
            aucune servitude.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_4">Sunt enim istae plane Heroicae uirtutes in
            ducibus aut ciuitatum gubernatoribus, citra omnem uel commodi spem uel incommodi metum
            apertam uim ab immerentibus propulsare aut dignos auxilio libere defendere ac tueri,
            etiamsi nulla obligatio officiorum aut cognatio generis aut foedus amicitiae
            intercesserit.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_4">Ce sont là en effet des vertus
            manifestement héroïques chez les chefs ou les gouverneurs des États, que de repousser,
            en dépit de tout espoir d'avantage ou de toute crainte d’embarras, la violence ouverte
            des innocents, ou de défendre et de soutenir librement ceux qui sont dignes d'être
            aidés, même s'il n'existe entre eux aucune obligation de service, aucun lien de sang ou
            aucun traité d'amitié.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_5">Porro Eurystheus, cuius minae et fastus
            paulo ante intolerabiles erant, nunc autem captus et uinctus in manusque uetulae
            traditus ut eius arbitrio mactaretur, et Herculis liberi iam uictores et praeter spem in
            libertatem asserti, quibus modo nullus locus per totam Graeciam satis tutus erat
            aduersus uim Eurysthei insignia dant documenta inconstantiae ac imbecillitatis humanae
            felicitatis.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_5" ana="#TH_fin">De plus, Eurysthée, dont les menaces et
            l'arrogance étaient intolérables peu de temps auparavant, est maintenant capturé,
            enchaîné et remis entre les mains d'une petite vieille femme afin qu'il soit abattu par
            sa décision, et les enfants d'Hercule sont maintenant les vainqueurs et libérés au-delà
            de toute espérance, eux pour qui, juste avant, il n'y avait aucun endroit dans toute la
            Grèce assez préservé de la puissance d'Eurysthée ; ces personnages donnent une preuve
            remarquable de l'inconstance et de la faiblesse du bonheur humain.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_6">De qua praeclara illa ex <persName
               ref="#ual_max">Valerio Maximo</persName> uerba huc adscribam : <cit type="refauteur">
               <quote>Caduca nimirum <seg type="incise">(inquit)</seg> et fragilia puerilibusque
                  consentanea crepundiis sunt ista, quae uires atque opes humanae uocantur. Affluunt
                  subito, repente dilabuntur, nullo in loco, nulla in persona, stabilibus nixa
                  radicibus, consistunt, sed incertissimo flatu fortunae huc atque illuc acta, quos
                  in sublime extulerunt, improuiso recursu destitutos, in profundo cladium
                  miserabiliter immergunt. Itaque neque debent existimari neque dici bona, quae
                  inflictorum malorum amaritudinem desiderio sui duplicant.</quote>
               <bibl resp="#SG"><author ref="#ual_max">Val. Max.</author>, <title ref="#ual_max_dfm"
                     >Dictorum factorumque memorabilium libri</title>
                  <biblScope>6.9.7</biblScope>.</bibl>
            </cit></ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_6">J'ajouterai ici les célèbres paroles de
            Valère Maxime : « Fugaces en effet (dit-il), fragiles et adaptées aux hochets enfantins,
            ces choses qu'on appelle forces et puissances humaines. Tout à coup, elles coulent en
            abondance, puis elles se tarissent à l'improviste ; en aucun lieu, avec aucune personne,
            reposant sur des racines stables, elles ne se maintiennent fermement, mais sont plutôt
            poussées çà et là par le souffle très incertain de la fortune : ceux qu'elles portaient
            en haut, abandonnés dans un revers inattendu, elles les plongent misérablement dans les
            profondeurs du désastre. Aussi ne doit-on ni les juger ni les appeler des biens, ces
            choses qui doublent l'amertume des maux infligés par le sentiment de la perte ».</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_7">Hactenus ille.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_7">Voilà ce que dit l'auteur.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_8">Insigni ergo hoc exemplo poeta eos quibus
            animos ac spiritus addunt uel opes uel honores ad modestiam et moderationem animi uocat,
            cum istas res quas tanti faciunt tam subito eripi ac alio transferri uideant.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_8" ana="#TH_fin">Aussi, par cet exemple remarquable, le
            Poète appelle-t-il à la modestie et à la modération d'esprit ceux à qui la richesse ou
            l'honneur ajoute l'orgueil et la fierté, lorsqu'ils constatent que ces biens qu'ils
            estiment tant peuvent être si soudainement arrachés et transférés à un autre.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_9">In Macaria pulcherrimum uiraginis exemplum
            adumbratur, quae suae ipsius saluti fratrum uitam ac ciuitatis Atticae incolumitatem
            anteposuit ac ingenti animo ultro se mactandam obtulit.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_9" ana="#PE">Dans Macaria, il dépeint un très bel
            exemple d'héroïne, qui a placé la vie de ses frères et la sécurité de la cité attique
            avant son propre bien-être et qui, avec un grand courage, s'est volontairement offerte
            au massacre.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_10">Praeterea in Eurystheo ignaui sed superbi
            ac caelum armis territantis effigiem uidere licet, qui prouocatus ad certamen singulare
            cum Hyllo recusauit congredi.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_10" ana="#PE">En outre, il est possible de voir en
            Eurysthée l'image d'un homme lâche mais fier et menaçant le ciel de ses armes, qui,
            défié en combat singulier avec Hyllos, a refusé de le combattre.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_11">Tandem autem in proelio ab Iolao captus ac
            ad Alcmenam perductus mulierculae fit ludibrio, Rex non ita pridem Argiuorum, qui
            Herculi quoque negotia exhibuerat, fortissimo ignauissimus.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_11">Mais enfin, capturé au combat par Iolas
            et conduit à Alcmène, il devient un objet de moquerie pour cette petite femme lui qui,
            il n'y a pas si longtemps, était roi des Argiens, qui avait causé des difficultés même à
            Hercule, le plus lâche au le plus brave.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_12">Argumentum actus primi.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_12">Argument de l’acte 1.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_13">Prologus sub persona Iolai aperit
            argumentum Fabulae, quod est persecutio liberorum defuncti Herculis, quos Eurystheus
            odio Iunonis iniquae tota profligabat Graecia, ne quid Herculeae stirpis reliquum
            maneret.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_13">Le prologue sous le masque de Iolas
            révèle la trame de la pièce, qui est la poursuite des enfants du défunt Hercule,
            qu'Eurysthée persécutait dans toute la Grèce à cause de la haine de l'hostile Junon, de
            peur qu'il ne reste un vestige de la race herculéenne.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_14">2. Copreus praeco Eurysthei, uix
            tolerandae ferociae, etiam ab aris deorum per uim supplicem Iolaum cum Herculea prole
            abstrahere conatur.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_14">2. Coprée, un héraut d'Eurysthée, d'une
            sauvagerie à peine tolérable, tente d'arracher par la force, même aux autels des dieux,
            le suppliant Iolas ainsi que la progéniture d'Hercule.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_15">Cui rei Chorus senum Atheniensium
            interuenit ferocientemque distinet donec audito tumultu Demophon ipse dux causas
            utriusque partis audiat.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_15">Le chœur des vieillards athéniens
            interrompt cet acte et retient l'homme enragé jusqu'à ce que, ayant entendu l'agitation,
            Démophon, le chef, entende les arguments de chaque partie.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_16">3. Contentio Coprei et Iolai apud iudicem
            Demophontem, quorum ille contendit sibi iure licere agere in Herculis liberos quae
            iussus fuerat ab Eurystheo.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_16">3. S’ouvre une lutte de Coprée et
            d’Iolas devant Démophon, leur juge, où le premier soutient qu'il est légalement autorisé
            à agir contre les enfants d'Hercule de la manière dont il avait été ordonné par
            Eurysthée.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_17">Hic contra defendit se confutatque
            argumenta aduersarii.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_17">Le second, au contraire, se défend et
            réfute les arguments de son adversaire.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_18">Vincit autem Iolaus : discedit praeco,
            superbe Atheniensibus bellum comminans.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_18">Iolas, cependant, l'emporte : le héraut
            s'en va en menaçant avec arrogance les Athéniens de la guerre.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_19">4. Iolaus magna gratulatione Demophonti et
            ciuitati Atheniensium de se et filiis gratitudinem pro tanto conseruationis beneficio
            pollicetur.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_19">4. Iolas, avec une immense
            reconnaissance, promet à Démophon et à la cité des Athéniens, en son nom et au nom des
            enfants, leur gratitude en échange d’une si grande faveur de protection.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_20">Quae autem ad bellum administrandum
            pertinent, expediturus abit Demophon.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_20">Démophon, cependant, s'en va pour
            préparer ce qui concerne la gestion de la guerre.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_21">5. Chorus Copreo et Eurystheo iratus longe
            alium belli euentum ominatur quam ipsi sibi magnifice promitterent.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_21">5. Le chœur, irrité contre Coprée et
            Eurysthée, présage une issue de la guerre bien différente de celle que ces derniers se
            promettent avec arrogance.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_22">Argumentum actus secundi.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_22">Argument de l’acte 2.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_23">Demophon cognitis uatum responsis Iolao
            refert non procul abesse Eurystheum, sibi uero omnia quae necessaria ad bellum sint
            expedita esse.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_23">Démophon, après avoir pris connaissance
            des réponses des devins, rapporte à Iolas qu'Eurysthée n'est pas loin, mais que lui-même
            a préparé tout ce qui est nécessaire à la guerre.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_24">Deinde uirginem ad uictimam posci oraculis
            quae generoso sit patre nata, se autem neque suos neque alterius immolaturum
            liberos.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_24">Puis il lui dit que l'oracle exige comme
            victime une vierge née d'un père noble, mais que lui, cependant, ne sacrifiera ni ses
            propres enfants ni ceux d'un autre.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_25">Quare ipsum debere dispicere quomodo citra
            damnum et ciuitatis et suum Herculeis liberis consulat.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_25">C'est pourquoi, dit-il, Iolas devrait
            réfléchir à la façon dont il pourrait subvenir aux besoins des enfants d'Hercule sans
            causer de préjudice à la ville ou à lui-même.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_26">2. Iolaus repente commutatis rebus
            deplorat et se et suos, quos modo in portum nauigare putabat.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_26">2. Iolas, face au changement inattendu
            de la situation, désespère de lui-même et de sa famille, dont il pensait naguère qu’elle
            atteignait un port sûr.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_27">Sic lubricae spes mortalium sunt.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_27">Si fragiles sont les espoirs des
            mortels.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_28">Deinde se ipsum Eurystheo offerri cupit ut
            tantum salui maneant parui filioli Herculis.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_28">Il souhaite alors être lui-même offert à
            Eurysthée, afin que les petits fils d'Hercule puissent au moins demeurer en
            sécurité.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_29">3. Generosissima uirgo Macaria ac plane
            Herculei sanguinis uirago, cum audiret mortem nobilis puellae morari uictoriam, ultro se
            offert uictimam pro salute fratrum et ciuitatis contenditque mortem honestam
            praestabiliorem esse uita contempta et probrosa.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_29">3. Macaria, une très noble jeune fille
            et héroïne tout à fait de sang herculéen, lorsqu'elle entend que la mort d'une fille de
            haute naissance retarde la victoire, s'offre volontairement comme victime pour le salut
            de ses frères et de la ville et elle soutient qu'une mort honorable est plus distinguée
            qu'une vie méprisable et honteuse.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_30">Ac ibi demum epitasis procedit
            affectuumque <foreign xml:lang="grc">δεινότης</foreign>, dum eam Iolaus reuocare studet
            a proposito, ut scilicet prius sortiretur cum reliquis sororibus : ipsa autem libere
            uitam suam impendere fratribus et Athenis statuit.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_30">Et c'est à ce moment précis que
            l’épitase progresse, ainsi que la force des émotions, lorsque Iolas s'efforce de la
            faire revenir sur son projet afin qu'elle puisse d'abord tirer au sort ses sœurs
            restantes : elle décide cependant de sacrifier librement sa propre vie pour ses frères
            et Athènes.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_31">Quibus ualere cum sene iussis, eis et
            studium sapientiae et pietatem ac gratitudinem erga suos manes commendat.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_31">Après avoir fait ses adieux à ses frères
            et sœurs en même temps qu'au vieillard, elle leur recommande à la fois de rechercher la
            sagesse et la piété et la reconnaissance envers ses Mânes.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_32">4. Chorus memorabilem primum sententiam
            effert siue de fato, siue de uarietate fortunae, quae duo inscrutabili ratione sursum
            deorsumque in morem euripi aestuariis uicibus res humanas iactent.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_32">4. Le chœur fait d'abord une réflexion
            mémorable sur le destin ou la variété de la fortune, qui tous deux, par un raisonnement
            impénétrable, font monter et descendre les affaires humaines dans des roulement de flots
            semblables à ceux d'un détroit.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_33">Deinde consolatur senem, ut moderatius
            ferat gloriosam mortem Macariae, quam dii sic prouiderint.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_33">Puis il console le vieillard, afin qu'il
            puisse supporter avec plus de mesure la mort glorieuse de Macaria, disant que les dieux
            l’ont ainsi prévue.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_34">Argumentum actus tertii.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_34">Argument de l’acte 3.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_35">Famulus nuntii uices agens Alcmenae et
            Iolao refert Hyllum cum numerosis copiis adesse ac iam acies instructas aperto campo ad
            pugnam procedere.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_35">Un serviteur jouant le rôle de messager
            rapporte à Alcmène et à Iolas qu'Hyllos est arrivé avec de nombreuses troupes et que les
            lignes de bataille, mise en place en plein champ, avancent maintenant vers le
            combat.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_36">2. Qua re cognita, senex miro studio ad
            proelium ipse contendit in effetoque corpore uim suscitat ira, frustra ipsum dehortante
            famulo pugnaeque inutilem dicente.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_36">2. Ayant appris cela, le vieillard, avec
            un zèle étonnant, s'efforce d'aller au combat et la colère réveille les forces de son
            corps épuisé, tandis que le serviteur essaie en vain de le dissuader et le dit inutile
            au combat.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_37">Non autem absque ratione hoc senis, ita
            operose in proelium se adornantis, spectaculum hic insertum est : ne otiosa scena esset
            donec ueniat nuntius qui euentum pugnae narret.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_37">Ce n'est cependant pas sans raison que
            ce spectacle du vieillard, s'équipant si laborieusement pour le combat, est inséré ici :
            c’est pour que la scène ne manque d’action jusqu'à l'arrivée du messager qui raconte le
            résultat de la bataille.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_38">3. Chorus Iouem ac Mineruam inuocat ut a
            parte Atheniensium, quorum causa iustior sit quia supplices defendant, stent dextrique
            pugnaturis aspirent.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_38">3. Le chœur invoque Jupiter et Minerve
            pour qu'ils se rangent du côté des Athéniens, dont la cause est plus juste puisqu'ils
            défendent les suppliants, et qu'ils favorisent ceux qui vont combattre.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_39">Argumentum actus quarti.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_39">Argument de l’acte 4.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_40">Actus quartus nuntii narratione quae est
            de uictoria Atheniensium et capto ab Iolao Eurystheo consumitur.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_40">Le quatrième acte est occupé par le
            récit du messager sur la victoire des Athéniens et la capture d'Eurysthée par
            Iolas.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_41">Quo nuntio mirifice gaudet Alcmena sibique
            ac Herculis liberis gratulatur de reliquae uitae libertate.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_41">À cette nouvelle, Alcmène se réjouit
            grandement et félicite les enfants d'Hercule et elle-même de la liberté de leur vie
            future.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_42">2. Chorus ad laetitiam agitandum ex re
            bene gesta suscitatur.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_42">2. Le chœur se lève pour exprimer la
            joie née de cette victoire.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_43">Docetque Eurysthei exemplum semper Deum
            aduersari insolentibus ac superbis : contra, modestos quique moderata animis concipiant
            iuuare ac extollere.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_43">Et l'exemple d'Eurysthée enseigne que
            Dieu s’élève toujours contre les hommes démesurés et orgueilleux : en revanche, il aide
            et élève les hommes modestes et ceux qui conçoivent dans leur âme des intentions
            modérées.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_44">Deinde Herculem in deorum consortium
            ascitum esse manifestis argumentis liquere.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_44">Ensuite, disent-ils, le fait qu'Hercule
            ait été admis dans la société des dieux est mis en évidence par des arguments
            clairs.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_45">Argumentum actus quinti.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_45">Argument de l’acte 5.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_46">Nuntius adducit in scenam ad Alcmenam
            uinctum Eurystheum, cui anus amare insultat ac acerbissime exprobrat iniurias quibus
            ille tum ipsum Herculem, tum eius liberos affecerat.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_46">Le messager conduit à Alcmène sur la
            scène Eurysthée dans les fers, que la vieille femme insulte amèrement et accuse très
            sévèrement des torts qu'il avait infligés autrefois à Hercule lui-même et maintenant à
            ses enfants.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_47">Mortem denique diram dignamque meritis
            minatur.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_47">Elle le menace enfin d'une mort
            affreuse, digne de ses offenses.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_48">2. Chorus dehortatur Alcmenam a caede
            Eurysthei, idque ex more ciuitatis, quo non solebant Athenienses captos in proelio
            necare.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_48">2. Le chœur dissuade Alcmène de tuer
            Eurysthée, et cela à cause de la coutume de la cité, selon laquelle les Athéniens
            n'avaient pas l'habitude de tuer ceux qui étaient capturés dans les combats.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_49">3. Eurystheus nullam spem uitae esse
            reliquam uidens minatur se etiam mortuum ut urbi Atheniensium salutatem sic Herculeae
            stirpi perniciosum fore.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_49">3. Eurysthée, voyant qu'il n'y a plus
            d'espoir de vie, menace que, même mort, il sera à la fois source de sécurité pour la
            cité des Athéniens et destructeur pour la race herculéenne.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p20_50">Talia iactantem Alcmena duci iubet.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p20_50">Comme il se vante ainsi, Alcmène ordonne
            qu'on le conduise dehors.</ab>
      </body>
   </text>
</TEI>
