<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Haec tragoedia est imago tyranni...</title>
                <author ref="#melanchtonus_philippus">[Mélanchthon]</author>
                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <respStmt>
                    <resp>Transcription</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Alexia DEDIEU</name>
                </respStmt>
                <respStmt>
                    <resp>Révision</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne Garcia-Fernandez</persName>
                </respStmt>
            </titleStmt>

            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>
            <sourceDesc>
                <bibl>
                    <title>Euripidis Tragoediae, quae hodie extant, omnes Latinè soluta oratione
                        redditae, ita ut versus versui respondeat</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#eur">Euripides</author>
                    <pubPlace ref="#basilea">Basileae</pubPlace>
                    <publisher ref="#oporinus_ioannes">Ioannes Oporinus</publisher>
                    <date when="1558">1558</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor role="traducteur" ref="#xylandrus_guilelmus">Guilielmus
                        Xylandrus</editor>
                    <editor ref="#melanchtonus_philippus">Philippus
                        Melanchthon</editor>
                    <ref
                        target="https://www.google.fr/books/edition/Euripidis_tragoediae_quae_hodie_extant_o/47xpAAAAcAAJ?hl=fr&amp;gbpv=1"
                        >https://www.google.fr/books/edition/Euripidis_tragoediae_quae_hodie_extant_o/47xpAAAAcAAJ?hl=fr&amp;gbpv=1</ref>
                </bibl>
                
                <listBibl>
                    <bibl>Micha Lazarus, ‘Tragedy at Wittenberg: Sophocles in Reformation Europe’,
                        Renaissance Quarterly, 73 (2020), 33–77 <ref
                            target="https://doi.org/10.1017/rqx.2019.494"
                            >https://doi.org/10.1017/rqx.2019.494</ref>.</bibl>
                    <bibl>Michael Lurje, ‘Misreading Sophocles: Or Why Does the History of
                        Interpretation Matter?’, Antike Und Abendland, 52 (2006), 1–15 <ref
                            target="https://doi.org/10.1515/9783110186345.1"
                            >https://doi.org/10.1515/9783110186345.1</ref></bibl>
                    <bibl> Michael Lurje, ‘Facing up to Tragedy: Toward an Intellectual History of
                        Sophocles in Europe from Camerarius to Nietzsche’, in A Companion to
                        Sophocles (Blackwell Publishing Ltd., 2012), pp. 440–61. </bibl>
                    
                    <!-- créer un <bibl> par référence -->
                </listBibl>
                
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Comme pour Les Phéniciennes, le Cyclope est une des tragédies d’Euripide qui a le
                    plus attiré l’attention de Mélanchthon et de ses disciples. Cette courte
                    présentation qui ne comporte pas de titre pourrait elle aussi être une brève
                    introduction qui s’apparente à des notes de cours, ou bien une notice
                    explicative en vue de la publication des traductions. Le texte n’est pas
                    ouvertement signé par Mélanchthon, mais il semble très probable qu’il soit de
                    lui.</p>
            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>
        <revisionDesc>
            <change when="2022-03-10">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
            <change when="2021-03-10">Sarah GAUCHER : création de la fiche TEI et remplissage du
                Header ; Encodage de la transcription et de la traduction</change>
        </revisionDesc>
    </teiHeader>

    <text>
        <body ana="#TH_dram #TH_fin #A">
            <ab type="orig" xml:id="Eu1558_Xylandrus_p5_1">Haec tragoedia est imago tyranni alicuius
                crudelissimi.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p5_1">Cette tragédie est l’image d’un tyran
                très cruel.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p5_2">Credo quod uoluerit describere regem uel
                tyrannum aliquem Aegyptium.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p5_2">Je crois que ce qu’elle a voulu décrire
                c’est un roi ou un tyran égyptien.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p5_3">Per Satyros intelligit moriones et
                impostores.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p5_3">Elle fait voir à travers des Satyres
                des imbéciles et des imposteurs.</ab>
            <ab type="orig" xml:id="Eu1558_Xylandrus_p5_4">Habet locum communem, neminem esse fidum
                tyranno, etiamsi obtemperet.</ab>
            <ab type="trad" corresp="#Eu1558_Xylandrus_p5_4">Elle renvoie au lieu commun selon
                lequel personne ne doit la fidélité à un tyran, même s’il obtempère.</ab>

        </body>
    </text>
</TEI>
