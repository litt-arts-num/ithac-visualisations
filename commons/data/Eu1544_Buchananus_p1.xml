<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Ad Illustrissimum Principem Ioannem a Lucenburgo, Iueraci
                    Abbatem, Georgii Buchanani Praefatio.</title>
                <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : "la" pour le latin et "grc" pour le grec -->
                <!-- Attention ! ne pas faire de retour charriot dans le titre -->
                <author ref="#buchananus_georgius">Georgius Buchananus</author>
                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Ajouter autant de tâches (<respStmt>) que nécessaire -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Alexia DEDIEU</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Diandra CRISTACHE</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>

                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne GARCIA-FERNANDEZ</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
            <sourceDesc>
                <bibl>
                    <title>Medea Euripidis poetae tragici Georgio Buchanano Scoto interprete</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#eur">Euripides</author>
                    <pubPlace ref="#lutetia">Lutetiae</pubPlace>
                    <publisher ref="#uascosanus_michael">Vascosanus</publisher>
                    <date when="1544">1544</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor role="traducteur" ref="#buchananus_georgius">Gergius Buchananus</editor>
                    <ref
                        target="http://reader.digitale-sammlungen.de/de/fs1/object/display/bsb11264635_00005.html"/>
                    <!-- pour les liens google book, remplacer les & par &amp; pour que ça fonctionne -->
                </bibl>
                <listBibl>
                    <bibl>George Buchanan: Poet and Dramatist, ed. by Philip Ford and Roger P. H.
                        Green (Classical Press of Wales, 2009).</bibl>
                    <bibl>Zoé Schweitzer, ‘Buchanan, helléniste et dramaturge, interprète d’Euripide
                        (Medea et Alcestis)’, Études Épistémè. Revue de littérature et de
                        civilisation (XVIe – XVIIIe siècles), 23, 2013.</bibl>
                    <bibl>Zoé Schweitzer, ‘La traduction d’Alceste par Buchanan, l’imago
                        retrouvée ?’, Anabases. Traditions et réceptions de l’Antiquité, 21, 2015,
                        113–24.</bibl>
                    <bibl>Debora Kuller Shuger, The Renaissance Bible: Scholarship, Sacrifice and
                        Subjectivity (Berkeley: University of California Press, 1994).</bibl>

                </listBibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p/>
            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>

        <revisionDesc>
            <change when="2022-03-09">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
            <change when="2021-11-24">Sarah GAUCHER : encodage de la traduction et correction de la
                transcription. Le fichier TT source est Soph1567_Buchananus_p1</change>
            <change when="2021-05-30">Diandra CRISTACHE : mise à jour et chargement sur le
                Pensoir</change>
            <change when="2021-07-04">Alexia DEDIEU: corrections dans la transcription du
                latin.</change>
        </revisionDesc>

    </teiHeader>

    <text>
        <body>

            <!-- RAPPEL : on encode le texte latin ou grec; on n'encode dans le texte de la traduction que les notes d'éditeur (=membres du projet) -->

            <!-- Le couple d'éléments <head> contient le titre ou l'incipit du paratexte -->
            <!-- L'identifiant du TITRE est composé du titre de la fiche TT suivi d'un underscore (_) puis du chiffre 0 -->
            <head type="orig" xml:id="Eu1544_Buchananus_p1_0">Ad Illustrissimum Principem <persName
                    ref="#alucenburgo_ioannes">Ioannem a Lucenburgo</persName>, Iueraci Abbatem,
                    <persName ref="#buchananus_georgius">Georgii Buchanani</persName>
                Praefatio.</head>
            <!-- Le deuxième élément du couple contient la traduction française du titre ou incipit -->
            <!-- L'identifiant de la traduction est le même que celui du latin, précédé d'un hashtag (#) -->
            <head corresp="#Eu1544_Buchananus_p1_0" type="trad">Au très illustre prince Johannes de
                Luxembourg, abbé d’Iveracus, préface de Georgius Buchananus.</head>

            <ab type="orig" xml:id="Eu1544_Buchananus_p1_1">Non dubito plerosque futuros, Princeps
                clarissime, qui statim ubi haec in manus sumpserint, meam admirentur impudentiam,
                uel potius temeritatem, quod in tanto bonorum ingeniorum prouentu, id ego potissimum
                sum ausus aggredi quod aliorum uel pudor refugerat, uel reformidarat audacia, atque
                    <persName ref="#eur">Euripidi</persName> uertendo manum admoliri non
                pertimuerim, praesertim quum non ignorarem hanc a plerisque rem prius tentatam, uni
                    <persName ref="#erasmus_desiderius">Erasmo</persName> ita successisse, ut iuxta
                ab incepto me deterrere debuerit illorum casus, atque huius felicitas ; qui
                praeterquam quod ad eam rem uenerat tot bonarium artium praesidio instructus, etiam
                siqua erat in hac parte nouitatis gratia, eam preoccupauerat.</ab>
            <ab type="trad" corresp="#Eu1544_Buchananus_p1_1" ana="#PH #T">Je ne doute pas, prince très illustre,
                qu’il y aura bien des gens pour s’étonner, aussitôt qu’ils auront pris dans leurs
                mains ces écrits, de mon impudence ou plutôt de ma témérité, sous prétexte que,
                alors que de grands esprits y réussissaient si bien, j’ai osé, moi, aborder ce que
                la pudeur des autres avait fui ou ce que leur audace avait redouté et que je n’ai
                pas craint de m’atteler à traduire Euripide, surtout alors que je n’ignorais pas que
                bien d’autres s’étaient essayés à cette traduction mais qu’elle avait réussi au seul
                Érasme si bien que les malheurs de ceux-là et le succès de celui-ci auraient dû me
                détourner dès le départ : de fait Érasme, outre qu’il avait abordé ce sujet aguerri
                par le secours de tant de beaux-arts, même s’il jouissait sur ce point du charme de
                la nouveauté, il s’en était emparé avant moi.</ab>
            <ab type="orig" xml:id="Eu1544_Buchananus_p1_2">Accedit et illud, quod praeter summam in
                choris obscuritatem (quae huic scriptori adeo familiaris est ut eam de industria
                sectatus esse uideatur) haec ipsa quondam fabula ab <persName ref="#enn"
                    >Ennio</persName> prius uersa fuerat, cuius non pauci uersus adhuc in ueterum
                commentariis passim leguntur ; quorum comparatio his nostris, alioqui obscuris,
                plurimum sua luce officere posset.</ab>
            <ab type="trad" corresp="#Eu1544_Buchananus_p1_2" ana="#PH #T">Il s’est également ajouté que, excepté
                la très grande obscurité des choeurs (qui est coutumière de cet auteur au point
                qu’il semble l’avoir volontairement recherchée), cette pièce avait déjà été traduite
                jadis par Ennius, dont nous lisons encore beaucoup de vers partout dans les
                commentaires des Anciens ; et la comparaison avec ces vers pourrait reléguer les
                nôtres, du reste obscurs, dans les ténèbres.</ab>
            <ab type="orig" xml:id="Eu1544_Buchananus_p1_3">Ego uero, tantum abest ut his a
                scribendo rebus fuerim deterritus, ut in utriusque fortunae qualemcunque euentum non
                mediocre praesidium hinc fore iam animo praeceperim, atque mihi ipsi quodammodo
                spoponderim.</ab>
            <ab type="trad" corresp="#Eu1544_Buchananus_p1_3" ana="#PH #T">Mais moi, il s’en faut de beaucoup
                pour que cela m’ait détourné d’écrire, si bien que, face à mon éventuelle réussite
                ou à mon potentiel échec, j’ai déjà ordonné à mon esprit de m’apporter un soutien
                important et que je me suis en quelque sorte porté garant de moi-même.</ab>
            <ab type="orig" xml:id="Eu1544_Buchananus_p1_4">Nam tametsi plausum a studiosorum
                theatro haec fabula non accipiat, si utcunque tamen steterit, nec ignominiose
                explodatur, nunquam quoque moleste feram, <persName ref="#enn">Ennii</persName>
                atque <persName ref="#erasmus_desiderius">Erasmi</persName> autoritate mihi tenebras
                offundi, quam me post tam praeclara nomina qualemcunque saltem locum tenuisse.</ab>
            <ab type="trad" corresp="#Eu1544_Buchananus_p1_4" ana="#PH #T">En effet, même si cette pièce ne
                recevait pas d’applaudissements du théâtre des érudits, pour peu qu’elle soit
                cependant présentée et qu’elle ne soit pas honteusement huée, il ne me serait jamais
                insupportable d’être relégué dans les ténèbres par l’autorité d’Ennius et d’Érasme
                ni d’avoir occupé une place, quelle qu’elle soit, après de si grands noms.</ab>
            <ab type="orig" xml:id="Eu1544_Buchananus_p1_5">Hoc uero quicquid est operae, nos tibi
                princeps clarissime potissimum censuimus dicandum, non modo ut grati saltem animi
                professione tuam erga me summam testarer humanitatem, aut a uestrae familiae
                nobilissimae splendore contra calumniantium inuidiam praesidium mihi peterem, sed ut
                meliori spe reliquorum doctorum suffragiis me committerem, si tui iudicii
                praerogatiuo subnixus essem, qui quum in omni disciplinarum genere principatum
                teneas, ac in hoc poetice studio singulari quadam felicitate uerseris, soles tamen
                (ut inquit ille) <seg type="reecriture">meas esse aliquid putare nugas<bibl
                        resp="#SG"><author ref="#catul">Catul.</author>, <title ref="#catul_ep"
                            >Ep.</title>
                        <biblScope>1.1.4</biblScope>.</bibl></seg>.</ab>
            <ab type="trad" corresp="#Eu1544_Buchananus_p1_5" ana="#PA">Quant à cette œuvre, quelle qu’en soit
                la qualité, nous nous avons pensé qu’elle devait t’être dédié, prince très illustre,
                non seulement pour que par le témoignage de ma reconnaissance je donne au moins à
                voir ta très grande humanité à mon égard ou que je réclame à la splendeur de votre
                très noble famille un soutien contre la jalousie des calomniateurs mais aussi pour
                que je m’expose avec un meilleur espoir aux suffrages du reste des savants, si je me
                suis d’abord appuyé sur ton jugement, toi qui, alors que tu tiens le premier rang
                dans tous les genres de disciplines et que tu t’es appliqué à cette étude poétique
                avec un succès remarquable, as cependant l’habitude (comme le dit le poète) "de
                considérer mes futilités".</ab>
        </body>
    </text>
</TEI>
