<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">


    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Des. Erasmus Roterodamus Ioanni et Stanislao Boneris fratribus
                    Polonis s.d.</title>
                <author ref="#erasmus_desiderius">Desiderius Erasmus</author>

                <respStmt>
                    <resp>Transcription</resp>
                    <name>Laure HERMAND</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Laure HERMAND</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Diandra CRISTACHE</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <sourceDesc>

                <bibl>
                    <title>Habes hic, amice lector, P. Terentii comoedias, una cum scholiis ex
                        Donati, Asperi et Cornuti commentariis decerptis, multo quam antehac unquam
                        prodierunt emendatiores, nisi quod in <foreign xml:lang="gr">ἑαυτόν
                            τιμωρούμενον</foreign> scripsit uir apprime doctus Jo. Calphurnius
                        Brixiensis, licet recentior. Indicata sunt diligentius carminum genera et in
                        his incidentes difficultates, correcta quaedam et consulum nomina, idque
                        studio et opera Des. Erasmi Roterodami, non sine praesidio veterum
                        exemplariorum. Ad haec accessit index accuratus vocum a commentatoribus
                        declaratarum </title>
                    <author ref="#ter">Terentius</author>
                    <pubPlace ref="#basilea">Basileae</pubPlace>
                    <publisher ref="#frobenius_ioannes">Johannes Frobenius </publisher>
                    <date when="1532">1532</date>
                    <editor ref="#erasmus_desiderius">Desiderius Erasmus</editor>
                </bibl>
                <listBibl>
                    <head>Bibliographie</head>
                    <bibl><author>Maria Citowska</author>, <title level="a">De l’épisode polonais
                            aux comédies de Térence</title>, dans <title level="m">Colloque érasmien
                            de Liège. Commémoration du 450e anniversaire de la mort
                        d'Érasme</title>, <publisher>éd. J.-P. Massaut</publisher>, <biblScope
                            unit="page">p. 99-106</biblScope>
                        <biblScope unit="page">(p. 135-145)</biblScope></bibl>
                    <bibl><author>Marian Hanik</author>, <title level="m">Trzy pokolenia z rodziny
                            Bonerów</title>, <publisher>wyd. COIT</publisher>,
                            <pubPlace>Kraków</pubPlace>, <date>1985</date></bibl>
                    <bibl>Władysław Pociecha : Boner Seweryn. W: Polski Słownik Biograficzny. T. 2:
                        Beyzym Jan – Brownsford Marja. Kraków: Polska Akademia Umiejętności – Skład
                        Główny w Księgarniach Gebethnera i Wolffa, 1936, s. 300–301. Reprint: Zakład
                        Narodowy im. Ossolińskich, Kraków 1989</bibl>
                    <bibl>Encyklopedya Powszechna Kieszonkowa, zeszyt X, Nakład druk i własność
                        Noskowskiego, Warszawa, 1888.</bibl>
                    <bibl>Bietenholz, Peter G., and Thomas Brian Deutscher (dir.), Contemporaries of
                        Erasmus: A Biographical Register of the Renaissance and
                        Reformation, Toronto-Buffalo, University of Toronto Press, 1985.</bibl>
                    <bibl><author>Lachs, J.</author>, <title level="a">Anselmus
                            Ephorinus</title>, <title level="m">Archiwum Historii i Filozofii
                            Medycyny</title>, <biblScope unit="volume">4</biblScope>
                        <date>1926</date>, <biblScope unit="page">p. 40-54</biblScope>, <biblScope
                            unit="page">194-209</biblScope>.</bibl>
                </listBibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Epître dédicatoire d’Erasme adressée à Jan et Stanislaw Boner (2 pages et demi in
                    fol.)</p>
                <p>Présentation Seweryn Boner</p>
            </abstract>

            <langUsage>
                <language ident="la">Latin</language>
            </langUsage>

        </profileDesc>

        <revisionDesc>
            <change when="2022-02-28">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
            <change when="2020-09-29">Diandra CRISTACHE : Encodage de la traduction et de la
                transcription</change>
        </revisionDesc>

    </teiHeader>

    <text>
        <body>

            <head type="orig" xml:id="Te1532_Erasmus_p1_0"><persName ref="#erasmus_desiderius">Des.
                    Erasmus Roterodamus</persName>
                <persName role="destinataire" ref="#bonerus_ioannes">Ioanni</persName> et <persName
                    role="destinataire" ref="#bonerus_stanislaus">Stanislao Boneris</persName>
                fratribus Polonis s.d.</head>
            <head corresp="#Te1532_Erasmus_p1_0" type="trad">Érasme de Rotterdam à Jan et Stanislaw
                Boner, frères polonais, salut !</head>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_1"><cit type="nonref">
                    <quote>« In teneris »<seg type="incise">, inquit ille,</seg> « consuescere
                        multum est »</quote>
                    <bibl resp="#LH"><author ref="#uirg">Virg.</author>, <title ref="uirg_g"
                            >G.</title>
                        <biblScope>2.272</biblScope>.<note>Passage cité également par Quint.,
                                <title>I.O.</title> 1.3.13.</note></bibl>
                </cit>, proinde consultum est protinus optimis assuescere.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_1" ana="#PE">« C’est chez les sujets
                tendres », dit le célèbre écrivain, « qu’il y a beaucoup d’habitudes à prendre » ;
                c’est pourquoi il est pertinent de les accoutumer d’emblée au meilleur.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_2">Sic enim fiet, ut quae natura sunt optima,
                eadem usu fiant et iucundissima.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_2" ana="#PE">En effet il arrivera que ce qui
                est par nature très bon devienne aussi très agréable à l’usage.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_3">Nihil autem est homini melius pietate,
                cuius semina cum ipso statim lacte sunt instillanda paruulis.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_3" ana="#PE">Or rien n’est meilleur pour
                l’homme que la piété aux principes de laquelle les petits enfants doivent s’abreuver
                au plus tôt en même temps que du lait.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_4">Proximum locum habent disciplinae
                liberales, quae licet per se uirtutes non sint, tamen ad uirtutem praeparant
                ingenium, dum id ex siluestri et immiti reddunt mansuetum ac tractabile.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_4" ana="#PE #PHILO">Les disciplines
                libérales ont une fonction très proche qui bien qu’elles ne soient pas en soi des
                vertus, préparent toutefois l’esprit à la vertu en le rendant, de sauvage et
                farouche qu’il était, doux et malléable.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_5">Porro quod <persName ref="#arstt"
                    >Aristoteles</persName> negat <seg type="paraphrase">adolescentes idoneos esse
                    discendae morali philosophiae<bibl resp="#LH"><author ref="#arstt"
                            >Arstt.</author>, <title ref="#arstt_nic">Nic.</title>
                        <biblScope>1.3.1095a</biblScope>.</bibl></seg>, fortassis haud omnino falsum
                est.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_5" ana="#PE #PHILO">En outre quant au fait
                qu’Aristote dit que les jeunes gens ne sont pas capables d’apprendre la philosophie
                morale, ce n’est peut-être pas totalement faux.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_6">Verum id non tam accidit uel rei, uel
                humani ingenii uitio, quam eorum culpa, qui uel sero tradunt eam animis iam
                educatione praua corruptis ac malis cupiditatibus occupatis, uel moleste et operose
                tradunt, magis illud affectantes, ut ipsi uideantur acuti, quam ut discipulos
                reddant meliores.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_6" ana="#PE">Mais cela n’arrive pas tant par
                un défaut de l’objet ou de l’esprit humain que par la faute de ceux qui soit
                l’enseignent tardivement à des esprits déjà corrompus par une éducation dépravée et
                accaparés par de mauvais désirs, soit l’enseignent de manière rebutante ou
                laborieuse, visant davantage à paraître eux-mêmes intelligents qu’à rendre leurs
                élèves meilleurs.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_7">Alioqui nihil magis secundum naturam est
                quam uirtus et eruditio, quae duo si adimas homini, iam homo esse desinit.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_7" ana="#PHILO">En outre rien n’est
                davantage conforme à la nature que la vertu et le savoir : si on enlève ces deux
                choses à l’homme, il cesse alors d’être homme.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_8">Vnumquodque autem animal eius rei maxime
                docile est, ad quam natura compositum est, uelut equus ad cursum, canis ad uenatum,
                auis ad uolandum, simius ad lusum.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_8" ana="#PE">Or chaque être vivant apprend
                facilement avant tout la chose pour laquelle il a été fait par la nature, comme le
                cheval pour la course, le chien pour la chasse, l’oiseau pour voler et le singe pour
                le jeu.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_9">Non est igitur quod naturam incusemus, sed
                magni refert unde pietatis et eruditionis elementa primum hauriantur, tum quem ad
                haec ducem nanciscatur puer, praesertim rudibus illis annis, quibus ingenium
                etiamnunc a uitiis purum, ueluti cera mollis in omnem fingentis habitum sequax est
                et obsecundans.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_9" ana="#PE">Il n’y a donc pas de raison
                d’accuser la nature, mais ce qui est important, c’est la source où puiser en premier
                les éléments de piété et de savoir, et le guide dans ces domaines à trouver pour
                l’enfant, surtout dans ces jeunes années au cours desquelles son esprit est encore
                exempt de vices, comme une cire tendre qui prend toutes les formes données par celui
                qui la modèle et s’y fond.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_10">Itaque multis nominibus uos felices
                iudico, ornatissimi pueri, primum quod hoc seculo nati sitis, quo ut uerae pietatis,
                ita et melioris literaturae sinceritas mirifice reuixit.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_10" ana="#PA">C’est pourquoi je vous estime
                heureux à beaucoup de titres, enfants si remarquables : la première raison est que
                vous êtes nés dans ce siècle qui a vu renaître de manière prodigieuse la pureté tant
                de la vraie piété que de la meilleure littérature.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_11">Nam me puero multum superstitionis habebat
                religio, et in scholis adolescentes summo cruciatu nihil fere discebant, nisi
                dediscendum.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_11" ana="#PE #RE">En effet, quand j’étais
                enfant, la religion comportait beaucoup de superstition, et les jeunes gens dans les
                écoles à grand renfort de tortures n’apprenaient presque rien sauf ce qu’il aurait
                fallu désapprendre.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_12">Deinde quod is uobis pater contigit, qui
                liberos suos parum beatos fore iudicat, si opum ac dignitatis, quibus inter
                Polonicae gentis proceres praecipuum tenet locum, eos parauerit haeredes, nisi et
                ueris animi bonis tum locupletes reddiderit tum expolitos.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_12" ana="#PA">La deuxième raison est que
                vous avez, par chance, un père qui estime que ses enfants ne seront pas heureux s’il
                les prépare à hériter des richesses et des honneurs, pour lesquels parmi les nobles
                du peuple polonais il occupe un rang excellent, sans aussi les rendre à la fois
                riches et parés des vrais biens de l’âme.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_13">Nec enim ille sibi uidetur parentis munus
                ad plenum obiisse, nisi quemadmodum genuit corpora, ita fingat et animos, haud
                ignarus hanc esse meliorem hominis partem, hanc esse praecipuam hominis
                possessionem.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_13" ana="#PA #PE">En effet il ne pense pas
                avoir accompli pleinement sa tâche de parent si, après avoir engendré des corps, il
                ne forge pas aussi des âmes sans ignorer que c’est la meilleure partie de l’homme,
                que c’est la propriété essentielle de l’homme.</ab>
            <ab type="orig" xml:id="Te1532_Erasmus_p1_14">Vir enim pius non tam sibi uos educat quam
                Christo et Reipublicae.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_14" ana="#PE #RE">En effet un homme pieux ne
                fait pas tant votre éducation pour lui-même que pour le Christ et l’État.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_15">Quod uos genuit, naturae est ; quod uos
                curat statim e nutricum sinu egressos, honestissimis disciplinis imbuendos, uerae
                pietatis est ; prudentiae uero quod ad haec <persName>Anselmum Ephorinum</persName>
                delegerit, uirum integritate, prudentia, fide, doctrina uigilantiaque cum primis
                spectatum et exploratum.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_15" ana="#PE #PA">Qu’il vous ait engendrés
                relève de la nature ; qu’il prenne soin de vous à peine sortis du sein des
                nourrices, pour vous imprégner des savoirs les plus honorables, relève de la vraie
                piété. Quant à ce qui relève de la sagesse, c’est d’avoir choisi pour cela Anselm
                Ephoryn, un homme à l’honnêteté, la sagesse, la foi, le savoir et la vigilance avant
                tout éprouvés et avérés.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_16">Quo nimirum acrius uobis enitendum est, ne
                quum omnia ad felicitatem suppeditata sint, ipsi uobis ipsis defuisse
                uideamini.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_16" ana="#PA">Vous devez assurément fournir
                d’autant plus d’efforts pour ne pas paraître, alors que vous avez tout pour réussir,
                avoir fait défaut à vous-mêmes.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_17">Magna de uobis, de te praesertim,
                    <persName ref="#bonerus_ioannes">Ioannes Bonere</persName>, expectatio est.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_17" ana="#PA">On attend beaucoup de vous, de
                toi surtout Jan Boner.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_18">Declarant hoc tam multae principum uirorum
                ad me literae, quibus indolem tuam mihi diligenter commendant, in primis
                prudentissimi regis <persName>Sigismundi</persName>, dein ornatissimorum praesulum
                Cracouiensis et Plocensis, clarissimi uiri <persName>Christophori a
                    Schydlouuitz</persName>, Sarmatici regni praefecti, ne iam commemorem
                    <persName>Antoninum</persName> medicum et <persName>Iustum</persName> regis a
                secretis, dignitate quidem inferiores, sed beneuolentia pares.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_18" ana="#PA">C’est ce que font voir les si
                nombreuses lettres de princes adressées à moi par lesquelles ils me recommandent
                avec soin ton caractère, d’abord le très sage roi Sigismond, puis les très honorés
                prélats de Cracovie et de Plotsk, le très illustre Krzysztof Szydlowiecki,
                chancelier du royaume de Pologne, pour ne pas mentionner en outre le médecin Antonin
                et Juste, secrétaire particulier du roi, de rang inférieur, mais de bienveillance
                égale.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_19">Intelligunt enim regna nullis ornamentis
                melius decorari quam uiris eruditione prudentiaque praecellentibus.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_19">Ils comprennent en effet que les
                meilleurs ornements possibles pour les royaumes sont les hommes qui se distinguent
                par leur savoir et leur sagesse.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_20">Hanc laudis partem non minus ambit rex
                huius aeui regum prudentissimus quam tot ex hostibus uictoriis delectatur.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_20">Cette partie de la gloire, le plus sage
                des rois de cette époque y aspire non moins qu’il ne se réjouit des si nombreuses
                victoires sur ses ennemis.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_21">Et hactenus quidem sat feliciter studiis
                effloruit uestra Sarmatia.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_21" ana="#PA">Et jusqu’à maintenant votre
                Pologne s’est épanouie avec assez de succès dans les études.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_22">Et si quid erat natiuae feritatis,
                commercio literarum exuit.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_22" ana="#PA">Et si elle possédait une forme
                de sauvagerie innée, elle s’en défait grâce au commerce des lettres.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_23">Sed bona spes est fore, ut hoc
                ornamentorum genere cum qualibet regione certare ualeat, praesertim ductu
                auspiciisque <persName>Sigismundi</persName> regis iunioris, qui praeter alias
                uirtutes excellenti principe dignas, optimis quoque literis sese regno parat.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_23">Mais il y a bon espoir que par ce genre
                d’ornements elle puisse rivaliser avec n’importe quelle région, surtout sous la
                conduite et les auspices du jeune roi Sigismond qui, outre les vertus dignes d’un
                excellent prince, se prépare à régner avec une très bonne culture.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_24">Verum exhortandi gratia nonnihil et antea
                scripsimus tibi et in posterum quoties dabitur occasio scripturi sumus.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_24" ana="#PA">Mais en guise d’exhortation
                nous t’avons déjà écrit quelques lettres auparavant et à l’avenir nous t’écrirons
                chaque fois que l’occasion nous en sera donnée.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_25">Nunc quo <seg type="reecriture">calcar
                    aliquod addamus currenti<bibl resp="#JYV"><author ref="#erasmus_desiderius"
                            >Erasme</author>, <title ref="#er_ad">Adagia</title>,
                            <biblScope>147</biblScope><ref
                            target="http://ihrim.huma-num.fr/nmh/Erasmus/Proverbia/Adagium_147.html"
                        /></bibl></seg>, licet <persName ref="#ter">Terentii</persName> comoedias,
                Bonerici nominis auspicio prodire uoluimus, sed multo quam antea fuerunt
                castigatiores.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_25" ana="#PH">Maintenant pour ajouter
                quelque stimulant à la course, nous avons certes voulu que les comédies de Térence,
                sous le patronage des Boner, soient publiées, mais beaucoup mieux corrigées qu’elles
                ne l’étaient auparavant.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_26">Hoc ut uellem, effecit tua
                    <persName>Stanislaique Glandini</persName> famuli tui dexteritas, qui nobis
                interdum aliquot scenas tam feliciter agere soletis ut et imitationem, primum ut ait
                    <persName ref="#quint">Fabius</persName>, <seg type="paraphrase">in pueris
                    ingenii signum<bibl resp="#LH"><author ref="#quint">Quint.</author>, <title
                            ref="#quint_io">I.O.</title>
                        <biblScope>1.3.1</biblScope>.</bibl></seg>, et memoriam uehementer
                admirarentur, quicumque nobiscum spectabant.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_26" ana="#TH_repr #PA">J’ai été amené à ce
                projet par ton habileté et celle de <seg>Stanislaw Aichler<note>Stanislaus Glandinus
                        = Stanislaw Aichler (vers 1519-vers 1585). À ne pas confondre avec Stanislaw
                        Boner (1523-1560, frère cadet de Jan (1516-1562).</note></seg>, un de ta
                suite, vous qui avec bonheur avez parfois coutume de jouer pour nous quelques scènes
                afin que tous ceux qui regardaient le spectacle avec nous admirent vivement à la
                fois votre imitation qui, comme le dit Quintilien, est le signe du talent chez les
                enfants, et votre mémoire.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_27">Non ex alio scriptore melius discitur
                Romani sermonis puritas, nec est alius lectu iucundior, aut puerorum ingeniis
                accommodatior.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_27" ana="#RH #PE">D’aucun autre écrivain on
                n’apprend mieux la pureté du style romain, et aucun autre n’est plus agréable à lire
                ni plus adapté aux esprits des enfants.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_28">Ceterum id praestabit
                    <persName>Ephorini</persName> praeceptoris dexteritas ut et ad eloquentiam et ad
                mores sit utilis.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_28" ana="#PA">Mais l’habileté du professeur
                Ephorin réussira à la rendre utile à la fois à l’éloquence et au sens moral.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_29">Quemadmodum enim bona prudentiae pars est,
                nosse uarios hominum mores et ingenium, ita decorum et tractatio affectuum, qui cum
                primis adferunt iucunditatem orationi, Graeci uocant <foreign xml:lang="grc"
                    >ἤθη</foreign>, ex nullo rhetorum melius discitur.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_29" ana="#RH #PHILO">En effet, de même
                qu’une partie bénéfique de la sagesse consiste à connaître les caractères et les
                esprits variés des hommes, de même les bienséances et le maniement des affects qui
                apportent avant tout de l’agrément au discours, ce que les Grecs appellent <foreign
                    xml:lang="grc">êthê</foreign>, ne s’apprennent mieux chez aucun orateur.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_30">Porro quum inter oratorias partes
                praecipua sit argumentorum inuentio, ad hanc quoque facultatem in omni causarum
                genere parandam Terentiana lectio conducit plurimum.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_30" ana="#RH">De plus, alors que parmi les
                parties de l’art oratoire, la plus importante est l’invention des arguments, la
                lecture de Térence est des plus utiles aussi pour acquérir cette faculté dans tous
                les genres de causes.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_31">Nec enim sine causa criticorum suffragia
                artem huic autori tribuere.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_31" ana="#RH">En effet ce n’est pas sans
                raison que les suffrages des critiques ont décerné à cet auteur le statut de
                grammairien.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_32">Plus enim exacti iudicii est in una
                comoedia <persName ref="#ter">Terentiana</persName>, absit Nemesis dicto, quam in
                    <persName ref="#pl">Plautinis</persName> omnibus.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_32" ana="#TH_dram">Il y a en effet dans une
                seule comédie de Térence – <seg>que Némésis soit absente de mon
                        propos<note>L’expression signifie « soit dit sans acrimonie » et équivaut à
                            <foreign xml:lang="la">absit invidia</foreign>.</note></seg> – plus
                d’exactitude dans le jugement que dans toutes celles de Plaute.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_33">Sed in tali autore uelut in tabula quapiam
                    <persName>Apellis</persName>, magni refert quis adsit commonstrator.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_33" ana="#PA">Mais chez un tel auteur, comme
                pour n’importe quel tableau d’Apelle, la personnalité du guide a une grande
                importance.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_34">Qui si contingat egregius artifex, haec
                lectio non solum ualebit ad exhilarandum animum, non tantum ad emendate loquendum,
                non modo ad uberiorem dicendi facultatem, uerum etiam non parum adferet philosophiae
                moralis, quam unam <persName>Socrates</persName> existimauit esse perdiscendam
                homini qui cupiat beate uiuere.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_34" ana="#PHILO #RH">Si ce guide se révèle
                un artiste remarquable, cette lecture non seulement sera très efficace pour réjouir
                l’âme, non seulement pour parler correctement, non seulement pour enrichir la
                faculté oratoire, mais elle apportera aussi une quantité non négligeable de
                philosophie morale, la seule que selon Socrate doit apprendre de manière approfondie
                l’homme qui désire vivre heureux.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_35">Itaque ni te sponte currentem uiderem,
                carissime <persName ref="#bonerus_ioannes">Ioannes</persName>, etiam atque etiam
                hortarer ut reputes quantum sis Deo nomen, cuius munificentia, tam felicem indolem
                docileque ingenium sortitus es, cui debes et parentem <persName>Seuerinum
                    Bonerum</persName> et <persName>Anselmum</persName> parentem alterum, tum
                qualium uirorum quanta de te sit expectatio, et in te coniecti oculi, postremo quam
                istaec aetas ad honestas disciplinas sit apposita.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_35" ana="#PA #PE #RE">C’est pourquoi, si je
                ne te voyais pas courir de toi-même, très cher Jan, je ne cesserais de t’exhorter à
                méditer quel grand nom tu es pour Dieu grâce à la générosité duquel tu as obtenu en
                partage un caractère si heureux et un esprit doué pour apprendre, Dieu à qui tu es
                redevable d’avoir à la fois ton père Seweryn Boner et ton autre parent Anzelm,
                combien est grande l’attente de tels hommes à ton égard et les yeux tournés vers
                toi, enfin combien ton âge est propice aux disciplines honorables.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_36">Nondum enim, ut opinor, annum
                decimumquartum excessisti.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_36" ana="#PE">En effet tu n’as pas encore,
                je pense, dépassé ta quatorzième année.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_37">Nam ad parandam eruditionem plus ualet in
                teneris annis unicus quam decem, ubi iam animus aliis occupatus curis ad docilitatem
                induruit.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_37" ana="#PE">Car pour acquérir un savoir,
                dans les jeunes années, mieux vaut une que dix quand l’esprit déjà occupé à d’autres
                soucis a perdu de sa malléabilité aux apprentissages.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_38">Adde huc quod ea demum in omnem uitam
                tenacissime haerent, quae pueri imbibimus.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_38" ana="#PE">Ajoute à cela qu’enfin
                s’enracinent très profondément pour toute la vie les choses dont nous nous sommes
                imprégnés enfants.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_39">Dimittam te, si prius illud admonuero ut
                in omni genere carminum temet diligenter exerceas, eo quod qui hanc eruditionis
                partem neglexerunt, minore tunc fructu, tum uoluptate uersantur in autoribus qui
                carmen conscripserunt.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_39" ana="#PA">Je prendrai congé de toi après
                t’avoir incité à t’exercer avec application dans tous les genres poétiques parce que
                ceux qui ont négligé cette partie du savoir, tirent à la fois moins de profit et
                moins de plaisir à fréquenter des auteurs qui ont composé des poèmes.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_40">Iidem facilius labuntur decepti mendis
                codicum quos saepenumero prodit metri ratio.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_40" ana="#PH #M">Ces mêmes personnes se
                laissent plus facilement abuser par les fautes des manuscrits, eux que bien souvent
                la métrique trahit.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_41">Quoniam autem in hoc autore perturbatior
                ac liberior est carminum ratio, pauca quaedam annotauimus, lucis, ut opinor,
                nonnihil allatura parum exercitatis.</ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_41" ana="#M">Or puisque la métrique chez cet
                auteur est assez irrégulière et libre, nous avons écrit quelques remarques destinées
                à apporter à mon avis quelque lumière à ceux qui ont peu d’expérience.</ab>

            <ab type="orig" xml:id="Te1532_Erasmus_p1_42">Vale et fruere. Friburgi Brisgauiae prid.
                Id. Decemb. Anno a Christo nato MDXXXII </ab>
            <ab type="trad" corresp="#Te1532_Erasmus_p1_42">À Fribourg-en-Brisgau la veille des Ides
                de décembre de l’année 1532 à partir de la naissance du Christ (<seg>12 décembre
                        1532<note>Les critiques datent la lettre de 1531. 1532 est sans doute une
                        erreur d’impression.</note></seg>).</ab>

        </body>
    </text>
</TEI>
