<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Argumentum</title>
                <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : "la" pour le latin et "grc" pour le grec -->
                <!-- Attention ! ne pas faire de retour charriot dans le titre -->
                <author ref="#chilius_adrianus">Adrianus Chilius</author>
                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Ajouter autant de tâches (<respStmt>) que nécessaire -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Diandra CRISTACHE</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Chrisitan NICOLAS</name>
                </respStmt>
                <respStmt>
                    <resp>Révision</resp>
                    <name>Malika BASTIN-HAMMOU</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Malika BASTIN-HAMMOU</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>

                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <principal>Malika BASTIN-HAMMOU</principal>
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne Garcia-Fernandez</persName>
                </respStmt>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
            <sourceDesc>
                <bibl>
                    <title>Aristophanis comici facetissimi Plutus. Adriano Chilio interprete.
                        Plutus. </title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#ar">Aristophanes</author>
                    <pubPlace ref="#antuerpia">Antuerpiae</pubPlace>
                    <publisher ref="#hillenius_michael">Michael Hillenius</publisher>
                    <date when="1533">1533</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor role="traducteur" ref="#chilius_adrianus">Adrianus Chilius</editor>
                    <ref target="url_edition_numérique"/>
                    <!-- pour les liens google book, remplacer les & par &amp; pour que ça fonctionne -->
                </bibl>

                <listBibl>
                    <bibl/>
                    <!-- créer un <bibl> par référence -->
                </listBibl>

            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p/>
                <!-- s'il s'agit d'une liste, utiliser les éléments ci-dessous -->
                <list>
                    <item/>
                    <item/>
                    <item/>
                </list>
            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>
            <textClass n="vers"></textClass>
        </profileDesc>
        <revisionDesc>
            <change when="2022-09-30">Malika BASTIN-HAMMOU : encodage sémantique</change>
            <change when="2022-03-29">Sarah GAUCHER : création de la fiche TEI, encodage de la
                transcription et de la traduction</change>
        </revisionDesc>
    </teiHeader>

    <text>
        <body ana="#A">

            <!-- RAPPEL : on encode le texte latin ou grec; on n'encode dans le texte de la traduction que les notes d'éditeur (=membres du projet) -->

            <!-- Le couple d'éléments <head> contient le titre ou l'incipit du paratexte -->
            <!-- L'identifiant du TITRE est composé du titre de la fiche TT suivi d'un underscore (_) puis du chiffre 0 -->
            <head type="orig" xml:id="Ar1533_Chilius_p2_0">Argumentum</head>
            <!-- Le deuxième élément du couple contient la traduction française du titre ou incipit -->
            <!-- L'identifiant de la traduction est le même que celui du latin, précédé d'un hashtag (#) -->
            <head corresp="#Ar1533_Chilius_p2_0" type="trad">Argument</head>
            
            <ab type="orig" xml:id="Ar1533_Chilius_p2_1"><l>Apollinem uir iustus, is pauper
                    tamen,</l><l> Consultat an uersis queat uitae uiis</l><l> Ditescere. Hic sequi
                    monetur, edito</l><l> Oraculo, sibi obuium primum uirum. </l></ab>
            <ab type="trad" corresp="#Ar1533_Chilius_p2_1">Un homme juste, mais pauvre, consulte
                Apollon pour savoir s’il peut en changeant de mode de vie, s’enrichir. Il reçoit,
                par l’oracle, le conseil de suivre le premier homme qu’il rencontre.</ab>
            
            <ab type="orig" xml:id="Ar1533_Chilius_p2_2"><l>Deducit hunc, ut nouit, ilico
                domum.</l></ab>
            <ab type="trad" corresp="#Ar1533_Chilius_p2_2">Il l’emmène aussitôt, dès qu’il l’a
                reconnu, chez lui.</ab>
            
            <ab type="orig" xml:id="Ar1533_Chilius_p2_3"><l>Popularibusque ad se uocatis
                    ceteris,</l><l> Eius fruendi una potestatem facit. </l></ab>
            <ab type="trad" corresp="#Ar1533_Chilius_p2_3">À tous ses concitoyens appelés chez lui,
                il donne accès à lui.</ab>
            
            <ab type="orig" xml:id="Ar1533_Chilius_p2_4"><l>Dein Aesculapii in aede sacra
                    collocant,</l><l> Vt eius oculos lumine illustret deus.</l></ab>
            <ab type="trad" corresp="#Ar1533_Chilius_p2_4">Puis ils font un sacrifice au temple
                d’Esculape et le dieu rend la vue à ses yeux.</ab>
            
            <ab type="orig" xml:id="Ar1533_Chilius_p2_5"><l>Quibus inopia reluctitans ludit
                    operam.</l></ab>
            <ab type="trad" corresp="#Ar1533_Chilius_p2_5">La pauvreté leur résiste et se moque de
                son œuvre.</ab>
            
            <ab type="orig" xml:id="Ar1533_Chilius_p2_6"><l>Pluto tamen cernente, quisquis est
                    malus</l><l> Eget, uicissim, diues et quisquis bonus. </l></ab>
            <ab type="trad" corresp="#Ar1533_Chilius_p2_6">Mais comme Plutus fait désormais la part
                des choses, tout méchant devient mendiant, tout homme de bien devient riche.</ab>


        </body>
    </text>
</TEI>
