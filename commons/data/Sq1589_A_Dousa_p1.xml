<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

  <teiHeader>

    <fileDesc>

      <titleStmt>

        <title xml:lang="la">Quae tibi rependent, docte LIPSI, praemia…</title>
        <!-- Attention ! ne pas faire de retour charriot dans le titre -->
        <author ref="#dousa_ianus_f">Janus Dousa Filius</author>

        <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
        <!-- Tâche n°1 -->
        <respStmt>
          <resp>Transcription</resp>
          <name>Pascale PARÉ-REY</name>
        </respStmt>
        <!-- Tâche n°2 -->
        <respStmt>
          <resp>Traduction</resp>
          <name>Pascale PARE-REY</name>
        </respStmt>
        <!-- Tâche n°3 -->
        <respStmt>
          <resp>Encodage</resp>
          <name>Valérie THIRION</name>
          <name>Sarah GAUCHER</name>
        </respStmt>
        <!-- Ajouter autant de tâches que nécessaire -->
        <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
        <respStmt>
          <resp>Modélisation et structuration</resp>
          <persName>Elisabeth GRESLOU</persName>
          <persName>Elysabeth HUE-GAY</persName>
        </respStmt>
        <principal>Malika BASTIN-HAMMOU</principal>
      </titleStmt>

      <!-- Informations de publication sur le laboratoire numérique du projet-->
      <publicationStmt>
        <publisher/>
        <availability>
          <p>Le site est en accès restreint</p>
        </availability>
        <pubPlace>ithac.elan-numerique.fr</pubPlace>
      </publicationStmt>

      <sourceDesc>

        <bibl>
          <title>Decem tragoediae quae Lucio Annaeo Senecae tribuuntur : operâ Francisci Raphelengii
            Fr. F. Plantiniani, ope v. cl. Iusti Lipsii emendatiores cum utriusque ad easdem
            Animadversionibus et Notis.</title>
          <!-- l'attribut @level permet d'indiquer s'il s'agit d'une monographie (m), d'un article ou chapitre d'ouvrage (a) ou d'un périodique (j) -->
          <author ref="#sen">Seneca</author>
          <pubPlace ref="#antuerpia">Antuerpiae</pubPlace>
          <publisher ref="#plantinus_christophorus">Christophe Plantin</publisher>
          <date when="1589">1589</date>
          <editor ref="#raphelengius_franciscus_f">Franciscus Raphelengius II</editor>
          <ref
            target="https://books.google.fr/books?id=MffMbAX5OroC&amp;pg=PA449&amp;dq=decem+tragoediae&amp;hl=fr&amp;sa=X&amp;ved=0ahUKEwioqMTFs8zJAhUCXhoKHRCTBC8Q6AEIUjAG#v=onepage&amp;q=decem%20tragoediae&amp;f=false"
            >https://books.google.fr/books?id=MffMbAX5OroC&amp;pg=PA449&amp;dq=decem+tragoediae&amp;hl=fr&amp;sa=X&amp;ved=0ahUKEwioqMTFs8zJAhUCXhoKHRCTBC8Q6AEIUjAG#v=onepage&amp;q=decem%20tragoediae&amp;f=false</ref>
        </bibl>
        <listBibl>
          <bibl>Freiburger Neulateinisches Symposion, Eckard Lefèvre, et Eckart Schäfer, éd. Ianus
            Dousa: neulateinischer Dichter und klassischer Philologe. Tübingen: GNV, Gunter Narr
            Verlag, 2009.</bibl>
          <bibl>Gemeentearchief Leiden, et C.L Heesakkers. Janus Dousa en zijn vrienden: leidraad
            bij een tentoonstelling. Leiden: Universiteitsbibliotheek, 1973.</bibl>
          <bibl>Heesakkers, C.L. Een netwerk aan de basis van de Leidse universiteit: het album
            amicorum van Janus Dousa. Leiden; Den Haag: Universiteitsbibliotheek Leiden ; Jongbloed,
            2000.</bibl>
          <bibl>Heesakkers, C.L, Wilma M.S Reinders, et Jan Biezen. Genoeglijk bovenal zijn mij de
            Muzen: de Leidse Neolatijnse dichter Janus Dousa (1545-1604). Leiden: Dimensie,
            1993.</bibl>

        </listBibl>
      </sourceDesc>
    </fileDesc>

    <profileDesc>

      <abstract>
        <p>Le poète et philologue Janus Dousa adresse ces sénaires iambiques à Juste Lipse, dont les
          annotations ont été reprises dans cette publication, et sur lesquelles Rapheleng s’est
          appuyé pour proposer les siennes. Le poème fait intervenir, notés en lettres capitales,
          les noms de Lipse, le commentateur, de Sénèque, l’auteur commenté, et trois de ses héros,
          Œdipe, Hercule, Hippolyte. Il s’articule autour d’un jeu principal sur l’adjectif <foreign
            xml:lang="la">doctus</foreign> (v. 1, 2 et 4, pour qualifier Lipse, les Camènes et
          Sénèque) et d’un jeu secondaire sur les adjectifs <foreign xml:lang="la">unus</foreign> et
            <foreign xml:lang="la">unicus</foreign> (v. 20, 21 et 22, pour parler de Sénèque et de
          Lipse et de leur œuvre unique).</p>
      </abstract>

      <langUsage>
        <!-- ne pas modifier -->
        <language ident="fr">Français</language>
        <language ident="la">Latin</language>
      </langUsage>

      <textClass n="vers"></textClass>
      
    </profileDesc>

    <revisionDesc>
      <!-- pour indiquer des modifications apportées au fichier -->
      <change when="2022-03-02">Sarah GAUCHER : identifiants Header, mise en conformité
        Elan</change>
      <change who="Diandra CRISTACHE" when="2021-05-23">Diandra CRISTACHE : mise à jour et
        publication sur le Pensoir</change>
      <change when="2021-03-22">Valérie Thirion: encodage de la transcription</change>
    </revisionDesc>

  </teiHeader>

  <text>
    <body ana="#PH #TH_hist #PA">

      <!-- le texte est composée de paragraphes (unités de traduction) <ab> de type orig pour le texte original et trad pour la traduction. Ils sont liés grâce à un identifiant (@xml:id et @corresp)  -->
      <ab type="orig" xml:id="Sq1589_A_Dousa_p1_1">
        <l>Quae tibi rependent, docte <persName ref="#lipsius_iustus">LIPSI</persName>, praemia</l>
        <l>Doctae Camœnae, quas iuuare sedulo</l>
        <l>Nulloque cessas demereri tempore ?</l>
      </ab>
      <ab corresp="#Sq1589_A_Dousa_p1_1" type="trad">
        <l>Tes faveurs, savant LIPSE, par quelles savantes Camènes</l>
        <l>seront-elles récompensées, elles que tu ne cesses</l>
        <l>de réjouir et de gagner sans nul répit ?</l>
      </ab>

      <ab type="orig" xml:id="Sq1589_A_Dousa_p1_2">
        <l>In lucis oras doctus en <persName ref="#sen">SENECA</persName> redit</l>
        <l>Quatiens cothurnos <persName ref="#eur">Euripidis</persName> ardui ;</l>
        <l>Heroes una, parte sui superstites</l>
        <l>Meliore, redeunt, non, ut ante, debiles,</l>
        <l>Non lancinati.</l>
      </ab>
      <ab corresp="#Sq1589_A_Dousa_p1_2" type="trad">
        <l>Voici que revient le docte SÉNÈQUE aux rives de la lumière,</l>
        <l>secouant les cothurnes du difficile Euripide ;</l>
        <l>ensemble les héros survivants reviennent,</l>
        <l>avec la meilleure part d’eux-mêmes, non, </l>
        <l>comme auparavant, faibles ou déchirés.</l>
      </ab>

      <ab type="orig" xml:id="Sq1589_A_Dousa_p1_3">
        <l>Dulcis usum luminis</l>
        <l>Sibi restitutum gratulatur ŒDIPUS ;</l>
        <l>Nec nomine uno debet HERCULES tibi ;</l>
        <l>Vel quod scelesta caede manantes manus</l>
        <l>Viuo abluisti rore fluminis tui ;</l>
        <l>Vel alta scandens astra ab Œtaeo rogo,</l>
        <l>Consumptus igne profuisse quod sibi</l>
        <l>Te plus uel ipso sensit Aesculapio.</l>
      </ab>
      <ab corresp="#Sq1589_A_Dousa_p1_3" type="trad">
        <l>ŒDIPE se félicite que l’usage</l>
        <l>de la douce lumière lui soit rendu ;</l>
        <l><seg>et HERCULE ne te doit pas un seul nom<note>Puisque Sénèque a écrit un <title>Hercule
                Furieux</title> et un <title>Hercule sur l’Œta</title>, auxquels il est
              respectivement fait allusion dans les vers qui suivent.</note></seg> ;</l>
        <l>soit que, les mains dégoutantes d’un crime affreux,</l>
        <l>tu l’as lavé par la vive rosée de ton fleuve ;</l>
        <l>soit que, gravissant les hautes étoiles depuis le bûcher de l’Œta,</l>
        <l>brûlé par le feu, il sentit que tu lui étais plus utile</l>
        <l>qu’Esculape lui-même.</l>
      </ab>

      <ab type="orig" xml:id="Sq1589_A_Dousa_p1_4">
        <l>Laudat renatus VIRBIUS tuam manum</l>
        <l>Reductus ipsis iam secundum ab inferis.</l>
      </ab>
      <ab corresp="#Sq1589_A_Dousa_p1_4" type="trad">
        <l><seg>VIRBIUS<note>Virbius : nom que reçut Hippolyte après que Diane lui rendit la
              vie.</note></seg>, ressuscité, loue ta main</l>
        <l>Après être revenu des Enfers eux-mêmes deux fois déjà.</l>
      </ab>

      <ab type="orig" xml:id="Sq1589_A_Dousa_p1_5">
        <l>Quid plura referam ? Romulus debet tibi</l>
        <l>Cothurnus omnis, inuidens ante Helladi,</l>
        <l>Plus quam poetae ; nam ille in hoc unus fuit,</l>
        <l>Quiritium unis ut placeret auribus ;</l>
        <l>Tu non, in urbe tantum ut unica populi</l>
        <l>Teneret aures usque, fecisti, uagas :</l>
        <l>Sed orbe toto reddidisti nobilem.</l>
      </ab>
      <ab corresp="#Sq1589_A_Dousa_p1_5" type="trad">
        <l>Pourquoi en rajouter ? Chaque cothurne romain</l>
        <l>t’est redevable, qui naguère jalousait la Grèce</l>
        <l>plus que notre poète ; car celui-là fut seul</l>
        <l>à charmer les oreilles seules des Quirites ;</l>
        <l>Toi, ce n’est pas dans une seule ville que tu as fait</l>
        <l>en sorte de tendre les oreilles vagabondes de la foule :</l>
        <l>mais c’est dans l’univers entier que tu l’as rendu célèbre.</l>
      </ab>

      <ab type="orig" xml:id="Sq1589_A_Dousa_p1_6">
        <persName ref="#dousa_ianus_f">Ianus Dousa Filius</persName>
      </ab>
      <ab corresp="#Sq1589_A_Dousa_p1_6" type="trad"> Janus Dousa Fils. </ab>


    </body>
  </text>
</TEI>
