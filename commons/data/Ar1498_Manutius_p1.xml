<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Aldus Manutius Romanus, Danieli Clario Parmensi, s.p.d</title>
                <author ref="#manutius_aldus">Aldus Manutius</author>

                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Diandra CRISTACHE</name>
                    <name>Sarah GAUCHER</name>
                    <name>Malika BASTIN-HAMMOU</name>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <sourceDesc>

                <bibl>
                    <title><foreign xml:lang="grc">ΑΡΙΣΤΟΦΆΝΟΥΣ ΚΩΜΩΙΔΊΑΙ ἘΝΝΕΑ</foreign>.
                        ARISTOPHANIS COMOEDIAE NOVEM.</title>
                    <author ref="#ar">Aristophanes</author>
                    <pubPlace ref="#uenetia">Venetiae</pubPlace>
                    <publisher ref="#manutius_aldus">Aldus Manutius</publisher>
                    <date when="1498">1498</date>
                    <editor ref="#manutius_aldus">Aldus Manutius</editor>
                    <ref
                        target="https://daten.digitale-sammlungen.de/0004/bsb00045673/images/index.html?id=00045673&amp;groesser=&amp;fip=yztseneayaeayaxdsydwqrseayaqrsyzts&amp;no=24&amp;seite=10"
                        >https://daten.digitale-sammlungen.de/0004/bsb00045673/images/index.html?id=00045673&amp;groesser=&amp;fip=yztseneayaeayaxdsydwqrseayaqrsyzts&amp;no=24&amp;seite=10
                    </ref>
                </bibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Liste des paratextes dans la même édition : <list>
                        <item>1. Aldus Manutius Romanus Danieli Clario Parmensi S.P.D.</item>
                        <item>2. Μάρκος Μουσοῦρος ὁ Κρὴς τοῖς ἐντευξομένοις εὖ πράττειν.</item>

                    </list>
                </p>
            </abstract>

            <langUsage>
                <language ident="la">Latin</language>
            </langUsage>

        </profileDesc>

        <revisionDesc>
            <change when="2022-03-03">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
            <change when="2020-09-27">Diandra CRISTACHE : Encodage de la transcription et de la
                traduction</change>
        </revisionDesc>

    </teiHeader>

    <text>
        <body>
            <!-- ajouter la balise LI) -->

            <head type="orig" xml:id="Ar1498_Manutius_Manutius_p1_0"><persName ref="#manutius_aldus"
                    >Aldus Manutius Romanus</persName>, <persName ref="#clarius_danielis"
                    role="destinataire">Danieli Clario Parmensi</persName>, s.p.d.</head>
            <head corresp="#Ar1498_Manutius_Manutius_p1_0" type="trad">Alde Manuce le Romain salue
                grandement Daniel Clarius de Parme </head>

            <ab type="orig" xml:id="Ar1498_Manutius_Manutius_p1_1">Perbeati illi mihi uidentur,
                    <persName ref="#clarius_danielis">Clari</persName>, uir doctissime, qui hoc
                tempore, in summa bonorum librorum copia, liberalibus disciplinis operam daturi,
                Graece discunt.</ab>
            <ab type="trad" corresp="#Ar1498_Manutius_Manutius_p1_1" ana="#PE">Ils me semblent
                parfaitement heureux, très savant Clarius, ceux qui aujourd’hui, dans la plus grande
                abondance de bons livres, apprennent le grec dans l’idée de se consacrer aux arts
                libéraux. </ab>

            <ab type="orig" xml:id="Ar1498_Manutius_Manutius_p1_2">Facile enim ac breui Graecam
                linguam, nisi ipsi sibi defuerint, consequentur, in qua multis saeculis nullus fere
                ex Latinis, culpa magis temporum quam ingeniorum, excelluit. </ab>
            <ab type="trad" corresp="#Ar1498_Manutius_Manutius_p1_2" ana="#PE">En effet, s’ils ne se
                font pas défaut à eux-mêmes, ils parviendront facilement et rapidement à la maîtrise
                de la langue grecque, dans laquelle pendant de nombreux siècles presque personne
                parmi les Latins n’a excellé en raison davantage des circonstances que par manque
                d’intelligence. </ab>

            <ab type="orig" xml:id="Ar1498_Manutius_Manutius_p1_3">Facillime, Graecis litteris
                adiutricibus, omnium laudatarum artium procreatricem philosophiam callebunt, nec
                medicinam minus. </ab>
            <ab type="trad" corresp="#Ar1498_Manutius_Manutius_p1_3" ana="#PE">Très facilement, avec
                l’aide des lettres grecques, ils deviendront experts en philosophie, qui est la mère
                de tous les arts dignes de louanges, et pas moins en médecine. </ab>

            <ab type="orig" xml:id="Ar1498_Manutius_Manutius_p1_4">Errant meo iudicio multum, qui se
                bonos philosophos medicosque euasuros hoc tempore existimant, si expertes fuerint
                litterarum Graecarum. </ab>

            <ab type="trad" corresp="#Ar1498_Manutius_Manutius_p1_4" ana="#PE">Ils se trompent
                grandement, selon moi, ceux qui pensent qu’ils finiront par devenir de bons
                philosophes et de bons médecins, sans avoir appris le grec.</ab>

            <ab type="orig" xml:id="Ar1498_Manutius_Manutius_p1_5">Quibus et <persName ref="#arstt"
                    >Aristoteles</persName> quicquid ad dialecticen, ad philosophiam, et naturalem
                et transnaturalem, et moralem, quicquid ad rhetoricen et poeticen pertinet,
                doctissime scripsit, et <persName ref="#ammon">Ammonius</persName>, <persName
                    ref="#simpl">Simplicius</persName>, <persName ref="#them">Themistius</persName>,
                    <persName ref="#aaphr">Alexander Aphrodisieus</persName>, <persName
                    ref="#philopon">Philoponus</persName>, <persName ref="#eustr"
                    >Eustrathius</persName>, et caeteri Peripateticae sectae eruditissimi uiri,
                omnia quaecunque uel scientiae peruestigatione, uel disserendi ratione
                comprehenderat <persName ref="#arstt">Aristoteles</persName>, optime ac
                luculentissime commentati sunt. </ab>

            <ab type="trad" corresp="#Ar1498_Manutius_Manutius_p1_5" ana="#PE">Car c’est en grec
                qu’Aristote a écrit de manière très savante tout ce qui a trait à la dialectique, la
                philosophie - naturelle, métaphysique et éthique -, tout ce qui a trait à la
                rhétorique et à la poétique, et c’est en grec qu’Ammonius, Simplicius, Themistius,
                Alexandre d’Aphrodise, Philopon, Eustrathe et d’autres grands savants de l’école des
                Péripatéticiens ont commenté au mieux et de manière tout à fait lumineuse tous ces
                domaines qu’Aristote a embrassés soit par l’investigation scientifique, soit par la
                raison discursive. </ab>

            <ab type="orig" xml:id="Ar1498_Manutius_Manutius_p1_6">Quibus item <persName ref="#hpc"
                    >Hippocrates</persName>, <persName ref="#gal">Galenus</persName>, <persName
                    ref="#peg">Paulus</persName>, et alii in medicina excellentissimi uiri omnia
                quae ad medicae artis spectant cognitionem copiosissime, uerissimeque literis
                commendarunt. </ab>
            <ab type="trad" corresp="#Ar1498_Manutius_Manutius_p1_6" ana="#PE">Et de la même manière
                c’est aux lettres grecques qu’Hippocrate, Galien, Paul et d’autres grands savants en
                matière de médecine ont confié abondamment et précisément tout ce qui regarde la
                connaissance de l’art médical.</ab>

            <ab type="orig" xml:id="Ar1498_Manutius_Manutius_p1_7">Non aliis quam Graecis literis ii
                qui mathematici uocantur artem suam obscuram, reconditam, multiplicem subtilemque,
                facillimam cognitu posteris tradiderunt ; quo in genere permulti, ut
                    <persName>Architas</persName>, <persName>Ptolemaeus</persName>,
                    <persName>Nicomachus</persName>, <persName>Porphyrius</persName>,
                    <persName>Euclides</persName>, perfecti homines extiterunt.</ab>
            <ab type="trad" corresp="#Ar1498_Manutius_Manutius_p1_7" ana="#PE">Et ça n’est pas à
                d’autres lettres que les grecques que ceux qu’on appelle les mathématiciens ont
                confié leur art obscur, difficilement accessible, complexe et subtil pour qu’il soit
                très accessible à la postérité ; discipline dans laquelle de très
                nombreux individus, comme Architas, Ptolémée, Nichomaque, Porphyre, Euclide, qui
                sont des hommes accomplis, se sont illustrés. </ab>

            <ab type="orig" xml:id="Ar1498_Manutius_Manutius_p1_8">Quae omnia quam deprauate et
                corrupte, quam mutilate et perperam, ut taceam etiam quam barbare et inepte Latinis
                scripta sint, qui uel mediocriter eruditus ignorat ?</ab>
            <ab type="trad" corresp="#Ar1498_Manutius_Manutius_p1_8" ana="#T">Toutes ces
                connaissances, quel savant ignore vraiment combien elles ont été rendues en latin de
                manière fautive et corrompue, partielle et erronée, pour ne pas dire même barbare et
                inepte ?</ab>

            <ab type="orig" xml:id="Ar1498_Manutius_Manutius_p1_9">Sed breui spero futurum ut,
                explosa barbarie, reiectisque ineptiis, bonis literis, uerisque disciplinis, non ut
                nunc a paucissimis, sed uno consensu ab omnibus, incumbatur.</ab>
            <ab type="trad" corresp="#Ar1498_Manutius_Manutius_p1_9" ana="#PE">Mais j’espère que
                bientôt, la barbarie repoussée et les inepties rejetées, il arrivera que ne
                s’attacheront aux belles-lettres, et aux vraies disciplines, non pas, comme
                maintenant, un tout petit nombre, mais tous avec un accord général.</ab>

            <ab type="orig" xml:id="Ar1498_Manutius_Manutius_p1_10">En! erit tandem ut, <seg
                    type="reecriture">glande neglecta<bibl resp="#MBH"><author ref="#cic"
                            >Cic.</author>
                        <title ref="#cic_or">Or.</title>
                        <biblScope>31</biblScope>
                        <note><foreign xml:lang="la">glande uesci</foreign>, « se nourrir de glands
                            »</note></bibl></seg>, inuentis uescamur frugibus. </ab>
            <ab type="trad" corresp="#Ar1498_Manutius_Manutius_p1_10" ana="#PE">Alors enfin il
                arrivera que, rejetant le gland, nous nous nourrissions des récoltes de ce que nous
                avons découvert. </ab>

            <ab type="orig" xml:id="Ar1498_Manutius_Manutius_p1_11">Optime igitur tu, mi Clari, in
                praestanti ista et opulenta urbe Ragusio iuuentuti consulis, qui eam et <seg
                    type="paraphrase">Graece et Latine simul<bibl resp="#SG"><author ref="#quint"
                            >Quint.</author>, <title ref="#quint_io">I.O.</title>
                        <biblScope>1.1.14</biblScope>.</bibl></seg>, ut praecipit <persName
                    ref="#quint">Quintilianus</persName>, summo studio ac fide iam multos annos,
                publico conductus stipendio, doces; quod ut tibi factu facilius sit, mitto ad te
                    <persName ref="#ar">Aristophanem</persName>, ut illum non modo legendum, sed
                ediscendum quoque discipulis praebeas tuis.</ab>
            <ab type="trad" corresp="#Ar1498_Manutius_Manutius_p1_11" ana="#PE">C’est donc pour le
                mieux, mon cher Clarius, que dans cette remarquable et opulente ville de Raguse tu
                veilles sur la jeunesse, toi qui lui enseignes à la fois le grec et le latin, comme
                le recommande Quintilien, depuis déjà de nombreuses années, avec le plus grand zèle
                et la plus grande loyauté, rémunéré avec de l’argent public ; et pour que cela te
                soit plus facile à faire, je t’envoie Aristophane, pour que tu le donnes non
                seulement à lire, mais aussi à apprendre par cœur à tes élèves.</ab>

            <ab type="orig" xml:id="Ar1498_Manutius_Manutius_p1_12">Quem etiam in tuo nomine
                publicare uoluimus, ut coniunctionem studiorum amorisque nostri, quo possem, munere
                declararem, et praesertim, cum tu, etsi de facie nos non nouimus, assiduis tamen me
                afficias beneficiis. </ab>
            <ab type="trad" corresp="#Ar1498_Manutius_Manutius_p1_12" ana="#PA">Aussi, nous avons
                voulu te le dédier, de manière à déclarer, par un cadeau que je peux offrir, la
                conjonction de nos études et de notre amitié, puisque toi, même si nous ne nous
                sommes jamais rencontrés, tu me gratifies de tes bienfaits réguliers</ab>

            <ab type="orig" xml:id="Ar1498_Manutius_Manutius_p1_13">Essem profecto ingratissimus, si
                te ualde amantem non redamarem</ab>
            <ab type="trad" corresp="#Ar1498_Manutius_Manutius_p1_13" ana="#PA">Je serais vraiment
                très ingrat, si je ne t’aimais pas en retour, toi qui m’aimes tant. </ab>

            <ab type="orig" xml:id="Ar1498_Manutius_Manutius_p1_14">Accipe, igitur, nouem <persName
                    ref="#ar">Aristophanis</persName> fabulas, nam decimam, <title ref="#ar_lys"
                    >Lysistraten</title>, ideo praetermisimus, quia uix dimitiata haberi a nobis
                potuit. </ab>
            <ab type="trad" corresp="#Ar1498_Manutius_Manutius_p1_14" ana="#PH">Reçois donc neuf
                pièces d’Aristophane ; en effet, la dixième, <title>Lysistrata</title>, nous l’avons
                omise, parce qu’il nous a été possible de l’avoir seulement à moitié. </ab>

            <ab type="orig" xml:id="Ar1498_Manutius_Manutius_p1_15">Sint satis hae nouem, cum
                optimis et antiquis (ut uides) commentariis. </ab>
            <ab type="trad" corresp="#Ar1498_Manutius_Manutius_p1_15" ana="#PH">Que ces neuf-là̀
                suffisent, avec d’excellents et antiques commentaires, comme tu le vois.</ab>

            <ab type="orig" xml:id="Ar1498_Manutius_Manutius_p1_16">Quibus Graece discere
                cupientibus nihil aptius, nihil melius legi potest, non meo solum iudicio, quod non
                magnifacio, sed etiam <persName>Theodori Gazae</persName>, uiri undecunque
                doctissimi; qui interrogatus, qui ex Graecis auctoribus assidue legendus foret
                Graecas litera discere uolentibus ? respondis, solus <persName ref="#ar"
                    >Aristophanes</persName> : quod esset sane quam acutus, copiosus, doctus, et
                merus Atticus. </ab>
            <ab type="trad" corresp="#Ar1498_Manutius_Manutius_p1_16" ana="#PE" >Rien ne convient
                mieux à ceux qui désirent apprendre le grec, rien de mieux ne peut être lu, non
                seulement selon mon jugement, dont je ne fais pas grand cas, mais aussi selon celui
                de Théodore Gaza, un homme très docte à tous égards ; lui qui, quand on lui a
                demandé́ qui parmi les auteurs grecs devait être lu assidûment par ceux qui veulent
                apprendre le grec, a répondu, « seulement Aristophane », parce que selon lui il est
                vraiment le plus précis, abondant, savant, et purement attique.</ab>

            <ab type="orig" xml:id="Ar1498_Manutius_Manutius_p1_17">Hunc item <persName ref="#chrys"
                    >Ioannes Chrysostomus</persName> tanti fecisse dicitur, ut duodetriginta
                comoedias <persName ref="#ar">Aristophanis</persName> semper haberet in manibus,
                adeo ut pro puluillo dormiens uteretur</ab>
            <ab type="trad" corresp="#Ar1498_Manutius_Manutius_p1_17" ana="#PE">De la même manière,
                Jean Chrysostome en faisait si grand cas que, dit-on, il avait toujours avec lui les
                28 comédies d’Aristophane, au point de s’en servir comme d’un petit coussin pour
                dormir. </ab>

            <ab type="orig" xml:id="Ar1498_Manutius_Manutius_p1_18">Hinc itaque et eloquentiam et
                seueritatem, quibus est mirabilis, didicisse dicitur. </ab>
            <ab type="trad" corresp="#Ar1498_Manutius_Manutius_p1_18" ana="#PE">Et c’est de cette
                façon qu’il acquit, dit-on, cette éloquence et cette rigueur qu’on admire chez
                lui.</ab>

            <ab type="orig" xml:id="Ar1498_Manutius_Manutius_p1_19">Ego sic assidue legendum a
                Graecis censeo <persName ref="#ar">Aristophanem</persName> ut a nostris <seg
                    type="paraphrase"><persName ref="#ter">Terentium</persName>, quem, quod semper
                    legeret, <persName ref="#cic">M. Tullius</persName> familiarem suum
                        appellabat<bibl resp="#SG" type="nontrouve"/></seg>. Vale. Venetiis, tertio
                Idus Oulias, M. IID. </ab>
            <ab type="trad" corresp="#Ar1498_Manutius_Manutius_p1_19" ana="#PE">On doit donc, selon
                moi, lire Aristophane parmi les auteurs grecs avec autant d’assiduité́ que Terence
                parmi les nôtres, lui que, parce qu’il le lisait sans relâche, Cicéron appelait son
                familier. Salut. Venise, le 13 juillet 1498.</ab>
        </body>
    </text>
</TEI>
