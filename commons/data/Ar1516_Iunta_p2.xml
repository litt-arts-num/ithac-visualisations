<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Bernardus Iunta Lectori. S. </title>
                <author ref="#iunta_bernardus">Bernardus Iunta</author>

                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Diandra CRISTACHE</name>
                    <name>Sarah GAUCHER</name>
                    <name>Malika BASTIN-HAMMOU</name>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <sourceDesc>

                <bibl>
                    <title>ΕΝ ΤΗιΔΕ ΜΙΚΡΑ ΒΙΒΛΩι ΤΑΔ’ΕΝΕΣΤΙΝ. Αριστοφάνους Θεσμοφοριάζουσαι. Τοῦ
                        αὐτοῦ Λυσιστράτη. In hoc parvo libro haec insunt. Aristophanis Cereris sacra
                        celebrantes. Eiusdem Lysistrate.</title>
                    <author ref="#ar">Aristophanes</author>
                    <pubPlace ref="#florentiae">Florentiae</pubPlace>
                    <publisher ref="#iunta_philippus">Philippus Iunta</publisher>
                    <date when="1516">1516</date>
                    <editor ref="#boninus_euphrosynus">Euphrosynus Boninus</editor>
                </bibl>
                <listBibl>
                    <bibl><title level="m">Bucoliques grecs</title>
                        <biblScope unit="volume">T.II</biblScope> : <title level="a"
                            >Pseudo-Théocrite, Moschos, Bion, divers</title>, texte établi et
                        traduit par <editor>Ph. E. Legrand</editor>, <publisher>Les Belles
                            Lettres</publisher>, <date>1927</date></bibl>
                </listBibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Liste de paratextes dans la même édition : <list>
                        <item>1. Bernardus Iunta, nobili patritio domino Francisco Accolto electo
                            Epo anconitano. S.P.D., Bernardus Iunta à Franciscus Accoltus, f.a
                            ii</item>
                        <item>2. Bernardus iunta lectori. S., Bernardus Iunta au lecteur, f.g
                            iiii</item>
                    </list>
                </p>
                <p>Sur la page de titre figure un extrait de l’épitaphe de Bion attribuée tantôt à
                    Moschos et tantôt à Théocrite dans les manuscrits - attribution impossible
                    puisque Bion vécut après eux (édition A.S.F. Gow, 1952): ἄρχετε Σικελικαί, τῶ
                    πένθεος ἄρχετε, Μοῖσαι. ϕάρμακον ἦλθε, Βίων, ποτὶ σὸν στόμα, ϕάρμακον ἦδες.
                    τοιούτοις χείλεσσι ποτέδραμε κοὐκ ἐγλυκάνθη;  τίς δὲ βροτὸς τοσσοῦτον ἀνάμερος ἢ
                    κεράσαι τοι ἢ δοῦναι καλέοντι τὸ ϕάρμακον; ἦ ϕύγεν ᾠδάν. ἄρχετε Σικελικαί, τῶ
                    πένθεος ἄρχετε, Μοῖσαι. « Commencez, Muses de Sicile, commencez à mener le
                    deuil. Un poison est venu à ta bouche, Bion. Quelle sorte de poison toucha tes
                    lèvres et ne s’adoucit pas ? Quel mortel fut assez sauvage pour préparer ce
                    poison contre ou pour te le donner à ton appel ? Il s’est dérobé à mon chant. »
                    (Traduction Ph. E. Legrand). En 1516, Eufrosino Bonini, l’éditeur des deux
                    comédies d’Aristophane, fit également paraître à Florence une édition de
                    Théocrite. (Sicherl, 575 et
                    https://archivesetmanuscrits.bnf.fr/ark:/12148/cc98335h). Un exemplaire à la
                    BnF : RES-YB-650</p>

                <p>L’éditeur date son texte de février 1515, or dans l’édition de septembre 1515
                    Aristophanis Comoediae Novem Bernardo Giunta signalait que les Thesmophories et
                    Lysistrata n’étaient pas encore prêtes à être publiées mais le seraient
                    bientôt ; il faut donc lire février 1516 pour ce volume. En effet, l’année
                    commençait et s’achevait alors, selon les lieux, le jour de l’Annonciation, ou à
                    Pâques, ou encore à Noël. Il s’agit donc selon le calendrier grégorien de
                    février 1516. </p>
            </abstract>

            <langUsage>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>

        <revisionDesc>
            <change when="2022-07-13">Malika BASTIN-HAMMOU : Encodage sémantique</change>
            <change when="2022-03-03">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
            <change when="2020-09-27">Diandra CRISTACHE : Structuration TEI de la transcription et
                de la traduction</change>
        </revisionDesc>

    </teiHeader>

    <text>
       
        <body ana="#PA #PH #LI">

            <head type="orig" xml:id="Ar1516_Iunta_p2_0"><persName ref="#iunta_bernardus">Bernardus
                    Iunta</persName> Lectori. S. </head>
            <head corresp="#Ar1516_Iunta_p2_0" type="trad">Bernardo Giunta au Lecteur. Salut.</head>

            <ab type="orig" xml:id="Ar1516_Iunta_p2_1">Habes, candide Lector, nusquam hactenus
                impressas binas <persName ref="#ar">Aristophanis</persName> Comoedias, <title
                    ref="#ar_th">Sacrificantes feminas</title>, atticamque <title ref="#ar_lys"
                    >Lysistraten</title>, quas ex Codice adeo vetusto excerpsimus, ut altera
                interdum dictionis pars ibi desideretur. </ab>

            <ab type="trad" corresp="#Ar1516_Iunta_p2_1">Reçois, Lecteur bienveillant,
                deux comédies d’Aristophane qui n’ont jusqu’à présent jamais été imprimées, les
                    <title>Femmes sacrifiant</title>, et l’attique <title>Lysistrata</title>, que
                nous avons tirées d’<seg>un codex si vieux qu’il manque parfois la moitié du
                        texte<note>En réalité, le Ravennas est en très bon état et très lisible.
                    </note></seg>.</ab>

            <ab type="orig" xml:id="Ar1516_Iunta_p2_2">Si quid igitur in illis, quod tibi molestum
                sit, inuenies, quia <foreign xml:lang="grc">ἁπαραλλάκτως</foreign> cudere uoluimus,
                id euenisse scias. Vale</ab>
            <ab type="trad" corresp="#Ar1516_Iunta_p2_2">Si donc tu trouves dans ces
                comédies quelque chose qui t’est pénible, sache que c’est arrivé parce que nous
                avons voulu imprimer fidèlement. </ab>
        </body>
    </text>
</TEI>
