<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Ad lectorem</title>
                <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : "la" pour le latin et "grc" pour le grec -->
                <!-- Attention ! ne pas faire de retour charriot dans le titre -->
                <author ref="#erasmus_desiderius">Erasmus Roterdamus</author>
                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Ajouter autant de tâches (<respStmt>) que nécessaire -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Alexia DEDIEU</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Alexia DEDIEU</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Sarah GAUCHER</name>
                    <name>Diandra CRISTACHE</name>
                </respStmt>

                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne GARCIA-FERNANDEZ</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
            <sourceDesc>
                <bibl>
                    <title>Hecuba et Iphigenia in Aulide Euripidis tragoediae in latinum translatae
                        Erasmo Roterdamo interprete </title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#eur">Euripides</author>
                    <pubPlace ref="#uenetia">Venetiae</pubPlace>
                    <publisher ref="#manutius_aldus">Aldus Manutius</publisher>
                    <date when="1507">1507</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor role="traducteur" ref="#erasmus_desiderius">Erasmus Roterdamus</editor>
                    <ref target="url_edition_numérique"/>
                    <!-- pour les liens google book, remplacer les & par &amp; pour que ça fonctionne -->
                </bibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Cet Ad Lectorem est ajouté à l’édition aldine de 1507 mais n’est pas présent dans
                    l’édition de Bade de 1506. </p>
            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>

        <revisionDesc>
            <change when="2022-03-09">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
            <change when="2021-05-30">Diandra CRISTACHE : mise à jour et chargement sur le
                Pensoir</change>
        </revisionDesc>

    </teiHeader>

    <text>
        <body ana="#M">

            <!-- RAPPEL : on encode le texte latin ou grec; on n'encode dans le texte de la traduction que les notes d'éditeur (=membres du projet) -->

            <!-- Le couple d'éléments <head> contient le titre ou l'incipit du paratexte -->
            <!-- L'identifiant du TITRE est composé du titre de la fiche TT suivi d'un underscore (_) puis du chiffre 0 -->
            <head type="orig" xml:id="Eu1507_Eramus_p1_0">Ad Lectorem</head>
            <!-- Le deuxième élément du couple contient la traduction française du titre ou incipit -->
            <!-- L'identifiant de la traduction est le même que celui du latin, précédé d'un hashtag (#) -->
            <head corresp="#Eu1507_Eramus_p1_0" type="trad">Au Lecteur</head>

            <ab type="orig" xml:id="Eu1507_Eramus_p1_1">De carminum generibus <seg>ut<note><foreign
                            xml:lang="la">ut paucis</foreign> ou <foreign xml:lang="la">ut admonitus
                            sis</foreign>.</note></seg> paucis obiter admonitus sis, lector optime :
                prima <title ref="#eur_hec">Hecubae</title> scaena constat iambico trimetro, secunda
                anapaestico dimetro, nonnumquam intermixtis eiusdem formae monometris ; quanquam hoc
                metrum et dactylum recipit, aliquoties et proceleusmaticum, nonnumquam et meris
                conficitur spondeis.</ab>
            <ab type="trad" corresp="#Eu1507_Eramus_p1_1">Vénérable lecteur, soit dit en passant,
                prends garde aux mètres de certains vers : la première scène d’<title>Hécube</title>
                est écrite en trimètres iambiques, la seconde en dimètres anapestiques, parfois
                mêlés à des monomètres du même mètre ; quoique ce mètre reçoive aussi des dactyles,
                quelquefois même des procéleusmatiques, parfois il est aussi composé uniquement de
                spondées.</ab>
            <ab type="orig" xml:id="Eu1507_Eramus_p1_2">Chorus, cui initium, <cit type="hyperref">
                    <quote type="traduction">Aura, pontica aura</quote>
                    <bibl resp="#AD"><author ref="#eur">Eur.</author>, <title ref="#eur_hec"
                            >Hec.</title>
                        <biblScope>444</biblScope>. <note>Il s'agit du premier vers du premier
                            stasimon de la pièce.</note></bibl>
                </cit>, rursum qui incipit. <cit type="hyperref">
                    <quote type="traduction">Mihi aerumnam atropos</quote>
                    <bibl resp="#AD"><author ref="#eur">Eur.</author>, <title ref="#eur_hec"
                            >Hec.</title>
                        <biblScope>629</biblScope>. <note>Il s'agit du premier vers du deuxième
                            stasimon de la pièce. </note></bibl>
                </cit>. </ab>
            <ab type="trad" corresp="#Eu1507_Eramus_p1_2">Le chœur, qui commence par : <seg
                    xml:lang="la">Aura, Pontica aura</seg> Et après, celui qui commence par : <seg
                    xml:lang="la">Mihi aerumnam atropos</seg></ab>
            <ab type="orig" xml:id="Eu1507_Eramus_p1_3">Iterum alius, cui initium, <cit
                    type="hyperref">
                    <quote type="traduction">Patria heu</quote>
                    <bibl resp="#AD"><author ref="#eur">Eur.</author>, <title ref="#eur_hec"
                            >Hec.</title>
                        <biblScope>905</biblScope>. <note>Il s'agit du premier vers du troisième
                            stasimon de la pièce.</note></bibl>
                </cit> praeterea Polymestoris oratio, quae sic incipit, <cit type="hyperref">
                    <quote>heu quo ferar</quote>
                    <bibl resp="#AD"><author ref="#eur">Eur.</author>, <title ref="#eur_hec"
                            >Hec.</title>
                        <biblScope>1056</biblScope>.</bibl>
                </cit>, ex uariis metrorum constat generibus, ac ferme iisdem quibus usus est
                    <persName ref="#eur">Euripides</persName>.</ab>
            <ab type="trad" corresp="#Eu1507_Eramus_p1_3">De même l’autre, qui commence par : <seg
                    xml:lang="la">Patria Heu</seg> Et également le chant de Polymestor qui commence
                de cette façon : <seg xml:lang="la">Heu, quo ferar ?</seg> Sont composés de
                différentes sortes de mètres, et ce sont presque les mêmes que ceux qu’a utilisés
                Euripide.</ab>

            <ab type="orig" xml:id="Eu1507_Eramus_p1_4">Polymestoris oratio, cui principium est <cit
                    type="hyperref">
                    <quote>heu ohe ferocis</quote>
                    <bibl resp="#SG" type="nontrouve"><author ref="#eur">Eur.</author>, <title
                            ref="#eur_hec">Hec.</title></bibl>
                </cit>, duobus dumtaxat generibus alternat, dimetro iambico catalecto, et eodem
                acatalecto, caetera constant aut iambicis trimetris, aut anapaesticis, de quibus
                modo meminimus. </ab>
            <ab type="trad" corresp="#Eu1507_Eramus_p1_4">Le chant de Polymestor qui commence
                    ainsi <seg xml:lang="la">Eheu ohe ferocis</seg> ne fait alterner que deux
                mètres, des dimètres iambiques catalectiques et acatalectiques. Tous les autres sont
                composés soit de trimètres iambiques, soit de trimètres anapestiques, je ne me
                souviens que de ceux-là.</ab>
            <ab type="orig" xml:id="Eu1507_Eramus_p1_5"><title ref="#eur_ia">Iphigeniae</title>
                prima scaena constat iisdem anapaesticis usque ad chorum <cit type="hyperref">
                    <quote type="traduction">Modo profecta</quote>
                    <bibl resp="#AD"><author ref="#eur">Eur.</author>, <title ref="#eur_ia"
                            >I.A.</title>
                        <biblScope>164</biblScope>.</bibl>
                </cit>, qui mixtus est ex alcaico composito ex iambica penthemimeri ac duobus
                dactylis, iambico dimetro hypercatalectico, dactylico e dactylis duobus, ac totidem
                trochaeis, choriambico dimetro, et eodem hypercatalectico, dactylico trimetro,
                glyconio, dactylico dimetro, asclepiadeo, iambico monometro hypercatalecto,
                dactylico dimetro hypercatalectico, iambico dimetro catalecto, pherecratio iambico
                dimetro acatalecto, iambico dimetro acephalo, iambico trimetro catalectico,
                anapaestico trimetro hypercatalecto, adonio trimetro, trochaico monometro
                hypercatalectico. </ab>
            <ab type="trad" corresp="#Eu1507_Eramus_p1_5">La première scène
                    d’<title>Iphigénie</title> est composée des mêmes anapestes jusqu’au chœur <seg
                    xml:lang="la">Modo profecta</seg> qui est composé à la fois d’alcaïques formés
                avec une penthémimère iambique, et de deux dactyles, de dimètres iambiques
                hypercatalectiques, de dimètres dactyliques avec deux dactyles et autant de
                trochées, de dimètres choriambiques, de dimètres choriambiques hypercatalectiques,
                de trimètres dactyliques, glyconique, d’asclépiades, de monomètres iambiques
                hypercatalectiques, de dimètres dactyliques hypercatalectiques, de dimètres
                iambiques catalectiques, de dimètres iambiques phérécratiens, de dimètres iambiques
                acéphales, de trimètres iambiques catalectiques, de trimètres anapestiques
                hypercatalectiques, de trimètres adoniques, de monomètres trochaïques
                    hypercatalectiques,<note>La longue énumération de vers à l’ablatif singulier est
                    ici rendue par des pluriels. Le premier chœur de l’<title>Iphigénie à
                        Aulis</title> traduit par Erasme faisant 190 vers, il est évident que chacun
                    de ces types de vers a servi plusieurs fois.</note>
            </ab>
            <ab type="orig" xml:id="Eu1507_Eramus_p1_6">Deinde sequuntur iambica trimetra, paulo
                post a versu <cit type="hyperref">
                    <quote type="traduction">Caeterum Menelae</quote>
                    <bibl resp="#AD"><author ref="#eur">Eur.</author>, <title ref="#eur_ia"
                            >I.A.</title>
                        <biblScope>319</biblScope></bibl>
                </cit> incipiunt trochaica tetrametra hypercatalecta. </ab>
            <ab type="trad" corresp="#Eu1507_Eramus_p1_6">et ensuite suivent des trimètres
                iambiques, peu après, à partir du vers <seg xml:lang="la">Caeterum Menelae</seg>,
                commencent des tétramètres trochaïques hypercatalectiques.</ab>
            <ab type="orig" xml:id="Eu1507_Eramus_p1_7">Chorus <cit type="hyperref">
                    <quote type="traduction">Felices quibus</quote>
                    <bibl resp="#SG" type="nontrouve"><author ref="#eur">Eur.</author>, <title
                            ref="#eur_ia">I.A.</title></bibl>
                </cit>, constat glyconio, deinde iambico dimetro catalectico.</ab>
            <ab type="trad" corresp="#Eu1507_Eramus_p1_7">Le chœur <seg xml:lang="la">Felices
                    quibus</seg> est composé de glyconiens, et ensuite de dimètres iambiques
                catalectiques. </ab>
            <ab type="orig" xml:id="Eu1507_Eramus_p1_8">Alter proximus anapaestico quo et prima
                scaena. </ab>
            <ab type="trad" corresp="#Eu1507_Eramus_p1_8">L’autre, juste après, d’anapestiques, dont
                est aussi composée la première scène.</ab>
            <ab type="orig" xml:id="Eu1507_Eramus_p1_9">Chorus <cit type="hyperref">
                    <quote type="traduction">Ilicet sacrum simoentis</quote>
                    <bibl resp="#SG" type="nontrouve"><author ref="#eur">Eur.</author>, <title
                            ref="#eur_ia">I.A.</title></bibl>
                </cit> carmine sapphico, intermixto adonio dimetro. Chorus <cit type="hyperref">
                    <quote type="traduction">festus ut ille</quote>
                    <bibl/>
                </cit>, constat dactylicis trimetris hypercatalecticis et anapaesticis.</ab>
            <ab type="trad" corresp="#Eu1507_Eramus_p1_9">Le chœur <seg xml:lang="la">Ilicet
                    sacrum Simoentis</seg> est un chant sapphique mêlé à des dimètres adoniques, et
                le chœur <seg xml:lang="la">Festus ut ille</seg> est composé de trimètres
                dactyliques hypercatalectiques et anapestiques.</ab>
            <ab type="orig" xml:id="Eu1507_Eramus_p1_10">Vale.</ab>
            <ab type="trad" corresp="#Eu1507_Eramus_p1_10">Adieu. </ab>
        </body>
    </text>
</TEI>
