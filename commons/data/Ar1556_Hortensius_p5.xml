<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>

        <fileDesc>
            <titleStmt>

                <!-- Titre et auteur du paratexte (en latin) -->
                <title xml:lang="la">Argumentum</title>
                <author ref="#hortensius_lambertus">Lambertus Hortensius Montfortius</author>

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Tâche n°1 -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Magalie SAUVAN</name>
                </respStmt>
                <!-- Tâche n°2 -->
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <!-- Tâche n°3 -->
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Sarah GAUCHER</name>
                    <name>Malika BASTIN-HAMMOU</name>
                   
                </respStmt>
                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <principal>Malika BASTIN-HAMMOU</principal>
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne Garcia-Fernandez</persName>
                </respStmt>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <sourceDesc>
                <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
                <bibl>
                    <!-- l'attribut @level permet d'indiquer s'il s'agit d'une monographie (m), d'un article ou chapitre d'ouvrage (a) ou d'un périodique (j) -->
                    <title>Aristophanis clarissimi comici, Plutus, interprete Laberto Hortensio
                        Montfortio</title>
                    <author ref="#ar">Aristophanes</author>
                    <pubPlace ref="#ultraiectum">Vltraiecti</pubPlace>
                    <publisher ref="#borculous_hermannus">Hermannus Borculous </publisher>
                    <date when="1556">1556</date>
                    <editor role="traducteur"  ref="#hortensius_lambertus">Lambertus Hortensius </editor>
                </bibl>

            </sourceDesc>

        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Lambertus Hortensius (1500-1574): Lambertus Hortensius est un prêtre historien et
                    humaniste néerlandais. Il est recteur de l’école latine (Gymnasium) de Naarden
                    en 1544 et bien qu’il ne s’éloigne pas vraiment de la foi catholique, il
                    sympathise avec les idées de la réforme. Son nom en langue vernaculaire n’est
                    pas connu. Il a beaucoup commenté les auteurs anciens, et notamment Aristophane,
                    Virgile et Lucain et il a également écrit des œuvres historiques en latin,
                    notamment autour de la Réforme et des Anabaptistes (comme l’Histoire des
                    Anabaptistes ou Relation curieuse de leur doctrine) qui ont un rayonnement
                    international. Ses commentaires sur l’Enéide de Virgile, Enarrationes in sex
                    priores libros Aeneidos Virgilianae ont été publiés en 1559, suivi en 1577 d’une
                    édition complétée avec les commentaires de Lambertus Hortensius sur les six
                    livres suivants. Cependant, les commentaires sur les livres VII à XII sont moins
                    nombreux.</p>
            </abstract>

            <langUsage>
                <!-- Garder seulement les éléments indiquant les langues utilisées dans le paratexte -->
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

            <textClass n="vers"></textClass>

        </profileDesc>

        <revisionDesc>
            <!-- Ajouter ici autant d'éléments <change> que de modifications effectuées -->
            <change when="2022-10-07">Malika BASTIN-HAMMOU : encodage sémantique</change>
            <change when="2022-03-23">Sarah GAUCHER : création de la fiche TEI, encodage de la
                traduction et de la transcription</change>
        </revisionDesc>

    </teiHeader>

    <text>
        <body ana="#A">
            <head type="orig" xml:id="Ar1556_Hortensius_p5_0">Argumentum</head>
            <head type="trad" corresp="#Ar1556_Hortensius_p5_0">Argument</head>
            
            <ab type="orig" xml:id="Ar1556_Hortensius_p5_1"><l>Vir iustus atque pauper idem
                    oraculum</l><l> Petit, an ualeat mutatus opes corradere.</l>
            </ab>
            <ab type="trad" corresp="#Ar1556_Hortensius_p5_1">Un homme juste et pauvre demande à
                l’oracle, s’il pourra, son destin ayant été changé, amasser des richesses.</ab>
            
            <ab type="orig" xml:id="Ar1556_Hortensius_p5_2"><l>Cui Deus respondit, ut sequeretur
                    hunc,</l><l> In quem uirum primum incideret. Mox obuius</l><l> Conspicitur
                    oculis Plutus orbatus suis. </l></ab>
            <ab type="trad" corresp="#Ar1556_Hortensius_p5_2">Or, le dieu lui répond de suivre le
                premier homme qu’il croisera. Bientôt, il voit Ploutos, sous des traits inconnus,
                venir à sa rencontre.</ab>
            
            <ab type="orig" xml:id="Ar1556_Hortensius_p5_3"><l>Hunc agnitum adduxit domum,
                    popularibus</l><l> Vt participes essent, uocatis caeteris. </l></ab>
            <ab type="trad" corresp="#Ar1556_Hortensius_p5_3">Après l’avoir reconnu, il l’emmène
                chez lui et fait appeler le reste des gens du peuple pour prendre part à
                l’affaire.</ab>
            
            <ab type="orig" xml:id="Ar1556_Hortensius_p5_4"><l>Mox hinc properarunt pharmacis
                    medicarier</l><l> Oculos, atque in aedem Asclepii abducunt dei. </l></ab>
            <ab type="trad" corresp="#Ar1556_Hortensius_p5_4">De là, ils se hâtèrent bientôt de
                soigner ses yeux grâce à des remèdes et le conduisent dans le temple du dieu
                Esculape.</ab>
            
            <ab type="orig" xml:id="Ar1556_Hortensius_p5_5"><l>Subito Penia uetat id fieri.
                    Veruntamen</l><l> Visu recepto, nemo diues factus est</l><l> Malorum, at omnium
                    bonorum erant bona. </l></ab>
            <ab type="trad" corresp="#Ar1556_Hortensius_p5_5">Soudain, Penia interdit qu’on mène le
                projet à bien. Cependant, après que Ploutos a recouvré la vue, aucun méchant ne devint riche, quand les biens allaient à tous les hommes de bien.</ab>


        </body>
    </text>

</TEI>
