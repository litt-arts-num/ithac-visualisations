<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
   <teiHeader>
      <fileDesc>
         <titleStmt>
            <title xml:lang="la">In Aristophanem Latinum Nicodemi Frisclini, Epigramma Friderici
               SYLBURGII</title>
            <author ref="#sylburgius_fridericus">Fridericus Sylburgius</author>


            <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
            <!-- Tâche n°1 -->
            <respStmt>
               <resp>Transcription</resp>
               <name>Christian NICOLAS</name>
            </respStmt>
            <!-- Tâche n°2 -->
            <respStmt>
               <resp>Traduction</resp>
               <name>Christian NICOLAS</name>
            </respStmt>
            <!-- Tâche n°3 -->
            <respStmt>
               <resp>Encodage</resp>
               <name>Malika BASTIN-HAMMOU</name>
               <name>Sarah GAUCHER</name>
            </respStmt>
            <!-- Ajouter autant de tâches que nécessaire -->

            <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
            <principal>Malika BASTIN-HAMMOU</principal>
            <respStmt>
               <resp>Modélisation et structuration</resp>
               <persName>Elisabeth GRESLOU</persName>
               <persName>Elysabeth HUE-GAY</persName>
            </respStmt>
            <respStmt>
               <resp>Première version du modèle d'encodage</resp>
               <persName>Elisabeth GRESLOU</persName>
               <persName>Anne Garcia-Fernandez</persName>
            </respStmt>
         </titleStmt>

         <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
         <publicationStmt>
            <publisher/>
            <availability>
               <p>Le site est en accès restreint</p>
            </availability>
            <pubPlace>ithac.elan-numerique.fr</pubPlace>
         </publicationStmt>

         <sourceDesc>
            <bibl>
               <title>Nicodemi Frischlini Aristophanes, veteris comoediae princeps: poeta longe
                  facetissimus et eloquentissimus, repurgatus a mendis, et imitatione Plauti atque
                  Terentii interpretatus, ita ut fere Carmen Carmini, numerus numero, pes pedi,
                  modus modo, Latinismus Graecismo respondeat. Opus Divo Rudolpho Caesari
                  Sacrum</title>
               <author ref="#ar">Aristophanes</author>
               <editor  role="traducteur" ref="#frischlinus_nicodemus">Nicodemus Frischlinus</editor>
               <publisher ref="#spies_ioannes">Ioannes Spies</publisher>
               <pubPlace ref="#francofurtum">Francoforti ad Moenum</pubPlace>
               <date when="1586">1586</date>
               <ref target="url_edition_numérique"/>
               <!-- pour les liens google book, remplacer les & par &amp; pour que ça fonctionne -->
            </bibl>

            <listBibl>
               <head>Bibliographie</head>
               <bibl><author>Thomas Baier</author><title>« Nicodemus Frischlin als
                     Aristophanes-Übersetzer »</title><editor>Ekkehard Stärk et Gregor
                     Vogt-Spira</editor><title>Dramatische Wäldchen. Festschrift für Eckard Lefèvre
                     zum 65.
                     Geburtstag</title><pubPlace>Hildesheim</pubPlace><publisher>Olms</publisher><date>2000</date></bibl>
               <bibl>
                  <author>Patrick Lucky Hadley</author>
                  <title>Athens in Rome, Rome in Germany. Nicodemus Frischlin and the Rehabilitation
                     of Aristophanes in the 16th Century</title>
                  <pubPlace>Tübingen</pubPlace>
                  <publisher>Narr Francke Attempto Verlag</publisher>
                  <date>2015</date>
               </bibl>
               <bibl><author>David Price</author><title>The Political Dramaturgy of Nicodemus
                     Frischlin : Essays on Humanist Drama in Germany</title><pubPlace>Chapel
                     Hill</pubPlace>
                  <publisher>University of North Carolina Press</publisher>,
                  <date>1990</date></bibl>
            </listBibl>
         </sourceDesc>

      </fileDesc>

      <profileDesc>

         <abstract>
            <p>Cette épigramme est faite de distiques inédits constitués à chaque fois d’un
               hexamètre et d’un sénaire iambique. C’est un échange de bons procédés car Frischlin
               fait de son côté un compliment versifié de Sylburg dans l’édition que ce dernier fait
               de Denys d’Halicarnasse, imprimée la même année. Friedrich Sylburg (Fridericus
               Sylburgius) naît en 1536 à Wetter. Dans les années 1550, il étudie dans plusieurs
               villes européennes, notamment Genève, Strasbourg et Paris, où il travaille avec Henri
               Estienne à l'édition du <title>Thesaurus Linguae Graecae</title>. A son retour, il refuse la chaire
               de Grec de l'Université de Marbourg pour se consacrer à son travail de traduction et
               d'édition des textes grecs. En 1583, il s'installe à Francfort où il travaille avec
               la famille Wechel, qui dirige le principal atelier d'imprimeurs de textes grecs de la
               ville. Il y donne des éditions complètes, avec traduction latine et nombreuses
               annexes, d'Aristote, d'Hérodote, de Denys d'Halicarnasse, entre autres. En 1591, il
               quitte Francfort pour Heidelberg et commence un collaboration avec l'imprimeur Jérôme
               Commelin. C'est chez ce dernier qu'il édite notamment Clément d'Alexandrie, Justin
               Martyr, Xénophon, Nonnos de Panopolis, et une anthologie des poètes gnomiques. En
               1595, un an avant sa mort, il devient directeur de la Bibliotheca palatina, ce qui
               lui permet de dresser un catalogue complet des manuscrits qu'elle contenait. Parmi
               ses oeuvres personnelles, on compte des manuels de grammaire et de syntaxe grecques,
               un dictionnaire de grec byzantin et des recueils de poésie en grec. Source : Julien
               Berguer, doctorant qui travaille sur Sylburg.</p>
         </abstract>

         <langUsage>
            <!-- ne pas modifier -->
            <language ident="fr">Français</language>
            <language ident="la">Latin</language>
            <language ident="grc">Grec</language>
         </langUsage>

         <textClass n="vers"></textClass>

      </profileDesc>

      <revisionDesc>
         <change when="2022-03-03">Sarah GAUCHER : identifiants Header, mise en conformité
            Elan</change>
         <change>110920</change>
      </revisionDesc>

   </teiHeader>

   <text>
      <body ana="#T #PA">
         <head type="orig" xml:id="Ar1586_Frischlinus_p2_0"> In
               <title>Aristophanem</title> Latinum <persName ref="#frischlinus_nicodemus">Nicodemi
               Frisclini</persName>, Epigramma <persName ref="#sylburgius_fridericus">Friderici
               SYLBURGII</persName></head>
         <head type="trad" corresp="#Ar1586_Frischlinus_p2_0">Sur l’<title>Aristophane</title> latin
            de Nicodemus Frischlin, épigramme de Friedrich Sylburg</head>

         <ab type="orig" xml:id="Ar1586_Frischlinus_p2_1">
            <l>Graecus <persName ref="#ar">Aristophanes</persName> longo iam tempore dignum</l>
            <l>Facetiis interpretem quaerens suis,</l>
            <l>Nullum tot saeclis potuit reperire uenustos</l>
            <l>Qui posset Atticae elegantiae sales,</l>
            <l>Cecropiosque pedes Latio uestire cothurno</l>
            <l>Et metra metris, sensa sensis reddere.</l>
            <l>Tandem Germanis unus se ostendit ab oris</l>
            <l>Qui scaenicam subiret hanc prouinciam.</l>
            <l> Cuius si nomen rogites famamque requiras,</l>
            <l> Camena nouit Graeca, nouit Itala.</l>
            <l>Praenomen puero <foreign xml:lang="grc">νίκη</foreign> dedit inclyta <foreign
                  xml:lang="grc">δήμου</foreign>,</l>
            <l>Nomenque stirpi uegeta dedit alacritas.</l>
            <l>Mercurius docuit, docuit citharoedus Apollo :</l>
            <l>Docuit Minerua, Gratia, Parnassides.</l>
            <l>Herois celsus numeris elegisque politus,</l>
            <l>Grauisque tragicis, comicis salsus iocis ;</l>
            <l>Aptus, seu carmen seu uerba soluta requiras,</l>
            <l>Res hoc uel illo prosequi dextre modo.</l>
            <l>Hoc igitur tandem fidens interprete Graecus,</l>
            <l>Scaenas serena fronte scandit Romulas ;</l>
            <l>Et Graecis Latiisque simul salibusque iocisque,</l>
            <l><persName ref="#pl">Plautos</persName> lacessit, <persName ref="#enn"
                  >Ennios</persName>, <persName ref="#pacuu">Pacuuios</persName>.</l>
         </ab>

         <ab type="trad" corresp="#Ar1586_Frischlinus_p2_1">Aristophane, Grec, depuis longtemps
            cherchant un digne traducteur pour ses plaisanteries, n’en put trouver aucun, depuis de
            si longs siècles, pour vêtir ses bons mots tout d’élégance attique et ses pieds théséens
            du cothurne latin, rendre mètre pour mètre, et rendre sens pour sens. Enfin, de
            Germanie, un homme s’est montré pour conquérir aussi la province scénique. Tu demandes
            son nom et ce qu’on dit de lui ? La Muse grecque sait, et celle d’Italie. Enfant il a
            reçu son prénom de Victoire acquise sur le Peuple et son nom de famille, il le doit à sa
            vive et allègre nature. Son maître fut Mercure, Apollon citharède, les Grâces, Athéna,
            les Muses du Parnasse. Il est haut dans l’épos, poli dans l’élégie, grave dans le
            tragique et drôle en comédie, capable en poésie, en prose à la demande, d’atteindre
            adroitement son but par l’une ou l’autre. Donc notre Grec, tranquille avec cet
            interprète, monte le front serein sur les scènes romaines et par ses jeux de mots et ses
            plaisanteries en grec et en latin vient provoquer chez eux un Plaute, un Ennius voire un
            Pacuvius. </ab>
      </body>
   </text>
</TEI>
