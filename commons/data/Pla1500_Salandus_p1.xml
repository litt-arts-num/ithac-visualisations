<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Ioannes Salandus</title>
                <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : "la" pour le latin et "grc" pour le grec -->
                <!-- Attention ! ne pas faire de retour charriot dans le titre -->
                <author ref="#salandus_ioannes">Ioannes Salandus</author>
                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Ajouter autant de tâches (<respStmt>) que nécessaire -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Dalia DAHR</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Déborah BOIJOUX</name>
                </respStmt>
                <respStmt>
                    <resp>Révision</resp>
                    <name>Mathieu FERRAND</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
            <sourceDesc>
                <bibl>
                    <title>Plautus integer cum interpretatione Joannis Baptistae Pii.</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#pl">Plautus</author>
                    <pubPlace ref="#mediolanum">Mediolani</pubPlace>
                    <publisher ref="#scinzenzeler_uldericus">Uldericus Scinzenzeler</publisher>
                    <date when="1500">1500</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor ref="#pius_ioannes">Ioannes Baptista Pius</editor>
                    <ref target="url_edition_numérique"/>
                    <!-- pour les liens google book, remplacer les & par &amp; pour que ça fonctionne -->
                </bibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p/>
            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

            <textClass n="vers"></textClass>

        </profileDesc>
        <revisionDesc>
            <change when="2022-10-05">Sarah GAUCHER : encodage de la traduction et de la
                transcription</change>
            <change when="2022-04-12">Sarah GAUCHER : création de la fiche TEI et du Header</change>
        </revisionDesc>
    </teiHeader>

    <text>
        <body ana="#PH">

            <!-- RAPPEL : on encode le texte latin ou grec; on n'encode dans le texte de la traduction que les notes d'éditeur (=membres du projet) -->

            <!-- Le couple d'éléments <head> contient le titre ou l'incipit du paratexte -->
            <!-- L'identifiant du TITRE est composé du titre de la fiche TT suivi d'un underscore (_) puis du chiffre 0 -->
            <head type="orig" xml:id="Pla1500_Salandus_p1_0"><persName ref="#salandus_ioannes"
                    >Ioannes Salandus</persName></head>
            <!-- Le deuxième élément du couple contient la traduction française du titre ou incipit -->
            <!-- L'identifiant de la traduction est le même que celui du latin, précédé d'un hashtag (#) -->
            <head corresp="#Pla1500_Salandus_p1_0" type="trad">Giovanni Salandi <note>On sait peu de
                    choses de ce contemporain de Battista Pio, peut-être apparenté à Pier Antonio
                    Salandi Reggiano, professeur à l’Université de Bologne de 1489 à 1503 (d’après
                    TIRABOSCHI G., Biblioteca modenese o notizie della vita e delle opere degli
                    scrittori natii degli Stati del Serenissimo Signor Duca di Modena, t. 5, Modène,
                    Società tipografica, 1784). Sans doute proche d’Antonio Motta, il clôt l’editio
                    princeps d’Apicius dirigée par ce dernier (Milan, 1498) par une épigramme de
                    cinq distiques adressée au lecteur (f°42v)<ref
                        target="https://portail.biblissima.fr/fr/ark:/43093/edatac48c0d866dc7866e6fbd00a8f943987fb67e5585"
                    />. Il est également l’auteur de deux épigrammes latines insérées à la fin
                        (f°516r)<ref
                        target="https://portail.biblissima.fr/fr/ark:/43093/edataaae743f785268a777dbd0a04ab0ceafc25872b3e"
                    /> de l’editio princeps de la Souda, publiée, à Milan, par Démétrios
                    Chaldondyle, en 1499 (dans laquelle on retrouve d’ailleurs deux poèmes
                    liminaires d’Antonio Motta, f°1v).</note>
            </head>

            <!-- Chaque couple d'éléments <ab> contient le texte du paratexte (cf. tableau dans la fiche TT) -->
            <!-- Attention ! Ne pas oublier de mettre à jour les identifiants en rouge ou utiliser le fichier xml qui permet de générer automatiquement les <ab> et leurs identifiants -->
            <!-- L'identifiant de l'UNITE TEXTUELLE est composé du titre de la fiche TT suivi d'un underscore (_) puis du chiffre 1, 2, 3... -->

            <ab type="orig" xml:id="Pla1500_Salandus_p1_1"><l>Scaena, Ioci, Charites Vmbrique
                    emuncta poetae</l><l> Musa, Venus Stygiis mersa rogabat aquis</l><l> Ferret opem
                    ut miserae quisquis bene sentit et optat</l><l> Consultum ingenuis artibus esse
                    bonis. </l><l>Nemo tamen precibus satis aurem admorat honestis,</l><l> Collapsam
                    erigeret qui modo nullus erat. </l></ab>
            <ab type="trad" corresp="#Pla1500_Salandus_p1_1"><l>Le théâtre, les jeux, les Charites,
                    la Muse raffinée</l><l> Du poète ombrien et Vénus suppliaient, depuis les
                    profondeurs du Styx,</l><l> De leur porter secours, dans leur détresse, tout
                    homme doué de sensibilité et désirant</l><l> qu’une action favorable aux arts
                    libéraux fût prise par les gens de bien. <note>On pourra rapprocher ces deux
                        premiers distiques du début de l’épigramme composée par Salandi « au lecteur
                        », à la fin de l’<foreign xml:lang="la">editio princeps</foreign> d’Apicius
                        (f°42v) : <foreign xml:lang="la">Accipe quisquis amas irritamenta palati /
                            Praecepta et leges oxigarumque nouum / Condiderat caput et Stygias
                            penitrauerat undas / Celius in lucem nec rediturus erat</foreign>. De
                        même, on retrouve l’image de la Muse implorante dans son poème à Démétrios
                        Chaldondyle (Souda, <foreign xml:lang="la">editio princeps</foreign>, Milan,
                        1499, f°516r) : <foreign xml:lang="la">Et Musae Argolicae locum
                            rogarent</foreign> (v. 13).</note></l><l> Mais personne n’avait prêté
                    l’oreille, de façon satisfaisante, à ces honnêtes prières ;</l><l> Il n’y avait
                    personne pour les relever de leur chute. </l></ab>

            <ab type="orig" xml:id="Pla1500_Salandus_p1_2"><l>Doctorum turba in tanta celebrique
                    corona,</l><l> Aurea Saturni saecula ut ista putes,</l><l> Per quae (pace frui
                    liceat modo) digna uidebis</l><l> Ingenia antiquis laudibus, ore, tuba.
                </l></ab>
            <ab type="trad" corresp="#Pla1500_Salandus_p1_2"><l>Il y avait pourtant une foule de
                    savants parmi ce cercle nombreux et d’une telle importance</l><l> Que l’on
                    pourrait considérer cette génération comme celle de l’âge d’or,</l><l> Parmi
                    laquelle – puissions-nous au moins jouir de la paix – on trouvera des hommes de
                    génie</l><l> Dignes des éloges, des voix et des trompettes antiques. </l></ab>

            <ab type="orig" xml:id="Pla1500_Salandus_p1_3"><l>Sed <persName ref="#pius_ioannes"
                        >Baptista Pius</persName> pietate insignis auita,</l><l> Censura et rerum
                    cognitione grauis, </l><l>Hunc uatem frustra auxilium implorare manusque</l><l>
                    Tendere non patiens, ecce rogatus adest.</l><l> Deque penu abstruso gemmas iubet
                    ire, lapillos</l><l> In medium et uulgus qui latuere prius. </l></ab>
            <ab type="trad" corresp="#Pla1500_Salandus_p1_3"><l>Mais voici que Battista Pio,
                    admirable pour sa piété envers les ancêtres,</l><l> Puissant par son jugement et
                    par sa connaissance du monde,<note>On retrouve cette association du jugement
                        critique et de la connaissance au début du poème de Salandi à Démétrios
                        Chaldondyle (op. cit., v. 4-8) : <foreign xml:lang="la">Sermo
                            CecropriusSophosque Achaeum, / Nostra a cognitione iam remotum / Propter
                            barbariem parum ferendam / A censoribus utriusque linguae, / Qum frustra
                            appeteretur in poetis.</foreign></note></l><l> Ne supportant pas de voir
                    ce poète implorer de l’aide et tendre</l><l> Ses mains en vain, répond à son
                    appel.</l><l> Il ordonne à ses joyaux et ses pierres précieuses, qui sont
                    jusque-là demeurées cachées,</l><l> De quitter leur temple secret pour gagner la
                    foule et l’espace public. </l></ab>

            <ab type="orig" xml:id="Pla1500_Salandus_p1_4"><l>Plura licet quidam media haud de plebe
                    notarint</l><l> Atque obseruarint laude ferenda sua,</l><l><persName
                        ref="#pius_ioannes">Baptistae</persName> tamen hanc frugem peperere lucernae
                    :</l><l> Hanc messem, hunc usum commoda, lector, habes. </l></ab>
            <ab type="trad" corresp="#Pla1500_Salandus_p1_4"><l>Bien que certains privilégiés aient
                    davantage relevé</l><l> Et observé des qualités dignes d’éloge,</l><l> Ce sont
                    les lumières de Battista qui ont mis au jour cette récolte :</l><l> Cette
                    moisson, cette ressource, lecteur, tu la détiens comme une richesse. </l></ab>

            <ab type="orig" xml:id="Pla1500_Salandus_p1_5"><l>Vt si forte loqui <persName ref="#pl"
                        >Plautus</persName> uelit, ore loquatur</l><l> Haud alio, dictet, digerat,
                    ornet, agat. </l></ab>
            <ab type="trad" corresp="#Pla1500_Salandus_p1_5"><l>À supposer que d’aventure Plaute
                    veuille parler, c’est avec la voix de cet homme qu’il parlerait,</l><l> Qu’il
                    dicterait et distribuerait ses rôles, qu’il préparerait la scène et serait
                    acteur. </l></ab>
        </body>
    </text>
</TEI>
