<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">And. Cratander Lectori S.</title>
                <author ref="#cratander_andreas">Andreas Cratander</author>

                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Malika BASTIN-HAMMOU</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Christian NICOLAS</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Malika BASTIN-HAMMOU</name>
                    <name>Diandra CRISTACHE</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <sourceDesc>

                <bibl>
                    <title><foreign xml:lang="gr">ΑΡΙΣΤΟΦΑΝΟΥΣ ΕΥΤΡΑΠΕΛΩΤΑΤΟΥ κωμωιδίαι
                            ἕνδεκα</foreign> ARISTOPHANIS FACETISSIMI comoediae undecim</title>
                    <author ref="#ar">Aristophanes</author>
                    <pubPlace ref="#basilea">Basileae</pubPlace>
                    <publisher ref="#cratander_andreas">Andreas Cratander</publisher>
                    <publisher ref="#bebelius_ioannes">Ioannis Bebelius</publisher>
                    <date when="1532">1532</date>
                    <editor ref="#grynaeus_simon">Simon Grynaeus</editor>
                </bibl>
                <listBibl>
                    <head>Bibliographie</head>
                    <bibl><author>E.A. Meier</author>, <author>Andreas Cratander</author>, <title
                            level="m">Ein Basler Drucker und Verleger der Reformationszeit</title>,
                            <pubPlace>Bâle</pubPlace>, <date>1966</date>
                    </bibl>
                </listBibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>1. Nombreuses annotations manuscrites.</p>
                <p>2. Lettrines.</p>
                <p>3. Il y a deux épitres dédicatoires : <list>
                        <item>-celle de l’imprimeur Andreas Cratander au lecteur</item>
                        <item>-celle de l’éditeur Simon Grynaeus à la jeunesse studieuse</item>
                    </list> Suivent : ΤΩΝ ΤΗΣ ΑΡΧΙΑΣ ΚΩΜΩΙΔΙΑΣ ποιητῶν ὀνόματα καὶ δράματα Et pour
                    chaque pièce : <list>
                        <item>Η ΥΠΟΘΕΣΙΣ ΤΟΥ ΠΑΡΟΝΤΣ δράματος ἔστιν αὕτη</item>
                        <item>ΤΑ ΤΟΥ ΔΡΑΜΑΤΟΣ ΠΡΟΣΩΠΑ</item>
                    </list></p>
                <p>4. Andreas Cratander : Originaire de Strasbourg, il est étudiant à Heidelberg en
                    1515 où il exerce la fonction de correcteur chez Adam Petri avant d'imprimer
                    pour son compte de 1518 à 1536. C'est lui qui imprime le premier dictionnaire de
                    grec à Bâle en 1519.</p>

            </abstract>

            <langUsage>
                <language ident="la">Latin</language>
            </langUsage>

        </profileDesc>

        <revisionDesc>
            <change when="2022-09-30">Malika BASTIN-HAMMOU : encodage sémantique</change>
            <change when="2022-03-03">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
            <change when="2020-09-21">Diandra Cristache : Encodage de la transcription et de la
                traduction</change>
        </revisionDesc>

    </teiHeader>

    <text>
        <body>

            <head type="orig" xml:id="Ar1532_Cratander_Cratander_p1_0"><persName
                    ref="#cratander_andreas">And. Cratander</persName> Lectori S.</head>
            <head corresp="#Ar1532_Cratander_Cratander_p1_0" type="trad">Andreas Cratander au
                Lecteur, salut</head>

            <ab type="orig" xml:id="Ar1532_Cratander_Cratander_p1_1">Habes candide lector, praeter
                nouem hactenus, editas <persName ref="#ar">Aristophanis</persName> comoedias, duas
                in calce adiunctas, nimirum <title ref="#ar_th">Sacrificantes foeminas</title>,
                Atticamque <title ref="#ar_lys">Lysistraten</title>. </ab>
            <ab type="trad" corresp="#Ar1532_Cratander_Cratander_p1_1">Voici, lecteur bienveillant,
                en plus des neuf comédies d’Aristophane déjà éditées à ce jour, deux autres
                imprimées qui viennent s’y ajouter : <title>Les femmes sacrifiant</title> et
                l’attique <title>Lysistrata</title>.</ab>

            <ab type="orig" xml:id="Ar1532_Cratander_Cratander_p1_2">In quibus si quid
                desiderabitur, in exemplar mutilum et corruptum culpam transferas oportet. </ab>
            <ab type="trad" corresp="#Ar1532_Cratander_Cratander_p1_2" ana="#PH #LI">Dans ces comédies, s’il
                manque quelque chose, il convient d’en attribuer la cause à
                l’exemplaire mutilé et corrompu. </ab>

            <ab type="orig" xml:id="Ar1532_Cratander_Cratander_p1_3">Nos studiis tuis fauentes,
                illas tibi ut invenimus, minus exploitas, quam nullas tradere maluimus. </ab>
            <ab type="trad" corresp="#Ar1532_Cratander_Cratander_p1_3" ana="#PH">Nous, qui sommes favorables à
                tes études, nous avons préféré te les transmettre dans l’état peu soigné où nous les
                avons trouvées, plutôt que de ne pas les transmettre du tout </ab>

            <ab type="orig" xml:id="Ar1532_Cratander_Cratander_p1_4">In reliquis uero quid
                praestiterimus, tute conferendo facile comperies. Vale. </ab>
            <ab type="trad" corresp="#Ar1532_Cratander_Cratander_p1_4" ana="#PH">Pour le reste, ce que nous
                avons corrigé, tu le trouveras toi-même facilement en comparant. Porte-toi bien.
            </ab>
        </body>
    </text>
</TEI>
