<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

   <teiHeader>

      <fileDesc>

         <titleStmt>
            <title xml:lang="la">Aliud</title>
            <author ref="#frischlinus_nicodemus">Nicodemus Frischlinus</author>

            <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
            <!-- Tâche n°1 -->
            <respStmt>
               <resp>Transcription</resp>
               <name>Christian NICOLAS</name>
            </respStmt>
            <!-- Tâche n°2 -->
            <respStmt>
               <resp>Traduction</resp>
               <name>Christian NICOLAS</name>
            </respStmt>
            <!-- Tâche n°3 -->
            <respStmt>
               <resp>Encodage</resp>
               <name>Malika BASTIN-HAMMOU</name>
               <name>Sarah GAUCHER</name>
            </respStmt>
            <!-- Ajouter autant de tâches que nécessaire -->

            <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
            <principal>Malika BASTIN-HAMMOU</principal>
            <respStmt>
               <resp>Modélisation et structuration</resp>
               <persName>Elisabeth GRESLOU</persName>
               <persName>Elysabeth HUE-GAY</persName>
            </respStmt>
            <respStmt>
               <resp>Première version du modèle d'encodage</resp>
               <persName>Elisabeth GRESLOU</persName>
               <persName>Anne Garcia-Fernandez</persName>
            </respStmt>
         </titleStmt>

         <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
         <publicationStmt>
            <publisher/>
            <availability>
               <p>Le site est en accès restreint</p>
            </availability>
            <pubPlace>ithac.elan-numerique.fr</pubPlace>
         </publicationStmt>

         <sourceDesc>
            <bibl>
               <title>Nicodemi Frischlini Aristophanes, veteris comoediae princeps: poeta longe
                  facetissimus et eloquentissimus, repurgatus a mendis, et imitatione Plauti atque
                  Terentii interpretatus, ita ut fere Carmen Carmini, numerus numero, pes pedi,
                  modus modo, Latinismus Graecismo respondeat. Opus Divo Rudolpho Caesari
                  Sacrum</title>
               <author ref="#ar">Aristophanes</author>
               <editor role="traducteur" ref="#frischlinus_nicodemus">Nicodemus Frischlinus</editor>
               <publisher ref="#spies_ioannes">Ioannes Spies</publisher>
               <pubPlace ref="#francofurtum">Francoforti ad Moenum</pubPlace>
               <date when="1586">1586</date>
               <ref target="url_edition_numérique"/>
               <!-- pour les liens google book, remplacer les & par &amp; pour que ça fonctionne -->
            </bibl>

            <listBibl>
               <head>Bibliographie</head>
               <bibl><author>Thomas Baier</author><title>« Nicodemus Frischlin als
                     Aristophanes-Übersetzer »</title><editor>Ekkehard Stärk et Gregor
                     Vogt-Spira</editor><title>Dramatische Wäldchen. Festschrift für Eckard Lefèvre
                     zum 65.
                     Geburtstag</title><pubPlace>Hildesheim</pubPlace><publisher>Olms</publisher><date>2000</date></bibl>
               <bibl>
                  <author>Patrick Lucky Hadley</author>
                  <title>Athens in Rome, Rome in Germany. Nicodemus Frischlin and the Rehabilitation
                     of Aristophanes in the 16th Century</title>
                  <pubPlace>Tübingen</pubPlace>
                  <publisher>Narr Francke Attempto Verlag</publisher>
                  <date>2015</date>
               </bibl>
               <bibl><author>David Price</author><title>The Political Dramaturgy of Nicodemus
                     Frischlin : Essays on Humanist Drama in Germany</title><pubPlace>Chapel
                     Hill</pubPlace>
                  <publisher>University of North Carolina Press</publisher>,
                  <date>1990</date></bibl>
            </listBibl>
         </sourceDesc>

      </fileDesc>

      <profileDesc>

         <abstract>
            <p>Traduction latine de cinq comédies d’Aristophane.</p>
         </abstract>

         <langUsage>
            <!-- ne pas modifier -->
            <language ident="fr">Français</language>
            <language ident="la">Latin</language>
            <language ident="grc">Grec</language>
         </langUsage>

         <textClass n="vers"></textClass>

      </profileDesc>

      <revisionDesc>
         <change when="2022-12-16">Malika BASTIN-HAMMOU : encodage sémantique</change>
         <change when="2022-11-08">Sarah GAUCHER : création de la fiche TEI, encodage de la
            transcription et de la traduction</change>
      </revisionDesc>
   </teiHeader>

   <text>

      <body ana="#A">

         <head type="orig" xml:id="Ar1586_Frischlinus_p15_0">Aliud</head>
         <head type="trad" corresp="#Ar1586_Frischlinus_p15_0">Autre argument</head>

         <ab type="orig" xml:id="Ar1586_Frischlinus_p15_1"><l>Plutum admonet uir pauper uisum
               recipere.</l><l> Laboriosos simul accersit agricolas.</l><l> Vbi Blepsidemus accessit
               socius, uenit</l><l> Tum pauperies ; repellitur. Caeco deo</l><l> Visum dat
               Aesculapius ; gaudent boni,</l><l> Saeuiunt mali. Pluto etiam cedit Iupiter.
            </l></ab>
         <ab type="trad" corresp="#Ar1586_Frischlinus_p15_1"><l>Plutus reçoit conseil de la part
               d’un fauché de recouvrer la vue.</l><l> Là, dans le même temps, il fait venir à lui
               des travailleurs des champs.</l><l> Un certain Blepsidème arrive et fait équipe ; et
               sur ces entrefaites,</l><l> Tout soudain vient Ladêche ; on la chasse en exil ;
               alors, au dieu aveugle,</l><l> Un autre rend la vue, Esculape, les bons peuvent se
               réjouir ;</l><l> Seul le vice est perdant. Plutus devient plus fort que le grand
               Jupiter. </l></ab>



      </body>

   </text>

</TEI>
