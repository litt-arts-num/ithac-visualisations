<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">


    <teiHeader>

        <fileDesc>

            <titleStmt>

                <!--Ci-dessous le titre du paratexte-->
                <title xml:lang="la">In Sophoclis Tragoedias Praefatiuncula Per Ioannem Lalamantium. </title>
                <author ref="#lalamantius_ioannes">Johannes Lalamantius</author>

                <respStmt>
                    <resp>Transcription</resp>
                    <persName>Diandra CRISTACHE</persName>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <persName>Sarah GAUCHER</persName>
                </respStmt>
                <respStmt>
                    <resp>Révision</resp>
                    <name>Christian NICOLAS</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Diandra CRISTACHE</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>

                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <sourceDesc>
                <bibl>
                    <title>Sophoclis Tragicorum ueterum facile principis Tragoediae, quotquot
                        extant, septem. Aiax, Electra, Oedipus Tyrannus, Antigone, Oedipus in
                        Colono, Trachiniae, Philoctetes. Nunc primum Latinae factae, et in lucem
                        emissae per Ioannem Lalamantium apud Augustudunum Heduorum Medicum. </title>
                    <author ref="#soph">Sophocles</author>
                    <publisher ref="#morellus_federicus">Federicus Morellus </publisher>
                    <pubPlace ref="#lutetia">Lutetiae</pubPlace>
                    <date when="1557">1557</date>
                    <editor ref="#lalamantius_ioannes" role="traducteur">Johannes Lalamantius </editor>
                    <ref
                        target="https://books.google.fr/books?id=WSg7AAAAcAAJ&amp;printsec=frontcover&amp;dq=jean+lalamant&amp;hl=fr&amp;sa=X&amp;ved=0ahUKEwiZwdairdXbAhUDaFAKHRpxAYkQ6AEIKjAA#v=onepage&amp;q=jean%20lalamant&amp;f=false"
                    />
                </bibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p/>
            </abstract>

            <langUsage>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>

        <revisionDesc>
            <change when="2022-02-11">Sarah GAUCHER : Vérification et ajouts PersName, PlaceName,
                hi, title, quote, seg, foreign. Vérification de la mise en conformité du schéma </change>
            <change when="2021-11-10">Sarah GAUCHER correction de la transcription et encodage de la
                traduction</change>
            <change when="2021-05-08">Diandra CRISTACHE : Mise à jour et chargement sur le
                Pensoir</change>
            <change when="2020-09-21">Diandra CRISTACHE : Encodage de la transcription et de la
                traduction</change>

        </revisionDesc>

    </teiHeader>

    <text>
        <body>
            <head type="orig" xml:id="Soph1557a_Lalamantius_p4_0">In <persName ref="#soph"
                    >Sophoclis</persName> Tragoedias Praefatiuncula Per <persName
                    ref="#lalamantius_ioannes">Ioannem Lalamantium</persName>. </head>
            <head type="trad" corresp="#Soph1557a_Lalamantius_p4_0">Courte preface aux tragedies de
                Sophocle par Joannes Lalamantius</head>
            <ab type="orig" xml:id="Soph1557_Lalamantius_p4_1">Quoniam poetae solent inuolucris et
                ambagibus ueritati tenebras offundere et ueluti suo uelum poemati praetendere, ut
                inde quasi a mysteriis sacrorum ignarum uulgus et indoctum (cuius tamen instruendi
                praecipua cura esset habenda) arceant, nihil me alienum a meo instituto facturum
                existimaui, si uelum, quod singulis <persName ref="#soph">Sophoclis</persName>
                tragoediis praetensum est, contraherem, et ueritatem, quae figmento poetico subest,
                patefacerem. </ab>
            <ab type="trad" corresp="#Soph1557_Lalamantius_p4_1" ana="#PE #PH #TH_fin">Puisque les poètes
                ont coutume de répandre des ténèbres sur la vérité par des ombrages et des détours
                et pour ainsi dire de tendre un voile sur leur poème comme pour tenir la foule
                ignare et imbécile (dont l’instruction cependant devrait être tenue pour une tâche
                fondamentale) loin des mystères des choses sacrées, j’ai pensé ne rien faire
                d’étranger à ma mission si je tirais le voile qui a été tendu sur chacune des
                tragédies de Sophocle et que je faisais apparaître la vérité qui se trouve sous la
                représentation poétique.</ab>
            <ab type="orig" xml:id="Soph1557_Lalamantius_p4_2">Sic enim existimaui fore uti noster
                hic labor inanis, et frustra susceptus permultis uideretur, nisi quae sub aenigmate
                dicta sunt a <persName ref="#soph">Sophocle</persName>, ob oculos eorum ponerem, qui
                uel <title ref="#soph_or">Œdipum</title> domi non habent uel Delio egent
                natatore.</ab>
            <ab type="trad" corresp="#Soph1557_Lalamantius_p4_2" ana="#PH">J’ai pensé en effet que beaucoup
                considéreraient notre travail comme dérisoire et entrepris en vain, si je ne faisais
                pas voir les énigmes de Sophocle à ceux qui ou bien ne possèdent pas
                    <title>Œdipe</title> chez eux ou bien réclament le nageur de Délos.</ab>
            <ab type="orig" xml:id="Soph1557_Lalamantius_p4_3">Namque profecto quicunque ad poetarum
                lectionem ita accedunt, ut quid Tityrus, quid Corydon, quid Simo, quid Dauus, quid
                hic, quid ille dicat, tantum considerent, uerba tantum ad aurium iudicium appendant
                et trutinent, formulas dicendi obseruent, hi fructum aliquem inde, fateor, reportant
                sed qui profecto ei, qui ex ueli contractione colligitur, quo nimirum uita et mores
                fiunt meliores, longo interuallo relinquatur.</ab>
            <ab type="trad" corresp="#Soph1557_Lalamantius_p4_3">Car assurément tous ceux qui ont
                accédé à la lecture des poètes en prenant en compte seulement ce que dit Tityre,
                Corydon, Simon, Davus, ou tel ou tel autre, en mesurant à l’oreille et en soupesant
                seulement leurs mots, en observant les formules de leurs discours, ceux-là, je
                l’avoue, en tirent de là quelque fruit mais un fruit qui se conserve longtemps à
                celui qui le récolte en retirant ce voile pour rendre excellentes sa vie et ses
                mœurs.</ab>
            <ab type="orig" xml:id="Soph1557_Lalamantius_p4_4">Et uero cum Tragoedia non minus quam
                Comoedia uitae humanae speculum sit, hominumque mores, dicta, facta, fortunae uarios
                incertosque euentus, et uitae calamitates, illa in Heroum personis repraesentet, non
                uideo, quomodo quis sese ueluti in speculo possit contemplari, ni apud se perpendat
                et consideret quid sibi res uelit et quo tendat operis argumentum ; quod et ipsum
                cum tectum sit et uelatum, ab his qui interpretationem poetarum suscipiunt, siquidem
                docere uolunt, est retegendum.</ab>
            <ab type="trad" corresp="#Soph1557_Lalamantius_p4_4" ana="#TH_dram #TH_fin #PE">Et en
                vérité puisque la tragédie est autant que la comédie le miroir de la vie humaine et
                que par le truchement de personnages héroïques elle représente les mœurs des hommes,
                leurs paroles, leurs actions, les tribulations variées et incertaines de leur sort
                et les malheurs de leur vie, je ne vois pas comment quelqu’un pourrait s’y
                contempler comme dans un miroir s’il n’évalue pas et ne considère pas en son for
                intérieur ce que cet écrit signifie et à quoi tend le sujet de l’œuvre ; et puisque
                cela a été caché et voilé il faut que le fassent connaître ceux qui se chargent de
                l’interprétation des poètes, si vraiment ils veulent l’enseigner.</ab>
            <ab type="orig" xml:id="Soph1557_Lalamantius_p4_5">Nam si quale est argumentum tantum
                consideras, uerba tantum auctoris ponderas, elocutionem spectas, genus dicendi
                admiraris, neque tamen uelum contrahis, quo remoto, quid subsit ueri intelligas, in
                sole caligabis, et uiatoris instar locorum ignari, a uia regia in semitas aberrabis,
                et deflectes, et demum eo fructu, qui ex poetarum lectione colligi debet, priuabere
                ; quo tamen intellecto, uitam tuam instrues, te contra fortunae insultus armabis,
                eius te esse existimabis ludibrium, ad omnia denique quae illa posset aliquando in
                te iaculari tela, parabis et munies.</ab>
            <ab type="trad" corresp="#Soph1557_Lalamantius_p4_5" ana="#A #RH #PE">Car si tu
                considères seulement la nature de l’argument, si tu pèses seulement les mots de
                l’auteur, si tu regardes l’élocution, si tu admires le style mais que cependant tu
                ne retires pas ce voile pour comprendre la vérité sous-jacente après qu’il a été
                retiré, alors tu seras à l’ombre en plein soleil et, semblable à un voyageur à qui
                les lieux sont inconnus, tu erreras dans des sentiers loin de la voie royale, tu te
                fourvoieras et tu seras finalement privé de ce fruit que la lecture des poètes
                devait te faire récolter ; mais si tu en as la pleine compréhension, tu bâtiras ton
                existence, tu t’armeras contre les outrages du destin, tu sauras que tu es son
                jouet, et finalement tu te prépareras et t’armeras contre tous les traits que la
                fortune pourrait un jour lancer contre toi.</ab>
            <ab type="orig" xml:id="Soph1557_Lalamantius_p4_6">Hoc igitur <title ref="#soph_aj"
                    >Aiacis</title> argumento quid sibi uelit <persName ref="#soph"
                    >Sophocles</persName>, paucis, ut intelligatur, efficiam. </ab>
            <ab type="trad" corresp="#Soph1557_Lalamantius_p4_6" ana="#A #PE">Ainsi, grâce
                l’argument d’<title>Ajax</title>, je ferai brièvement en sorte que tu comprennes ce
                que Sophocle veut dire.</ab>
        </body>
    </text>
</TEI>
