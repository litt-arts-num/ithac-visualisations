<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Illustrissimo ac eruditissimo Alberto Pio principi Carpensi,
                    ingeniorum fautori, Franciscus</title>
                <author ref="#passius_franciscus">Franciscus Passius Carpensis</author>

                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Diandra CRISTACHE</name>
                    <name>Sarah GAUCHER</name>
                    <name>Malika BASTIN-HAMMOU</name>

                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <sourceDesc>

                <bibl>
                    <title>Plutus Antiqua Comoedia ex Aristophane quae nuper in Linguam Latinam
                        translata est </title>
                    <author ref="#ar">Aristophanes</author>
                    <pubPlace ref="#parma">Parmae</pubPlace>
                    <publisher ref="#ugoletus_angelus">Angelus Vgoletus</publisher>
                    <date when="1501">1501</date>
                    <editor ref="#passius_franciscus">Franciscus Passius Carpensis</editor>
                    <editor role="traducteur" ref="#passius_franciscus" >Franciscus Passius Carpensis</editor>
                </bibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Liste des paratextes dans la même édition : <list>
                        <item>1. Illustrissimo ac eruitissimo Alberto Pio principi Carpensi :
                            ingeniorum fautori Franciscus Passius Carpensis domino suo Felicitatem.
                            D.</item>
                        <item>2. Antonii Zandemariae equitis hyerosolymitani Parmensis. ad
                            lectorem.</item>
                        <item>3. Tranquilli Molossi Cremonsis Apologeticon.</item>
                        <item>4. Argumentum</item>
                        <item>5. Francisci Passii Carpensis Plutus Comoedia Incipit. Prologus</item>
                    </list></p>

            </abstract>

            <langUsage>
                <language ident="la">Latin</language>
            </langUsage>

        </profileDesc>

        <revisionDesc>
            <change when="2022-07-13">Malika BASTIN-HAMMOU : Encodage sémantique</change>
            <change when="2022-03-03">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
            <change when="2020-09-27">Diandra CRISTACHE : Encodage de la transcription et de la
                traduction</change>

        </revisionDesc>

    </teiHeader>

    <text>
        <body ana="#PA">

            <head type="orig" xml:id="Ar1501_Passius_p1_0">Illustrissimo ac eruditissimo <persName
                    role="destinataire" ref="#pius_albertus">Alberto Pio</persName> principi
                Carpensi, ingeniorum fautori, <persName ref="#passius_franciscus"
                    >Franciscus</persName>
            </head>
            <head corresp="#Ar1501_Passius_p1_0" type="trad">Au très illustre et érudit Albert III
                Pio prince de Carpi, protecteur des talents, Francesco Passi de Carpi souhaite le
                bonheur à son maître. </head>

            <ab type="orig" xml:id="Ar1501_Passius_p1_1">Cum aliquando intensissimi caloris euitandi
                gratia ruratum (ut <persName ref="#pl">plautino</persName> uerbo utar) <seg
                    type="paraphrase">ab urbe secessissemus<bibl resp="#SG"><author ref="#pl"
                            >Pl.</author>, <title ref="#pl_men">Men.</title>
                        <biblScope>63-64</biblScope>.</bibl></seg>, ibi ad Camenas quodammodo
                inuitari uisus sum : cum loci amenitate : ut pote qui a lympidis frequentibusque
                iugis aquae fontibus nomen sibi uendicat Tum ne illud temporis : quo nil parcius a
                diis in mortalibus nobis tributum est : totum ocio contererem. </ab>
            <ab type="trad" corresp="#Ar1501_Passius_p1_1">Un jour que, pour échapper à
                une chaleur intense, je m’étais retiré au vert (pour prendre un mot plautinien) et
                avais quitté la ville, je me crus d’une certaine façon invité chez les Muses, d’une
                part par la beauté d’un lieu à qui la limpidité de ses sources et le nombre de ses
                collines donne son nom, d’autre part pour ne pas laisser perdre dans l’oisiveté ce
                temps que les dieux immortels nous ont si parcimonieusement compté.</ab>

            <ab type="orig" xml:id="Ar1501_Passius_p1_2">dubitanti quam mihi potissimum scribendi
                materiam ex multis quae occurebant assumerem : <persName ref="#ar"
                    >Aristophanis</persName>
                <title ref="#ar_pl">Plutus</title> in primis mihi digna uisa fuit : quam in latino
                sermone imitaremur. Partim nouitate rei (non est enim apud nos ulla antiqua
                comoedia) Partim quod non minus grauitatis in se contineret quam facetiarum :
                Quodque hominibus graecarum literarum expertibus non minus iucundum quam gratum et
                perutile fore confiderem. </ab>
            <ab type="trad" corresp="#Ar1501_Passius_p1_2" ana="#T #TH_hist #TH_fin #TH_dram"
                >Alors que je me demandais sur quelle matière écrire de préférence parmi les
                nombreuses possibilités qui s’offraient à moi, le <title>Plutus</title> d’<persName
                    ref="aristophanes">Aristophane</persName> me parut en premier digne d’une
                imitation en langue latine, en partie pour la nouveauté du projet (car nous ne
                possédons pas en latin de comédie ancienne), en partie parce qu’il recèle autant de
                gravité que de plaisanteries et que les non hellénistes y trouveraient, j’en étais
                sûr, du plaisir, de l’agrément et de l’utilité. </ab>

            <ab type="orig" xml:id="Ar1501_Passius_p1_3">Quam olim cum caeteris <persName ref="#ar"
                    >Aristophanis</persName> fabulis mihi et condiscipulo <persName>Balthasari
                    Alioto</persName> non Vulgaris doctrinae iuueni interpretatus est
                    <persName>Thadaeus Vgoletus Parmensis</persName> : numquam sine aliqua utriusque
                linguae uiri doctissimi praefatione nominandus : Quem constat iudicio et rerum
                cognitione : quam assidua lectione contraxit inuictissimo <persName>Matthiae
                    Panoniae Regi</persName> a secretis fuisse carissimum. Cuique non disimulanter
                debere fateor quicquid literis ualeo si quid ualeo.</ab>
            <ab type="trad" corresp="#Ar1501_Passius_p1_3" ana="#T">Cette pièce naguère, avec
                les autres pièces d’Aristophane, c’est pour moi et mon condisciple Balthazar
                Aliotus, jeune homme au savoir éminent, qu’en a donné une traduction Thadeus
                Ugoletus de Parme, un homme qu’il faut toujours nommer dans une préface d’auteur
                érudit dans les deux langues, dont on sait que le jugement et la culture, acquise
                par des lectures assidues, l’ont rendu très cher au cœur du roi Matthias Ier de
                Hongrie dont il fut secrétaire et à qui sans ambages j’avoue que je dois toute ma
                valeur en littérature, si j’ai une valeur. </ab>

            <ab type="orig" xml:id="Ar1501_Passius_p1_4">Hanc igitur tibi comoediam uel potius
                satyrocomoediam (ut unde digressi sumus revertamur) non tantum recognoscendi quam
                emendandi gratia missimus. Quam precipue <persName ref="#zandemaria_antonius"
                    >Antonii Zandemariae</persName> equitis hyerosolymitani, non aspernandae indolis
                adulescentuli et cuius amicicia plurimum utor assiduis obtestationibus ab incude in
                publicum dare exoratus : existimaui turbidum uulgi iudicium subituram : tuo quam
                caeterorum patrocinio tutiorem fore. quo protectam quocumque ingredi haud
                dubitauero.</ab>
            <ab type="trad" corresp="#Ar1501_Passius_p1_4" ana="#TH_hist #TH_dram #PH">C’est
                donc cette comédie ou plutôt satyrocomédie (pour revenir au point d’où nous sommes
                partis) que nous t’envoyons et pour que tu la révises et pour que tu la corriges.
                C’est surtout Antonius Zandemaria, chevalier de Jérusalem, jeune homme au caractère
                bien trempé et avec qui j’entretiens la plus grande amitié, qui par ses constantes
                prières, me somme de l’imprimer et de la livrer au public ; mais j’ai pensé que,
                comme elle devrait subir le jugement trouble de la foule, c’est sous ton patronage
                plutôt que n’importe quel autre qu’elle serait en sûreté, avec la certitude que sous
                cette égide elle pouvait aller partout. </ab>

            <ab type="orig" xml:id="Ar1501_Passius_p1_5">Nam si tibi pensiculatim omnia trutinanti :
                et perspicacis iudicii uiro placuerit : caeteris quoque non displicuisse oportebit.
                (Nam te : praeterquam philosophiae arti poetices deditum esse cognoui) Sed ne illud
                quoque animaduersione dignum uideatur : solis trimetris fabula haec continetur :
                quam dimetris trimetris tetrametris acataleticis et hypercataleticis fecerat
                    <persName ref="#ar">Aristophanes</persName>. ubi illud maxime <persName
                    ref="#quint">Quintiliani</persName> iudicium sequti sumus adserentis <seg
                    type="paraphrase"><persName ref="#ter">Terentium</persName> maiorem laudem
                    assecuturum fuisse si se inter trimetros continuisset<bibl resp="#SG"><author
                            ref="#quint">Quint.</author>, <title ref="#quint_io">I.O.</title>
                        <biblScope>10.1.99</biblScope>.</bibl></seg>. Noli igitur obeliscis
                parcere : si quid caecus hic noster tibi caecutire uidebitur Vale. </ab>
            <ab type="trad" corresp="#Ar1501_Passius_p1_5" ana="#M #PH">Car si après une pesée
                rigoureuse au trébuchet elle plaît à l’homme de goût que tu es, il faudra aussi
                qu’elle ne déplaise pas à tous les autres (car en plus de la philosophie, tu es
                connaisseur en poétique, je le sais). Mais, sans que cela aussi mérite une remarque,
                c’est en trimètres seulement que ma pièce est construite, alors qu’Aristophane
                l’avait écrite en dimètres, trimètres, tétramètres acatalectiques et
                hypercatalectes. Mais là, c’est surtout le jugement de Quintilien que nous avons
                suivi, qui dit que Térence aurait eu plus grand mérite s’il s’était cantonné aux
                trimètres. N’épargne donc pas les marques de correction, dès lors que notre aveugle
                te paraîtra voir trouble. Porte-toi bien. </ab>


        </body>
    </text>
</TEI>
