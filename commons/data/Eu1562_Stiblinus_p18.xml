<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

   <teiHeader>

      <fileDesc>

         <titleStmt>

            <title xml:lang="la">Praefatio Gaspari Stiblini in Euripidis Bacchas</title>
            <author ref="#stiblinus_gasparus">Gasparus Stiblinus</author>
            <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

            <respStmt>
               <resp>Transcription</resp>
               <persName>Sarah GAUCHER</persName>
            </respStmt>
            <respStmt>
               <resp>Traduction</resp>
               <persName>Sarah GAUCHER</persName>
            </respStmt>
            <respStmt>
               <resp>Encodage</resp>
               <name>Sarah GAUCHER</name>
            </respStmt>
            <principal>Malika BASTIN-HAMMOU</principal>
            <respStmt>
               <resp>Modélisation et structuration</resp>
               <persName>Elisabeth GRESLOU</persName>
               <persName>Elysabeth HUE-GAY</persName>
            </respStmt>
            <respStmt>
               <resp>Première version du modèle d'encodage</resp>
               <persName>Elisabeth GRESLOU</persName>
               <persName>Anne Garcia-Fernandez</persName>
            </respStmt>
         </titleStmt>

         <publicationStmt>
            <publisher/>
            <availability>
               <p>Le site est en accès restreint</p>
            </availability>
            <pubPlace>ithac.elan-numerique.fr</pubPlace>
         </publicationStmt>
         <sourceDesc>
            <bibl>
               <title>Euripides Poeta Tragicorum princeps, in Latinum sermonem conuersus, adiecto
                  eregione textu Graeco : CUM ANNOTATIONIBUS ET PRAEFATIONIBUS in omnes eius
                  Tragoediae, autore GASPARO STIBLINO, Accesserunt JACOBI MICYLLI, de Euripidis
                  uita, ex diuersis autoribus collecta : item, De tragoedia et eius partibus
                  προλεγομένα quaedam. Item IOANNIS BRODAEI Turonensis Annotationes doctissimae
                  numquam antea in lucem editae. Ad haec Rerum et uerborum toto Opere Praecipue
                  memorabilium copiosus INDEX. Cum caesaris Maiestatis et Christianissimi Gallorum
                  Regis gratia ac priuilegio, ad decennium. Basileae, per Ioannem Oporinum.</title>
               <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
               <author ref="#eur">Euripides</author>
               <pubPlace ref="#basilea">Basilea</pubPlace>
               <publisher ref="#oporinus_ioannes">Iohannes Oporinus</publisher>
               <date when="1562">1562</date>
               <!-- dans l'attribut @when, l'année en chiffre arabes -->
               <editor ref="#stiblinus_gasparus">Gasparus Stiblinus</editor>
               <editor role="traducteur" ref="#stiblinus_gasparus">Gasparus Stiblinus</editor>
               <!-- pour les liens google book, remplacer les & par &amp; pour que ça fonctionne -->
            </bibl>

            <listBibl>
               <bibl/>
               <!-- créer un <bibl> par référence -->
            </listBibl>

         </sourceDesc>
      </fileDesc>

      <profileDesc>

         <abstract>
            <p/>
            <!-- s'il s'agit d'une liste, utiliser les éléments ci-dessous -->
            <list>
               <item/>
               <item/>
               <item/>
            </list>
         </abstract>

         <langUsage>
            <!-- ne pas modifier -->
            <language ident="fr">Français</language>
            <language ident="la">Latin</language>
            <language ident="grc">Grec</language>
         </langUsage>

      </profileDesc>
      <revisionDesc>
         <change when="2022-02-19">Sarah GAUCHER : identifiants Header, mise en conformité
            Elan</change>
         <change when="2022-01-06">Sarah GAUCHER : création de la fiche TEI, encodage de la
            transcription et de la traduction</change>

      </revisionDesc>
   </teiHeader>
   <text>
      <body ana="#A">
         <head type="orig" xml:id="Eu1562_Stiblinus_p18_0">Praefatio <persName
               ref="#stiblinus_gasparus">Gaspari Stiblini</persName> in <persName ref="#eur"
               >Euripidis</persName>
            <title ref="#eur_bacch">Bacchas</title></head>
         <head type="trad" corresp="#Eu1562_Stiblinus_p18_0">Préface de Gaspar Stiblinus aux
               <title>Bacchantes</title> d’Euripide.</head>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_1">Dionysus propter ingentia beneficia in
            humanum genus et res abs se praeclare gestas inter deos relatus, cum ab una Thebarum
            ciuitate non reciperetur, quam tamen in primis colebat ac praestantissimis uinetis
            nobilitarat, manifestum suae diuinitatis documentum dare uoluit.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_1"> Dionysos, qui a été accepté comme l'un
            des dieux en raison de ses immenses bienfaits envers le genre humain et des actes qu’il
            avait brillamment accomplis, alors qu'il n'était pas reçu par la seule cité de Thèbes,
            que cependant il avait particulièrement favorisée et avait rendue glorieuse avec des
            vignobles exceptionnels, voulut donner une preuve évidente de sa divinité.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_2">Venit itaque Thebas omnesque mulieres in
            furorem actas ad montem Cithaeronem compulit utque ibi orgia sua celebrarent
            effecit.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_2"> En conséquence, il vint à Thèbes, rendit
            folles toutes les femmes, les obligea à se rendre sur le mont Cithéron et fit en sorte
            qu'elles y célèbrent ses rites extatiques.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_3">Hanc rem diuinitus fieri seniores, ut
            Cadmus et Tiresias, facile ut crederent adducebantur.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_3"> Les hommes les plus âgés, tels que
            Cadmus et Tirésias, furent facilement amenés à croire que cela était dû à une cause
            divine.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_4">Quare et se ad ferendum honores debitos Deo
            nouo parant.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_4">Et c'est pourquoi ils se préparent à
            porter les honneurs dus au nouveau Dieu. </ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_5">Pentheus autem solus refractarius
            Bacchantibus poenas minatur, orgia disturbare meditatur : Dionysum ridet ac in uincula
            ducit.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_5"> Penthée seul, cependant, obstiné qu’il
            est, menace de punir les Bacchantes et entend perturber leurs rites : il se moque de
            Dionysos et le jette dans les fers.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_6">Nec illum mouent ea quae cum domi a deo
            fieri cernebat tum a pastore facta foris narrari audiebat.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_6">Il n'est pas non plus ému par ce qu'il a
            vu le dieu faire dans sa maison, ni par ce qu’il est arrivé hors de la ville et qu'il a
            entendu raconter par un berger.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_7">Tandem Dionysus, ut impius <foreign
               xml:lang="grc">θεόμαχος</foreign> pertinaciae poenas daret, ei rectam mentem
            eripit.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_7">Enfin Dionysos, pour que l'impie
            théomaque paie le prix de son obstination, le prive de son bon sens.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_8">Et ut indutus Baccharum amictum iret uisum
            Maenadas <foreign xml:lang="grc">ὀργιάζοντας</foreign> persuadet.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_8"> Et il le persuade qu'après avoir revêtu
            l'habit des Bacchantes, il doit aller voir les Ménades célébrer leurs mystères.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_9">Quocirca impos sui Pentheus ad Cithaeronem
            ridiculus in ueste muliebri praeeunte sub mutata figura Dionyso pergit correptusque a
            Bacchis miserrime et crudelissime laniatur.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_9">Pour cette raison, Penthée, sans pouvoir
            sur lui-même, risible dans des vêtements de femme, avec pour guide Dionysos
            métamorphosé, se rend au mont Cithéron et, saisi par les Bacchantes, est très
            misérablement et cruellement mis en pièces.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_10">Agaue autem cum sororibus postquam ad se
            rediissent cum decrepito patre in exilium mittuntur.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_10">De plus, Agavé et ses sœurs, après être
            revenues à elles-mêmes, sont envoyées en exil avec leur vieux père.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_11">Poeta ergo hoc dramate quo Pentheus
            stolidae pertinaciae et impietatis poenas dat sui saeculi homines hortari uoluit ad
            colendam pietatem : cui neglectae subeunt impietas, temeritas, <foreign xml:lang="grc"
               >αὐθάδεια</foreign> aliaeque eiusdem generis pestes quae respublicas subuertunt.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_11" ana="#TH_fin #RE"> Le poète a donc voulu, avec cette pièce
            où Penthée est puni pour son obstination stupide et son impiété, exhorter les hommes de
            son temps à cultiver la piété : lorsque celle-ci est négligée, elle est remplacée par
            l'impiété, l'insouciance, l'égoïsme et d'autres maladies du même type qui subvertissent
            les États.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_12">Nec id obscure agit, cum dicit <cit
               type="hyperref">
               <quote xml:lang="grc">τὸ σωφρονεῖν δὲ καὶ σέβειν τὰ τῶν θεῶν κάλλιστον οἶμαι</quote>
               <bibl resp="#SG"><author ref="#eur">Eur.</author>, <title ref="#eur_bacch"
                     >Bacch.</title>
                  <biblScope>1150</biblScope>.</bibl>
            </cit>, etc.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_12" ana="#TH_fin #RE"> Et il ne le fait pas de manière
            dissimulée, puisqu'il dit : « Je crois que la meilleure chose est d'avoir un esprit sain
            et modeste et de révérer les dieux et leurs lois, etc. ».</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_13">Item circa finem actus 5.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_13">De même vers la fin de l'acte 5.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_14"><cit type="hyperref">
               <quote xml:lang="grc">
                  <l>εἰ δ’ ἔστιν ὅστις δαιμόνων ὑπερφρονεῖ,</l>
                  <l>εἰς τοῦδ’ ἀθρήσας θάνατον ἡγείσθω θεοὺς, etc.</l>
               </quote>
               <bibl resp="#SG"><author ref="#eur">Eur.</author>, <title ref="#eur_bacch"
                     >Bacch.</title>, <biblScope>1325-1326</biblScope>.</bibl>
            </cit>
         </ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_14" ana="#RE">S'il y a quelqu'un qui méprise les
            dieux, qu'il regarde la mort de cet homme et qu'il croie en eux, etc.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_15">Nam ut <cit type="refauteur">
               <quote>pietas <seg type="incise">(teste <persName>Cicerone</persName>)</seg>
                  fundamentum est omnium uirtutum</quote>
               <bibl resp="#SG"><author resp="#cic">Cic.</author>, <title ref="#cic_planc"
                     >Planc.</title>
                  <biblScope>29</biblScope>.</bibl>
            </cit>, sic interitus religionis radix omnium malorum.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_15" ana="#RE">En effet, de même que la piété (selon
            Cicéron) est la base de toute vertu, la destruction de la religion est la racine de tous
            les maux.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_16">Vnde <persName ref="#plat"
               >Plato</persName>
            <foreign xml:lang="grc">ἐν τῷ ι</foreign>. <title ref="#plat_leg">de Legibus</title>
            docet <seg type="paraphrase">impios, hoc est in Deum ac religionem delinquentes,
               grauissime uindicandos esse eo quod talium hominum scelera saepe totius ciuitatis
               malo expientur et ab his publica corruptela morum ueniat nisi puniantur <bibl
                  resp="#SG"><author ref="#plat">Plat.</author>, <title ref="#plat_leg">Leg.</title>
                  <biblScope>10.907d-912d</biblScope>.</bibl>
            </seg>.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_16" ana="#RE #PHILO"> C'est pourquoi Platon, dans le dixième
            livre des <title>Lois</title>, enseigne que les impies, c'est-à-dire ceux qui offensent
            Dieu et la religion, doivent être punis le plus sévèrement, parce que les crimes de ces
            hommes sont souvent expiés par le malheur de la cité toute entière, et que c'est de ces
            hommes que vient la corruption publique des mœurs, si on ne les punit pas.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_17">Sicut ergo neglectas religiones clades
            sequi solent, ita semper Respublicas amplicatas fuisse eorum imperiis qui religionibus
            paruissent constat.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_17" ana="#RE"> De même donc que les catastrophes ont
            pour habitude de suivre la négligence des prescriptions religieuses, de même il est
            convenu que les États se sont toujours améliorés sous le règne de ceux qui leur ont
            obéi.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_18">Quamobrem uere <persName ref="#plat"
               >Plato</persName> in <title ref="#plat_euthyphr">Euthyphrone</title> : <cit
               type="hyperref">
               <quote xml:lang="grc">τὸ δὲ μέν σοι <seg type="incise">(inquit
                        <persName>Euthyphron</persName>)</seg> ἁπλῶς λέγω ὅτι ἐὰν μὲν κεχαρισμένα
                  τὶς ἐπίστηται τοῖς θεοῖς λέγειν τε καὶ πράττειν εὐχόμενός τε καὶ θύων, ταῦτ’ ἐστὶ
                  τὰ ὅσια, καὶ σώζει τὰ τοιαῦτα τούσδε ἰδίους οἴκους καὶ τὰ κοινὰ τῶν πόλεων· τὰ δ’
                  ἐναντία τῶν κεχαρισμένων ἀσεβῆ, ἃ δὴ καὶ ἀνατρέπει ἅπαντα καὶ ἀπόλλυσι</quote>
               <bibl resp="#SG"><author ref="#plat">Plat.</author>, <title ref="#plat_euthyphr"
                     >Euthyphr.</title>
                  <biblScope>14b</biblScope>.</bibl>
            </cit>.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_18" ana="#RE #PHILO"> C'est pourquoi Platon écrit en toute
            vérité dans <title>Euthyphron</title> : « Je vous dis tout simplement que si quelqu'un
            sait dire et faire des choses agréables aux dieux, en priant et en sacrifiant, c'est
            cela la piété, et un tel comportement préserve sa propre maison et les intérêts communs
            de sa cité. Tout ce qui est à l'opposé des choses agréables est impie, et ces choses en
            fait renversent et détruisent tout. »</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_19">Quod si ad hoc nostrum saeculum
            respiciamus, in quo ea religio uigere debebat quam ipse Dei filius e caelo ad homines et
            tulit et fundauit quaeque sui obseruantes caelestibus et aeternis bonis beat, facile
            intelligemus, quale omnium bonarum rerum diluuium ex huius neglectu ac interitu hodie
            orbem terrarum obruerit, adeo ut publica disciplina extincta sit, iaceat uirtus,
            dissentientium ac disputantium pugnae perpertuae gliscant, denique licentia, clades,
            caedes, periuria, seditiones, euersiones ciuitatum, omnia profanent funestentque.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_19" ana="#RE #HC"> Mais si nous considérons notre propre
            époque, dans laquelle doit prospérer cette religion que le fils de Dieu lui-même a
            portée aux hommes depuis le ciel et établie, et qui bénit ceux qui l'observent par des
            récompenses célestes et éternelles, nous comprendrons aisément quelle sorte de déluge
            destructeur de toutes les bonnes choses a aujourd'hui envahi le monde entier par suite
            de la négligence et de l'abaissement de celle-ci, à tel point que la discipline publique
            a été détruite, que la vertu est prostrée, que les batailles incessantes d'hommes en
            désaccord et en dispute se multiplient, qu’enfin la licence, le malheur, l'effusion de
            sang, les purges, les insurrections, les renversements de cités profanent et souillent
            tout.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_20">Et <seg type="paraphrase"> um tantum
               studium priscis fuerit, non solum obseruandae <seg type="incise">(ut <persName
                     ref="#ual_max">Valerius Maximus</persName> inquit)</seg> sed etiam
               amplificandae religionis eiusque falsae <bibl resp="#SG"><author ref="#ual_max">Val.
                     Max.</author>, <title ref="#ual_max_dfm">Dictorum factorumque memorabilium
                     libri</title>
                  <biblScope>1.1.8</biblScope>.</bibl>
            </seg>, erubescenda certe et piacularis nostra uaecordia est, qui tantis praemiis
            inuitati ueram religionem, sanguine Christi consecratam, diuinitus traditam, uel
            negligimus uel extinctam cupimus, a cuius incolumitate tamen reliquae partes reipublicae
            pendent et conseruantur.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_20" ana="#RE"> Et alors que les Anciens avaient un tel
            zèle, non seulement pour suivre (comme le dit Valère Maxime) mais aussi pour étendre et
            améliorer la religion, et une fausse religion, nous devrions certainement rougir de
            honte de notre propre folie et penser qu'elle nécessite une expiation, nous qui, bien
            que séduits par de si grandes récompenses, négligeons ou voulons voir s'éteindre la
            vraie religion, consacrée par le sang du Christ, transmise du ciel, de la survie de
            laquelle, cependant, le reste de l’État dépend et est préservé.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_21">Cadmi et Tiresiae senum spectata prudentia
            et salutaria consilia sunt : uerum ea contumax repudiat Pentheus.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_21">La sagesse des vieillards Cadmus et
            Tirésias est évidente et leurs conseils salutaires : mais Penthée, obstiné, les
            rejette.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_22">Denique obseruandum figurari in hac fabula
            id quod plerumque mortalibus usuuenit, ut post acceptam cladem sapere et sero paenitere
            incipiant.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_22" ana="#PE">Enfin, il convient d'observer que cette
            pièce montre ce qui arrive à de nombreux mortels : après avoir connu un désastre, ils
            commencent à être raisonnables et se repentent trop tard.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_23">Sic Penthetus iam inter manus
            discerpentium resipescere et agnoscere errorem sero incipit ; sic Agaue iam caeso a se
            filio frustra ingemiscit.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_23"> Ainsi, c’est seulement après qu'il est
            entre les mains de celles qui le mettent en pièces que Penthée commence à reprendre ses
            esprits et reconnaît trop tard son erreur ; ainsi, c’est seulement une fois qu’elle a
            mis son fils en pièces qu’Agavé gémit en vain.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_24">Ita male coepta et temeraria consilia
            efferati iuuenis tristissimus exitus sequitur, funera nimirum et post longam felicitatem
            aerumnosum exilium.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_24">Ainsi, l'issue la plus malheureuse suit
            les mauvais débuts et les décisions irréfléchies du jeune homme sauvage, à savoir la
            mort, et après un long bonheur, un exil plein de misère.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_25">Argumentum actus primi.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_25">Argument de l’acte 1.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_26">Dionysus prologum agens argumentum et
            occasionem praesentis fabulae aperit.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_26">Dionysos, dans le prologue, révèle le
            sujet et la cause de la présente pièce.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_27">Dicit enim se uenisse ut certo aliquo
            specimine diuinitatem suam probet aduersus Pentheum impie orgiis aduersantem.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_27"> Il dit en effet qu'il est venu prouver
            sa propre divinité par des preuves certaines contre Penthée, qui s'oppose impudiquement
            à ses rites.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_28">Deinde Bacchas sacerdotes suas hortatur ad
            sollemnia carmina quae Bacchanalibus cani solebant.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_28">Ensuite il exhorte ses prêtresses, les
            Bacchantes, aux chants rituels qui étaient habituellement chantés lors des
            Bacchanales.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_29">2 Chorus Bacchum celebrat et quasi
            praeludia quaedam ad thiasos futuros increpat.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_29"> 2 Le chœur fait l'éloge de Bacchus et,
            pour ainsi dire, fait retentir avec grandiloquence certains préludes des futures bandes
            bachiques.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_30">Item Bacchi genus, sacra, et eorundem
            apparatum, ut thyrsos, hederas, faces, cymbala, tympana, nebrides, cantus,
            praedicat.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_30">De même, il chante la naissance de
            Bacchus, les rites sacrés et le matériel de ces rites, comme le thyrse, le lierre, les
            torches, les cymbales, les tambours, les fauves et les chants.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_31">3 Senile colloquium est de peragendis
            orgiis.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_31"> 3 La conversation entre les vieillards
            porte sur l'accomplissement des rites.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_32">Cui interuenit Pentheus, ac iratus in
            orgia tamquam in pestiferi cuiusdam Asiatici hominis corruptelas inuehitur, poenasque
            minatur Bacchantibus.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_32">Penthée les interrompt et, dans sa
            colère, s'insurge contre ces rites qu'il considère comme les pratiques corrompues d'un
            Asiatique pernicieux, et il menace de punir les Bacchantes.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_33">4 Tiresias pulcherrima oratione conatur
            reuocare Pentheum ab obstinatione nimia et impietate in Deum, id quod non minore studio
            Cadmus quoque facit.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_33"> 4. Tirésias, dans un très beau
            discours, essaie de rappeler Penthée de son entêtement excessif et de son impiété contre
            la divinité, ce que fait aussi Cadmus avec non moins de zèle.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_34">Sed ille spreto senum prudentissimo
            consilio destinata persequi nihilominus cogitat.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_34">Mais, après avoir rejeté les conseils
            très avisés des vieillards, Penthée entend bien aller jusqu'au bout de ses
            décisions.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_35">Quin ad hoc Tiresiae etiam minatur, et qui
            Dionysum uinctum ad lapidationem ducant dimittit : unde senes desperata iam Penthei
            salute ad orgia pergunt.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_35">De plus, il menace même Tirésias, et il
            renvoie ceux qui doivent ramener Dionysos enchaînés pour être lapidés : d’où les
            vieillards, ayant maintenant désespéré de la sécurité de Penthée, procèdent aux
            rites.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_36">5 Chorus Bacchi laudes uinique effecta
            nobilia commemorat.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_36"> 5. Le chœur chante les louanges de
            Bacchus et les célèbres effets du vin.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_37">Argumentum actus secundi.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_37">Argument de l’acte 2.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_38">Adducunt famuli uinctum Dionysum nec uultu
            mutatum nec restitantem : praeterea mulieres e custodiis euasisse, non sine aliqua
            diuina ope, nuntiant.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_38"> Les serviteurs conduisent Dionysos
            ligoté, dont le visage n'est pas changé et qui n'oppose aucune résistance : de plus, ils
            rapportent que les femmes se sont échappées de leur prison, non sans quelque aide
            divine.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_39">2 Pentheus genus Dionysi et unde, cuiusue
            iussu orgia introduxerit, quaerit.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_39">2 Penthée demande quelle est l'origine
            familiale de Dionysos, d'où il vient ou sous l’ordre de qui il a introduit les
            rites.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_40">In quo colloquio miram Dionysi
            simulationem ac tergiuersationem (nam induerat se in uultus luxuriosi et deliciosi
            cuiusdam adolescentis) ac in excipiendis dictis Penthei acumen obseruandum.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_40">Dans cette conversation, il faut noter
            le jeu étonnant de Dionysos, son subterfuge (car le dieu avait pris l'apparence d'un
            jeune homme extravagant et délicat), et son habileté à parer les paroles de
            Penthée.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_41">Vnde et tyrannus tandem commotus nec
            diutius ferens aenigmatistam eum abduci iubet.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_41"> C'est pourquoi le roi, finalement
            irrité et ne supportant plus ce faiseur d'énigmes, ordonne qu'on l'emmène.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_42">3 Chorus Pentheo Orgiis aduersanti pestem
            ominatur Bacchumque vocat, ubiubi ille tandem sit, ad uindicandum contumeliam et
            impietatem uaecordis tyranni tum in se tum in ipsum numen.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_42"> 3 Le Chorus prédit la ruine à Penthée
            pour s'être opposé aux rites, et appelle Bacchus, où qu'il soit, à venger l'insulte et
            l'impiété du roi insensé, tant contre eux-mêmes que contre la divinité elle-même.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_43">Argumentum actus tertii.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_43">Argument de l’acte 3.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_44">Dionysus e carcere euadens Bacchas uocat
            attonitas stupore, cum omnia ardere, tremere ac collabi uiderentur : eisque exponit
            quomodo praestigiis quibusdam illuserit Pentheo ac miris simulacris rerum insanientem
            distinuerit, ad testificandum numinis sui praesentiam, nisi caecus furore Pentheus
            fuisset ; qui et ipse accurrit, indigne ferens se a Dionyso ludificari.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_44"> Dionysos, s'échappant de la prison,
            appelle les Bacchantes, qui sont stupéfaites de voir que tout semble s'enflammer,
            trembler et tomber en ruines : il leur explique comment il a trompé Penthée par
            certaines ruses et détourné l'aliéné par des images merveilleuses pour démontrer la
            présence de sa propre divinité, si Penthée n'avait pas été aveugle de folie ; et Penthée
            s'enfuit, indigné d'être moqué par Dionysos.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_45">2 Pastor quidam ex Cithaerone adueniens
            longa narratione exponit quibus ceremoniis, quo ritu, uestitu, more, ubi, Bacchae orgia
            celebrent, item quomodo pecudes discerpserint.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_45"> 2 Un berger, arrivant du mont Cithéron,
            explique dans un long récit avec quelles cérémonies, quel rituel, quels vêtements, de
            quelle manière et où les Bacchantes célèbrent leurs rites, et de même de quelle manière
            elles déchirent le bétail en morceaux.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_46">Ex quibus rebus colligit manifesta
            praesentiae numinis argumenta simulque ut Dionysum nouum deum in urbem recipiat tyrannum
            hortatur.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_46">De ces faits, il conclut qu'il existe
            des preuves évidentes de la présence d'une divinité et, en même temps, il demande
            instamment au roi d'accepter Dionysos, le nouveau dieu, dans la ville.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_47">3 Nihil his mouetur Pentheus, sed primum
            arma capere ciues iubet: deinde mutata sententia, ipse prius speculatum abire statuit,
            idque ductore nuntio.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_47"> 3 Tout cela n'émeut en rien Penthée,
            d'abord il ordonne au contraire aux citoyens de prendre les armes, puis, se ravisant, il
            décide de partir d'abord lui-même pour espionner, et de le faire avec le messager comme
            guide.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_48">Dionysi autem uerba habent parasceuen
            sequentis actus, qui continet lacerationem ac flebile fatum Penthei.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_48">Les paroles de Dionysos contiennent
            cependant la préparation de l'acte suivant, qui traite de la mise en pièces et du triste
            destin de Penthée.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_49">4 Chorus optat choreas ac sollemnes
            Orgiorum saltationes.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_49"> 4. Le chœur aspire aux danses et aux
            sauts religieux des rites bachiques.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_50">Deinde subiicit locum communem de uindicta
            deorum in contemptores religionum: quae etsi aliquando tardius uenit, tamen aliquando
            uenit.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_50">Puis il ajoute le lieu commun du
            châtiment des dieux contre ceux qui méprisent la religion, qui, même s'il arrive parfois
            assez tard, arrive néanmoins à un moment donné.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_51">Monet praeterea nihil temere tentandum
            aduersus receptos religionum ritus statasque ceremonias.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_51">En outre, il avertit qu'il ne faut pas
            tenter inconsidérément quelque chose contre les rites admis des religions et les
            cérémonies établies.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_52">Non enim carere eam rem periculo.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_52">Cet acte, en effet, n'est pas sans
            danger.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_53">Denique uitam mediocrem cum pietate
            coniunctam et tranquillo animo omnibus ceteris quibus mundus inhiat felicitatibus
            anteferendam esse dicit.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_53"> Enfin, il dit qu'une vie modérée jointe
            à la piété et à un esprit tranquille doit être préférée à toutes les autres formes de
            bonheur dont le monde est avide.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_54">Argumentum actus quarti.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_54">Argument de l’acte 4.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_55">Dionysus Pentheum iam Baccharum ornatu
            indutum ac mente alienatum foras uocat, spectatorumque ludibrio exponit, ac simul cum eo
            pergit Bacchas exploratum, hoc est, dux illi fit ad manifestum exitium.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_55">Dionysos appelle Penthée, désormais vêtu
            de l'habit des Bacchantes et privé du contrôle de son esprit, et l'expose aux moqueries
            des spectateurs, et en même temps il part avec lui espionner les Bacchantes,
            c'est-à-dire qu'il devient pour Penthée le guide de sa ruine manifeste.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_56">2 Chorus euntem Pentheum prosequitur
            carmine insultatorio, quo uindictam et poenam in hominem impium contemptoremque numinis
            exsuscitat.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_56"> 2 Au moment où Penthée s'en va, le
            Chœur l'attaque par un chant injurieux, par lequel il excite la vengeance et le
            châtiment contre l'homme impie et contempteur de la divinité.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_57">Monet etiam non impunitum manere si quid
            in deorum religionem peccetur ac raro bene cedere iis qui aduersus consuetudines ueteres
            ac priscos longoque usu receptos ritus quid moliantur.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_57">Il avertit aussi qu'on ne reste pas
            impuni si l'on commet quelque péché contre les prérogatives des dieux et qu'il arrive
            rarement du bien à ceux qui luttent contre les antiques coutumes et les anciens rites
            acceptés d’après un long usage.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_58">3 Secunda pars huius actus, quae et ad
            sequentem actum commode referri posset, continet narrationem nuntii de discerpto a
            Bacchis Pentheo : quae locorum, personarum et rerum elegantissimis descriptionibus
            constat, quibus crebra <foreign xml:lang="grc">πάθη καὶ ἤθη</foreign>
            intersperguntur.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_58"> 3 La seconde partie de cet acte, qui
            pourrait être convenablement attribuée aussi à l'acte suivant, contient le récit du
            messager sur Penthée, mis en pièces par les Bacchantes : on y trouve des descriptions
            très élégantes des lieux, des personnages et des événements, auxquelles se mêlent de
            nombreuses expressions de souffrance et de caractère.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_59">4 Chorus uictoriae Bacchi de Pentheo
            gratulatur et applaudit. </ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_59"> 4 Le chœur se réjouit de la victoire de
            Bacchus sur Penthée et l'applaudit.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_60">Argumentum actus quinti.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_60">Argument de l’acte 5.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_61">Vltimus actus summam habet epitasin.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_61">Le dernier acte présente l'épitase.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_62">Nam Agaue adhuc insana in scenam procedit
            iactatque caput Penthei et serio triumphat : ac, quod sibi palmarium putat, leonem a se
            occisum dicit.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_62">En effet, Agavé monte sur scène, encore
            folle, se vante de posséder la tête de Penthée et exulte sincèrement : elle dit qu'elle
            a tué un lion, ce qu'elle pense être un exploit digne d'elle.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_63">Quare omnes ad congratulandum
            hortatur.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_63">Elle exhorte donc tout le monde à la
            féliciter.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_64">2 Cadmus ex monte rediens et collectos
            Penthei artus secum ferens uix ad pristinam mentem tandem reuocat Agauem: quae agnoscens
            filium a se interemptum acerbissimum sentit dolorem.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_64"> 2 Cadmus, revenant de la montagne et
            portant avec lui les membres de Penthée qu’il a ramassés, rappelle enfin avec peine
            Agavé à son état d'esprit habituel, et celle-ci, reconnaissant son fils qu’elle a tué,
            éprouve le plus vif chagrin.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_65">Queritur et Cadmus futuram domus
            solitudinem.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_65"> Cadmus aussi se lamente sur le
            dénuement futur de sa maison.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_66">Dionysus uero dicit poenam esse hanc
            neglecti sui numinis. </ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_66"> Mais Dionysos lui répond que c'est la
            punition pour avoir négligé sa divinité.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_67">Tristis sequitur finis et tragicus.</ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_67">Une triste et tragique fin
            s'ensuit.</ab>
         <ab type="orig" xml:id="Eu1562_Stiblinus_p18_68">Cadmus enim cum suis in exilium mittitur. </ab>
         <ab type="trad" corresp="#Eu1562_Stiblinus_p18_68">En effet, Cadmus est envoyé en exil avec
            sa famille.</ab>

      </body>
   </text>
</TEI>
