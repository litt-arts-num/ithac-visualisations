<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="grc">ΦΕΔΕΡΙΚΟΥ ΜΟΡΕΛΛΟΥ ΕΙΣ ΕΙΡΗΝΗΝ δίγλωττον</title>

                <author ref="#morellus_federicus">Federicus Morellus </author>

                <respStmt>
                    <resp>Transcription</resp>
                    <name>Mendel Leandro JIMÉNEZ</name>
                </respStmt>

                <respStmt>
                    <resp>Traduction</resp>
                    <name>Malika BASTIN-HAMMOU</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Mendel Leandro JIMÉNEZ</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>

                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <principal>Malika BASTIN-HAMMOU</principal>
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne Garcia-Fernandez</persName>
                </respStmt>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <sourceDesc>
                <bibl>
                    <title>Q. Septimii Florentis Christiani in Aristophanis Irenam vel Pacem
                        Commentatia Glossemata : Ubi aliquot veterum Grammaticorum aliorumque
                        auctorum loci aut correcti aut animaduersi. Cum Latina Graeci Dramatis
                        Interpretatione Latinorum Comicorum stylum imitata, et eodem genere uersuum
                        cum Graecis conscripta. Βασιλει τ’ ἀγαθῷ κρατερῷ τ’ αἰχμητῇ. Lutetiae, apud
                        Federicum Morellum Typographum Regium, uico Bellouavo ad urbanam Morum,
                        M.D.LXXXIX. </title>
                    <!-- l'attribut @level permet d'indiquer s'il s'agit d'une monographie (m), d'un article ou chapitre d'ouvrage (a) ou d'un périodique (j) -->
                    <author ref="#ar">Aristophanes</author>
                    <pubPlace ref="#lutetia">Lutetia</pubPlace>
                    <publisher ref="#morellus_federicus">Apud Federicum Morellum Typographum Regium
                        vico Bellouaco ad urbanam Morum.</publisher>
                    <date when="1589">1589</date>
                    <editor role="traducteur" ref="#christianus_florens">Christianus
                        Florens</editor>
                    <ref
                        target="https://books.google.fr/books?id=OW4TAAAAQAAJ&amp;pg=PP6#v=onepage&amp;q&amp;f=false"/>
                    <!-- lien vers l'édition numérique du texte si elle existe -->
                </bibl>

            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>« Fédéric Morel est le fils de Fédéric Morel (1523-1583), dit l'Ancien, imprimeur
                    à Paris notamment des œuvres de Joachim Du Bellay. Il est également le gendre de
                    Michel de Vascosan. Il fut imprimeur du roi pour le grec à partir de 1581. En
                    1579, il est nommé imprimeur ordinaire du roi. Il a publié les premières
                    éditions des traités d'architecture de Philibert De l'Orme. » Source : <ref
                        target="https://fr.wikipedia.org/wiki/F%C3%A9d%C3%A9ric_Morel_(1552-1630)"/>
                </p>
            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

            <textClass n="vers"></textClass>

        </profileDesc>
        <revisionDesc>
            <change when="2022-11-08">Malika BASTIN-HAMMOU : encodage sémantique</change>
            <change when="2022-03-03">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
        </revisionDesc>
    </teiHeader>

    <text xml:lang="grc">
        <body>

            <head type="orig" xml:id="Ar1589_Morellus_p1_0"><persName ref="#morellus_federicus"
                    >ΦΕΔΕΡΙΚΟΥ ΜΟΡΕΛΛΟΥ</persName> ΕΙΣ <title ref="#ar_pax">ΕΙΡΗΝΗΝ</title>
                δίγλωττον. </head>
            <head type="trad" corresp="#Ar1589_Morellus_p1_0">Poème bilingue de Fédéric Morel sur la
                    <title>Paix</title>
            </head>

            <ab type="orig" xml:id="Ar1589_Morellus_p1_1">
                <l>ΔΡᾶμα τόδ᾽ ἱππεύσαντος ἐς οὐρανὸν ἀμπελοέργου </l>
                <l>Εἰρήνην δ᾽ εὑρόντος ἐνὶ σπεσσὶ γλαφυρῇσιν, </l>
                <l>Ὅνπερ <persName ref="#ar">Αριστοφάνης </persName>ἀμὰ Ἑλλάδα θῆκε πρόφαντον,</l>
                <l>Καὶ Φλωρέντιος Αὐσονίδην δι᾽ ἐξ Ἑλλάδος ἦρε</l>
                <l><persName ref="#christianus_florens">Χριστιανὸς</persName> Μούσης τροφὸς Ατθίδος
                    ἠδὲ Λατίνης,</l>
                <l>Εἰρήνην ποθέων χαρίτων Μουσῶν τε Ἀθήνην·</l>
            </ab>
            <ab type="trad" corresp="#Ar1589_Morellus_p1_1" ana="#A #T"><l>C'est l'histoire d'un vigneron qui
                    chevauche vers le ciel</l><l>Et qui trouve la Paix dans une caverne
                    creuse,</l><l>Et qu'Aristophane a placé dans la célèbre Grèce,</l><l>Et Florent
                    Chrétien l'a sortie de Grèce pour la rendre italienne,</l><l>Nourriture d'une
                    Muse Attique ou Latine,</l><l>Désirant la Paix des Muses et des charites et Athèna </l></ab>

            <ab type="orig" xml:id="Ar1589_Morellus_p1_2">
                <l>Ἧς τινος εὐσεβέες μάλ᾽ ἐπαυρίσκονται ἃπαντες. </l>
            </ab>
            <ab type="trad" corresp="#Ar1589_Morellus_p1_2" ana="#RE"> Elle dont tous ceux qui sont pieux
                jouissent le plus</ab>

            <ab type="orig" xml:id="Ar1589_Morellus_p1_3">
                <l>Δεῦρ᾽ ἴθι νύμφα φίλη πολέων σώτειρα βροτων τε </l>
                <l>Οὐκ ὄναρ, ἀλλ᾽ ὕπαρ, οὐκ ἐν δράματι αὐτὰρ ἐπ᾽ ἔργῳ,</l>
                <l>Κελτῶν μειλίσσουσα νόον θεὰ θυμολεόντων.</l>
            </ab>
            <ab type="trad" corresp="#Ar1589_Morellus_p1_3" ana="#HC"><l>Viens ici, nymphe chérie, toi qui
                    sauves les villes et les mortels,</l><l>Tu n'es pas un rêve, mais une vision,
                    non dans le drame mais en réalité,</l><l>Toi la déesse qui calme l'esprit des
                    Celtes au cœur de lion.</l></ab>

        </body>
    </text>
</TEI>
