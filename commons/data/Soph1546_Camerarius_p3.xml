<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">


    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">In Antigonen Sophoclis Argumentum Ioachimi Camerarii
                    Pabenbergensis</title>
                <!-- Attention ! ne pas faire de retour charriot dans le titre -->
                <author ref="#camerarius_ioachimus">ioachimus Camerarius</author>

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Tâche n°1 -->
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Marius MASSUCCO</name>
                </respStmt>
                <!-- Tâche n°2 (à compléter après la traduction)-->
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Diandra CRISTACHE</name>
                </respStmt>
                <!-- Tâche n°3 -->
                <respStmt>
                    <resp>Révision</resp>
                    <name>Pascale PARE-REY</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Marius MASSUCCO</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Commentaire</resp>
                    <name>Malika Bastin-Hammou</name>
                    <name>Diandra Christache</name>
                </respStmt>
                <!-- Ajouter autant de tâches que nécessaire -->
                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet : ne pas y toucher !-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
            <sourceDesc>

                <bibl>
                    <!-- l'attribut @level permet d'indiquer s'il s'agit d'une monographie (m), d'un article ou chapitre d'ouvrage (a) ou d'un périodique (j) -->
                    <title>Interpretatio Tragoediarum Sophoclis, ad utilitatem iuuentutis, quae
                        studiosa est graecae linguae, edita a Vito Winschemio.</title>
                    <author ref="#soph">Sophocles</author>
                    <pubPlace ref="#francofurtum
                        ">Francoforti ad
                        Moenum</pubPlace>
                    <publisher ref="#brubachius_petrus">Petrus Brubachius</publisher>
                    <date when="1546">1546</date>
                    <editor ref="#winshemius_uitus
                        ">Vitus
                        Winschemius</editor>
                </bibl>
                <listBibl>
                    <bibl/>
                </listBibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Type d'édition : texte grec seul Court traité de Camerarius sur les poètes
                    dramatiques grecs. Camerarius : Joachim Camerarius (Bamberg, 1500 — Leipzig,
                    1574), dit « l’ancien » (der Ältere). Son père était trésorier épiscopal et
                    conseiller à Bamberg. Le nom de « Camerarius » témoigne d’ancêtres ayant exercé
                    de hautes fonctions au sein du Saint-Empire (Magister Camerae). Camerarius est
                    élève à Bamberg, avant de recevoir l’enseignement de Georg Helt (Georgius
                    Heltus, théologien protestant) à Leipzig dès 1512. En 1516, il étudie le grec
                    avec Petrus Mosellanus. En 1518 il étudie à l’université d’Erfurt, puis à celle
                    de Wittenberg en 1521 où il devient l’ami de Philipp Melanchthon. En 1522 il y
                    obtient la chaire de Zoologie, puis en 1525 il y devient professeur de langue et
                    littérature grecque. En 1526, il enseigne à l’Aegidianum (« Engydiengymnasium »
                    en allemand) fondée à Nuremberg la même année et que fréquentent de grandes
                    figures de la Réforme. En 1532 il traduit plusieurs écrits de Dürer, puis il
                    prend part à la réorganisation de l’université de Tübingen avant d’enseigner à
                    l’université de Leipzig de 1541 jusqu’à sa mort. On lui doit la publication de
                    nombreux auteurs grecs et latins : Démosthène, Esope, Hérodote, Homère,
                    Quintilien, Sophocle, Théocrite, Théophraste, Xénophon, Thucydide, et des
                    commentaires sur Cicéron et César. Parmi les enfants qu’il eut avec sa femme
                    Anna Truchsess von Grünberg, Joachim Camerarius (1534-1598), dit « le jeune »
                    s’est illustré comme botaniste et Philipp Camerarius (1537-1624) comme
                    historien. [source : wikipédia:
                    https://de.wikipedia.org/wiki/Joachim_Camerarius_der_%C3%84ltere] Winshemius :
                    Vitus Vuinshemius (Veit Winsheim). (Windsheim 1501 — Wittenberg 1570), est un
                    philologue, helléniste et médecin allemand, diplômé d’une maîtrise en
                    philosophie à la faculté de Wittenberg en 1529. Il est proche de Mélanchthon,
                    auquel il succède dans sa chaire de langue grecque en 1541. Docteur en médecine
                    en 1550, puis successivement doyen de la faculté de philologie, recteur de
                    l’université de Wittenberg, et vice-recteur de l’Académie. Il est aussi
                    conseiller de la ville de Wittenberg. Il a laissé de nombreuses études sur les
                    textes antiques, notamment sur Démosthène, Thucydide, Théocrite, Homère,
                    Sophocle et Euripide, ainsi que des discours et éloges funèbres (notamment celui
                    de Melanchthon). Son fils s’appelle aussi Veit Winsheim, dit « le jeune »(der
                    Jüngere), professeur de droit. [source : wikipédia :
                    https://de.wikipedia.org/wiki/Veit_Winsheim] </p>
            </abstract>


            <langUsage>
                <!-- Garder seulement les éléments indiquant les langues utilisées dans le paratexte -->
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>

        <revisionDesc>
            <!-- Ajouter ici autant d'éléments <change> que de modifications effectuées -->
            <change when="2022-02-16">Sarah GAUCHER : Vérification et ajouts PersName, PlaceName,
                hi, title, quote, seg, foreign. Vérification de la mise en conformité du
                schéma</change>
            <change when="2022-02-14">Sarah GAUCHER : reprise de l'intégralité de la TEI,
                harmonisation, reprise de la trasncription et de la traduction</change>
            <change when="2021-03-29">Diandra CRISTACHE : Mise à jour et chargement sur le
                Pensoir</change>
            <change when="2021-02-05">Diandra CRISTACHE : encodage de la transcription</change>
        </revisionDesc>

    </teiHeader>


    <text>
        <body ana="#TH_dram">

            <head type="orig" xml:id="Soph1546_Camerarius_p3_0">In <title ref="#soph_ant"
                    >Antigonen</title>
                <persName ref="#soph">Sophoclis</persName> Argumentum <persName
                    ref="#camerarius_ioachimus">Ioachimi Camerarii</persName> Pabenbergensis.</head>
            <head type="trad" corresp="#Soph1546_Camerarius_p3_0"><title>Antigone</title> de
                Sophocle : argument par Joachim Camerarius, natif de Bamberg.</head>

            <ab type="orig" xml:id="Soph1546_Camerarius_p3_1">Iam ceciderant mutuis uulneribus
                fratres Antigonae, Œdipi filii.</ab>
            <ab type="trad" corresp="#Soph1546_Camerarius_p3_1">Les frères d’Antigone, fils d’Œdipe,
                avaient déjà succombé de leurs blessures mutuelles.</ab>
            <ab type="orig" xml:id="Soph1546_Camerarius_p3_2">At Creo, qui occupasset regnum
                Thebanum, siue quia noua potestate superbiret, siue quia receperat Eteocli, abiici
                iubet Polynicis cadauer et humantibus aut aliquo honore exequiarum afficientibus,
                poenam proponit mortem ; quibus contemptis Antigone audet noctu clam sepelire
                fratrem.</ab>
            <ab type="trad" corresp="#Soph1546_Camerarius_p3_2">Mais Créon, qui s’était emparé du
                trône de Thèbes, soit parce qu’il s’était enorgueilli de son nouveau pouvoir, soit
                parce qu’il avait permis qu’on enterre Étéocle, ordonne que le cadavre de Polynice
                soit abandonné et impose la peine de mort pour ceux qui voudraient l’enterrer ou lui
                assurer quelque honneur funèbre ; mais Antigone méprise ces ordres et ose enterrer
                son frère de nuit, en cachette.</ab>
            <ab type="orig" xml:id="Soph1546_Camerarius_p3_3">Quae re cognita Creo grauiter custodes
                increpat, iubetque, si uitam retinere cupiant, exquirere autorem sepulturae
                Polynicis.</ab>
            <ab type="trad" corresp="#Soph1546_Camerarius_p3_3">Après l’avoir appris, Créon
                incrimine lourdement ceux qui gardaient le cadavre et leur ordonne, s’ils tiennent à
                rester en vie, de rechercher le responsable la sépulture de Polynice.</ab>
            <ab type="orig" xml:id="Soph1546_Camerarius_p3_4">Illi igitur disiecto tumulo, quem
                cadaueri ingesserat uirgo, diligentiori illud cura asseruant.</ab>
            <ab type="trad" corresp="#Soph1546_Camerarius_p3_4">Par conséquent, eux, après avoir
                détruit le tombeau où la jeune fille avait déposé le cadavre le surveillent avec
                plus de soin.</ab>
            <ab type="orig" xml:id="Soph1546_Camerarius_p3_5">Ea uero ut noctu uisit ad fratris a se
                structum monumentum, illudque dissipatum, et nudum iacere cadauer conspexit, luctu
                et eiulatu suo ipsa se indicat.</ab>
            <ab type="trad" corresp="#Soph1546_Camerarius_p3_5">Quant à elle, quand elle vint durant
                la nuit voir le tombeau et s’aperçut qu’il avait été détruit et que le corps gisait
                nu, elle trahit sa présence par sa douleur et son gémissement.</ab>
            <ab type="orig" xml:id="Soph1546_Camerarius_p3_6">Custodes ergo arreptam deducunt ad
                regem ; is ira et indignatione impos animi, damnatam iubet uacuo busto includi, ubi
                inedia contabesceret ; quam mortis calamitatem ipsa anteuertit suspendio.</ab>
            <ab type="trad" corresp="#Soph1546_Camerarius_p3_6">Les gardiens, par conséquent,
                l’amènent au roi après l’avoir capturée ; lui, saisi par la colère et l’indignation,
                ordonne que la condamnée soit enfermée dans un tombeau vide pour y mourir de faim ;
                mais elle prend elle-même les devants de sa mort en se pendant.</ab>
            <ab type="orig" xml:id="Soph1546_Camerarius_p3_7">Erat autem desponsa Haemoni Creonti
                filio, qui cum conatus patrem ab illa peruersitate deducere, nihil proficeret, ipse
                ad bustum, in quod deducta uirgo laqueo finiisset uitam, sese iugulat.</ab>
            <ab type="trad" corresp="#Soph1546_Camerarius_p3_7">Or, elle était l’épouse d’Hémon, le
                fils de Créon, puisque, alors qu’il s’était efforcé de détourner son père de cet
                acte cruel, n’y parvenait pas , se rendit au tombeau où la jeune fille, après y
                avoir été menée, avait mis fin à ses jours en se pendant, et se tranche lui-même la
                gorge.</ab>
            <ab type="orig" xml:id="Soph1546_Camerarius_p3_8">Sero igitur Creo mutat animi sui
                inhumanum consilium, maxime repraehensus a Tiresia, et conseruaturus Antigonen, pro
                uno geminum luctum inuenit. </ab>
            <ab type="trad" corresp="#Soph1546_Camerarius_p3_8">Accablé de lourds reproches de
                Tirésias, Créon renonce donc trop tard à sa résolution inhumaine, et alors qu’il
                s’apprête à gracier Antigone, il fait face à deux deuils au lieu d’un.</ab>
            <ab type="orig" xml:id="Soph1546_Camerarius_p3_9">His accedit et Eurydicae mors, quae
                nupta esset cum Creonte: cognito enim filii interitu, doloris impatiens et ipsa sese
                interemit.</ab>
            <ab type="trad" corresp="#Soph1546_Camerarius_p3_9">À ces morts s’ajoute aussi celle
                d’Eurydice qui s’était unie à Créon ; car, ayant appris la mort de son fils et ne
                pouvant supporter la douleur, elle mit fin elle-même à ses jours.</ab>
            <ab type="orig" xml:id="Soph1546_Camerarius_p3_10">Ita multiplicium lacrimarum causa sit
                animus impotens Creontis.</ab>
            <ab type="trad" corresp="#Soph1546_Camerarius_p3_10">Ainsi l’esprit emporté de Créon
                serait la cause de ses multiples douleurs.</ab>
            <ab type="orig" xml:id="Soph1546_Camerarius_p3_11">Hanc fabulam etiam <persName
                    ref="#eur">Euripidem</persName> tradunt edidisse, sed aliter ; nam apud illum
                laetiorem finem esse, collocata Antigona in matrimonium Haemoni.</ab>
            <ab type="trad" corresp="#Soph1546_Camerarius_p3_11" ana="#TH_hist">On rapporte
                qu’Euripide a également fait jouer cette histoire, mais de manière différente ;
                qu’en effet, chez lui, le final est plus gai, puisque Antigone est donnée en mariage
                à Hémon.</ab>
            <ab type="orig" xml:id="Soph1546_Camerarius_p3_12">Sed <persName ref="#soph"
                    >Sophocleam</persName> ita ferunt placuisse, ut poetae praefectura Sami praemium
                illius decerneretur. </ab>
            <ab type="trad" corresp="#Soph1546_Camerarius_p3_12" ana="#TH_hist">Mais on dit que la
                pièce de Sophocle a tellement plu que le prix de la préfecture de Samos lui a été
                décerné.</ab>
            <ab type="orig" xml:id="Soph1546_Camerarius_p3_13">Res quasi Thebis in Boeotia geritur ;
                chorus est senum Thebanorum.</ab>
            <ab type="trad" corresp="#Soph1546_Camerarius_p3_13">L’action est située dans les
                environs de Thèbes, en Béotie ; le chœur est composé de vieillards thébains.</ab>


        </body>

    </text>
</TEI>
