<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Q. Septimus Florens Christianus V. C. Jacobo Augusto Thuano,
                    Aemerio, Christophori Senatus quondam Principis filio, S.P.D.</title>
                <author ref="#christianus_florens">Christianus Florens</author>
                <respStmt>
                    <resp>Transcription</resp>
                    <name>Mendel Leandro JIMÉNEZ</name>
                </respStmt>

                <respStmt>
                    <resp>Traduction</resp>
                    <name>Christian NICOLAS</name>
                </respStmt>
                <respStmt>
                    <resp>Révision</resp>
                    <name>Malika BASTIN-HAMMOU</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Diandra CRISTACHE</name>
                    <name>Sarah GAUCHER</name>
                    <name>Malika BASTIN-HAMMOU</name>
                </respStmt>

                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <principal>Malika BASTIN-HAMMOU</principal>
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne Garcia-Fernandez</persName>
                </respStmt>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <sourceDesc>
                <bibl>
                    <title>Q. Septimii Florentis Christiani in Aristophanis Irenam vel Pacem
                        Commentatia Glossemata : Ubi aliquot veterum Grammaticorum aliorumque
                        auctorum loci aut correcti aut animaduersi. Cum Latina Graeci Dramatis
                        Interpretatione Latinorum Comicorum stylum imitata, et eodem genere uersuum
                        cum Graecis conscripta. Βασιλει τ’ ἀγαθῷ κρατερῷ τ’ αἰχμητῇ. Lutetiae, apud
                        Federicum Morellum Typographum Regium, uico Bellouavo ad urbanam Morum,
                        M.D.LXXXIX. </title>
                    <!-- l'attribut @level permet d'indiquer s'il s'agit d'une monographie (m), d'un article ou chapitre d'ouvrage (a) ou d'un périodique (j) -->
                    <author ref="#ar">Aristophanes</author>
                    <pubPlace ref="#lutetia">Lutetia</pubPlace>
                    <publisher ref="#morellus_federicus">Apud Federicum Morellum Typographum Regium
                        vico Bellouaco ad urbanam Morum.</publisher>
                    <date when="1589">1589</date>
                    <editor role="traducteur" ref="#christianus_florens">Christianus
                        Florens</editor>
                    <ref
                        target="https://books.google.fr/books?id=OW4TAAAAQAAJ&amp;pg=PP6#v=onepage&amp;q&amp;f=false"/>
                    <!-- lien vers l'édition numérique du texte si elle existe -->
                </bibl>
                <listBibl>
                    <bibl>Buzon, Christine de, et Jean-Eudes Girot. Jean Dorat: poète humaniste de
                        la Renaissance : actes du colloque international (Limoges, 6-8 juin 2001).
                        Droz, 2007.</bibl>
                    <bibl>Buzon, Christine de, et Jean-Eudes Girot. Jean Dorat: poète humaniste de
                        la Renaissance : actes du colloque international (Limoges, 6-8 juin 2001).
                        Droz, 2007. Cazes, Hélène, « Chrestien (Florent) (1541-1596) », dans C.
                        Nativel (dir.), Centuriae Latinae. Cent une figures humanistes de la
                        Renaissance aux Lumières, Droz, 2006, 211-221. </bibl>
                </listBibl>
            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Florent Chrestien (1541-1596), humaniste français, poète et professeur de grec,
                    protestant, précepteur de Henri de Navarre, ami des plus grands savants du
                    royaume. Il apprit le grec auprès d’Henri Estienne, fut ami de Robert Estienne,
                    fréquenta Casaubon, Buchanan, Dorat, Joseph Scaliger, Grévin… La part qu’il a
                    prise à la polémique contre Ronsard en 1563 (sous le pseudonyme de La Baronie) a
                    occulté son œuvre philologique et poétique très importante.</p>
                <p>Jacques Auguste de Thou (1553-1617), magistrat, historien, écrivain, bibliophile
                    et homme politique français. Fils de Christophe de Thou (1508-1582), premier
                    président du parlement de Paris.</p>
            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>
        <revisionDesc>
            <change when="2022-11-08">Malika BASTIN-HAMMOU : encodage sémantique</change>
            <change when="2022-03-03">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
        </revisionDesc>
    </teiHeader>

    <text>
        <body>

            <!-- Le couple d'éléments <head> contient le titre ou l'incipit du paratexte -->
            <!-- L'identifiant du TITRE est composé du titre de la fiche TT suivi d'un underscore (_) puis du chiffre 0 -->
            <head type="orig" xml:id="Ar1589_Christianus_p1_0"><persName ref="#christianus_florens"
                    >Q. Septimius Florens Christianus</persName> Viro Clarissimo <persName
                    ref="#thuanus_iacobus" role="destinataire">Iacobo Augusto Thuano</persName>,
                Aemerio, <persName>Christophori</persName> Senatus quondam Principis filio,
                S.P.D.</head>
            <head corresp="Ar1589_Christianus_p1_0" type="trad"><seg>Q. Septimius Florens
                        Christianus<note>Sur ce nom, voir Adrien Baillet, <title>Déguisemens des
                            auteurs</title> ll, 2, in: <title>Jugemens des sauans sur les principaux
                            ouurages des auteurs... revus et corrigés par M. de La Monnoye</title>,
                        Amsterdam, 1725; Hildesheim / New York, 1971. T. 5, p. 178, cité par Cazes
                        2008, p. 214 : « Florent Chretien d’Orléans, autrefois précepteur du Roi
                        Henri le Grand et son Bibliothécaire à Vendome. Cet auteur pour tâcher de se
                        rendre plus semblable aux Anciens, se fit appeler Quintus Septimius Florens
                        Christianus. Il prit le nom de Quintus, parce qu’il étoit le cinquième des
                        Enfants de ses père et mère ; et celui de Septimius parce qu’il étoit né au
                        septième mois de la grossesse de sa mère. Néanmoins on peut remarquer à son
                        avantage que sa passion pour l’Antiquité semble n’avoir eu rien de profane,
                        non-seulement parce qu’il a eu soin de conserver son surnom de Christianus,
                        mais encore parce qu’il a pu se proposer, pour l’exemple des autres noms, un
                        célèbre Auteur de l’<title>Antiquité Ecclésiastique</title>. Car vous pouvez
                        vous souvenir, Monsieur, que Tertullien s’appeloit aussi Quintus Septimius
                        Florens ».</note></seg> à l’illustre Jacques-Auguste de Thou, fils de feu
                Christophe de Thou, Premier Président du Parlement, donne son salut appuyé.</head>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_1">Nemo est homo, <persName
                    ref="#thuanus_iacobus">Iacobe Auguste</persName>, quem non aliqua gloriae et
                laudis cupiditas tangat, ac ne illi quidem qui de gloria contemnenda scripserunt, ab
                eius amore abhorruisse uidentur.</ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_1" ana="#PA">Il n’y a pas un homme,
                Jacques-Auguste, qui ne soit pas tant soit peu touché par le désir de gloire et
                d’éloge ; même ceux qui ont écrit sur le mépris de la gloire ne semblent pas
                épargnés par cette affection-là.</ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_2">Quo factum est ut et praeclara domi
                militiaeque fortium uirorum facinora, et docta hodie scripta eorum qui illa litteris
                prodiderunt, legamus, amemus, admiremur.</ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_2" ana="#PA">Il en résulte que tant les
                hauts faits civils et militaires des grands héros que les livres savants de nos
                contemporains qui les ont transmis par écrit, nous les lisons, les aimons, les
                admirons.</ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_3">Nam uirtutes rerum gerendarum et
                scribendarum pariter aestimandas duco, nedum dissociandas ; neque is ego sum qui
                    <persName ref="#hom">Homerum</persName> tanti non aestimem quanti Achillem, <seg
                    type="allusion">cum sciam a celata uirtute parum distare sepultam inertiam<bibl
                        resp="#CN"><author ref="#hor">Hor.</author>, <title ref="#hor_o">O.</title>
                        <biblScope>4.9.29-30</biblScope> : <note><foreign xml:lang="la">paulum
                                sepultae distat inertiae / celata uirtus</foreign>, « faible est la
                            distance entre l’inertie enterrée et la vertu tenue cachée
                        ».</note></bibl></seg>, et recte <persName>Democritus</persName> censuerit
                    <cit type="refauteur">
                    <quote xml:lang="grc">λόγον <foreign xml:lang="la">esse</foreign> ἔργου
                        σκιάν</quote>
                    <bibl resp="#CN" type="correctionattribution"><author ref="#phalar"
                            >Phalar.</author>, <title ref="#phalar_ep">Ep.</title>
                        <biblScope>35.1 (Hercher)</biblScope>. <note>Cette formule est en réalité
                            prêtée à Phalaris par Stobée, <title>Anth.</title> 2.15.42 ; à beaucoup
                            de monde par Themistius (Harduin p. 200 d, ligne 5) : <foreign
                                xml:lang="grc">λέγεται μὲν οὖν παρὰ πολλῶν ὡς λόγος ἐστὶν ἔργου
                                σκιὰ</foreign>, « on lit chez beaucoup que la parole est l’ombre de
                            l’acte », et une scolie à Aelius Aristide (122.6.3) en fait un proverbe.
                            La formule ne se trouve néanmoins pas dans les <title>Adages</title>
                            d’Érasme.</note></bibl>
                </cit> : sane enim non aliis temere uirtus egregia aedificatur aut consistit
                munimentis, quam litterarum et doctrinae monimentis, neque immerito <seg
                    type="paraphrase"><persName ref="#ual_max">Valerius Maximus</persName> ad laudem
                        <persName ref="#cic">Ciceronis</persName> dixit plus meruisse qui Romani
                    eloquii fines promouit, quam qui imperii.<bibl resp="#SG" type="nontrouve"
                    /></seg></ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_3" ana="#PA">Car les qualités pour faire
                l’histoire et pour l’écrire sont également estimables, selon moi, et ne doivent pas
                être dissociées ; et je ne suis pas homme à donner moins de valeur à Homère qu’à
                Achille, vu que je sais la faible distance qu’il y a de la vertu qu’on tait à
                l’inertie enterrée et je donne raison à Démocrite de penser que « la parole est
                l’ombre de l’acte » : car il n’y a pas de meilleurs instruments pour construire ou
                mettre en forme une splendide vertu que les archives des lettres et de la science et
                c’est à bon droit que Valère-Maxime a dit, pour honorer Cicéron, que le mérite était
                plus grand chez celui qui avait repoussé les limites de l’éloquence romaine que
                celle de l’empire.</ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_4">Sed iamdudum uitio maloque fato humanae
                gentis factum est, cum hominis uirtus diuisa est in uires corporis et ingenii, et
                breuis fuit felicitas illius saeculi quo <seg type="paraphrase">uirtutes<seg
                        type="incise">, ut ait <persName ref="#tac">Tacitus</persName>,</seg> facile
                    gignebantur, et ab ipsis auctoribus bonae tantum conscientiae pretio ductis
                    scriptae cum fide et sine obtrectatione optime aestimabantur.<bibl resp="#CN"
                            ><author ref="#tac">Tac.</author>, <title ref="#tac_agr">Agr.</title>
                        <biblScope>1.2</biblScope>. <note>Réminiscence libre : <foreign
                                xml:lang="la">Sed apud priores ut agere digna memoratu pronum
                                magisque in aperto erat, ita celeberrimus quisque ingenio ad
                                prodendam uirtutis memoriam sine gratia aut ambitione bonae tantum
                                conscientiae pretio ducebatur</foreign>, « Mais chez les Anciens,
                            autant pour accomplir des exploits il y avait de la spontanéité et
                            davantage d’occasions, autant les meilleurs talents étaient poussés à
                            promouvoir la mémoire de ces hauts faits sans en attendre de
                            reconnaissance, sans rien briguer, pour le seul prix de la bonne
                            conscience ».</note></bibl></seg></ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_4" ana="#PA">Mais depuis longtemps cela
                s’est tourné en défaut et en malheur pour l’espèce humaine, quand la vertu de
                l’homme a été divisée en qualités physiques et morales et bien court fut le bonheur
                de ce siècle où les vertus, comme dit Tacite, naissaient facilement et où des
                auteurs mus seulement par l’appât de la bonne conscience les décrivaient avec
                loyauté et sans jalousie pour les valoriser.</ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_5">Nunc eo deuenimus, ut et litteras scire
                milites metuant, et litterati homines arma tractare nesciant, uel si sciant non
                perinde existimentur.</ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_5" ana="#PA">Aujourd’hui nous en sommes
                au point où les soldats ont peur de savoir lire et où les lettrés ignorent le
                maniement des armes ou, s’ils le savent, n’en sont pas pour autant valorisés.</ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_6">Et tamen quicquid aut illi gerunt aut
                isti scribunt pulchrae laudis amore incensi faciunt et famae quaerendae studio, nisi
                penitus ab iis exsulatum iuit animi nobilitas.</ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_6" ana="#PA">Et pourtant, les exploits
                pour les premiers, les œuvres pour les seconds, ils les font poussés par l’amour
                d’une belle gloire et dans l’idée d’acquérir la célébrité, sauf si toute noblesse
                d’âme s’est complètement exilée de chez eux.</ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_7">In ista gente nunquam me puduit nomen
                profiteri meum et quotidie magis magisque adducor ut in litterario exercitu laudis
                stipendia meream. </ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_7" ana="#PA">C’est à cette engeance que
                jamais je n’ai eu honte de dire que j’appartiens et chaque jour davantage je
                travaille à mériter dans cette armée des lettrés ma solde de louanges.</ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_8">Et poteram huic gloriae idoneus neque
                impar uideri, nisi artibus illis utilibus magis quam nobilibus, quas <seg
                    type="reference" xml:lang="grc">χρηματιστικὴν<bibl resp="#CN"><author
                            ref="#arstt">Arstt.</author>, <title ref="#arstt_pol">Pol.</title>
                        <biblScope>1256a4-1258a37, passim.</biblScope></bibl></seg> et <seg
                    type="reference" xml:lang="grc">ποριστικὴν<bibl resp="#CN"><author ref="#arstt"
                            >Arstt.</author>, <title ref="#arstt_rhet">Rhet.</title>
                        <biblScope>1366a35</biblScope> : <note><foreign xml:lang="grc">ἀρετὴ δ’ ἐστὶ
                                μὲν δύναμις ὡς δοκεῖ ποριστικὴ ἀγαθῶν καὶ
                            φυλακτική</foreign>.</note></bibl></seg> uocat <persName ref="#arstt"
                    >Aristoteles</persName>, exerceri me uoluisset Deus, et ad rei familiaris
                auctionem attendere iam <foreign xml:lang="grc">μεσήλικα</foreign>, et fortassis
                maturius annis in hoc rerum aestu occasurum.</ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_8">Et j’aurais pu, cette gloire, y
                sembler légitime et en prendre ma part si ce n’avait pas été vers ces arts plus
                utiles que nobles, qu’Aristote nomme arts de la finance et du commerce, que Dieu
                avait voulu que je me tourne et que je travaille à fructifier mon bien, pour ne
                tomber <seg>qu’au milieu de ma vie<note>En grec dans le texte.</note></seg> et
                peut-être trop âgé <seg>dans cette activité bouillonnante<note>Difficile de dire à
                        quelles activités commerciales Chrestien fait allusion. Il semble avoir
                        plutôt toujours été impliqué dans l’édition, la philologie, la traduction
                        et, globalement, la pratique intellectuelle. Une preuve supplémentaire que «
                        Chrestien lui-même égare ses biographes et ses lecteurs » (Cazes, 2006, p.
                        214) ?</note></seg>.</ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_9">Quamuis in mea ipse commoda nihilominus
                pecco saepe, praeuortente amore litterarum et sane siue naturae meae genium secutus,
                siue quo melioris saeculi moras fallam, nihil est quod libentius amplectar quam
                studia litterarum, nihil quod potius aemuler quam uiros doctrinae claritate sine
                ambitione conspicuos, et qui tibi maxime, Auguste <persName ref="#thuanus_iacobus"
                    >Thuane</persName>, sint similes.</ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_9" ana="#PA">Même si je ne manque pas de
                fauter souvent contre mes propres intérêts, l’amour des lettres l’emportant, et tant
                mieux, soit que j’aie suivi mon inclination naturelle soit que je trompe les
                attentes d’une génération meilleure, il n’est rien que j’embrasse avec plus de
                plaisir que les études littéraires, rien qui suscite plus mon émulation que les
                hommes à la doctrine claire, qui brillent sans ambition et qui, surtout, cher
                Jacques de Thou, te ressemblent. </ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_10">Itaque quotidie lectioni et scriptioni
                operam do et horas in negotiis collocandas subseco et diem non somno sed studio
                diffindo insiticio, quae mihi labitur felix quoties cum Deo aliquid didici quod
                ignorauerim.</ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_10" ana="#PA">Aussi chaque jour je
                m’occupe à lire et à écrire et je rogne sur les heures consacrées aux affaires et je
                segmente le jour non en siestes mais en plages de travail intercalées et je pense
                avoir eu une bonne journée quand, avec l’aide de Dieu, j’ai appris quelque chose que
                j’ignorais.</ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_11">Sic in discendo omnis meus aut
                praecipuus labor, omnis gloria posita est, quam hactenus incitam amo, ut mihi tantum
                et Musis canendum putem.</ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_11" ana="#PA">Ainsi c’est dans
                l’apprentissage que je mets tout ou l’essentiel de mon travail, toute ma gloire,
                dont j’apprécie la hâte du moment que j’aie l’impression que c’est à moi et aux
                Muses de chanter.</ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_12">Ac properabam ad exemplum tuum (uir
                clarissime), neque admodum curabam ut me aliquid scire aut nescire alius praeter me
                sciret.</ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_12" ana="#PA">Et je me hâtais de suivre
                ton exemple, illustre ami, sans me soucier le moins du monde qu’un autre que moi
                sache que je sais ou ignore quelque chose.</ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_13">Sapienter enim huic malo cauisti
                nominis suppressione in illis numquam abdicandis liberis tuis <foreign
                    xml:lang="grc">ἱερακοσοφίῳ</foreign> et Constantia, qui aetatem bene olim
                ferent, et hoc aeuum pareteribunt : uolebam, inquam, hoc exemplo mihi cauere, immo
                publicationis abstinentia cautior esse.</ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_13" ana="#PA">Car avec sagesse tu t’es
                prémuni de ce malheur en supprimant ton nom sans jamais renier ces enfants que tu as
                faits, <seg>Hieracosophium<note>Ce mot grec n’est connu, semble-t-il, que par le
                        titre d’un manuel de fauconnerie byzantine du 14e ou du 15e s. prêté à
                        Démétrius Pepagomène ou Démétrius le Sarmate.</note></seg> et
                        <seg>Constantia<note>Allusion à deux œuvres poétiques de Jacques de Thou,
                            <title>Hieracosophioy, sive de re accipitraria libri tres</title>,
                        Paris, Mamert Patisson, 1584, poème didactique sur la fauconnerie en près de
                        2800 hexamètres, et <title>Iobus, sive De constantia libri IIII. Poetica
                            metaphrasi explicati.</title> Paris, Mamert Patisson, 1588. Il semble
                        que le nom de Thou ne figure pas sur la couverture de ces
                    livres.</note></seg>, qui vieilliront bien et passeront notre génération ; je
                voulais, dis-je, suivant cet exemple, me prémunir, voire être encore plus prudent en
                m’abstenant de publier.</ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_14">Sed ecce tu quasi Cerberum Hercules
                inuitum trahis ad lucem, qua si <persName ref="#uarro">Varrone</persName> teste <seg
                    type="paraphrase">gallinae pinguescere prohibentur<bibl resp="#CN"><author
                            ref="#uarro">Varro.</author>, <title ref="#uarro_r">R.</title>
                        <biblScope>3.9.19</biblScope>.</bibl></seg>, quam uereor ut scriptionis
                nostrae macescat obscura diligentia, et arescat fama, cuius uadum tentanti mihi et,
                ut proprie loquar, percontanti, tu unus e paucis occurristi qui iudicii tui
                acutissimo conto cymbam nostram impelleres, et ut ad portum publicae lucis
                applicaret, adminiculares.</ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_14" ana="#PA">Mais voici que, tel
                Hercule, tu amènes bien malgré lui un Cerbère à cette lumière qui, si l’on en croit
                Varron, empêche les poulets d’engraisser ; j’ai bien peur que l’obscur zèle de notre
                livre maigrisse et que sa réputation s’assèche ; alors que je tâchais d’en passer le
                gué et, plus proprement, de le sonder, toi seul parmi peu t’es présenté pour pousser
                notre esquif de la perche pointue de ton jugement et le guider pour qu’il aborde au
                port de la lumière publique.</ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_15">Adeo me audacem et gloriosum fecit
                iudicium illud tuum quo nostras Burras, quisquilias ineptiasque laudasti tantopere
                ad <persName ref="#soph">Sophocleum</persName>
                <title ref="#soph_ph">Philoctetam</title> ; quod quia optimum et integrum alias
                expertus sum, amore laudis meae non refello, et quamuis hic mutet, serio dissimulo,
                ac in eo usque adeo mihi placeo, ut pertrahi me in scenam facile patiar et libentius
                quam si <persName ref="#caes">Caesaris</persName> potestas me roget ut
                    <persName>Laberium</persName>.</ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_15" ana="#PA #T #TH_repr">Je suis plein
                de hardiesse et de vantardise après le jugement si élogieux que tu as porté de nos
                chutes, rebuts et sottises, <seg>jusqu’à notre <title>Philoctète</title> de
                        Sophocle<note>Chrestien a fait paraître sa traduction latine de
                            <title>Philoctète</title> en 1586.</note></seg> ; pour l’avoir trouvé
                très bon et entier, je ne démens pas cela par amour-propre et, même si ce dernier
                est variable, je fais sérieusement semblant de rien et suis si content de moi que je
                pourrais supporter d’être traîné sur scène, et ce, encore plus volontiers que si
                c’était la puissance de César qui le réclamât de moi comme d’un Labérius.</ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_16">In has scena recognosces <title
                    ref="#ar_pax">Pacem</title>
                <persName ref="#ar">Aristophanicam</persName> quam ego iam pridem (<seg
                    type="reecriture">alter est a septimo annus<bibl resp="#CN"><author ref="#uirg"
                            >Virg.</author>, <title ref="#uirg_b">B.</title>
                        <biblScope>8.39</biblScope> : <note><foreign xml:lang="la">alter ab undecimo
                                tum me iam acceperat annus</foreign>.</note></bibl></seg>) attentius
                perlectam, ex Palliata feci Togatam, tempore paludato et galeato, dum veram in
                motibus Gallicanis Pacem desideramus.</ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_16" ana="#T #HC">Sur cette scène, tu
                reconnaîtras La Paix d’Aristophane, que depuis longtemps (<seg>depuis huit
                        ans<note>Littéralement : « c’était l’année suivant la septième
                    »</note></seg>) j’ai fini de bien lire et que, de palliata, <seg>j’ai faite
                        togata<note>Autre manière de dire « que j’ai latinisée ».</note></seg>,
                    <seg>dans un temps de cape<note><foreign xml:lang="la">Paludatus</foreign>, «
                        habillé du manteau de soldat », fait jeu de mot avec <foreign xml:lang="la"
                            >palliata</foreign> et <foreign xml:lang="la">togata</foreign>,
                        désignant respectivement les pièces en <foreign xml:lang="la"
                            >pallium</foreign> (grecques) et les pièces en toge
                    (latines).</note></seg> et de casque où nous avions vraiment, lors des
                événements de France, besoin de paix.</ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_17">Scilicet os interpolauimus antiquo
                Poetae senique caluo, neque hac injuria contenti, oggessimus tumentes plagas, eae
                sunt correctiones aliquot et Glossematum commentarii.</ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_17" ana="#T #PH">Évidemment, nous avons
                refait le visage du vieux poète et du vieillard chauve ; et, non content de cette
                injure, nous lui avons laissé des plaies enflées, c’est-à-dire quelques corrections
                et des notes lexicales.</ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_18">Sed ut ille ait Satyricus, <cit
                    type="nonref">
                    <quote>ecquis custodiet ipsos custodes</quote>
                    <bibl resp="#CN"><author ref="#iuu">Juv.</author>, <title ref="#iuu_sat"
                            >Sat.</title>
                        <biblScope>6.347b-348a</biblScope> : <note><foreign xml:lang="la">sed quis
                                custodiet ipsos/ custodes ?</foreign></note></bibl>
                </cit> ?</ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_18">Mais, comme le dit le satiriste, «
                qui gardera le gardiens eux-mêmes » ?</ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_19">Sic uereor ut huic Correctori
                corrector sit quaerendus.</ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_19" ana="#PH">Ainsi je crains qu’à ce
                correcteur, il ne faille trouver un correcteur.</ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_20">Propterea ad te mitto, doctissime
                    <persName ref="#thuanus_iacobus">Thuane</persName>, ut prius publicatione in
                    <persName ref="#christianus_florens">Christiani</persName> clientis errata et
                culpas magnus amicus et patronus inquiras, deinde pro arbitrio tuo omnia
                constituas.</ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_20" ana="#PH #PA">C’est la raison pour
                laquelle je te l’envoie, savantissime de Thou, pour qu’avant publication, sur les
                erreurs et fautes de ton client Chrestien, en grand ami et en patron, tu fasses ton
                enquête, puis que tu ranges tout selon ton jugement.</ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_21">Non erit hoc <seg type="reecriture"
                    >offendere in nugis<bibl resp="#CN"><author ref="#hor">Hor.</author>, <title
                            ref="#hor_p">P.</title>
                        <biblScope>444b-445a</biblScope> : <note><foreign xml:lang="la">cur ego
                                amicum / offendam in nugis ?</foreign></note></bibl></seg>, ut ait
                    <persName ref="#hor">Horatius</persName>, immo uere defendere.</ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_21" ana="#PA">Il ne s’agira pas d’en «
                faire offense dans des bagatelles », comme dit Horace, mais bien plutôt d’en faire
                la défense.</ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_22">Sed altera etiam defensio paranda est
                aduersus istos qui Catones simulant.</ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_22" ana="#PA">Mais c’est l’autre sorte
                de défense qu’il faut préparer, contre ceux qui se prennent pour des Catons.</ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_23">Cum enim comoedia uetus tota satyra
                fuerit quae cum uitia et flagitia reprehendit necesse est aut apertis uerbis id
                faciat, aut ita tectis, ut turpitudo nihilominus et obscaenitas appareant, obicient
                -certo scio- ab hac scabie abstinendum fuisse quae pruritum ipsis etiam Catonibus
                ingeneret. </ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_23" ana="#TH_dram #TH_fin">Car, alors
                que la Comédie ancienne est tout entière une satire qui, quand elle blâme les vices
                et les travers, doit le faire à mots ouverts ou, s’ils sont couverts, laisser du
                moins apparaître le scandale ou l’obscénité, ils objecteront (j’en suis sûr) qu’il
                aurait fallu se tenir à l’écart de cette gale qui va provoquer une démangeaison
                jusque chez ces Catons.</ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_24">Huic obiectioni diluendae satis multa
                suppetunt, ut facile sit horum temere litigantium cauillationes insuperhabere et
                adfectatae seueritatis ictus eludere.</ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_24" ana="#PA">Pour contrer l’objection,
                il y a bien des moyens disponibles, en sorte que l’on peut facilement surmonter les
                sophismes de ces téméraires adversaires et esquiver les coups de leur sévérité
                affectée.</ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_25">Ego praecipue auctoritate maximi et
                disertissimi Ecclesiastae <persName ref="#chrys">Ioannis Chrystostomi</persName>
                innocentiam meam tuebor, qui ex <persName ref="#ar">Aristophanis</persName> sordibus
                quas cum cura et cautione tractauit optimum illud et pudicum os suum uidetur
                inaurasse, et fortassis cognomen inde reportasse. </ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_25" ana="#TH_hist">Pour ma part, c’est
                sous l’autorité du grand et très éloquent homme d’église qu’est Jean Chrysostome que
                je vais préserver mon innocence : c’est avec les souillures aristophaniennes, qu’il
                a lues et relues avec soin et attention, qu’il semble s’être forgé cette pudique
                bouche d’or et, peut-être, avoir acquis son surnom.<note>La remarque sur la passion
                    paradoxale de Chrysostome pour Aristophane est récurrente dans les paratextes
                    sur ce poète. Voir par exemple les contributions de Manutius (1498) <ref
                        target="Ar1498_Manutius_p1"/> Mosellanus (1517)<ref target="Ar1517_Mosellanus_p1"/>,
                    Gelenius (1547)<ref target="Ar1547_Gelenius_p1"/>, Frischlin (1585)<ref
                        target="Ar1585_Frischlinus_p9"/>.</note></ab>

            <ab type="orig" xml:id="Ar1589_Christianus_p1_26">Vale. Vindocini, pridie Idus
                Septembres MDLXXXVIII.</ab>
            <ab type="trad" corresp="#Ar1589_Christianus_p1_26">Adieu. Vendôme, 12 septembre
                1588.</ab>
        </body>
    </text>
</TEI>
