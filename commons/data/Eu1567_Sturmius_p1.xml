<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">


    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Ioannes Sturmius Tidemanno Gisio Dantiscano S.P.D.</title>
                <author ref="#sturmius_ioannes">Ioannes Sturmius</author>
                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <respStmt>
                    <resp>Transcription</resp>
                    <persName>Sarah GAUCHER</persName>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <persName>Sarah GAUCHER</persName>
                </respStmt>
                <respStmt>
                    <resp>Révision</resp>
                    <persName>Christian NICOLAS</persName>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne Garcia-Fernandez</persName>
                </respStmt>
            </titleStmt>

            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>
            <sourceDesc>
                <bibl>
                    <title>Euripidis poetae Tragici Alcestis a Georgio Buchanano conuersa. Tum
                        Georgii Buchanani Iephthes, Tragoedia. Praefatio Ioan. Sturmii.</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#eur">Euripides</author>
                    <pubPlace ref="#argentoratum">Argentorati</pubPlace>
                    <publisher ref="#rihelius_iosias">Iosias Rihelius</publisher>
                    <date when="1567">1567</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor ref="#sturmius_ioannes">Ioannes Sturmius</editor>
                    <editor role="traducteur" ref="#buchananus_georgius">Georgius
                        Buchananus</editor>
                    <ref target="https://www.dilibri.de/stbtrdfg/content/pageview/831809"
                        >https://www.dilibri.de/stbtrdfg/content/pageview/831809</ref>
                    <!-- pour les liens google book, remplacer les & par &amp; pour que ça fonctionne -->
                </bibl>

                <listBibl>
                    <bibl/>
                    <!-- créer un <bibl> par référence -->
                </listBibl>

            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p/>
                <!-- s'il s'agit d'une liste, utiliser les éléments ci-dessous -->
                <list>
                    <item/>
                    <item/>
                    <item/>
                </list>
            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>
        <revisionDesc>
            <change when="2022-03-10">Sarah GAUCHER : identifiants Header, mise en conformité
                Elan</change>
            <change when="2021-12-10">Sarah GAUCHER : encodage de la transcription et de la
                traduction</change>
            <change when="2021-11-19">Sarah GAUCHER : création de la fiche TEI et remplissage du
                Header</change>
        </revisionDesc>
    </teiHeader>

    <text>
        <body ana="#PA">
            <head type="orig" xml:id="Eu1567_Sturmius_p1_0"><persName ref="#sturmius_ioannes"
                    >Ioannes Sturmius</persName>
                <persName ref="#gisius_tidemannus">Tidemanno Gisio Dantiscano</persName>
                S.P.D.</head>
            <!-- Le deuxième élément du couple contient la traduction française du titre ou incipit -->
            <!-- L'identifiant de la traduction est le même que celui du latin, précédé d'un hashtag (#) -->
            <head corresp="#Eu1567_Sturmius_p1_0" type="trad">Johannes Sturmius adresse son
                chaleureux salut à Tidemannus Gisius Dantiscanus.</head>
            <ab type="orig" xml:id="Eu1567_Sturmius_p1_1">Tragoedias ego duas <persName
                    ref="#buchananus_georgius">Buchanani</persName> ad te mitto <persName
                    ref="#gisius_tidemannus">Tidemanne</persName> : unam <title ref="#eur_alc"
                    >Alcestidem</title>, quam mutata stola Graecorum Romana ueste ornauit, alteram
                    <title>Iephten</title>, quem etiam togatum maluit quam alio uestitu in theatrum
                ingredi.</ab>
            <ab type="trad" corresp="#Eu1567_Sturmius_p1_1">Moi, cher Tidemannus, je t’envoie deux
                tragédies de Buchanan : d’une part <title>Alceste</title>, qu’il a embelli en
                changeant sa robe grecque contre le vêtement romain, d’autre part
                    <title>Jephté</title>, qu’il a également préféré introduire sur le théâtre en
                toge plutôt que dans un autre habit.</ab>
            <ab type="orig" xml:id="Eu1567_Sturmius_p1_2">Mitto autem idcirco tibi ut tecum in
                Galliam deportes et <persName ref="#buchananus_georgius">Buchanano</persName> des,
                quo uideat et nos ex illius scriptis uoluptatem capere, et ipsum suae gloriae, quam
                ingenio assecutus est, fructum ex nobis percipere et non solum famam eius, sed etiam
                Musas peruenisse ad aures totius Germaniae.</ab>
            <ab type="trad" corresp="#Eu1567_Sturmius_p1_2">Je te les envoie donc pour que tu les
                emportes avec toi en France et que tu les donnes à Buchanan afin qu’il voie que nous
                tirons du plaisir de ses écrits, qu’il voie lui-même à travers nous le fruit de la
                gloire à laquelle il est parvenu par son talent et que non seulement sa renommée
                mais aussi les Muses sont parvenues aux oreilles de la Germanie tout entière.</ab>
            <ab type="orig" xml:id="Eu1567_Sturmius_p1_3">Me certe excitauit ut idem conari cogitem
                ; dices enim ei de arte mea minutula in qua fungor uice cotis exors ipsa secandi ;
                dices tamen et de meis <title>Helidis</title>, in quibus intelliget meum <foreign
                    xml:lang="grc">ζῆλον</foreign> sui amantem, non quod aequiparari ei uelim sed ut
                illius laus emineat magis si assequi eum non potero.</ab>
            <ab type="trad" corresp="#Eu1567_Sturmius_p1_3">Assurément il m’a poussé à méditer la
                même entreprise ; tu lui parleras, en effet, de ce petit art où je passe mon
                existence comme la pierre ponce, privé de la propriété de couper ; cependant tu lui
                parleras aussi de mes <title>Elidiennes</title> où il comprendra que ma concurrence
                est une preuve de mon affection pour lui, non que je veuille être son égal mais
                parce que sa gloire sera plus grande si j’échoue à l’égaler.</ab>
            <ab type="orig" xml:id="Eu1567_Sturmius_p1_4">Non enim potero.</ab>
            <ab type="trad" corresp="#Eu1567_Sturmius_p1_4">Car j’échouerai.</ab>
            <ab type="orig" xml:id="Eu1567_Sturmius_p1_5">Ita enim <title ref="#eur_alc"
                    >Alcestidem</title> conuertit ut ne <persName ref="#eur">Euripides</persName>
                quidem melius posset, si latine loqueretur, dicere ; et illius
                    <title>Iephthes</title> ita cothurnatus incedit ut Atticum heroa agnoscas ; et
                Iphis eius ita ad mortem accedit ut Iphigeneam Graecam animi magnitudine
                superet.</ab>
            <ab type="trad" corresp="#Eu1567_Sturmius_p1_5" ana="#T">De fait, il a traduit
                    <title>Alceste</title> si bien que même Euripide ne pourrait mieux dire s’il
                parlait latin ; son <title>Jephté</title> s’avance si bien sur ses cothurnes que
                l’on reconnaît le héros attique ; son Iphis accède à la mort en surpassant par la
                grandeur de son âme l’<title>Iphigénie</title> grecque.</ab>
            <ab type="orig" xml:id="Eu1567_Sturmius_p1_6">Dicas fortassis cur igitur aemularis si
                assequi non queas, ut te, <persName ref="#gisius_tidemannus">Tidemanne</persName>,
                ut <persName>Carolum Quoerningum</persName> et uestri similes alios excitem : neque
                uobis putetis turpe esse quod me dedecere non sum arbitratus.</ab>
            <ab type="trad" corresp="#Eu1567_Sturmius_p1_6">Mais tu pourrais me dire : « pourquoi
                rivalises-tu avec lui si tu ne peux pas l’égaler ? » C’est pour t’y pousser,
                Tidemannus, pour y pousser Carolus Quoerningus et d’autres qui sont vos semblables ;
                pour que vous ne pensiez pas qu’il est honteux pour vous de faire ce que je n’ai pas
                jugé déshonorant d’entreprendre.</ab>
            <ab type="orig" xml:id="Eu1567_Sturmius_p1_7">Quamquam profecto et <persName
                    ref="#buchananus_georgius">Buchananum</persName> longe ante me currere gaudeo,
                et cupio me a uobis superari non secus ac parens gaudet filiorum gloriam suis
                laudibus anteponi, <persName>Arnoldum</persName> et <persName>Iacobum
                    Vuitueldios</persName> fratres non audeo neque possum a suo uitae instituto
                reuocare ad ludos hos et hunc Heliconem.</ab>
            <ab type="trad" corresp="#Eu1567_Sturmius_p1_7">Bien qu’assurément je me réjouisse que
                Buchanan me devance de beaucoup et que je souhaite que vous me surpassiez de la même
                façon qu’un père se réjouit de placer la gloire de ses fils avant la sienne, je
                n’ose ni ne peux ramener les frères Arnoldus et Jacobus Witueldius du cours de leurs
                existences vers ces jeux et cet Hélicon.</ab>
            <ab type="orig" xml:id="Eu1567_Sturmius_p1_8">Carolus putat se <seg>utroque<note>On
                        attendrait <foreign xml:lang="la">utrique</foreign>.</note></seg> studio
                satisfacere posse : et huic musarum et illi administrandae Reipublicae.</ab>
            <ab type="trad" corresp="#Eu1567_Sturmius_p1_8">Carolus pense qu’il peut satisfaire à
                l’une et l’autre étude, celle des Muses et celle d’administrer l’État.</ab>
            <ab type="orig" xml:id="Eu1567_Sturmius_p1_9"><persName>Arnoldus</persName> et
                    <persName>Iacobus</persName>, nisi ad patriae utilitates omnes suos labores
                dirigant, existimant se non posse assequi quod uolunt et quod possunt, si quantum
                ingenio possint, queant agnoscere.</ab>
            <ab type="trad" corresp="#Eu1567_Sturmius_p1_9">Arnoldus et Jacobus, à moins de vouer
                tous leurs travaux au bien de la patrie, pensent qu’ils ne peuvent pas atteindre le
                but qu’ils voudraient et pourraient atteindre, s’ils pouvaient percevoir le pouvoir
                de leurs esprits.</ab>
            <ab type="orig" xml:id="Eu1567_Sturmius_p1_10">De te quid uis ut dicam aut quid sperem ? </ab>
            <ab type="trad" corresp="#Eu1567_Sturmius_p1_10">Quant à toi, que veux-tu que je dise ou
                qu’espèrerais-je ?</ab>
            <ab type="orig" xml:id="Eu1567_Sturmius_p1_11">Dicere tibi nihil uolo quod ad laudes
                tuas pertineat ; hortor autem te ut pergas hac tua industria, diligentia,
                temperantia, pudore.</ab>
            <ab type="trad" corresp="#Eu1567_Sturmius_p1_11">Je ne veux rien te dire qui touche à
                tes louanges ; mais je t’exhorte à continuer avec cette assiduité, ce soin
                scrupuleux, cette tempérance et cette pudeur qui te caractérisent.</ab>
            <ab type="orig" xml:id="Eu1567_Sturmius_p1_12">Ac tametsi amorem erga te meum augere non
                potes (est enim summus), uelim tamen abs te hunc dum abes cum tuis epistolis, tum
                bonorum uirorum de te testimoniiis, magis magisque confirmari.</ab>
            <ab type="trad" corresp="#Eu1567_Sturmius_p1_12">Et bien que tu ne puisses faire grandir
                mon affection à ton égard (car elle est à son apogée), je voudrais cependant que,
                pendant ton absence, tu l’affermisses toujours davantage tant par tes lettres que
                par les témoignages des gens de bien à ton sujet.</ab>
            <ab type="orig" xml:id="Eu1567_Sturmius_p1_13">Id fiet si meam expectationem <persName
                    ref="#buchananus_georgius">Buchananus</persName>, <persName>Cuiatius</persName>,
                    <persName>Ramus</persName>, <persName ref="#lambinus_dionysus"
                    >Lambinus</persName>, <persName>Mercerius</persName>, quorum frueris
                consuetudine confirmabunt.</ab>
            <ab type="trad" corresp="#Eu1567_Sturmius_p1_13">C’est ce qui arrivera si Buchanan,
                Cujas, la Ramée, Lambin, Mercier, ces hommes que j’aime à fréquenter, confirment mon
                espérance.</ab>
            <ab type="orig" xml:id="Eu1567_Sturmius_p1_14">Vale.</ab>
            <ab type="trad" corresp="#Eu1567_Sturmius_p1_14">Adieu.</ab>
            <ab type="orig" xml:id="Eu1567_Sturmius_p1_15">Argentorati XVIII. Calend.
                Februarii.</ab>
            <ab type="trad" corresp="#Eu1567_Sturmius_p1_15">À Strasbourg, le 18ième jour des
                Calendes de février.</ab>


        </body>
    </text>
</TEI>
