<?xml version="1.0" encoding="UTF-8"?>

<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://gitlab.com/litt-arts-num/tei/-/raw/master/schema/elan_ithac.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">

    <teiHeader>

        <fileDesc>

            <titleStmt>

                <title xml:lang="la">Hecyra. Argumentum per Philippum Melanchthonem.</title>
                <!-- Indiquer la langue du paratexte dans l'attribut xml:lang : "la" pour le latin et "grc" pour le grec -->
                <!-- Attention ! ne pas faire de retour charriot dans le titre -->
                <author ref="#melanchtonus_philippus">Philippus Melanchthon</author>
                <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence en minuscules, précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->

                <!-- Déclarations de responsabilité : indiquent qui a effectué quelle tâche -->
                <!-- Ajouter autant de tâches (<respStmt>) que nécessaire -->
                <respStmt>
                    <resp>Transcription</resp>
                    <resp>Edouard LEBOURG</resp>
                    <name>Théophane TREDEZ</name>
                </respStmt>
                <respStmt>
                    <resp>Traduction</resp>
                    <name>Théophane TREDEZ</name>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Révision</resp>
                    <name>Laure HERMAND</name>
                    <name>Christian NICOLAS</name>
                </respStmt>
                <respStmt>
                    <resp>Encodage</resp>
                    <name>Sarah GAUCHER</name>
                </respStmt>
                <respStmt>
                    <resp>Commentaire</resp>
                    <name>Laure HERMAND</name>
                </respStmt>
                <!-- Attention ! Ne pas modifier les tâches ci-dessous -->
                <respStmt>
                    <resp>Modélisation et structuration</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Elysabeth HUE-GAY</persName>
                </respStmt>
                <respStmt>
                    <resp>Première version du modèle d'encodage</resp>
                    <persName>Elisabeth GRESLOU</persName>
                    <persName>Anne GARCIA-FERNANDEZ</persName>
                </respStmt>
                <principal>Malika BASTIN-HAMMOU</principal>
            </titleStmt>

            <!-- Informations de publication sur le laboratoire numérique du projet. Ne pas modifier-->
            <publicationStmt>
                <publisher/>
                <availability>
                    <p>Le site est en accès restreint</p>
                </availability>
                <pubPlace>ithac.elan-numerique.fr</pubPlace>
            </publicationStmt>

            <!-- Informations bibliographiques sur l'édition ancienne d'où le paratexte est tiré -->
            <sourceDesc>
                <bibl>
                    <title>Publii Terentii comoediae sex, cum prioribus ferme castigationibus et
                        plerisque explicationibus, et auctario insuper quodam, editae studio et cura
                        Ioachimi Camerarii Pabergensis.</title>
                    <!-- Pour les identifiants de personnes et de lieu, mettre le nom latin de référence précédé d'un # Attention : pas d’espace, pas de caractères spéciaux, doit être unique, utiliser le tiret bas pour séparer les mots -->
                    <author ref="#ter">Terentius</author>
                    <pubPlace ref="#lipsia">Lipsiae</pubPlace>
                    <publisher ref="#papa_ualentinus">Valentinus Papa</publisher>
                    <date when="1546">1546</date>
                    <!-- dans l'attribut @when, l'année en chiffre arabes -->
                    <editor ref="#camerarius_ioachimus">Ioachimus Camerarius</editor>
                    <ref target="https://www.digitale-sammlungen.de/en/view/bsb00016248?page=,1"/>
                </bibl>

                <listBibl>
                    <bibl>HAMM, Joachim, « Joachim Camerarius d.Ä. », in W. Kühlmann et al. (éd.),
                        Frühe Neuzeit in Deutschland 1520-1620. Literaturwissenschaftliches
                        Verfasserlexikon. Bd. 1 (VL 16), Berlin, De Gruyter, 2011, p. 425-438 </bibl>
                    <bibl> LAWTON, Harold W., Térence en France au XVIe siècle. Editions et
                        traductions, Paris, Jouve, 1926 (Genève, Slatkine repr., 1970-1972)</bibl>
                    <bibl> STÄHLIN, Friedrich, « Camerarius, Joachim », in Neue Deutsche Biographie,
                        3, 1957, p. 104-105<ref
                            target="https://www.deutsche-biographie.de/pnd118518569.html#ndbcontent"
                        />
                    </bibl>
                </listBibl>

            </sourceDesc>
        </fileDesc>

        <profileDesc>

            <abstract>
                <p>Cette édition n’est pas mentionnée par Lawton. L’éditeur scientifique en est
                    Joachim Camerarius l’Ancien, humaniste originaire de Bamberg. Elle paraît en
                    1546 chez l’imprimeur de Leipzig Valentin Bapst l’Ancien. Camerarius avait déjà
                    publié chez lui en 1545 cinq pièces de Plaute (<title xml:lang="la">Amphitruo,
                        Asinaria, Curculio, Casina, Cistellaria</title>).</p>
                <p> Il s’agit d’une édition scolaire où le texte des six pièces de Térence est suivi
                    de résumés des pièces et d’annotations dues à Camerarius. Ce dernier indique
                    également quelques variantes textuelles qu’il a observées. L’édition ne propose
                    pas de commentaire proprement dit.</p>
                <p>Mais elle contient plusieurs paratextes dus à Philippe Melanchthon. Le premier
                    prend la forme d’un bref exposé théorique illustré d’exemples, et porte le titre
                    « Exhortation à lire les tragédies et les comédies ». Melanchthon y justifie du
                    point de vue réformé l’utilité de la lecture du théâtre antique païen par les
                    jeunes étudiants. Il insiste sur la valeur morale tant de la tragédie que de la
                    comédie. Le second est une lettre adressée aux enseignants. Enfin Melanchthon
                    rédige pour chaque pièce un bref argument à visée pédagogique.</p>
                <p> Philippe Melanchthon est le nom traduit en grec de Philipp Schwarzerd,
                    humaniste, philosophe et réformateur protestant allemand, né le 16 février 1497
                    à Bretten et mort le 19 avril 1560 à Wittenberg, en Allemagne. Professeur de
                    grec à l’université de Wittenberg, il adhère dès le début à la Réforme et
                    devient l’ami et le collaborateur de Luther. Il donne un premier exposé
                    systématique de la pensée de Luther dans ses Loci Communes (1521). Il se
                    préoccupe de concilier la Réforme et l’humanisme chrétien. Il est l’auteur de la
                    Confession d’Augsbourg présentée à la diète d’Augsbourg en 1530. À la mort de
                    Luther en 1546, il devient le principal chef du luthéranisme. Il fait paraître
                    en 1516 à Tübingen une édition des comédies de Térence (<title>Comoediae metro
                        numerisque restitutae</title>). Voir Lawton n°154, p. 123. </p>


            </abstract>

            <langUsage>
                <!-- ne pas modifier -->
                <language ident="fr">Français</language>
                <language ident="la">Latin</language>
                <language ident="grc">Grec</language>
            </langUsage>

        </profileDesc>
        <revisionDesc>
            <change when="2022-09-08">Sarah GAUCHER : création de la TEI, encodage de la
                transcription et de la traduction, encodage complet</change>
        </revisionDesc>
    </teiHeader>

    <text>
        <body ana="#A">

            <!-- RAPPEL : on encode le texte latin ou grec; on n'encode dans le texte de la traduction que les notes d'éditeur (=membres du projet) -->

            <!-- Le couple d'éléments <head> contient le titre ou l'incipit du paratexte -->
            <!-- L'identifiant du TITRE est composé du titre de la fiche TT suivi d'un underscore (_) puis du chiffre 0 -->
            <head type="orig" xml:id="Te1546_Melanchthon_p7_0"><title ref="#ter_hec">Hecyra</title>.
                Argumentum per <persName ref="#melanchtonus_philippus">Philippum
                    Melanchthonem</persName>.</head>
            <!-- Le deuxième élément du couple contient la traduction française du titre ou incipit -->
            <!-- L'identifiant de la traduction est le même que celui du latin, précédé d'un hashtag (#) -->
            <head corresp="#Te1546_Melanchthon_p7_0" type="trad"><title>Hécyre</title>. Argument de
                Philippe Mélanchthon.</head>

            <ab type="orig" xml:id="Te1546_Melanchthon_p7_1"> Vxorem duxit Pamphilus cum Bacchidis
                meretriculae amore tantopere teneretur ut duobus mensibus uxorem ne attingeret
                quidem. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p7_1"> Pamphile prit une épouse, alors
                qu'il était si amoureux de la courtisane Bacchis qu'en deux mois il n'avait pas même
                touché son épouse. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p7_2"> Postea per consuetudinem animus uictus
                est, coepit placere uxor. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p7_2"> Ensuite, son sentiment fut vaincu
                par l'habitude : son épouse commença à lui plaire. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p7_3"> Porro cum ipse peregre abesset, illa
                domum redit ad matrem, atque ibi parit mense septimo postquam nupsisset Pamphilo :
                eam rem celat et affines et patrem anus. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p7_3"> En outre, comme il était parti à
                l'étranger, la femme revient chez sa mère et y accouche sept mois après avoir épousé
                Pamphile : la vieille femme le cache à ses parents et au père. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p7_4"> Vbi Pamphilus redit, comperit
                peperisse. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p7_4"> Lorsque Pamphile revient, il apprend
                l'accouchement. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p7_5"> Hic grauiter adeo commouetur cum
                aliunde concepisse existimaret. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p7_5"> Il est encore plus gravement
                tourmenté en pensant que l'enfant est celui d'un autre. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p7_6"> Et cum per sese dolent res Pamphilo
                (amabat enim mulierculam) tum molestum est ab ea diuelli, neque enim honestum erat
                reducere. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p7_6"> Et d’un côté Pamphile en souffre (il
                aimait la pauvre femme), d’un autre côté il lui est douloureux de se séparer d'elle,
                car il n'était pas honnête de la ramener chez lui. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p7_7"> Periclitatur itaque Pamphilus, quomodo
                honeste dimittat uxorem, neue illius peccatum proderet. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p7_7"> Pamphile est donc mis à l'épreuve :
                comment répudier honnêtement son épouse, ou comment ne pas trahir sa faute ? </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p7_8"> Haec fabulae protasis est. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p7_8" ana="#TH_dram"> Voilà la protase de
                la pièce. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p7_9"> Epitasis </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p7_9" ana="#TH_dram"> Epitase. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p7_10"> Instant pater et socer, inscii quid
                accidisset, ut domum reducat, praesertim cum nec socrus nec mater, id quod senes
                falso suspicati fuerant, obstarent. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p7_10"> Son père et son beau-père, ignorant
                ce qui s'était passé, le pressent de la ramener chez lui surtout que ni la
                belle-mère ni la mère ne s’y opposent, comme les vieillards l’avaient supposé par
                erreur. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p7_11"> Quanquam autem permoueri non posset
                Pamphilus ut reduceret, tamen causam constantissime dissimulat. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p7_11"> Mais bien que Pamphile ne puisse
                pas être convaincu de la ramener, il en dissimule la raison avec la plus grande
                fermeté. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p7_12"> Itur eo ut putent Bacchidis amore
                deuinctum alieno ab uxore animo esse. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p7_12"> On en vient à penser qu’il est
                prisonnier de son amour pour Bacchis, sentiment qui l’éloigne de sa femme. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p7_13"> Catastrophe </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p7_13" ana="#TH_dram"> Catastrophe. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p7_14"> Bacchis accersita fidem facit nihil
                sibi cum Pamphilo rei esse. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p7_14"> On fait venir Bacchis qui promet
                qu'il ne se passe rien entre Pamphile et elle. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p7_15"> Hic aut prodendum peccatum uxoris
                fuit, aut reducenda, uerum casu periculum discutitur. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p7_15"> Là, il fallait soit trahir la faute
                de l'épouse, soit la ramener, mais le hasard résout l’épreuve. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p7_16"> Forte enim Philumenae annulum socrus
                in digito Bacchidis conspexit, quem olim Pamphilus compressae in tenebris Philumenae
                detraxerat. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p7_16"> En effet, par hasard la belle-mère
                aperçoit au doigt de Bacchis l’anneau de Philomène que Pamphile avait jadis ôté à
                Philomène quand il l’avait violée dans le noir. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p7_17"> Is annulus testatur ex Pamphilo
                puerum susceptum esse. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p7_17"> Cet anneau prouve que l'enfant a
                été conçu par Pamphile. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p7_18"> Ita redit in gratiam cum uxore
                Pamphilus. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p7_18"> Ainsi Pamphile revient dans les
                grâces de son épouse. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p7_19"> Iucunda est cum uxoribus senum
                concertatio quae monet saepe errore et falsis suspitionibus fieri ut dissentiant
                coniuges. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p7_19"> La dispute entre les vieillards et
                leurs épouses est charmante et rappelle qu'il arrive souvent qu’une erreur et de
                faux soupçons engendrent des disputes conjugales. </ab>
            <ab type="orig" xml:id="Te1546_Melanchthon_p7_20"> In primis autem insignis est
                humanitas et fides Pamphili in hac fabula. </ab>
            <ab type="trad" corresp="#Te1546_Melanchthon_p7_20" ana="#TH_dram"> L'humanité et la
                fidélité de Pamphile sont surtout remarquables dans cette pièce. </ab>


        </body>
    </text>
</TEI>
