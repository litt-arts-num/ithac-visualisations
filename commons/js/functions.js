function createPensoirLink(filename) {
  let a = document.createElement("a")
  a.classList.add("pensoir-link")
  a.href = pensoirUrl + "admin/paratext/r/" + filename
  a.target = '_blank'
  a.innerHTML = "Voir sur le pensoir"

  return a
}

function createDownloadXmlLink(filename) {
  let a = document.createElement("a")
  a.classList.add("pensoir-link")
  a.href = pensoirUrl + "upload/tei/" + filename
  a.innerHTML = "Télécharger"
  a.target = '_blank'

  return a
}

/* FETCH */

async function getData() {
    var files = await fetchXML(apiUrl+"paratexts")
    files = JSON.parse(files)
    var data = [];
    for (var i = 0; i < files.length; i++) {
        data.push(await getDatum(files[i]));
        loading(i, files.length);
    }
    return data;
}

async function getDatum(path, toHTML = false) {
    var datum = await fetchXML(apiUrl+"paratext/"+path);

    for (var el in sanitizer) datum = datum.replaceAll(el, sanitizer[el]);
    if (toHTML) for (var el in sanitizer_for_html) datum = datum.replaceAll(el, sanitizer_for_html[el]);

    let tei_body = document.createElement("div");
    tei_body.innerHTML = datum;
    var filename = path.replaceAll("../","").replaceAll("commons/data/","").replaceAll(".xml","");
    tei_body.setAttribute("data-file", filename);
    return tei_body;
}

async function fetchXML(path) {
    var options = {
        method: 'GET',
        mode: "cors",
    };
    const response = await fetch(path, options);
    const text = await response.text();

    return text;
}

/* FILTER */
function filterData(data, searchParams) {
    /*
        Retourne un array de 'data' filtré par 'searchParams' (key/value).
        Des valeurs multiples pour un même filtre sont possibles.
        Pour filtrer par dates, la key de comparaison est l'attribut 'when'.
        Pour filtrer par tous les autres filtres, la key de comparaison est l'attribut définie dans "data_model".
        Par conséquence les values de 'searchParams' doivent être comparables avec ces attributs.

        @param array 'data' Tableau de fiches xml brutes
        @param URLSearchParam 'searchParams' Objet avec les filtres et les valeurs pour trier 'data'

        Pour les dates :

        - pour filtrer par une seule date :
            searchParams = {
                "paratxt_date": "1500",
                "date_operator: "eq"
            }
        - pour filtrer par une plage de dates, mettre la plus petite date en premier :
            searchParams = {
                "paratxt_date": "1500",
                "paratxt_date": "1600",
                "date_operator: "range"
            }

        Pour tous les autres filtres :
            searchParams = {
                "paratxt_pubplace": "alost",
                "paratxt_pubplace": "antuerpia"
            }

        TODO :
        - [x] gérer les valeurs muliples pour un même filtre ds 'searchParams'
        - [x] gérer les valeurs multiples d'une string pour un même tag ds 'datum'
        - [x] gérer les valeurs multiples de plusieurs éléments pour un même path ds 'datum'
        - [ ] pouvoir changer l'opérateur (ET ou OU)
        - [x] intégrer une gestion des @ana sur les <ab>
    */

    // boucle ds ttes les fiches xml
    var data_filtered = data.filter(function (datum) {
        // variable pr stocker tous les booleans d'un xml par rapport aux couples filtres/valeurs choisies
        var bool_array = [];
        // transformation de 'searchParams' en une liste de keys avec des tableaux en valeur
        var params_list = combineParams(searchParams);
        // boucle ds ttes les keys de 'params_list'
        for (var filter in params_list) {
            // aller à l'iteration suivante pr des params qui ne sont pas des filtres
            if (filter.includes("operator")) continue;

            var filter_values = params_list[filter];
            var allowed_attr = ["ref", "xml:lang", "ana"];
            // console.log(filter, filter_values);
            if (data_model[filter] && data_model[filter].attribute_value === undefined) {
                var bool = filter_values.includes(datum.querySelector(data_model[filter].path).innerHTML);
            } else if (data_model[filter] && allowed_attr.includes(data_model[filter].attribute_value)) {

                if (filter == "paratxt_ana") {
                    var nodes = datum.querySelectorAll(data_model[filter].path + "[ana]");
                    var ana_arr = [];
                    if (nodes.length) {
                        var ana_arr = Array.from(nodes).map(el => el.getAttribute("ana"));
                        var ana_str = ana_arr.join(" ").replaceAll('#', '');
                        var datum_val = ana_str;
                    } else {
                        var datum_val = "";
                    }
                    // division des valeurs multiples
                    if (data_model[filter].is_list_of_values) {
                        var splitted_datum_val = datum_val.split(" ");
                        var bool = filter_values.some(r=> splitted_datum_val.indexOf(r) >= 0);
                    } else {
                        var bool = filter_values.includes(datum_val);
                    }
                } else if (datum.querySelector(data_model[filter].path) != null && datum.querySelector(data_model[filter].path).getAttribute(data_model[filter].attribute_value) != null) {
                    var datum_val = datum.querySelector(data_model[filter].path).getAttribute(data_model[filter].attribute_value);
                    var datum_val = datum_val.includes("#") ? datum_val.replaceAll('#', '') : datum_val;

                    // division des valeurs multiples
                    if (data_model[filter].is_list_of_values) {
                        var splitted_datum_val = datum_val.split(" ");
                        var bool = filter_values.some(r=> splitted_datum_val.indexOf(r) >= 0);
                    } else {
                        var bool = filter_values.includes(datum_val);
                    }
                } else {
                    var undefined_operator = searchParams.has("undefined_operator") ? searchParams.get("undefined_operator") : false;
                    // console.log(generateError(datum));
                    var bool = undefined_operator ? true : false;
                }
            } else if (filter.includes("paratxt_date")) {
                if (datum.querySelector(data_model[filter].path) != null) {
                    var paratxt_date = parseInt(datum.querySelector(data_model[filter].path).getAttribute("when"));
                } else {
                    console.log(generateError(datum));
                    var paratxt_date = undefined;
                }
                var filter_min = parseInt(searchParams.getAll("paratxt_date")[0]);
                var filter_max = searchParams.getAll("paratxt_date")[1] ? parseInt(searchParams.getAll("paratxt_date")[1]) : 2000;

                // vérif de 'date_operator'
                // si =="eq" alors comparaison à une seule date
                // si =="range" alors comparaison dans un range de dates
                var date_operator = searchParams.has("date_operator") ? searchParams.get("date_operator") : "eq";
                if (date_operator == "eq") {
                    var bool = filter_min == paratxt_date;
                } else {
                    var bool = filter_min <= paratxt_date && paratxt_date <= filter_max;
                }
            }
            // stockage de tous les bouleans pour cette fiche xml
            bool_array.push(bool);
        };
        // synthèse de tous les booleans de cette fiche xml
        return bool_array.every(el => el);
        // return bool_array.length ? bool_array.every(el => el) : false;
    });
    return data_filtered;
}



/* FACETS */

function listData(data, key, uniq = false) {
    /*
        Retourne un tableau d'obj de toutes les valeurs d'une 'key' de 'data'.
        Peut être appelée après getData() ou filterData().
        Le tableau contient des objets avec ces keys 'attribute_value' et 'readable_value'.

        @param array 'data' Tableau de fiches xml brutes
        @param string 'key' La key pour piocher dans 'data'
        @param boolean 'uniq' Permet de retourner un tableau sans doublon

        Le tableau retourné ressemble à ça :

        [
            {
                attribute_value: "basilea"
                readable_value: "Basileae"
            },
            ...
        ]

        Pour un tableau sans doublons :

        - uniq=true
        - la comparaison se fait sur la valeur de 'attribute_value' de l'obj global 'data_model'

        TODO :

        - [x] intégrer une gestion des @ana sur les <ab>
    */

    var arr = [];
    data.forEach(function(datum) {
        var node = datum.querySelector(data_model[key].path);

        if (node != null) {

            if (data_model[key].attribute_value == undefined) {
                var attr_val = undefined;

                // erreur si la balise est vide
                if (node.innerHTML.length === 0) console.log(generateError(datum));
            } else {

                // exception "paratxt_ana" car il y a plusieurs éléments à récupérer (body, head, ab)
                // puis à combiner en une seule string
                if (key == "paratxt_ana") {
                    var nodes = datum.querySelectorAll(data_model[key].path + "[ana]");
                    var ana_arr = [];
                    if (nodes.length) {
                        var ana_arr = Array.from(nodes).map(el => el.getAttribute("ana"));
                        var ana_str = ana_arr.join(" ").replaceAll('#', '');
                        var attr_val = ana_str;
                    } else {
                        var attr_val = undefined;
                    }
                } else {
                    if (node.getAttribute(data_model[key].attribute_value) != null) {
                        var attr_val = node.getAttribute(data_model[key].attribute_value);
                        var attr_val = attr_val.includes("#") ? attr_val.replaceAll('#', '') : attr_val;
                    } else {
                        console.log(generateError(datum));
                    }
                }
            }

            if (data_model[key].readable_value == undefined) {
                if (attr_val == undefined) {
                    var read_val = "Pas de données";
                } else {
                    var read_val = data_model[key].alignment[attr_val];
                    arr.push({
                        "readable_value": read_val,
                        "attribute_value": attr_val
                    });
                }
            } else {
                var read_val = node.innerHTML;
                arr.push({
                    "readable_value": read_val,
                    "attribute_value": attr_val
                });
            }



            if (key == "paratxt_ana") {
                // console.log(arr);
            }

        } else {
            console.log(generateError(datum));
        }
    });
    return arr;
}

function uniqList(list, key) {
    /*
        Retourne un tableau d'obj sans doublons des valeurs d'une 'key' de 'list'.
        Ne peut pas être appelé après getData() sur un tableau de fiches xml brutes.
        Peut être appelé après listData().
        Le tableau retourné contient des objets avec ces keys : 'attribute_value' et 'readable_value'.

        @param array 'list' Tableau d'obj de toutes les valeurs d'une 'key' d'un tableau de fiches xml brutes
        @param string 'key' La key pour comparer les obj dans 'list'

        Le tableau retourné ressemble à ça :

        [
            {
                attribute_value: "basilea"
                readable_value: "Basileae"
            },
            ...
        ]
    */

    var uniq_list = [];
    list.filter(function(item){
        var i = uniq_list.findIndex(x => (x[key] == item[key]));
        if (i <= -1) {
            uniq_list.push(item);
        }
        return null;
    });
    return uniq_list;
}

function sortList(list, key, sort_order = "asc") {
    /*
        Retourne un tableau d'obj trié par les valeurs sur 'key' de 'list'.
        Ne peut pas être appelé après getData() sur un tableau de fiches xml brutes.
        Peut être appelé après listData().
        Le tableau retourné contient des objets avec ces keys : 'attribute_value' et 'readable_value'.

        @param array 'list' Tableau d'obj de toutes les valeurs d'une 'key' d'un tableau de fiches xml brutes
        @param string 'key' La key pour trier les obj dans 'list'
        @param string 'sort_order' L'ordre de tri

        Le tableau retourné ressemble à ça :

        [
            {
                attribute_value: "basilea"
                readable_value: "Basileae"
            },
            ...
        ]

        Ordre du tri :

        - par défaut l'ordre est ascendant
        - l'ordre est descendant si sort_order = "desc"
    */

    var collator = new Intl.Collator("fr", { numeric: true, sensitivity: "base" });
    // spread operator pour copier list et donc ne pas affecter la variable originale
    var list = [...list].sort(function (a, b) {
        return collator.compare(a[key], b[key]);
    });
    if (sort_order === "desc") list.reverse();
    return list;
}

function occList(list, key) {
    /*
        Retourne un array d'obj sans doublons avec les occurrences des valeurs de 'key' dans 'list'
        Ne peut pas être appelé après getData() ou filterData().
        Peut être appelé après listData().

        @param array 'list' Tableau d'objets dont les occ sont à calculer
        @param string 'key' Clé sur laquelle calculer les occ

        Le tableau retourné ressemble à ça :

        [
            {
                attribute_value: "basilea"
                readable_value: "Basileae"
                occ: 10
            },
            ...
        ]
    */
    var result = [];
    list.forEach(function(obj) {
        // verif pr ne pas copier un 'obj' existant déjà dans 'result'
        if (!this[obj[key]]) {
            // copie de 'obj'
            this[obj[key]] = {...obj};
            // init des occ pr la valeur
            this[obj[key]].occ = 0;
            // push du nouvel 'obj' dans la list uniq avec les occ
            result.push(this[obj[key]]);
        }
        // increment de occ
        this[obj[key]].occ ++;
    }, Object.create(null));

    return result;
}

function splitList(list, key) {
    /*
        Coupe une liste sur "attribute_value" par " "
    */
    var splitted_list = [];

    list.forEach(function(src_obj) {
        var attr_val = src_obj.attribute_value;
        var array_attr_val = (attr_val != undefined) ? attr_val.split(" ") : [undefined];

        for (var i = 0; i < array_attr_val.length; i++) {
            var splitted_attr_val = array_attr_val[i];
            var splitted_obj = {};

            splitted_obj.readable_value = src_obj.readable_value;

            if (data_model[key].readable_value == undefined) {
                if (data_model[key].alignment[splitted_attr_val] != undefined) {
                    splitted_obj.readable_value = data_model[key].alignment[splitted_attr_val];
                    splitted_obj.attribute_value = splitted_attr_val;
                    splitted_list.push(splitted_obj);
                } else if (splitted_attr_val.length) {
                    splitted_obj.readable_value = splitted_attr_val;
                    splitted_obj.attribute_value = splitted_attr_val;
                    splitted_list.push(splitted_obj);
                }
            } else {
                splitted_obj.readable_value = src_obj.readable_value;
                splitted_obj.attribute_value = splitted_attr_val;
                splitted_list.push(splitted_obj);
            }

        }
    });
    return splitted_list;
}

function filterList(list, key, search_params) {
    var filtered_list = [];
    var filtered_list = list.filter(function (obj) {
        var bool_array = [];
        var params_list = combineParams(search_params);
        if (params_list[key]) {
            if (obj.attribute_value == undefined) {
                var undefined_operator = search_params.has("undefined_operator") ? search_params.get("undefined_operator") : false;
                var bool = undefined_operator ? true : false;
            } else {
                var bool = params_list[key].includes(obj.attribute_value);
            }
            return bool;
        } else { return true; }
    });
    return filtered_list;
}


/* ERRORS */

function generateError(datum) {
    /*
        Retourne un message d'erreur pour identifer le tag qui n'est pas atteignable
    */

    var tag_issue = "unidentified";

    if (datum.querySelector("tei") == null) {
        tag_issue = "tei";
    } else if (datum.querySelector("tei teiHeader") == null) {
        tag_issue = "teiHeader";
    } else if (datum.querySelector("tei teiHeader fileDesc") == null) {
        tag_issue = "fileDesc";
    } else if (datum.querySelector("tei teiHeader fileDesc sourceDesc") == null) {
        tag_issue = "sourceDesc";
    } else if (datum.querySelector("tei teiHeader fileDesc sourceDesc bibl") == null) {
        tag_issue = "bibl";
    } else if (datum.querySelector("tei teiHeader fileDesc sourceDesc bibl date") == null) {
        tag_issue = "date";
    } else if (datum.querySelector("tei teiHeader fileDesc sourceDesc bibl author") == null) {
        tag_issue = "sourceDesc bibl author";
    } else if (datum.querySelector("tei teiHeader fileDesc sourceDesc bibl author[ref]") == null) {
        tag_issue = "sourceDesc bibl author[ref]";
    } else if (datum.querySelector("tei teiHeader fileDesc sourceDesc bibl pubPlace") == null) {
        tag_issue = "bibl pubPlace";
    } else if (datum.querySelector("tei teiHeader fileDesc sourceDesc bibl publisher") == null) {
        tag_issue = "bibl publisher";
    } else if (datum.querySelector("tei teiHeader fileDesc sourceDesc bibl editor") == null) {
        tag_issue = "bibl editor";

    } else if (datum.querySelector("tei teiHeader fileDesc titleStmt") == null) {
        tag_issue = "titleStmt";
    } else if (datum.querySelector("tei teiHeader fileDesc titleStmt author") == null) {
        tag_issue = "titleStmt author";
    } else if (datum.querySelector("tei teiHeader fileDesc titleStmt author[ref]") == null) {
        tag_issue = "titleStmt author[ref]";
    } else if (datum.querySelector("tei teiHeader fileDesc titleStmt title") == null) {
        tag_issue = "titleStmt title";
    } else if (datum.querySelector("tei teiHeader fileDesc titleStmt title").innerHTML.length === 0) {
        tag_issue = "titleStmt title";
    } else if (datum.querySelector("tei teiHeader fileDesc titleStmt title[xml\\:lang]") == null) {
        tag_issue = "titleStmt title[xml:lang]";
    }


    var filename = datum.getAttribute("data-file");
    return `Problem with ${filename} at tag '${tag_issue}'`;
}



/* HTML */

function convert(arr, tag, trad = true) {
    var dom_arr = [];
    for (var i = 0; i < arr.length; i++) {
        var el = document.createElement(tag);
        el.innerHTML = arr[i].innerHTML;
        if (i % 2 != 0 && trad) el.classList.add("trad");
        dom_arr.push(el);
    }
    return dom_arr;
}




/* URLS */

function getURL() {
    var url = new URL(document.location.href);
    return url;
}
function getURLparams() {
    var url = getURL();
    var searchParams = new URLSearchParams(url.search);
    return searchParams;
}
function combineParams(searchParams) {
    /*
        Transforme URLSearchParams en un obj avec key et values en tableau
    */
    var arr = {};
    searchParams.forEach(function(value, filter) {
        if (!arr[filter]) {
            arr[filter] = [];
        }
        arr[filter].push(value);
    });
    return arr;
}

// LOADING FUNCTIONS
function loading(rank, length) {
    var el = document.querySelector(".loading_message");
    if (el) document.querySelector(".loading_message").innerHTML = "Chargement de " + rank + "/" + length + " fiches xml";

    var progress_bar = document.querySelector(".progress_bar");
    if (progress_bar) {
        var percentage = map_range(rank, 0, length, 0, 100);
        progress_bar.style.width = percentage + '%';
    }
}

function map_range(value, low1, high1, low2, high2) {
    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
}

// HIERARCHIZER

function hierarchizer(data, hierarchy, src_search_params, obj, attributes_values_path, key, level) {
    //////////////////////////////////////////////////////////////////////////////
    //                                                                          //
    //  Extrait un fichier JSON hierarchisé à partir d'une liste d'objects XML  //
    //                                                                          //
    //  Exemple :                                                               //
    //                                                                          //
    //  var obj = {};                                                           //
    //  hierarchizer(                                                           //
    //   data,                                                                  //
    //   ["paratxt_pubplace","ancient_author","paratxt_date"],                  //
    //   obj                                                                    //
    //  );                                                                      //
    //                                                                          //
    //  obj va contenir                                                         //
    //                                                                          //
    //  {                                                                       //
    //    "paratxt_pubplace A" {                                                //
    //      "ancient_author A" : {                                              //
    //        "paratxt_date A" : occurences,                                    //
    //        "paratxt_date B" : occurences,                                    //
    //        ...                                                               //
    //      }                                                                   //
    //      "ancient_author B" : {                                              //
    //        ...                                                               //
    //      }                                                                   //
    //      ...                                                                 //
    //    }                                                                     //
    //    "paratxt_pubplace B" {                                                //
    //      ...                                                                 //
    //    }                                                                     //
    //    ...                                                                   //
    //  }                                                                       //
    //                                                                          //
    //////////////////////////////////////////////////////////////////////////////
    if (!level) level = 0;
    if (level == hierarchy.length) return;
    if (!obj) obj = {};
    var scope = obj;
    var searchParams = new URLSearchParams();
    for (var i = 0; i < level; i++) {
        scope = scope[attributes_values_path[i]];
        searchParams.append(hierarchy[i],attributes_values_path[i]);
        // console.log(hierarchy[i]);
    }
    var filtered_data = level ? filterData(data, searchParams) : data;
    var keys = listData(filtered_data, hierarchy[level]);

    // division des valeurs multiples
    // si data_model[hierarchy[level]] est true sur "is_list_of_values"
    // exemple : paratxt_ana peut renvoyer "PH PA"
    if (data_model[hierarchy[level]].is_list_of_values) {
        var keys = splitList(keys, hierarchy[level]);
        console.log(keys);
        // filtre de la liste pour enlever des valeurs qui ne correspondent pas à "src_search_params"
        var keys = filterList(keys, [hierarchy[level]], src_search_params);
    }

    // toggle entre "readable_value" et "attribute_value" suivant le data model
    // pr faire des comparaisons compatibles avec les données
    var comparator = getComparator(hierarchy[level]);

    keys = sortList(keys,comparator);
    if (level+1 == hierarchy.length) {
        keys = occList(keys, comparator);
    } else {
        keys = uniqList(keys, comparator);
    }
    keys.forEach( key => {
        if (!attributes_values_path) attributes_values_path = [];
        if (level+1 == hierarchy.length) {
            scope[key[comparator]] = key.occ;
        } else {
            scope[key[comparator]] = {};
        }
        hierarchizer(data, hierarchy, src_search_params, obj, attributes_values_path.concat([key[comparator]]), key, level+1);
    })
}

function getComparator(key) {
    var comparator = "";
    if (data_model[key].attribute_value == undefined) {
        comparator = "readable_value";
    } else {
        comparator = "attribute_value";
    }
    return comparator;
}

function get_total_from_data(data,total) {
  for (var key in data) {
    if (Object.keys(data[key]).length) {
      get_total_from_data(data[key],total);
    } else if (data[key] > 0) {
      total.result += data[key];
    }
  }
}
function get_all_child_total_from_data(data) {
  let results = {};
  for (var i in data) {
    let total = {"result":0};
    get_total_from_data(data[i],total);
    results[i] = total.result;
  }
  return results;
}
